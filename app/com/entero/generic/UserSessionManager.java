package com.entero.generic;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by brad on 3/3/15.
 * Manages all of the user sessions currently on the server
 */
public class UserSessionManager {
    //Store the current active sessions
    static private Map<String, UserSession> activeSessions = new HashMap<String, UserSession>();

    /*
        Gets a UserSession object based off a uuid (which is a cookie)
     */
    static public UserSession getSession(String uuid) {
        if (uuid==null) return null;
        return activeSessions.get(uuid);
    }
    /*
        Adds a new session to the session manager
        Returns a uuid for the session which should be set as a cookie for future reference
    */
    static public String addSession(UserSession session) {
        String uuid = java.util.UUID.randomUUID().toString();   //Generate a UUID for the session
        activeSessions.put(uuid, session);
        return uuid;
    }

}
