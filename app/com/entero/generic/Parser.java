package com.entero.generic;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

import models.EnteroObject;

import com.entero.identifiers.ItemIdentifier;
import com.fasterxml.jackson.databind.JsonNode;

public class Parser {
	private List<String> fields;
	private List<String> tableFields; 
	private HashMap<String, JsonNode> propertyMap = new HashMap<String, JsonNode>(); //mapped by <fieldname, jsonnode>
	private List<EnteroObject> tableList = new ArrayList<EnteroObject>();
	private EnteroObject tableObject;
	
	/**
	 * Returns the populated table
	 * @return
	 */
	public List<EnteroObject> getTableData() {
		return this.tableList;
	}
	
	/**
	 * Returns the last populated table object.
	 * @return
	 */
	public EnteroObject getTableObject(){
		return this.tableObject;
	}
	
	/**
	 * Takes in either a list of Identifiers (e.g., ItemIdentifier.table()), or one identifier as the first argument.
	 * Takes in the root node to begin parsing at (can either be an array or a container).
	 * Takes in a boolean of true or false whether we want to build a table object, and in turn a table or not.
	 * Use false for the last arg, if only interested in a map<string,jsonnode>
	 * Else use true if interested in a table, or just a table object
	 * @param idList
	 * @param rootNode
	 * @param buildTable
	 * @return
	 * @throws Exception
	 */
	public static Parser createParser(Object idList, JsonNode rootNode, boolean buildTable) throws Exception{
		return new Parser(idList, rootNode, buildTable);
	}
	
	private Parser(Object idList, JsonNode rootNode, boolean buildTable) throws Exception {
		Class<?> m;
		if(idList.getClass().isArray()){
			m = idList.getClass().getComponentType();
			Method fieldsOf = m.getMethod("fieldsOf", idList.getClass());
			Method columnsOf = m.getMethod("columnsOf", idList.getClass());
			this.fields = Arrays.asList((String[])fieldsOf.invoke(null, idList));
			this.tableFields = Arrays.asList((String[])columnsOf.invoke(null, idList));
		}else{
			m = idList.getClass();
			Method fieldName = m.getMethod("fieldName");
			Method columnName = m.getMethod("columnName");
			this.fields = Arrays.asList((String)fieldName.invoke(idList));
			this.tableFields = Arrays.asList((String)columnName.invoke(idList));
		}
		this.parse(rootNode, buildTable);
	}

	private void parse(JsonNode nodeToParse, boolean buildTable) {
		if(nodeToParse.isArray()){
			this.parseArray(nodeToParse, buildTable);
		}else this.parseContainer(nodeToParse, buildTable);
	}
	
	/**
	 * Method maps each field as a key to its data,
	 * which is a JsonNode.
	 * @param nodeToParse
	 * @param buildTable 
	 * @return 
	 */
	private void parseArray(JsonNode nodeToParse, boolean buildTable) {
		this.tableList = new ArrayList<EnteroObject>();
		nodeToParse.forEach(node -> {
			this.parseContainer(node, buildTable);
		});
		
	}
	
	private void parseContainer(JsonNode nodeToParse, boolean buildTable){
		if(buildTable){
			fields.stream().forEach(
					field -> propertyMap.put(field, nodeToParse.path(field)));
			this.tableList.add(this.buildTableObject());
			this.propertyMap.clear();
		}else fields.stream().forEach(
				field -> propertyMap.put(field, nodeToParse.path(field)));
	}
	
	/**
	 * Return the result of the parsing as a map.
	 * @return
	 */
	public HashMap<String, JsonNode> getParsedMap(){
		return this.propertyMap;
	}
	
	public EnteroObject buildTableObject(){
		ListIterator<String> fieldsIter = this.fields.listIterator();
		this.tableObject = new EnteroObject();
		this.tableFields.stream().forEach(
				p -> tableObject.setProperty(p, this.getParsedMap().get(fieldsIter.next()).asText()));
		return this.getTableObject();
	}	
}
