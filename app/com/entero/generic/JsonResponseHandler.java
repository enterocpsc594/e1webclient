package com.entero.generic;

import java.util.Iterator;
import java.util.Map.Entry;

import com.entero.rest.RestClient;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.Multimap;

public class JsonResponseHandler {
	private final RestClient client;
	private JsonNode responseAsJsonNode;
	private String defaultEndpoint;
	
	/**
	 * Takes in an instance of the RestClient and a default endpoint (e.g., "/item")
	 * @param client
	 * @param defaultEndpoint
	 * @throws Exception
	 */
	public JsonResponseHandler(RestClient client, String defaultEndpoint) throws Exception{
		this.client = client;
		this.defaultEndpoint = defaultEndpoint;
		this.client.setHolder(defaultEndpoint);
	}
	
	/**
	 * Fetches the response data from the server at the default endpoint as a JsonNode
	 * @throws Exception
	 */
	public void fetchJsonAtDefaultEndpoint() throws Exception{
		this.client.setHolder(defaultEndpoint);
		this.responseAsJsonNode = this.client.getResponseAsJson();
	}
	
	/**
	 * Can take a multimap of key,values for request parameters at an endpoint.
	 * Note that: keys, and values should be opposite prior to method call
	 * (e.g., [itemid1234=itemGroup], [itemid167543=itemGroup], [key=value], etc..)
	 * @param keyVal
	 * @throws Exception
	 */
	public void fetchJsonAtKeyValPairs(Multimap<String, String> keyVal) throws Exception{
		if(!keyVal.isEmpty()){
			Iterator<Entry<String,String>> iter = keyVal.entries().iterator();
			while(iter.hasNext()){
				Entry<String,String> mapEntry = iter.next();
				this.client.setFlag(mapEntry.getValue(), mapEntry.getKey());
			}
		}
		this.responseAsJsonNode = this.client.getResponseAsJson();
		this.client.setHolder(this.defaultEndpoint);
	}
	
	/**
	 * Set the default endpoint.
	 * @param endpoint
	 */
	public void setDefaultEndpoint(String endpoint){
		this.defaultEndpoint = endpoint;
		this.client.setHolder(defaultEndpoint);
	}
	
	/**
	 * Returns the default endpoint. 
	 * @return
	 */
	public String getDefaultEndpoint(){
		return this.defaultEndpoint;
	}
	
	/**
	 * Returns the currentURL of the server including the token.
	 * @return
	 */
	public String getCurrentURL(){
		return this.client.getHolder().getUrl();
	}
	
	/**
	 * Returns the current jsonNode, after a fetch method has been called.
	 * Returns null otherwise.
	 * @return
	 */
	public JsonNode getResponseJsonNode(){
		return this.responseAsJsonNode;
	}
}
