package com.entero.generic;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.entero.rest.RestClient;
import com.entero.workspace.Workspace;

public class UserSession {



	//The workspaces for this user
	private Map<String, Workspace> clientWorkspaces = new HashMap<String, Workspace>();
	//The RestClient for this user
	private RestClient client;

	/*
		Set the rest client for this user.
	*/
	public void setRestClient(RestClient c) {
		client = c;
	}

	/*
		Get a workspace based on the workspace unique identifier
	*/	
	public Workspace getWorkspace(String wsid) {
		return clientWorkspaces.get(wsid);
	}
	/*
		Add a new workspace
	*/
	public void addWorkspace(String wsid, Workspace ws) {
		clientWorkspaces.put(wsid, ws);
	}

	/*
		Get the rest client for this user
	*/
	public RestClient getRestClient() {
		return client;
	}
}
