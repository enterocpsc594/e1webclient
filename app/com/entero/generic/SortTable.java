package com.entero.generic;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import com.entero.rest.RestClient;
import models.EnteroObject;

public class SortTable {
	private HashMap<String, String> currentSortOrder = new HashMap<String, String>(); //key = field, value = "asc" or "desc"
	private final String listFields[];
	private String currentSortedColumn;
	
	private SortTable(String listFields[]){
		this.listFields = listFields;
		for(String field: this.listFields){
			this.currentSortOrder.put(field, "asc");
		}
	}
	
	/**
	 * Method will create, and as a consequence,
	 * initialize an instance of the class for sorting.
	 * Must pass in a list of fields containing names which correspond 
	 * to column names, that are used as keys in the hashmap.
	 * @param listFields
	 * @return new instance of the class
	 */
	public static SortTable create(String listFields[]){
		return new SortTable(listFields);
	}
	
	/**
	 * Method sorts the list of objects based
	 * on a specified property (i.e. fieldName).
	 * Alternates the sort order for that field.
	 * @param fieldName
	 * @param objects
	 * @return
	 */
	public List<EnteroObject> sortBy(String fieldName, List<EnteroObject> objects) throws Exception{
		this.currentSortedColumn = fieldName;
		String currentSortOrder = this.currentSortOrder.get(fieldName); //"asc" or "desc"
		Comparator<EnteroObject> comparator = new Comparator<EnteroObject>(){
			public int compare(EnteroObject o1, EnteroObject o2){
				return o1.getProperty(fieldName).compareTo(o2.getProperty(fieldName));
			}
		};
		if(currentSortOrder.equals("asc")){
			Collections.sort(objects, comparator);
			currentSortOrder = "desc";
		}else{
			Collections.sort(objects, comparator.reversed());
			currentSortOrder = "asc";
		}
		this.currentSortOrder.put(fieldName, currentSortOrder);
		return objects;
	}
	
	public String getCurrentSortedColumn(){
		return this.currentSortedColumn;
	}
	
	
}
