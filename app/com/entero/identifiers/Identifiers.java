package com.entero.identifiers;

public interface Identifiers {
	/**
	 * Returns the field name of an identifier tuple.
	 * @return
	 */
	public String fieldName();
	
	/**
	 * Returns the column name of an identifier tuple.
	 * @return
	 */
	public String columnName();
}
