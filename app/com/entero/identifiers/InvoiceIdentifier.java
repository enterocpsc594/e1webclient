package com.entero.identifiers;

import java.util.ArrayList;
import java.util.List;

public enum InvoiceIdentifier implements Identifiers {
	ID("id","ID"),
	INVOICEPAYREC("payRec","Pay/Rec"), //req->payRecType=
	INVOICETYPE("type","Type"), //req->invoiceTypeCode=
	COUNTERPARTY("counterparty","Counterparty"), //req->counterpartyUUID=
	COUNTERPARTYDESCRIPTION("description","Counterparty"),
	BUSINESSUNIT("businessUnit","Business Unit"), //req->businessUnitUUID=
	BUSINESSUNITDESCRIPTION("description","Business Unit"),
	INVOICESTATUS("invoiceStatus","Invoice Status"), //req->invoiceStatus=
	INVOICEID("invoiceId","Invoice Id"),
	INVOICEREF("invoiceRef","Invoice Reference"), //req->invoiceRef=
	PRETAXTOTAL("netAmount","Pre-Tax Total"),
	TAXTOTAL("taxAmount","Tax Total"),
	SELFASSESSEDTOTAL("selfAssessmentAmount","Self Assessed Total"),
	TOTAL("totalAmount","Total"),
	ADJUSTMENTAMOUNT("adjustmentAmount","Adjustment"),
	DISCOUNTAMOUNT("discountAmount","Discount"),
	OUTSTANDINGINVOICEAMOUNT("outstandingAmount","Outstanding Invoice Amount"),
	PAYMENTTERMS("paymentTerms","Payment Terms"),
	CURRENTSTATUS("status","Current Status"),
	ISSUEDDATE("issuedDate","Issued Date"), //req->fromDate=
	DUEDATE("dueDate","Due Date"), //req->toDate=
	DISCOUNTDATE("discountDate","Discount Date"),
	RECEIPTDATE("receiptDate","Receipt Date"),
	ACCOUNT("account","Account"),
	CURRENCY("currency","Currency"),
	CURRENCYDESCRIPTION("description","Currency"),
	PAYMENTMODE("paymentMode","Payment Mode"),
	FORMAT("format","Format"),
	LASTPAYMENTAMOUNT("lastPaymentAmount","Last Payment Amount"),
	LASTPAYMENTREF("lastPaymentRef","Last Payment Reference"),
	AUDITINFO("auditInfo","Invoice Audit Info"),
	CREATEDATE("createDate","Create Date"),
	CREATEUSER("createUser","Create User"),
	UPDATEDATE("updateDate","Update Date"),
	UPDATEUSER("updateUser","Update User"),
	MODE("invoiceMode","Mode"),
	//----------------------PAY/REC TYPE----------------------
	PAY("PAY","Payable"),
	REC("REC","Receivable"),
	//----------------------INVOICE TYPES----------------------
	DEMURRAGE("DEMURRAGE","Demurrage"),
	INTRA_RELATED("INTRA_RELATED","Intra Related"),
	INTER_RELATED("INTER_RELATED","Inter Related"),
	INTERCOMPANY("INTERCOMPANY","Intercompany"),
	JOINT_INTEREST_BILLING("JOINT_INTEREST_BILLING","Joint Interest Billing"),
	NET_BILLING("NET_BILLING","Net Billing"),
	PREPAYMENT("PREPAYMENT","Prepayment"),
	PURCHASE("PURCHASE","Purchase"),
	ROYALTY("ROYALTY","Royalty"),
	SALES("SALES","Sales"),
	SERVICE("SERVICE","Service"),
	SERVICE_CHARGE("SERVICE_CHARGE","Service Charge"),
	//----------------------INVOICE STATUS----------------------
	CURRENT("CURRENT","Current"),
	HOLD("HOLD","Hold"),
	OUTSTANDING("OUTSTANDING","Outstanding"),
	PAID("PAID","Paid"),
	PAST_DUE("PAST_DUE","Past Due"),
	//----------------------DATE TYPES----------------------
	DATETYPE("dateType","Date Type"),
	ACCOUNTING("ACCOUNTING","Accounting"),
	APPROVAL("APPROVAL","Approval"),
	CREATE("CREATE","Create"),
	DUE("DUE","Due"),
	EARLY_PAY_DUE("EARLY_PAY_DUE","Early Pay Due"),
	ISSUED("ISSUED","Issued"),
	UPDATE("UPDATE","Update"),
	//----------------------COUNTERPARTY INFO----------------------
	CP("counterparty","CP"),
	CPDESCRIPTION("description","CP"),
	CPCONTACT("contact", "CP Contact"),
	CPCONTACTDESCRIPTION("description", "CP Contact"),
	CPADDRESS("address","CP Mail Addr"),
	CPADDRESSDESCRIPTION("description","CP Mail Addr"),
	//----------------------BUSINESS UNIT INFO----------------------
	BU("businessUnit","BU"),
	BUDESCRIPTION("description","BU"),
	BUCONTACT("contact", "BU Contact"),
	BUCONTACTDESCRIPTION("description", "BU Contact"),
	BUADDRESS("address","BU Mail Addr"),
	BUADDRESSDESCRIPTION("description","BU Mail Addr"),
	//----------------------REMIT ADDRESS----------------------
	REMITADDRESS("remitAddress","Remit Addr"),
	REMITADDRESSDESCRIPTION("description","Remit Addr"),
	//----------------------INVOICE DETAILS BOTTOM PANE----------------------
	REFNUMBER("refNumber","Ref #"),
	BOLTICKET("bolTicket","BOL/Ticket"),
	QUANTITYDELIVERED("quantityDelivered","Qty"),
	QUANTITY("quantity","Qty"),
	UOM("uom","UoM"),
	UOMSHORTDESC("shortDescription","UoM"),
	UNITCOST("unitCost","Unit Cost"),
	TRANSACTIONAMOUNT("invoiceAmount","Trans Amt."),
	INVOICEAMOUNT("invoiceAmount","Trans Amt."),
	ACCOUNTINGDATE("accountingDate","Acctg. Date"),
	EFFECTIVEDATE("shipDate","Effective Date"),
	CONTRACTLINEREF("contractDescription","Contract Line Ref."),
	SOURCELOCATION("sourceLocation","Location"),
	SOURCELOCATIONDESC("description","Location"),
	SOURCEITEM("item","Item"),
	SOURCEITEMSHORTDESC("shortDescription","Item"),
	COSTTYPE("type","Cost Type"),
	DETAILID("id","Invoice Detail ID");
	
	private final String fieldName;
	private final String columnName;
	
	public String fieldName(){ return this.fieldName; }
	public String columnName(){ return this.columnName; }
	
	InvoiceIdentifier(String fieldName, String columnName){
		this.fieldName = fieldName;
		this.columnName = columnName;
	}
	
	public static InvoiceIdentifier[] table(){
		return new InvoiceIdentifier[]{INVOICEPAYREC,INVOICETYPE,COUNTERPARTY,BUSINESSUNIT,INVOICEID,INVOICEREF
				,PRETAXTOTAL,TAXTOTAL,SELFASSESSEDTOTAL,TOTAL,ADJUSTMENTAMOUNT,OUTSTANDINGINVOICEAMOUNT
				,PAYMENTTERMS,CURRENTSTATUS,ISSUEDDATE,DUEDATE,DISCOUNTDATE,RECEIPTDATE,ACCOUNT,CURRENCY,PAYMENTMODE,FORMAT
				,LASTPAYMENTAMOUNT,LASTPAYMENTREF};
	}
	
	public static InvoiceIdentifier[] tableShort(){
		return new InvoiceIdentifier[]{BUSINESSUNIT,COUNTERPARTY,CURRENCY};
	}
	
	public static InvoiceIdentifier[] invoiceDetails(){
		return new InvoiceIdentifier[]{INVOICEPAYREC,INVOICETYPE,CP,CPCONTACT,CPADDRESS,BU,BUCONTACT,BUADDRESS,ISSUEDDATE,
				DUEDATE,DISCOUNTDATE,RECEIPTDATE,INVOICEID,INVOICEREF,CURRENTSTATUS,
				PRETAXTOTAL,TAXTOTAL,ADJUSTMENTAMOUNT,DISCOUNTAMOUNT,TOTAL,SELFASSESSEDTOTAL,
				REMITADDRESS,ACCOUNT,CURRENCY,PAYMENTTERMS,FORMAT,MODE,PAYMENTMODE,CREATEUSER};
	}
	
	public static InvoiceIdentifier[] invoiceDetailsBottom(){
		return new InvoiceIdentifier[]{REFNUMBER,BOLTICKET,QUANTITY,UOM,
				UNITCOST,TRANSACTIONAMOUNT,INVOICEAMOUNT,ACCOUNTINGDATE,EFFECTIVEDATE,CONTRACTLINEREF,
				SOURCELOCATION,SOURCEITEM,COSTTYPE};
	}
	
	public static InvoiceIdentifier[] invoiceDetailsContainers(){
		return new InvoiceIdentifier[]{CP,BU,REMITADDRESS,CURRENCY,AUDITINFO};
	}
	
	public static InvoiceIdentifier[] invoiceDetailsShort(){
		return new InvoiceIdentifier[]{INVOICEPAYREC,INVOICETYPE,ISSUEDDATE,
				DUEDATE,DISCOUNTDATE,RECEIPTDATE,INVOICEID,INVOICEREF,CURRENTSTATUS,
				PRETAXTOTAL,TAXTOTAL,ADJUSTMENTAMOUNT,DISCOUNTAMOUNT,TOTAL,SELFASSESSEDTOTAL,
				ACCOUNT,PAYMENTTERMS,FORMAT,MODE,PAYMENTMODE};
	}
	
	public static InvoiceIdentifier[] invoiceDetailsBottomContainers(){
		return new InvoiceIdentifier[]{QUANTITYDELIVERED,SOURCELOCATION,SOURCEITEM,DETAILID};
	}
	
	public static InvoiceIdentifier[] invoiceDetailsBottomShort(){
		return new InvoiceIdentifier[]{REFNUMBER,BOLTICKET,UNITCOST,TRANSACTIONAMOUNT,
				INVOICEAMOUNT,ACCOUNTINGDATE,EFFECTIVEDATE,CONTRACTLINEREF,COSTTYPE};
	}
	
	public static InvoiceIdentifier[] invoiceAuditInfo(){
		return new InvoiceIdentifier[]{CREATEUSER};
	}
	
	public static InvoiceIdentifier[] payRecTypes(){
		return new InvoiceIdentifier[]{PAY,REC};
	}
	
	public static InvoiceIdentifier[] invoiceTypes(){
		return new InvoiceIdentifier[]{DEMURRAGE,INTRA_RELATED,INTER_RELATED
				,INTERCOMPANY,JOINT_INTEREST_BILLING,NET_BILLING,PREPAYMENT,
				PURCHASE,ROYALTY,SALES,SERVICE,SERVICE_CHARGE};
	}
	
	public static InvoiceIdentifier[] invoiceStatus(){
		return new InvoiceIdentifier[]{CURRENT, HOLD, OUTSTANDING, PAID, PAST_DUE};
	}
	
	public static InvoiceIdentifier[] invoiceDateTypes(){
		return new InvoiceIdentifier[]{ACCOUNTING,APPROVAL,CREATE,DUE,EARLY_PAY_DUE,ISSUED,UPDATE};
	}
	
	public static String[] fieldsOf(InvoiceIdentifier[] idArray){
		List<String> result = new ArrayList<String>();
		for(InvoiceIdentifier itm : idArray)
			result.add(itm.fieldName());
		return result.toArray(new String[0]);
	}
	
	public static String[] columnsOf(InvoiceIdentifier[] idArray){
		List<String> result = new ArrayList<String>();
		for(InvoiceIdentifier itm : idArray)
			result.add(itm.columnName());
		return result.toArray(new String[0]);
	}
}
