package com.entero.identifiers;

import java.util.ArrayList;
import java.util.List;

public enum LocationIdentifier implements Identifiers {
	LOCATIONDESC("description", "Location Description"),
	LOCATIONTYPE("type", "Type"),
	ID("id", "ID");
	
	
	private String fieldName;
	private String columnName;
	
	LocationIdentifier(String field, String column){
		this.fieldName = field;
		this.columnName = column;
	}
	
	public String fieldName() {	return this.fieldName; }
	public String columnName() { return this.columnName; }
	
	public static LocationIdentifier[] table(){
		return new LocationIdentifier[]{LOCATIONTYPE, LOCATIONDESC};
	}
	
	public static LocationIdentifier[] locations(){
		return new LocationIdentifier[]{LOCATIONTYPE, LOCATIONDESC, ID};
	}
	
	public static String[] locationTypes(){
		return new String[]{"BANKROUTING","CASCADE","EFT","FACILITY",
				"FINANCIAL_TRADING","FORMATION","GRAPHING_REGION","JURISDICTION",
				"MAILING","OFFICE","NOTIONAL","PROPERTY","REPORTING_REGION",
				"RESIDENCE","RESERVOIR","RISK_REGION","TRACT","WELL","UNIT","YARD"};
	}
	
	public static String[] fieldsOf(LocationIdentifier[] idArray){
		List<String> result = new ArrayList<String>();
		for(LocationIdentifier itm : idArray)
			result.add(itm.fieldName());
		return result.toArray(new String[0]);
	}
	
	public static String[] columnsOf(LocationIdentifier[] idArray){
		List<String> result = new ArrayList<String>();
		for(LocationIdentifier itm : idArray)
			result.add(itm.columnName());
		return result.toArray(new String[0]);
	}
}
