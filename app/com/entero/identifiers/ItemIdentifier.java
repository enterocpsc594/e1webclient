package com.entero.identifiers;

import java.util.ArrayList;
import java.util.List;

public enum ItemIdentifier implements Identifiers{
	ITEMLONGDESC("description", "Long description"),
	ITEMSHORTDESC("shortDescription", "Short description"),
	ID("id", "ID"),
	ITEMREF("ref","Item ref"),
	
	ITEMGROUP("itemGroup", "Item group"),
	ITEMCODE("code", "Item code"),
	ITEMGROUPDESCRIPTION("description", "Item group"),
	ITEMGROUPID("id", "ItemGroup id"),
	ITEMGROUPREF("ref","ItemGroup ref"),
	ITEMSINGROUP("items", "Items"),
	
	ITEMAUDITINFO("auditInfo", "Item Audit Info"),
	ITEMCREATEUSR("createUser", "Create user"),
	ITEMUPDATEUSR("updateUser", "Update user"),
	ITEMUPDATEDATE("updateDate", "Update date"),
	
	ATTRIBUTE("attribute","Attribute"),
	ATTRIBUTEDESCRIPTION("description","Attribute"),
	ATTRIBUTETYPE("type","Attribute Type"),
	ATTRIBUTEUOM("unitOfMeasure", "UoM"),
	ATTRIBUTEUOMSHORTDESCRIPTION("shortDescription", "UoM"),
	REQUIREMENTRULE("requirementRule","Requirement Rule"),
	DISPLAYORDER("displayOrder", "Display Order"),
	
	ITEMTYPE("type", "Type"),
	ITEMTYPEDESCRIPTION("description", "Type"),
	ITEMTYPEID("id", "Item Type id"),
	
	
	EXPIRYDATE("expiryDate", "Expiry Date"),
	LOCATIONCONTROL("locationControl", "Location Control"),
	TEMPCORRECTIONFACTOR("tempCorrectionFactor", "Temp Correction Factor"),
	ODORIZED("odorized", "Odorized Product"),
	RINREQUIRED("rinRequired", "RIN Required"),
	ALLOCATIONBYCOMPONENT("allocationByComponent", "Allocation By Component");
	
	private final String fieldName;
	private final String columnName;

	ItemIdentifier(String fieldName, String columnName){
		this.fieldName = fieldName;
		this.columnName = columnName;
	}
	
	public String fieldName(){ return this.fieldName; }
	public String columnName(){ return this.columnName; }
	
	public static ItemIdentifier[] table(){
		ItemIdentifier result[] = {ITEMCODE, ITEMGROUP, ITEMTYPE, ITEMLONGDESC, ITEMSHORTDESC};
		return result;
	}
	
	public static ItemIdentifier[] tableShort(){
		ItemIdentifier result[] = {ITEMGROUP, ITEMTYPE, ID};
		return result;
	}
	
	public static ItemIdentifier[] itemGroup(){
		ItemIdentifier result[] = {ITEMCODE, ITEMGROUPDESCRIPTION, ITEMGROUPID};
		return result;
	}
	
	public static ItemIdentifier[] itemType(){
		ItemIdentifier result[] = {ITEMTYPEDESCRIPTION, ITEMTYPEID};
		return result;
	}
	
	public static ItemIdentifier[] itemAttributes(){
		return new ItemIdentifier[]{ATTRIBUTE,ATTRIBUTEUOM,REQUIREMENTRULE,DISPLAYORDER};
	}
	
	public static ItemIdentifier[] itemAttributesShort(){
		return new ItemIdentifier[]{REQUIREMENTRULE,DISPLAYORDER};
	}
	
	public static ItemIdentifier[] itemDetails(){
		ItemIdentifier result[] = {ITEMLONGDESC, ITEMSHORTDESC, ITEMAUDITINFO, ITEMTYPE, ITEMGROUP, EXPIRYDATE,
				LOCATIONCONTROL, TEMPCORRECTIONFACTOR, ODORIZED, DISPLAYORDER, RINREQUIRED, ALLOCATIONBYCOMPONENT};
		return result;
	}
	
	public static ItemIdentifier[] itemDetailsShort(){
		ItemIdentifier result[] = {ITEMLONGDESC, ITEMSHORTDESC, EXPIRYDATE,
				LOCATIONCONTROL, TEMPCORRECTIONFACTOR, ODORIZED, RINREQUIRED, DISPLAYORDER, ALLOCATIONBYCOMPONENT, ID};
		return result;
	}
	
	
	
	public static ItemIdentifier[] itemAuditInfo(){
		ItemIdentifier result[] = {ITEMCREATEUSR, ITEMUPDATEUSR, ITEMUPDATEDATE};
		return result;
	}
	
	public static String[] fieldsOf(ItemIdentifier[] idArray){
		List<String> result = new ArrayList<String>();
		for(ItemIdentifier itm : idArray)
			result.add(itm.fieldName());
		return result.toArray(new String[0]);
	}
	
	public static String[] columnsOf(ItemIdentifier[] idArray){
		List<String> result = new ArrayList<String>();
		for(ItemIdentifier itm : idArray)
			result.add(itm.columnName());
		return result.toArray(new String[0]);
	}
}
