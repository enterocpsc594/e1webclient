package com.entero.identifiers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public enum PriceIdentifier implements Identifiers{
	PRICEDESCRIPTION("description","Description"),
	ITEMUUID("itemUUID", "Item"),
	ITEMSHORTDESC("shortDescription","Item"),
	ITEM("item","Item"),
	SOURCELOCATIONDESC("description", "Source Location"),
	DESTINATIONLOCATIONDESC("description", "Destination Location"),
	SOURCELOCATION("sourceLocation","Source Location"),
	DESTINATIONLOCATION("destinationLocation","Destination Location"),
	PRICECATEGORY("priceCategory","Price Category"),
	CURRENCY("currency","Currency"),
	CURRENCYDESC("description","Currency"),
	UOMDESC("description","UoM"),
	UOMSHORTDESC("shortDescription","UoM"),
	UOMCODE("code","UoM"),
	UOM("uom","UoM"),
	ID("id","ID"),
	PRICEREF("ref","Price REF"),
	CURRENTPRICE("currentPrice","Current Rate"),
	PRICEAUDITINFO("auditInfo", "Price Audit Info"),
	PRICEDETAILS("priceDetails","Price Details"), //right pane
	PRICEDETAILSREF("ref","Price Details REF"), //right pane
	PRICEUPDATEDATE("updateDate", "Update Date"),
	PRICEUPDATEUSR("updateUser", "Update User"),
	PRICECREATEUSR("createUser", "Create User"),
	EFFECTIVEDATE("effectiveDate", "Effective Date"),
	PRICEAMOUNT("priceAmount", "Amount"),
	PRICECOMPLETEDATE("priceCompleteDate", "Complete Date"),
	PRICEDETAILTYPE("type", "Type"),
	PRICEDETAILID("id", "Price Detail ID"),
	PRICEDETAILREF("ref", "Price Detail REF");
	
	private final String fieldName;
	private final String columnName;
	
	PriceIdentifier(String fieldName, String columnName){
		this.fieldName = fieldName;
		this.columnName = columnName;
	}
	
	public String fieldName(){ return this.fieldName; }
	public String columnName(){ return this.columnName; }
	
	public static PriceIdentifier[] table(){
		PriceIdentifier result[] = {PRICEDESCRIPTION, ITEM, SOURCELOCATION,
				DESTINATIONLOCATION, PRICECATEGORY,
				CURRENCY, UOM};
		return result;
	}
	
	public static PriceIdentifier[] tableShort(){
		PriceIdentifier result[] = {ITEM, SOURCELOCATION, DESTINATIONLOCATION, CURRENCY, UOM};
		return result;
	}
	
	public static PriceIdentifier[] priceDetails(){
		PriceIdentifier result[] = {PRICECATEGORY, PRICEDESCRIPTION, SOURCELOCATION,
				DESTINATIONLOCATION, ITEM,
				UOM, CURRENCY, PRICEAUDITINFO};
		return result;
	}
	
	public static PriceIdentifier[] priceDetailsShort(){
		PriceIdentifier result[] = {PRICECATEGORY, PRICEDESCRIPTION};
		return result;
	}
	
	public static PriceIdentifier[] priceDetailsRightPane(){
		PriceIdentifier result[] = {EFFECTIVEDATE, PRICEAMOUNT, PRICECOMPLETEDATE, PRICEDETAILTYPE};
		return result;
	}
	
	public static PriceIdentifier[] priceAuditInfo(){
		PriceIdentifier result[] = {PRICECREATEUSR, PRICEUPDATEUSR, PRICEUPDATEDATE};
		return result;
	}
	
	public static String[] fieldsOf(PriceIdentifier[] idArray){
		List<String> result = new ArrayList<String>();
		for(PriceIdentifier itm : idArray)
			result.add(itm.fieldName());
		return result.toArray(new String[0]);
	}
	
	public static String[] columnsOf(PriceIdentifier[] idArray){
		List<String> result = new ArrayList<String>();
		for(PriceIdentifier itm : idArray)
			result.add(itm.columnName());
		return result.toArray(new String[0]);
	}
}
