package com.entero.rest;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.*;

import play.libs.ws.*;
import play.libs.F.Promise;

import com.fasterxml.jackson.databind.JsonNode;

/*
 * @author sid
 * Restclient: 
 * 1> Establishes the connection to the REST api
 * 2> Authenticates a specified user
 * 3> methods to get response in json format
 */

public class RestClient {
	private WSRequestHolder holder;
	private String apiURL = "";
	private String authToken;
	private String endpoint = "";
	
	/**
	 * Constructor authorizes with the rest server using a username, and password
	 * We get the address of the server from a properties file in the root
	 * directory of the project -> "E1Server.properties"
	 * Can modify the address in the file as needed.
	 * @param user
	 * @param pass
	 * @throws Exception
	 */
	public RestClient(String user, String pass) throws Exception{
		URL serverURL = null;
		File propFile = new File("E1Server.properties");
		FileInputStream in = null;
		Properties serverProperties = new Properties();
		if(propFile.isFile() && propFile.canRead()){
			in = new FileInputStream("E1Server.properties");
		}else throw new Exception("File Does not Exist or cannot be read!");
		if(in != null) serverProperties.load(in);
		in.close();
		try{
			serverURL = new URL(serverProperties.getProperty("serverAddress"));
			this.apiURL = serverURL.toString();
		}catch(MalformedURLException m){
			System.out.println("URL format is invalid!");
		}
		this.holder = WS.url(apiURL+"/auth/token?user="+user+"&password="+pass).setFollowRedirects(true);
		this.authToken = getAuthToken();
	}
	
	/**
	 * Validates if the url has proper form
	 * @param url
	 * @return
	 */
	private boolean validateURL(String url){
		try{
			new URL(url);
		}catch(MalformedURLException m){
			return false;
		}
		return true;
	}
	
	/**
	 * Returns the request holder for a give client.
	 * @return
	 */
	public WSRequestHolder getHolder(){
		return this.holder;
	}
	
	/**
	 * Adds/Changes the endpoint to the current url in the request holder
	 * for the client 
	 * @param url
	 */
	public void setHolder(String url){
		this.endpoint = url;
		this.holder = WS.url(apiURL+this.endpoint+"?token="+authToken).setFollowRedirects(true);
	}
	
	/**
	 * Can set the key, value pairs of request parameters
	 * for the url in the request holder.
	 * @param key
	 * @param value
	 */
	public void setRequestParam(String key, String value){
		this.holder.setQueryParameter(key, value);
	}
	
	/**
	 * returns the response from the server using the
	 * request holder, in json format. Timeout can be modified (default=120000)
	 * @return
	 * @throws Exception
	 */
	public JsonNode getResponseAsJson() throws Exception{
		Promise<JsonNode> jsonPromise = this.holder.get().map(response -> {
			return response.asJson();
		});
		return jsonPromise.get(120000);
	}
	
	/**
	 * Downloads the pdf given:
	 * A link, and a specified filename (Note that ".pdf" should be excluded from the filename argument).
	 * We only save the pdf of the last request that was make under "public/pdfReports/"
	 * @param linkToFile
	 * @param fileName
	 * @return
	 * @throws Exception
	 */
	public String getResponseAsPDF(String linkToFile, String fileName) throws Exception{
		if(!this.validateURL(linkToFile)) return new String();
		URL remoteFileURL = new URL(linkToFile+"&format=pdf");
		File folder = new File(new File(".").getCanonicalPath()+"/bin/public/pdfReports/");
		clearFolder(folder); //empty out the contents
		new File(new File(".").getCanonicalPath()+"/bin/public/pdfReports/").mkdirs();
		String serverLocalPath = new File(".").getCanonicalPath()+"/bin/public/pdfReports/"+fileName+".pdf";
		ReadableByteChannel rbc = Channels.newChannel(remoteFileURL.openStream());
		FileOutputStream fos = new FileOutputStream(serverLocalPath);
		fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
		fos.close();
		return "pdfReports/"+fileName+".pdf";
	}
	
	/**
	 * Empty out any content (files/folders) within a given directory.
	 * @param rootFolder
	 */
	public static void clearFolder(File rootFolder){
		if(rootFolder.isDirectory()){
			for(String fileOrDir : rootFolder.list()){
				clearFolder(new File(rootFolder.getPath()+"/"+fileOrDir));
			}
		}
		else rootFolder.delete();
	}

	/**
	 *Used to authenticate and re-authenticate when
	 * new instance of class is created 
	 * @return
	 * @throws Exception
	 */
	private String getAuthToken() throws Exception{
		return authToken = getResponseAsJson().get("token").asText();
	}

	/**
	 * Can set a field, and value as a query parameter.
	 * @param field
	 * @param value
	 */
	public void setFlag(String field, String value){
		if(field != null){
			if(value == null || value.isEmpty())
				this.holder.setQueryString(field); 
			else this.holder.setQueryParameter(field, value);
		}
	}
}
