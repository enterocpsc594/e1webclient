package com.entero.workspace;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ArrayUtils;

import models.EnteroObject;

import com.entero.generic.JsonResponseHandler;
import com.entero.generic.Parser;
import com.entero.generic.SortTable;
import com.entero.identifiers.InvoiceIdentifier;
import com.entero.identifiers.ItemIdentifier;
import com.entero.rest.RestClient;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;


public class Items extends WSHelper implements Workspace {
	private RestClient client;
	private final String itemUrl = "/item";
	private List<EnteroObject> items = new ArrayList<EnteroObject>();
	private List<EnteroObject> itemsMaster = new ArrayList<EnteroObject>();
	private SortTable sortTable;
	private Integer offset = 0;
	private Integer max = 50;
	
	public Items() throws Exception{
		this.sortTable = super.initSort(ItemIdentifier.table());
	}
	
	@Override
    public String getWSName(){ return "Item"; }
	
	@Override
	public Integer getMax(){ return this.max; }
	
	@Override
	public List<EnteroObject> getTable(){
		return this.items;
	}
	
	@Override
	public List<String> getItemGroups() throws Exception{
		return Collections.list(Collections.enumeration(super.getItemGroups(this.client).keySet()));
	}
	
	@Override
	public List<String> getItemTypes() throws Exception{
		return Collections.list(Collections.enumeration(super.getItemTypes(this.client).keySet()));
	}
	
	@Override
	public List<EnteroObject> getItemListWindow(String selectedItemShortDesc) throws Exception{
		Items itm = new Items();
		itm.setClient(this.client);
		if(!super.isEmpty(selectedItemShortDesc)){
			Integer previousMax = new Integer(this.max);
			itm.max = Short.MAX_VALUE-1;
			itm.searchItems(0, new String(), itm.getItemGroups(), new String()); //get all items
			itm.max = previousMax;
			itm.setTable(itm.filterByField(itm.getTable(), ItemIdentifier.ITEMSHORTDESC, selectedItemShortDesc, false)); //local	
		}
		return itm.getTable();
	}
	
	@Override
	public void setClient(RestClient client) throws Exception{
		this.client = client;
	}
	
	@Override
	public List<EnteroObject> sort(String columnName) throws Exception{
		this.itemsMaster = this.sortTable.sortBy(columnName, this.itemsMaster);
		return this.itemsMaster;
	}
	
	@Override
	public void searchItems(Integer currentOffset, String selectedItemType, List<String> itemGroups, String itemDescription) throws Exception{
		Multimap<String,String> requestParams = LinkedListMultimap.create();
		if(offset == 0) this.itemsMaster = super.flushTable();
		this.items = super.flushTable();
		if(!super.isEmpty(selectedItemType))
			requestParams.putAll(this.createRequestParams(ItemIdentifier.ITEMTYPE, super.getItemTypes(this.client).get(selectedItemType)));
		if(!super.isEmpty(itemDescription))
			requestParams.putAll(this.createRequestParams(ItemIdentifier.ITEMLONGDESC, itemDescription));
		if(!super.isEmpty(itemGroups)){
			HashMap<String,String> itemGroupIDMap = super.getItemGroups(this.client);
			itemGroups.forEach(groupDesc -> {
				try {
					requestParams.putAll(this.createRequestParams(ItemIdentifier.ITEMGROUP, itemGroupIDMap.get(groupDesc)));
				} catch (Exception e) {}});
		}
		if(!(super.isEmpty(currentOffset.toString()) || currentOffset < 0)){
			this.offset = currentOffset;
			requestParams.put(this.offset.toString(), "offset");
			requestParams.put(this.max.toString(), "max");
		}
		requestParams.put("itemGroup", "detail");
		requestParams.put("type", "detail");
		this.filterByParameters(requestParams);
		requestParams.clear();
		this.itemsMaster.addAll(this.items);
	}
	
	@Override
	public EnteroObject filterDetails(String selectedObjectID) throws Exception{
		if(super.isEmpty(selectedObjectID)) return new EnteroObject();
		Multimap<String,String> requestParameters = LinkedListMultimap.create();
		JsonResponseHandler response = new JsonResponseHandler(this.client, this.itemUrl);
		requestParameters.put(selectedObjectID, "id");
		requestParameters.put("", "detail");
		response.fetchJsonAtKeyValPairs(requestParameters); //fetch details of an item
		JsonNode nodeToParse = response.getResponseJsonNode(); //JsonNode of details
		HashMap<String,JsonNode> map = Parser.createParser(ItemIdentifier.itemDetails(), nodeToParse, false).getParsedMap();
		Multimap<JsonNode,Object> resultMap = LinkedListMultimap.create();
		resultMap.put(nodeToParse, ItemIdentifier.itemDetailsShort());
		resultMap.put(map.get(ItemIdentifier.ITEMAUDITINFO.fieldName()), ItemIdentifier.itemAuditInfo());
		resultMap.put(map.get(ItemIdentifier.ITEMTYPE.fieldName()), ItemIdentifier.ITEMTYPEDESCRIPTION);
		resultMap.put(map.get(ItemIdentifier.ITEMGROUP.fieldName()), ItemIdentifier.itemGroup());
		return super.buildObject(resultMap);
	}
	
	@Override
	public List<EnteroObject> filterAttributes(String selectedObjectID) throws Exception{
		if(super.isEmpty(selectedObjectID)) return new ArrayList<EnteroObject>();
		List<EnteroObject> resultObjects = new ArrayList<EnteroObject>();
		Multimap<String,String> requestParameters = LinkedListMultimap.create();
		JsonResponseHandler response = new JsonResponseHandler(this.client, "/itemAttribute");
		requestParameters.put(selectedObjectID, "itemUUID");
		response.fetchJsonAtKeyValPairs(requestParameters); //fetch details of an item
		JsonNode nodeToParse = response.getResponseJsonNode(); //JsonNode of all the attributes
		if(!nodeToParse.isNull()){
			nodeToParse.forEach(node -> {
				HashMap<String, JsonNode> map;
				try {
					map = Parser.createParser(ItemIdentifier.itemAttributes(), node, false).getParsedMap();
					Multimap<JsonNode,Object> resultMap = LinkedListMultimap.create();
					resultMap.put(nodeToParse, ItemIdentifier.itemAttributesShort());
					resultMap.put(map.get(ItemIdentifier.ATTRIBUTE.fieldName()), new ItemIdentifier[]{ItemIdentifier.ATTRIBUTEDESCRIPTION,ItemIdentifier.ATTRIBUTETYPE});
					resultMap.put(map.get(ItemIdentifier.ATTRIBUTEUOM.fieldName()), ItemIdentifier.ATTRIBUTEUOMSHORTDESCRIPTION);
					resultObjects.add(super.buildObject(resultMap));
				} catch (Exception e) {}
			});
		}
		return resultObjects;
	}
	
	/**
	 * Request Parameters are created for the rest api.
	 * @param invoiceID
	 * @param searchValue
	 * @return
	 * @throws Exception
	 */
	private Multimap<String,String> createRequestParams(ItemIdentifier invoiceID, Object searchValue) throws Exception {
		Multimap<String,String> requestParams = LinkedListMultimap.create();
		String param = "";
		if(!super.isEmpty(searchValue)){
			switch(invoiceID){
			case ITEMLONGDESC:
				param = "description";
				break;
			case ITEMGROUP:
				param = "itemGroup";
				break;
			case ITEMTYPE:
				param = "itemType";
				break;
			default: break;
			}
			if(!(param.isEmpty() || searchValue.getClass().getSimpleName().equals(ArrayList.class.getSimpleName()))){
				requestParams.put(searchValue.toString(), param);
			}else{
				@SuppressWarnings("unchecked")
				List<String> list = (List<String>)searchValue;
				for(String element : list) requestParams.put(element, param);
			}
		}
		return requestParams;
	}
	
	/**
	 * The input is a multimap of request parameters for the endpoint as keys <-> values.
	 * e.g., 50->offset, petcoke->description, etc..
	 * @param requestParams
	 * @throws Exception
	 */
	private void filterByParameters(Multimap<String,String> requestParams) throws Exception{
		if(!(requestParams.values().contains("itemGroup") && requestParams.entries().size() > 4)){
			this.items = super.flushTable();
			return;
		}
		JsonNode nodeToParse = super.getServerResponse(requestParams, this.client, this.itemUrl);
		this.items = Parser.createParser(ArrayUtils.addAll(ItemIdentifier.table(), new ItemIdentifier[]{ItemIdentifier.ID}), nodeToParse, true).getTableData();
		Multimap<JsonNode,Object> map = LinkedListMultimap.create();
		HashMap<String,JsonNode> currentNodeMap = new HashMap<String,JsonNode>(); 
		Iterator<JsonNode> nodeIter = nodeToParse.iterator();
		this.items.stream().map(object ->{ map.clear();
		currentNodeMap.clear();
		try {
			currentNodeMap.putAll(Parser.createParser(ItemIdentifier.tableShort(), nodeIter.next(), false).getParsedMap());
			map.put(currentNodeMap.get(ItemIdentifier.ITEMGROUP.fieldName()), ItemIdentifier.itemGroup());
			map.put(currentNodeMap.get(ItemIdentifier.ITEMTYPE.fieldName()), ItemIdentifier.itemType());
			return super.merge(object, super.buildObject(map));
		} catch (Exception e) {
			return null;
		}}).collect(Collectors.toList());
	}
	
	/**
	 * Clears and sets the partial list of items (i.e., from the most recent offset)
	 * @param items
	 */
	private void setTable(List<EnteroObject> items){
		this.items = super.flushTable();
		this.items = items;
	}
}
