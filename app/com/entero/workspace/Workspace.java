package com.entero.workspace;

import java.util.List;

import com.entero.rest.RestClient;
import com.fasterxml.jackson.databind.JsonNode;

import models.EnteroObject;

/**
 * This interface makes it easy to see which features
 * are supported at a particular endpoint/"workspace"
 * 
 * @author sid
 *
 */
public interface Workspace {
	//---------------------------Methods available to all workspaces
	/**
	 * Method to set the client of a workspace
	 * Must be called at-least once.
	 * @throws Exception 
	 */
	default public void setClient(RestClient client) throws Exception{
		throw new NoSuchMethodException("Method is not valid for this workspace.");
	}

	/**
	 * Returns a list of objects used to render the table.
	 * @return the list of items
	 * @throws NoSuchMethodException 
	 */
	default public List<EnteroObject> getTable() throws NoSuchMethodException{
		throw new NoSuchMethodException("Method is not valid for this workspace.");
	}

	/**
	 * 
	 * @return The max value to add to the offset (i.e. if offset = 0 then new_offset = offset + max - 1)
	 * @throws NoSuchMethodException 
	 */
	default public Integer getMax() throws NoSuchMethodException{
		throw new NoSuchMethodException("Method is not valid for this workspace.");
	}

	/**
	 * Sort a table with a selected/clicked column.
	 * Alternates between ascending and descending upon each call for a column.
	 * @param columnName
	 * @return the whole sorted list (from offset=0)
	 */
	default public List<EnteroObject> sort(String columnName) throws Exception{
		throw new NoSuchMethodException("Method is not valid for this workspace.");
	}

	/**
	 * Method finds details of the selected objects, using its id as an argument.
	 * @param selectedObjectID
	 * @return the EnteroObject containing details of that id
	 */
	default public EnteroObject filterDetails(String selectedObjectID) throws Exception{
		throw new NoSuchMethodException("Method is not valid for this workspace.");
	}

	/**
	 * Returns the name of a workspace
	 * @return "Item", "Price", etc..
	 * @throws NoSuchMethodException 
	 */
	default public String getWSName() throws NoSuchMethodException{
		throw new NoSuchMethodException("Method is not valid for this workspace.");
	}

	//---------------------------Methods exclusive to the Items workspace:
	
	/**
	 * Search Methods are followed by getTable to get the partial table to paste.
	 * Items can be searched using:
	 * -a selectedItemType (use workspace.getItemTypes())
	 * -a list of selected item groups (use workspace.getItemGroups())
	 * -an item description(which is case insensitive but order sensitive, i.e. search string can be partial
	 * but must have the same sequence from the beginning)
	 * 
	 * @param currentOffset
	 * @param selectedItemType
	 * @param itemGroups
	 * @param itemDescription
	 * @throws Exception
	 */
	default public void searchItems(Integer currentOffset, String selectedItemType,
			List<String> itemGroups, String itemDescription) throws Exception{
		throw new NoSuchMethodException("Method is not valid for this workspace.");
	}

	/**
	 * Returns a list of of itemgroups.
	 * This can be used for the dropdown.
	 * @return
	 * @throws Exception 
	 */
	default public List<String> getItemGroups() throws Exception{
		throw new NoSuchMethodException("Method is not valid for this workspace.");
	}

	/**
	 * Method returns all known item types.
	 * @return
	 * @throws Exception
	 */
	default public List<String> getItemTypes() throws Exception{
		throw new NoSuchMethodException("Method is not valid for this workspace.");
	}

	/**
	 * Method takes in the id of the selected entity on the table, and returns
	 * a list of attributes.
	 * @param selectedObjectID
	 * @return List<EnteroObject> of attributes
	 * @throws Exception
	 */
	default public List<EnteroObject> filterAttributes(String selectedObjectID) throws Exception{
		throw new NoSuchMethodException("Method is not valid for this workspace.");
	}

	//---------------------------Methods exclusive to the PRICE Workspace:

	/**
	 * Returns all the known location types for selection.
	 * for the dropdown of location search window (e.g., in the price workspace)
	 * @return
	 * @throws Exception
	 */
	default public List<String> getLocationTypes() throws Exception{
		throw new NoSuchMethodException("Method is not valid for this workspace.");
	}

	/**
	 * This was added as a helper for price search by location UUID
	 * The uuid of a selected location can be fetched using LocationIdentifier.ID.columnName()
	 * (which is "ID") and passed into either the sourceLocation or destinationLocation UUID arguments
	 * for the Price.searchPrice method.
	 * @param locationDescription
	 * @param selectedLocationType
	 * @return
	 * @throws Exception
	 */
	default public List<EnteroObject> searchLocations(String locationDescription, List<String> selectedLocationType) throws Exception{
		throw new NoSuchMethodException("Method is not valid for this workspace.");
	}

	/**
	 * Arguments 2,3,4 take in a selected EnteroObject's uuid from the search popup window.
	 * This can be fetched using EnteroObject.getProperty("ID")
	 * or instead of hardcoding "ID", one can simply invoke workspace.ID.columnName() which
	 * applies to all workspaces.
	 * The last parameter is an entered priceDescription which must start with the same sequence, but
	 * is case insensitive.
	 * @param currentOffset
	 * @param sourceLocationUUID
	 * @param destinationLocationUUID
	 * @param selectedItemUUID
	 * @param priceDescription
	 * @throws Exception
	 */
	default public void searchPrice(Integer currentOffset, String sourceLocationUUID, String destinationLocationUUID, String selectedItemUUID, String priceDescription) throws Exception{
		throw new NoSuchMethodException("Method is not valid for this workspace.");
	}

	/**
	 * The method takes in an entered short description for an item (case insensitive),
	 * and returns a list of matching items for the popup item search window (in the price workspace).
	 * @param enteredItemShortDesc
	 * @return List<EnteroObject> of matching items to the provided short description
	 * @throws Exception
	 */
	default public List<EnteroObject> getItemListWindow(String enteredItemShortDesc) throws Exception{
		throw new NoSuchMethodException("Method is not valid for this workspace.");
	}

	/**
	 * This method takes as an argument the selected entity's ID and returns
	 * a list of price details for that price object (i.e., the right pane of the existing client)
	 * @param selectedObjectID
	 * @return List<EnteroObject> price details
	 * @throws Exception
	 */
	default public List<EnteroObject> filterDetailsRightPane(String selectedObjectID) throws Exception{
		throw new NoSuchMethodException("Method is not valid for this workspace.");
	}

	//---------------------------Methods exclusive to the INVOICE Workspace:	
	/**
	 * Method takes in an invoice reference, a pay/rec type.
	 * Both InvoiceTypeCode and invoiceTypeStatus can be one of:
	 * 1> A single string
	 * 2> A List<String>
	 * The front-end makes the dateType (i.e., one is always selected) persist, hence the user will have to select
	 * a fromDate, and a toDate for any relevant results for invoices.
	 * @param offset
	 * @param invoiceReference
	 * @param payRec
	 * @param invoiceTypeCode
	 * @param invoiceStatus
	 * @param dateType
	 * @param fromDate
	 * @param toDate
	 * @throws Exception
	 */
	default public void searchInvoice(Integer offset, String invoiceReference, String payRec, Object invoiceTypeCode, Object invoiceStatus, String dateType, String fromDate, String toDate) throws Exception{
		throw new NoSuchMethodException("Method is not valid for this workspace.");
	}
	
	/**
	 * Method takes in the ID of the selected entity from the invoice table,
	 * and returns the list of objects pertaining to the bottom pane of the details section.
	 * @param selectedObjectID
	 * @return
	 * @throws Exception
	 */
	default public List<EnteroObject> filterDetailsBottomPane(String selectedObjectID) throws Exception{
		throw new NoSuchMethodException("Method is not valid for this workspace.");
	}
	
	/**
	 * Returns local path to pdf file given the ID of the selected invoice object
	 * e.g., if returned path=FILEPATH web client should serve the file using:
	 * return ok(new java.io.File(FILEPATH)
	 * @param selectedObjectID
	 * @return
	 * @throws Exception
	 */
	default public String generatePDFReport(String selectedObjectID) throws Exception {
		throw new NoSuchMethodException("Method is not valid for this workspace.");
	}
}
