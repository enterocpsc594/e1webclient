package com.entero.workspace;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import models.EnteroObject;

import com.entero.generic.JsonResponseHandler;
import com.entero.generic.Parser;
import com.entero.generic.SortTable;
import com.entero.identifiers.LocationIdentifier;
import com.entero.rest.RestClient;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;

public class Location extends WSHelper implements Workspace {
	private RestClient client;
	private JsonResponseHandler response;
	private List<EnteroObject> locations = new ArrayList<EnteroObject>();
	private final String locationUrl = "/location";
	private SortTable sortTable;
	private final Integer max = 100;
	
	public Location() throws Exception{
		this.sortTable = super.initSort(LocationIdentifier.table());
	}
	
	@Override
	public Integer getMax(){
		return this.max;
	}
	
	/**
	 * Returns all the known location types for the checkbox.
	 * Then use Price.searchLocation to get the locations as an enteroObject
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<String> getLocationTypes() throws Exception{
		return Arrays.asList(LocationIdentifier.locationTypes());
	}
	
	/**
	 * Method to return the results for the location popup window (i.e., price search by location)
	 * @param locationDescription
	 * @param selectedLocationType
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<EnteroObject> searchLocations(String locationDescription, List<String> selectedLocationType) throws Exception{
		this.locations = super.flushTable();
		if(!(super.isEmpty(locationDescription) || super.isEmpty(selectedLocationType))){
			if(response == null)
				this.response = new JsonResponseHandler(this.client, this.locationUrl);
			Multimap<String,String> requestParameters = LinkedListMultimap.create();
			requestParameters.put("0", "offset");
			requestParameters.put(this.max.toString(), "max");
			selectedLocationType.forEach(type -> requestParameters.put(type, "locationType"));
			requestParameters.put(locationDescription, "description");
			this.response.fetchJsonAtKeyValPairs(requestParameters); //get response with locationType
			JsonNode nodeToParse = this.response.getResponseJsonNode(); //JsonNode of location
			//we then parse the node
			this.locations = Parser.createParser(LocationIdentifier.locations(), nodeToParse, true).getTableData();
			this.response.setDefaultEndpoint(this.locationUrl); //reset to price url		
		}
		return this.locations;
	}
	
	@Override
	public void setClient(RestClient client){
		this.client = client;
	}
	
	@Override
	public List<EnteroObject> getTable(){
		return this.locations;
	}
	
	@Override
	public List<EnteroObject> sort(String columnName) throws Exception {
		this.locations = this.sortTable.sortBy(columnName, this.locations);
		return this.locations;
	}

}
