package com.entero.workspace;

/**
 * A driver/factory class to return a new type of workspace
 * as needed.
 * @author sid
 *
 */
public class WorkspaceFactory {
	private static Endpoint endpointName;
	
	public enum Endpoint{
		ITEMS,
		PRICE,
		INVOICE,
		LOCATION
	}
	
	
	
	/**
	 * Creates a new instance of a specified workspace
	 * @param name
	 * @return
	 * @throws Exception
	 */
	public static Workspace createWorkspace(Endpoint name) throws Exception{
		endpointName = name;
		switch (name){
			case ITEMS:
				return new Items();
			case PRICE:
				return new Price();
			case INVOICE:
				return new Invoice();
			case LOCATION:
				return new Location();
			default:
				return null;
		}
	}
	
	public static Endpoint getWorkspaceName(){
		return endpointName;
	}
}
