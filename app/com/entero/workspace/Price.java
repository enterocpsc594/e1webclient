package com.entero.workspace;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ArrayUtils;

import models.EnteroObject;

import com.entero.generic.JsonResponseHandler;
import com.entero.generic.Parser;
import com.entero.generic.SortTable;
import com.entero.identifiers.Identifiers;
import com.entero.identifiers.ItemIdentifier;
import com.entero.identifiers.LocationIdentifier;
import com.entero.identifiers.PriceIdentifier;
import com.entero.rest.RestClient;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;

public class Price extends WSHelper implements Workspace{
	private RestClient client;
	private List<EnteroObject> pricesMaster = new ArrayList<EnteroObject>();
	private List<EnteroObject> prices = new ArrayList<EnteroObject>();
	private final String priceUrl = "/price";
	private SortTable sortTable;
	private Integer offset = 0;
	private final Integer max = 50;

	public Price() throws Exception{
		this.sortTable = super.initSort(PriceIdentifier.table());
	}
	
	@Override
	public Integer getMax(){ return this.max; }
	
	@Override
	public String getWSName(){ return "Price"; }
	
	@Override
	public List<EnteroObject> getTable(){
		return this.prices;
	}
	
	@Override
	public void setClient(RestClient client){
		this.client = client;
	}
	
	@Override
	public List<EnteroObject> sort(String columnName) throws Exception{
		this.pricesMaster = this.sortTable.sortBy(columnName, this.pricesMaster);
		return this.pricesMaster;
	}
	
	/**
	 * The uuid of the selected location object can be obtained using getProperty("Location ID")
	 * which correspond to the second and third parameter.
	 * The UUID of the selected item object can be found using getProperty("Item id").
	 * 
	 * @param currentOffset
	 * @param sourceLocationUUID
	 * @param destinationLocationUUID
	 * @param selectedItemUUID
	 * @param priceDescription
	 * @throws Exception
	 */
	@Override
	public void searchPrice(Integer currentOffset, String sourceLocationUUID, String destinationLocationUUID, String selectedItemUUID, String priceDescription) throws Exception{
		Multimap<String,String> requestParams = LinkedListMultimap.create();
		this.prices = super.flushTable();
		if(offset == 0) this.pricesMaster = super.flushTable();
		if(!super.isEmpty(sourceLocationUUID))
			requestParams.putAll(this.createRequestParams(PriceIdentifier.SOURCELOCATION, sourceLocationUUID));
		if(!super.isEmpty(destinationLocationUUID))
			requestParams.putAll(this.createRequestParams(PriceIdentifier.DESTINATIONLOCATION, destinationLocationUUID));
		if(!super.isEmpty(selectedItemUUID))
			requestParams.putAll(this.createRequestParams(PriceIdentifier.ITEM, selectedItemUUID));
		if(!super.isEmpty(priceDescription))
			requestParams.putAll(this.createRequestParams(PriceIdentifier.PRICEDESCRIPTION, priceDescription));
		if(!(super.isEmpty(currentOffset.toString()) || currentOffset < 0)){
			this.offset = currentOffset;
			requestParams.put(this.offset.toString(), "offset");
			requestParams.put(this.max.toString(), "max");
		}
		this.filterByParameters(requestParams);
		requestParams.clear();
		this.pricesMaster.addAll(this.prices);
	}
	
	@Override
	public EnteroObject filterDetails(String selectedObjectID) throws Exception{
		if(super.isEmpty(selectedObjectID)) return new EnteroObject();
		Multimap<String,String> requestParameters = LinkedListMultimap.create();
		requestParameters.put(selectedObjectID, "id");
		requestParameters.put("", "detail");
		JsonNode nodeToParse = super.getServerResponse(requestParameters, this.client, this.priceUrl);
		HashMap<String, JsonNode> map = Parser.createParser(PriceIdentifier.priceDetails(), nodeToParse, false).getParsedMap();
		Multimap<JsonNode,Object> resultMap = LinkedListMultimap.create();
		resultMap.put(nodeToParse, PriceIdentifier.priceDetailsShort());
		resultMap.put(map.get(PriceIdentifier.PRICEAUDITINFO.fieldName()), PriceIdentifier.priceAuditInfo());
		resultMap.put(map.get(PriceIdentifier.ITEM.fieldName()), PriceIdentifier.ITEMSHORTDESC);
		resultMap.put(map.get(PriceIdentifier.SOURCELOCATION.fieldName()), PriceIdentifier.SOURCELOCATIONDESC);
		resultMap.put(map.get(PriceIdentifier.DESTINATIONLOCATION.fieldName()), PriceIdentifier.DESTINATIONLOCATIONDESC);
		resultMap.put(map.get(PriceIdentifier.UOM.fieldName()), PriceIdentifier.UOMSHORTDESC);
		resultMap.put(map.get(PriceIdentifier.CURRENCY.fieldName()), PriceIdentifier.CURRENCYDESC);
		return super.buildObject(resultMap);
	}
	
	@Override
	public List<EnteroObject> filterDetailsRightPane(String selectedObjectID) throws Exception{
		if(super.isEmpty(selectedObjectID)) return new ArrayList<EnteroObject>();
		List<EnteroObject> result = new ArrayList<EnteroObject>();
		Multimap<String,String> requestParameters = LinkedListMultimap.create();
		JsonResponseHandler response = new JsonResponseHandler(this.client, this.priceUrl);
		requestParameters.put(selectedObjectID, "id");
		requestParameters.put("priceDetails", "detail");
		response.fetchJsonAtKeyValPairs(requestParameters); //fetch details of an object
		JsonNode nodeToParse = response.getResponseJsonNode().findPath("priceDetails"); //JsonNode of priceDetails field
		nodeToParse.forEach(node ->{
			try {
				result.add(Parser.createParser(ArrayUtils.addAll(PriceIdentifier.priceDetailsRightPane()
						,new PriceIdentifier[]{PriceIdentifier.PRICEDETAILID, PriceIdentifier.PRICEDETAILREF})
						, node, true).getTableObject());
			} catch (Exception e) {}
		});
		return result;
	}
	
	private Multimap<String,String> createRequestParams(PriceIdentifier priceID, Object searchValue) throws Exception {
		Multimap<String,String> requestParams = LinkedListMultimap.create();
		String param = "";
		if(!super.isEmpty(searchValue)){
			switch(priceID){
			case PRICEDESCRIPTION:
				param = "description";
				break;
			case SOURCELOCATION:
				param = "sourceLocationUUID";
				break;
			case DESTINATIONLOCATION:
				param = "destinationLocationUUID";
				break;
			case ITEM:
				param = "itemUUID";
			default: break;
			}
			if(!(param.isEmpty() || searchValue.getClass().getSimpleName().equals(ArrayList.class.getSimpleName()))){
				requestParams.put(searchValue.toString(), param);
			}else{
				@SuppressWarnings("unchecked")
				List<String> list = (List<String>)searchValue;
				for(String element : list) requestParams.put(element, param);
			}
		}
		return requestParams;
	}
	
	private void filterByParameters(Multimap<String,String> requestParams) throws Exception{
		if(!(requestParams.entries().size() >= 2)){
			this.prices = super.flushTable();
			return;
		}
		JsonNode nodeToParse = super.getServerResponse(requestParams, this.client, this.priceUrl);
		this.prices= Parser.createParser(ArrayUtils.add(PriceIdentifier.table(), PriceIdentifier.ID), nodeToParse, true).getTableData();
		Multimap<JsonNode,Object> map = LinkedListMultimap.create();
		HashMap<String,JsonNode> currentNodeMap = new HashMap<String,JsonNode>(); 
		Iterator<JsonNode> nodeIter = nodeToParse.iterator();
		this.prices.stream().map(object ->{ map.clear();
		currentNodeMap.clear();
		try {
			currentNodeMap.putAll(Parser.createParser(PriceIdentifier.tableShort(), nodeIter.next(), false).getParsedMap());
			map.put(currentNodeMap.get(PriceIdentifier.ITEM.fieldName()), PriceIdentifier.ITEMSHORTDESC);
			map.put(currentNodeMap.get(PriceIdentifier.SOURCELOCATION.fieldName()), PriceIdentifier.SOURCELOCATIONDESC);
			map.put(currentNodeMap.get(PriceIdentifier.DESTINATIONLOCATION.fieldName()), PriceIdentifier.DESTINATIONLOCATIONDESC);
			map.put(currentNodeMap.get(PriceIdentifier.CURRENCY.fieldName()), PriceIdentifier.CURRENCYDESC);
			map.put(currentNodeMap.get(PriceIdentifier.UOM.fieldName()), PriceIdentifier.UOMSHORTDESC);
			return super.merge(object, super.buildObject(map));
		} catch (Exception e) {
			return null;
		}}).collect(Collectors.toList());
	}
}