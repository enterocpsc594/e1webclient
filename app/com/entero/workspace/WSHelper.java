package com.entero.workspace;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map.Entry;

import org.apache.commons.lang3.ArrayUtils;

import com.entero.generic.JsonResponseHandler;
import com.entero.generic.Parser;
import com.entero.generic.SortTable;
import com.entero.identifiers.Identifiers;
import com.entero.identifiers.ItemIdentifier;
import com.entero.rest.RestClient;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.Multimap;

import models.EnteroObject;

public class WSHelper {
	
	public WSHelper(){ }
	
	/**
	 * This method simply filters the existing table of objects locally.
	 * We need to specify the list of objects, the field to filter by, and a list
	 * of descriptions of that field to filter with.
	 * Strict matching can only be applied if descriptions is a string.
	 * remote -> flag = 1
	 * local -> flag = 0
	 * @param tableData
	 * @param field
	 * @param descriptions
	 * @return the filtered table
	 * @throws Exception
	 */
	public List<EnteroObject> filterByField(List<EnteroObject> tableData, Identifiers field, Object descriptions, boolean strictMatch) throws Exception{
		List<EnteroObject> list = new ArrayList<EnteroObject>(tableData);
		if(descriptions.getClass().isInstance("") && !isEmpty(descriptions)){ //descriptions is string
			if(strictMatch){
				list.removeIf(object -> 
				!object.getProperty(field.columnName()).toLowerCase().equals(descriptions.toString().toLowerCase()));
			}else
				list.removeIf(object -> 
				!object.getProperty(field.columnName()).toLowerCase().startsWith(descriptions.toString().toLowerCase()));
		}else list = flushTable();
		return list;
	}
	
	/**
	 * Method added to support the dropdown of itemType
	 * @return
	 * @throws Exception
	 */
	public static HashMap<String,String> getItemTypes(RestClient client) throws Exception{
		JsonResponseHandler response = new JsonResponseHandler(client, "/itemType");
		response.fetchJsonAtDefaultEndpoint();
		return createMap(response.getResponseJsonNode(), 
				ItemIdentifier.ITEMTYPEDESCRIPTION.fieldName(), ItemIdentifier.ITEMTYPEID.fieldName());
	}
	
	/**
	 * Method added to support the dropdown of itemType
	 * @return
	 * @throws Exception
	 */
	public static HashMap<String,String> getItemGroups(RestClient client) throws Exception{
		JsonResponseHandler response = new JsonResponseHandler(client, "/itemGroup");
		response.fetchJsonAtDefaultEndpoint();
		return createMap(response.getResponseJsonNode(), 
				ItemIdentifier.ITEMGROUPDESCRIPTION.fieldName(), ItemIdentifier.ITEMGROUPID.fieldName());
	}
	
	/**
	 * 
	 * @param map
	 * @return
	 * @throws Exception
	 */
	public static EnteroObject buildObject(Multimap<JsonNode,Object> map) throws Exception{
		EnteroObject result = new EnteroObject();
		if(map != null){
			Iterator<Entry<JsonNode,Object>> iter = map.entries().iterator();
			while(iter.hasNext()){
				Entry<JsonNode,Object> mapEntry = iter.next();
				merge(result, Parser.createParser(mapEntry.getValue(), mapEntry.getKey(), true).getTableObject());
			}

		}
		return result;
	}
	
	/**
	 * Method to create a hashmap of one field to another within a node
	 * @param node
	 * @param keyField
	 * @param valueField
	 * @return
	 */
	public static HashMap<String, String> createMap(JsonNode node, String keyField, String valueField){
		HashMap<String, String> resultMap = new HashMap<String, String>();
		node.forEach(container ->{
			resultMap.put(container.path(keyField).asText(), container.path(valueField).asText());
		});
		return resultMap;
	}
	
	/**
	 * Method to merge two entero objects
	 * @param o1
	 * @param o2
	 * @return
	 */
	public static EnteroObject merge(EnteroObject o1, EnteroObject o2){
		HashMap<String,String> resultMap = new HashMap<String,String>();
		EnteroObject result = new EnteroObject();
		resultMap = o1.getPropertyMap();
		resultMap.putAll(o2.getPropertyMap());
		result.setPropertyMap(resultMap);
		return result;
	}
	
	/**
	 * Method for checks.
	 * @param str
	 * @return
	 */
	public static boolean isEmpty(Object str){
		boolean isNull = str == null ? true : false;
		if(isNull) return true;
//		else if(str.getClass().isInstance(""))
		else if(str.getClass().getSimpleName().equals(String.class.getSimpleName()))
			return String.class.cast(str).isEmpty();
		else if(str.getClass().getSimpleName().equals(ArrayList.class.getSimpleName())){
			return List.class.cast(str).size() == 0 || List.class.cast(str).get(0).toString().isEmpty();
		}
		else return false;
	}
	
	/**
	 * 
	 * @param WSIdentifier
	 * @return
	 * @throws Exception
	 */
	public static SortTable initSort(Object WSIdentifier) throws Exception{
		Method columns = WSIdentifier.getClass().getComponentType().getDeclaredMethod("columnsOf", WSIdentifier.getClass());
		return SortTable.create((String[])columns.invoke(null, WSIdentifier));
	}
	
	/**
	 * 
	 * @param requestParameters
	 * @param client
	 * @param endpoint
	 * @return
	 * @throws Exception
	 */
	public static JsonNode getServerResponse(Multimap<String,String> requestParameters, RestClient client, String endpoint) throws Exception{
		JsonResponseHandler response = new JsonResponseHandler(client, endpoint);
		response.fetchJsonAtKeyValPairs(requestParameters);
		return response.getResponseJsonNode();
	}
	
	/**
	 * Method to flush the table.
	 */
	public static List<EnteroObject> flushTable(){
		return new ArrayList<EnteroObject>();
	}

	
}
