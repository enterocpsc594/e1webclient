package com.entero.workspace;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.ArrayUtils;

import play.libs.ws.WSRequestHolder;
import play.libs.ws.WS;
import play.mvc.Call;
import play.mvc.Http;

import com.entero.generic.JsonResponseHandler;
import com.entero.generic.Parser;
import com.entero.generic.SortTable;
import com.entero.identifiers.InvoiceIdentifier;
import com.entero.identifiers.ItemIdentifier;
import com.entero.identifiers.PriceIdentifier;
import com.entero.rest.RestClient;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;

import models.EnteroObject;

public class Invoice extends WSHelper implements Workspace {
	private RestClient client;
	private List<EnteroObject> invoicesMaster = new ArrayList<EnteroObject>();
	private List<EnteroObject> invoices = new ArrayList<EnteroObject>();
	private final String invoiceUrl = "/invoice";
	private final Integer max = 50;
	private Integer offset = 0;
	private SortTable sortTable;

	public Invoice() throws Exception{
		this.sortTable = super.initSort(InvoiceIdentifier.table());
	}

	@Override
	public String getWSName(){ return "Invoice"; }
	
	@Override
	public Integer getMax(){ return this.max;}

	@Override
	public void setClient(RestClient client) throws Exception{this.client = client;}

	@Override
	public List<EnteroObject> getTable(){
		return this.invoices; 
	}

	@Override
	public List<EnteroObject> sort(String columnName) throws Exception {
		this.invoicesMaster = this.sortTable.sortBy(columnName, this.invoicesMaster);
		return this.invoicesMaster;
	}
	
	@Override
	public void searchInvoice(Integer currentOffset, String invoiceReference, String payRec, Object invoiceTypeCode, Object invoiceStatus, String dateType, String fromDate, String toDate) throws Exception{
		Multimap<String,String> requestParams = LinkedListMultimap.create();
		this.invoices = super.flushTable();
		if(offset == 0) this.invoicesMaster = super.flushTable();
		if(!super.isEmpty(invoiceReference))
			requestParams.putAll(this.createRequestParams(InvoiceIdentifier.INVOICEREF, invoiceReference));
		if(!super.isEmpty(payRec))
			requestParams.putAll(this.createRequestParams(InvoiceIdentifier.INVOICEPAYREC, payRec));
		if(!super.isEmpty(invoiceTypeCode))
			requestParams.putAll(this.createRequestParams(InvoiceIdentifier.INVOICETYPE, invoiceTypeCode));
		if(!super.isEmpty(invoiceStatus))
			requestParams.putAll(this.createRequestParams(InvoiceIdentifier.INVOICESTATUS, invoiceStatus));
		if(!super.isEmpty(dateType))
			requestParams.putAll(this.createRequestParams(InvoiceIdentifier.DATETYPE, dateType));
		if(!(super.isEmpty(fromDate) || super.isEmpty(toDate))){
			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
			String startDate = "";
			String endDate = "";
			try{
				Date end = formatter.parse(toDate);
				Date start = formatter.parse(fromDate);
				startDate = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a").format(start);
				endDate = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a").format(end);
			}catch(ParseException e){}
			requestParams.putAll(this.createRequestParams(InvoiceIdentifier.ISSUEDDATE, startDate));
			requestParams.putAll(this.createRequestParams(InvoiceIdentifier.DUEDATE, endDate));
		}
		if(!(super.isEmpty(offset.toString()) || this.offset < 0)){
			this.offset = currentOffset;
			requestParams.put(this.offset.toString(), "offset");
			requestParams.put(this.max.toString(), "max");
		}
		this.filterByParameters(requestParams);
		this.invoicesMaster.addAll(this.invoices);
		//TEST PDF GENERATION--------
//		this.generatePDFReport("65730d76-9870-4162-85cb-a8a10f6f8cca");
//		this.generatePDFReport("a1361083-4ca5-42de-a868-ac3f9e7e40e7");
		//
		
	}

	@Override
	public EnteroObject filterDetails(String selectedObjectID) throws Exception {
		if(super.isEmpty(selectedObjectID)) return new EnteroObject();
		Multimap<JsonNode,Object> resultMap = LinkedListMultimap.create();
		JsonNode nodeToParse = super.getServerResponse(LinkedListMultimap.create(), this.client, this.invoiceUrl+"/"+selectedObjectID);
		HashMap<String,JsonNode> map = Parser.createParser(InvoiceIdentifier.invoiceDetailsContainers(), nodeToParse, false).getParsedMap();
		resultMap.put(nodeToParse, InvoiceIdentifier.invoiceDetailsShort());
		resultMap.put(map.get(InvoiceIdentifier.CURRENCY.fieldName()),InvoiceIdentifier.CURRENCYDESCRIPTION);
		resultMap.put(map.get(InvoiceIdentifier.REMITADDRESS.fieldName()),InvoiceIdentifier.REMITADDRESSDESCRIPTION);
		resultMap.put(map.get(InvoiceIdentifier.CP.fieldName()),InvoiceIdentifier.CPDESCRIPTION);
		resultMap.put(map.get(InvoiceIdentifier.CP.fieldName()).path(InvoiceIdentifier.CPCONTACT.fieldName()),InvoiceIdentifier.CPCONTACTDESCRIPTION);
		resultMap.put(map.get(InvoiceIdentifier.CP.fieldName()).path(InvoiceIdentifier.CPADDRESS.fieldName()),InvoiceIdentifier.CPADDRESSDESCRIPTION);
		resultMap.put(map.get(InvoiceIdentifier.BU.fieldName()),InvoiceIdentifier.BUDESCRIPTION);
		resultMap.put(map.get(InvoiceIdentifier.BU.fieldName()).path(InvoiceIdentifier.BUCONTACT.fieldName()),InvoiceIdentifier.BUCONTACTDESCRIPTION);
		resultMap.put(map.get(InvoiceIdentifier.BU.fieldName()).path(InvoiceIdentifier.BUADDRESS.fieldName()),InvoiceIdentifier.BUADDRESSDESCRIPTION);
		resultMap.put(map.get(InvoiceIdentifier.AUDITINFO.fieldName()), InvoiceIdentifier.invoiceAuditInfo());
		EnteroObject details = super.buildObject(resultMap);
		return details;
	}
	
	@Override
	public List<EnteroObject> filterDetailsBottomPane(String selectedObjectID) throws Exception {
		if(super.isEmpty(selectedObjectID)) return new ArrayList<EnteroObject>();
		List<EnteroObject> detailsTable = new ArrayList<EnteroObject>();
		Multimap<JsonNode,Object> resultMap = LinkedListMultimap.create();
		JsonNode nodeToParse = super.getServerResponse(LinkedListMultimap.create(), this.client, this.invoiceUrl+"/"+selectedObjectID).path("details"); //JsonNode of details (ROOTNODE)
		HashMap<String,JsonNode> map = new HashMap<String,JsonNode>();
		nodeToParse.forEach(node -> {
			try{
				map.putAll(Parser.createParser(InvoiceIdentifier.invoiceDetailsBottomContainers(), node, false).getParsedMap());
				resultMap.put(node, InvoiceIdentifier.invoiceDetailsBottomShort());
				resultMap.put(map.get(InvoiceIdentifier.SOURCELOCATION.fieldName()),InvoiceIdentifier.SOURCELOCATIONDESC);
				resultMap.put(map.get(InvoiceIdentifier.QUANTITYDELIVERED.fieldName()),InvoiceIdentifier.QUANTITY);
				resultMap.put(map.get(InvoiceIdentifier.SOURCEITEM.fieldName()),InvoiceIdentifier.SOURCEITEMSHORTDESC);
				resultMap.put(map.get(InvoiceIdentifier.QUANTITYDELIVERED.fieldName()).path(InvoiceIdentifier.UOM.fieldName()),InvoiceIdentifier.UOMSHORTDESC);
				detailsTable.add(super.buildObject(resultMap));
				map.clear();
				resultMap.clear();
			} catch (Exception e) {}
		});
		return detailsTable;
	}
	
	public String generatePDFReport(String selectedObjectID) throws Exception {
		if(isEmpty(selectedObjectID)) return "";
		String resultPath = "";
		String linkToFile = new JsonResponseHandler(this.client, this.invoiceUrl + "/"+selectedObjectID).getCurrentURL();
		resultPath = this.client.getResponseAsPDF(linkToFile, selectedObjectID);
		//System.out.println("PATH-> " +  resultPath);
		return resultPath; 
	}
	
	private Multimap<String,String> createRequestParams(InvoiceIdentifier invoiceID, Object searchValue) throws Exception {
		Multimap<String,String> requestParams = LinkedListMultimap.create();
		String param = "";
		if(!super.isEmpty(searchValue)){
			switch(invoiceID){
			case INVOICEREF:
				param = "invoiceReference";
				break;
			case INVOICEPAYREC:
				param = "payRecType";
				break;
			case INVOICETYPE:
				param = "invoiceTypeCode";
				break;
			case INVOICESTATUS:
				param = "invoiceStatus";
				break;
			case DATETYPE:
				param = "dateType";
				break;				
			case ISSUEDDATE:
				param = "fromDate";
				break;
			case DUEDATE:
				param = "toDate";
				break;
			default: break;
			}
			if(!(param.isEmpty() || searchValue.getClass().getSimpleName().equals(ArrayList.class.getSimpleName()))){
				requestParams.put(searchValue.toString(), param);
			}else{
				@SuppressWarnings("unchecked")
				List<String> list = (List<String>)searchValue;
				for(String element : list) requestParams.put(element, param);
			}
		}
		return requestParams;
	}

	private void filterByParameters(Multimap<String,String> requestParams) throws Exception{
		if(!(requestParams.entries().size() >= 2)){
			this.invoices = super.flushTable();
			return;
		}
		JsonNode nodeToParse = super.getServerResponse(requestParams, this.client, this.invoiceUrl);
		this.invoices = Parser.createParser(ArrayUtils.add(InvoiceIdentifier.table(), InvoiceIdentifier.ID), nodeToParse, true).getTableData();
		Multimap<JsonNode,Object> map = LinkedListMultimap.create();
		HashMap<String,JsonNode> currentNodeMap = new HashMap<String,JsonNode>(); 
		Iterator<JsonNode> nodeIter = nodeToParse.iterator();
		this.invoices.stream().map(object ->{ map.clear();
		currentNodeMap.clear();
		try {
			currentNodeMap.putAll(Parser.createParser(InvoiceIdentifier.tableShort(), nodeIter.next(), false).getParsedMap());
			map.put(currentNodeMap.get(InvoiceIdentifier.BUSINESSUNIT.fieldName()), InvoiceIdentifier.BUSINESSUNITDESCRIPTION);
			map.put(currentNodeMap.get(InvoiceIdentifier.COUNTERPARTY.fieldName()), InvoiceIdentifier.COUNTERPARTYDESCRIPTION);
			map.put(currentNodeMap.get(InvoiceIdentifier.CURRENCY.fieldName()), InvoiceIdentifier.CURRENCYDESCRIPTION);
			return super.merge(object,
					super.buildObject(map)
					);
		} catch (Exception e) {
			return null;
		}}).collect(Collectors.toList());
	}
}
