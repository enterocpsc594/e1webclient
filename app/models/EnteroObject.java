package models;

import java.util.HashMap;


/*
 * Represents an entero object (ie. Item, Price, Invoice, etc)
 * Set various properties on the item with setProperty.
 * For example an "Item" would have a shortDescription and longDescription
 */

public class EnteroObject {
	private HashMap<String,String> propertyMap = new HashMap<String,String>();
	public void setProperty(String name, String value) {
		propertyMap.put(name, value);
	}
	public String getProperty(String name) {
		return propertyMap.get(name);
	}
	public HashMap<String,String> getPropertyMap(){
		return this.propertyMap;
	}
	public void setPropertyMap(HashMap<String,String> properties){
		this.propertyMap = properties;
	}
	
}
