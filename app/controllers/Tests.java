package controllers;

import com.entero.identifiers.InvoiceIdentifier;
import com.entero.rest.RestClient;
import com.entero.workspace.WorkspaceFactory;
import com.entero.workspace.Workspace;

import java.util.Arrays;
import java.util.List;

import models.EnteroObject;
import play.mvc.*;
import views.html.*;

/**
 * Controller used for testing some stuff
 */
public class Tests extends Controller{
    static Workspace workspace = null;
    static RestClient client = null;
    /*
    * Refer to javadoc of searchInvoice
    */
    public static Result testInvoiceSearch() throws Exception {
    	if(client == null) client = new RestClient("webtest", "entero");
        if(workspace == null || WorkspaceFactory.getWorkspaceName() != WorkspaceFactory.Endpoint.INVOICE)
            workspace = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.INVOICE);
        workspace.setClient(client);
        //TO GET THE DROPDOWN OF ANY LIST OF CONSTANTS PLEASE USE: InvoiceIdentifier.fieldsOf(InvoiceIdentifier.invoiceTypes())
        //detail test id = 65730d76-9870-4162-85cb-a8a10f6f8cca
        workspace.searchInvoice(0,InvoiceIdentifier.payRecTypes()[1].fieldName(), //REC 
        		"",
        		InvoiceIdentifier.invoiceTypes()[9].fieldName(), //SALES
        		InvoiceIdentifier.invoiceStatus()[3].fieldName(), //PAID
        		"", //date type
        		"", //from date e.g., "Nov 10, 2008 12:00:00 AM"
        		""//to date e.g., "Dec 10, 2013 12:00:00 AM"
        		);
        List<EnteroObject> result = workspace.getTable();
        String[] showColumns = InvoiceIdentifier
                .columnsOf(InvoiceIdentifier.table()); //Put here the columns we want to display on the table.
        return ok(searchresult.render(workspace.getTable(), Arrays.asList(showColumns), "", workspace.getWSName(),0));
    }

}
