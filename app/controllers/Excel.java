package controllers;

import com.entero.generic.UserSession;
import com.entero.workspace.Workspace;
import com.entero.generic.UserSessionManager;
import com.entero.identifiers.ItemIdentifier;
import models.EnteroObject;

import com.fasterxml.jackson.databind.JsonNode;
import play.*;
import play.libs.F;
import play.libs.F.Promise;
import play.libs.ws.*;
import play.mvc.*;
import views.html.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.hssf.usermodel.*;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;

/*
    Controller used for generating excel documents for workspace data
 */

public class Excel extends Controller {
    /*
	 *	Generates an Excel spreadsheet based off of the selected items
	 *  Returns the path of the generated spreadsheet so the user can download it.
	 *
	 *
	 */
    public static Result itemGenerateExcel() throws Exception {
        System.out.println(routes.Assets.at("javascripts/main.js"));
        UserSession userSession= UserSessionManager.getSession(session("uuid"));
        if (userSession==null) return redirect("/login");   //Redirect if the user is not logged in
        JsonNode params = request().body().asJson();
        Workspace workspace = userSession.getWorkspace(params.get(0).asText());
        List<EnteroObject> items = workspace.getTable();
        Workbook wb = new HSSFWorkbook(); 	//Create the Excel workbook
        Sheet s = wb.createSheet();			//Create a new sheet
        Row r = null;
        Cell c = null;
        int rowCount = 1;
        r = s.createRow(0);					//Create the row for the header

        //Prepare the table header.
        String[] showColumns = ItemIdentifier
                .columnsOf(ItemIdentifier.table());
        c = r.createCell(0);
        c.setCellValue("ID");
        for (int i=0; i<showColumns.length; i++) {
            c = r.createCell(i+1);
            c.setCellValue(showColumns[i]);
            s.setColumnWidth(i, 20*256);
        }

        //Add all selected items to the spreadsheet
        for (int i=0; i<items.size(); i++) {
            String itemID = items.get(i).getProperty("Item id");
            for (int ii=0; ii<params.get(1).size(); ii++) {
                if (itemID.compareTo(params.get(1).get(ii).asText())==0) {
                    r = s.createRow(rowCount);
                    rowCount++;
                    c = r.createCell(0);
                    c.setCellValue(itemID);
                    for (int x=0; x<showColumns.length; x++) {
                        c = r.createCell(x+1);
                        c.setCellValue(items.get(i).getProperty(showColumns[x]));
                    }

                    break;
                }
            }
        }
        new File("public/Excel/").mkdirs();
        File folder = new File("public/Excel/");
        com.entero.rest.RestClient.clearFolder(folder); //empty out the contents
        //Save the spreadsheet as a random file name and return it
        String uuid = java.util.UUID.randomUUID().toString();
        String fileName = "Excel/"+uuid+".xls";
        FileOutputStream out = new FileOutputStream("public/"+fileName); // Or .xlsx
        wb.write(out);
        out.close();

        return ok(fileName).as("text/html");
    }


    /*
        Generates an xml file based on data passed from the client.

     */
    public static Result datatableGenerateExcel() throws Exception {
        System.out.println(routes.Assets.at("javascripts/main.js"));
        JsonNode params = request().body().asJson();
        Workbook wb = new HSSFWorkbook(); 	//Create the Excel workbook
        Sheet s = wb.createSheet();			//Create a new sheet
        Row r = null;
        Cell c = null;
        int rowCount = 1;
        r = s.createRow(0);					//Create the row for the header


        int colCount = 0;
        for (final JsonNode objNode : params.get(0)) {
            c = r.createCell(colCount);
            c.setCellValue(objNode.asText());
            colCount++;
        }

        for (final JsonNode row : params.get(1)) {
            r = s.createRow(rowCount);
            rowCount++;
            int col=0;
            for (final JsonNode objNode : row) {
                c = r.createCell(col);
                c.setCellValue(objNode.asText());
                col++;
            }
        }
        new File("public/Excel/").mkdirs();
        File folder = new File("public/Excel/");
        com.entero.rest.RestClient.clearFolder(folder); //empty out the contents
        //Save the spreadsheet as a random file name and return it
        String uuid = java.util.UUID.randomUUID().toString();
        String fileName = "Excel/"+uuid+".xls";
        FileOutputStream out = new FileOutputStream("public/"+fileName); // Or .xlsx
        wb.write(out);
        out.close();

        return ok(fileName).as("text/html");
    }



}
