package controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Arrays;


import com.entero.generic.UserSessionManager;
import com.entero.identifiers.InvoiceIdentifier;
import models.EnteroObject;

import com.entero.identifiers.ItemIdentifier;
import com.entero.identifiers.PriceIdentifier;
import com.entero.rest.RestClient;
import com.entero.workspace.Workspace;
import com.entero.workspace.WorkspaceFactory;
import com.entero.workspace.WorkspaceFactory.Endpoint;
import com.entero.generic.UserSession;


import play.*;
import play.libs.F;
import play.libs.F.Promise;
import play.libs.ws.*;
import play.mvc.*;
import views.html.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.hssf.usermodel.*;
public class Application extends Controller {
	
    //Returns the index page (ie. load the main application content)
    public static Result index() throws Exception{
        if (UserSessionManager.getSession(session("uuid"))==null) {
            return redirect("/login");
        }
        UserSession userSession= UserSessionManager.getSession(session("uuid"));
        RestClient client = userSession.getRestClient();
        Workspace workspace = null;
            if(workspace == null || WorkspaceFactory.getWorkspaceName() != Endpoint.ITEMS) {
                workspace = WorkspaceFactory.createWorkspace(Endpoint.ITEMS);
            }
            workspace.setClient(client);
        List<String> itemType = workspace.getItemTypes();
        List<String> payrecs = Arrays.asList( InvoiceIdentifier.fieldsOf(InvoiceIdentifier.payRecTypes()));
        List<String> itemtypes = Arrays.asList( InvoiceIdentifier.fieldsOf(InvoiceIdentifier.invoiceTypes()));
        List<String> itemstatuses = Arrays.asList( InvoiceIdentifier.fieldsOf(InvoiceIdentifier.invoiceStatus()));
        List<String> invoicedatetypes = Arrays.asList( InvoiceIdentifier.fieldsOf(InvoiceIdentifier.invoiceDateTypes()));
        return ok(index.render(itemType,payrecs, itemtypes, itemstatuses,invoicedatetypes));
    }

    public static Result report(String path) throws Exception {

        response().setHeader("Content-disposition","");

        return ok(new java.io.File("public/"+path));
    }

	/*
	 * Sorts the search result table by a specified column
	 *
	 *
	*/
	public static Result sortSearchResultTable() throws Exception {
        UserSession userSession= UserSessionManager.getSession(session("uuid"));
		Map<String, String[]> params = request().body().asFormUrlEncoded();
        Workspace workspace = userSession.getWorkspace(params.get("ws_uid")[0]);
		//This is the list with all of the itemGroups selected by the user
        List<EnteroObject> sorted = workspace.sort(params.get("column_sort")[0]);
		String[] showColumns = ItemIdentifier
				.columnsOf(ItemIdentifier.table()); //Put here the columns we want to display on the table.
		return ok(searchresult.render(sorted, Arrays.asList(showColumns), params.get("column_sort")[0], workspace.getWSName(), 0));
	}

    /*
	 * Sorts the search result table by a specified column
	 *
	 *
	*/
    public static Result sortSearchResultTablePrice() throws Exception {
        UserSession userSession= UserSessionManager.getSession(session("uuid"));
        Map<String, String[]> params = request().body().asFormUrlEncoded();
        Workspace workspace = userSession.getWorkspace(params.get("ws_uid")[0]);
        //This is the list with all of the itemGroups selected by the user
        List<EnteroObject> sorted = workspace.sort(params.get("column_sort")[0]);
        String[] showColumns = PriceIdentifier
                .columnsOf(PriceIdentifier.table()); //Put here the columns we want to display on the table.
        return ok(searchresult.render(sorted, Arrays.asList(showColumns), params.get("column_sort")[0], workspace.getWSName(), 0));
    }

    /*
	 * Sorts the search result table by a specified column
	 *
	 *
	*/
    public static Result sortInvoiceResultTable() throws Exception {
        UserSession userSession= UserSessionManager.getSession(session("uuid"));
        Map<String, String[]> params = request().body().asFormUrlEncoded();
        Workspace workspace = userSession.getWorkspace(params.get("ws_uid")[0]);
        //This is the list with all of the itemGroups selected by the user
        List<EnteroObject> sorted = workspace.sort(params.get("column_sort")[0]);
        String[] showColumns = InvoiceIdentifier
                .columnsOf(InvoiceIdentifier.table()); //Put here the columns we want to display on the table.
        return ok(searchresult.render(sorted, Arrays.asList(showColumns), params.get("column_sort")[0], workspace.getWSName(), 0));
    }

	/*
	* 	Get the details for an item based on it's ID eg: 23A53BBC-C495-4CCA-9EEF-70AC901A1848
	* 	Note: please change "id" in searchresult.scala.html to incorporate 
	* 	enumerated types (namely ItemIdentifier.itemTable)..all enteroobjects are referred to by the second
	* 	string in a tuple.
		wsID: The Workspace UID (Unique Identifier)
		itemID: The UID of the item to show the details for
	*
	*/

	public static Result itemDetails(int wsID, String itemID) throws Exception {
        UserSession userSession= UserSessionManager.getSession(session("uuid"));
		Workspace workspace = userSession.getWorkspace(String.valueOf(wsID)); //Get the workspace handle based on the uid
		EnteroObject item = workspace.filterDetails(itemID);
		return  ok(detailviewItems.render(item));
	}

    /*
	* 	Get the details for an invoice based on it's ID eg: 23A53BBC-C495-4CCA-9EEF-70AC901A1848
	* 	Note: please change "id" in searchresult.scala.html to incorporate
	* 	enumerated types (namely ItemIdentifier.itemTable)..all enteroobjects are referred to by the second
	* 	string in a tuple.
		wsID: The Workspace UID (Unique Identifier)
		itemID: The UID of the item to show the details for
	*
	*/

    public static Result invoiceDetails(int wsID, String itemID) throws Exception {
        UserSession userSession= UserSessionManager.getSession(session("uuid"));
        Workspace workspace = userSession.getWorkspace(String.valueOf(wsID)); //Get the workspace handle based on the uid
        List<EnteroObject> bottomPane = workspace.filterDetailsBottomPane(itemID);
        EnteroObject item = workspace.filterDetails(itemID);
        return  ok(detailviewInvoice.render(item,bottomPane));
    }

    public static Result invoicePDF(int wsID, String itemID) throws Exception {
        UserSession userSession= UserSessionManager.getSession(session("uuid"));
        Workspace workspace = userSession.getWorkspace(String.valueOf(wsID)); //Get the workspace handle based on the uid
        String pdfPath = workspace.generatePDFReport(itemID);
        return  ok(invoiceview.render("report/"+pdfPath)); //ok(detailviewInvoice.render(item,bottomPane));
    }

    public static Result invoicePDFDownload(int wsID, String itemID) throws Exception {
        UserSession userSession= UserSessionManager.getSession(session("uuid"));
        Workspace workspace = userSession.getWorkspace(String.valueOf(wsID)); //Get the workspace handle based on the uid
        String pdfPath = workspace.generatePDFReport(itemID);
        response().setContentType("application/x-download");
        response().setHeader("Content-disposition","attachment; filename=report.pdf");
        return  ok(pdfPath).as("text/html"); //ok(detailviewInvoice.render(item,bottomPane));
    }



    /*
    *   Get the itemAttributes for an item based on it's ID eg: 23A53BBC-C495-4CCA-9EEF-70AC901A1848
    *   Note: please change "id" in searchresult.scala.html to incorporate 
    *   enumerated types (namely ItemIdentifier.itemTable)..all enteroobjects are referred to by the second
    *   string in a tuple.
        wsID: The Workspace UID (Unique Identifier)
        itemID: The UID of the item to show the details for
    *
    */

    public static Result itemAttributes(int wsID, String itemID) throws Exception {
        UserSession userSession= UserSessionManager.getSession(session("uuid"));
        Workspace workspace = userSession.getWorkspace(String.valueOf(wsID)); //Get the workspace handle based on the uid
        List<EnteroObject> items = workspace.filterAttributes(itemID);
        return  ok(itemattributes.render(items));
    }

    	/*
	* 	Get the details for a price based on it's ID eg: 23A53BBC-C495-4CCA-9EEF-70AC901A1848
	* 	Note: please change "id" in searchresult.scala.html to incorporate
	* 	enumerated types (namely ItemIdentifier.itemTable)..all enteroobjects are referred to by the second
	* 	string in a tuple.
		wsID: The Workspace UID (Unique Identifier)
		itemID: The UID of the item to show the details for
	*
	*/

    public static Result priceDetails(int wsID, String itemID) throws Exception {
        UserSession userSession= UserSessionManager.getSession(session("uuid"));
        Workspace workspace = userSession.getWorkspace(String.valueOf(wsID)); //Get the workspace handle based on the uid
        List<EnteroObject> rightPane = workspace.filterDetailsRightPane(itemID);
        EnteroObject item = workspace.filterDetails(itemID);
        return  ok(detailviewPrice.render(item,rightPane));
    }

	/*
	 *   This is used to render the "Multi parameter" search fields in the search area. For example the "itemGroup" options
	 *
	 */
	public static Result multiParameterList(String listType) throws Exception {
        UserSession userSession= UserSessionManager.getSession(session("uuid"));
		RestClient client = userSession.getRestClient();
        Workspace workspace = null;
		if (listType.toLowerCase().equals("itemgroup")) {
            if(workspace == null || WorkspaceFactory.getWorkspaceName() != Endpoint.ITEMS) {
                workspace = WorkspaceFactory.createWorkspace(Endpoint.ITEMS);
            }
            workspace.setClient(client);
			return ok(multiSelectParameter.render(workspace.getItemGroups()));
		}
        if (listType.toLowerCase().equals("locationtype")) {
            if(workspace == null || WorkspaceFactory.getWorkspaceName() != Endpoint.LOCATION) {
                workspace = WorkspaceFactory.createWorkspace(Endpoint.LOCATION);
            }
            workspace.setClient(client);
            return ok(multiSelectParameter.render(workspace.getLocationTypes()));
        }
        if (listType.toLowerCase().equals("invoicetype")) {
            if(workspace == null || WorkspaceFactory.getWorkspaceName() != Endpoint.INVOICE) {
                workspace = WorkspaceFactory.createWorkspace(Endpoint.INVOICE);
            }
            workspace.setClient(client);
            return ok(multiSelectParameter.render(Arrays.asList(InvoiceIdentifier.fieldsOf(InvoiceIdentifier.invoiceTypes()))));
        }
        if (listType.toLowerCase().equals("invoicestatus")) {
            if(workspace == null || WorkspaceFactory.getWorkspaceName() != Endpoint.INVOICE) {
                workspace = WorkspaceFactory.createWorkspace(Endpoint.INVOICE);
            }
            workspace.setClient(client);
            return ok(multiSelectParameter.render(Arrays.asList( InvoiceIdentifier.fieldsOf(InvoiceIdentifier.invoiceStatus()))));
        }
		return badRequest();
	}


    public static Result uuidlookup(String type) throws Exception {
        Workspace workspace = null;
        RestClient client = null;
        Map<String, String[]> params = request().body().asFormUrlEncoded();
        if (type.compareTo("item")==0) {
            if (client == null) client = new RestClient("webtest", "entero");
            if (workspace == null || WorkspaceFactory.getWorkspaceName() != WorkspaceFactory.Endpoint.ITEMS)
                workspace = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.ITEMS);
            workspace.setClient(client);
            List<EnteroObject> uuidResult = workspace.getItemListWindow(params.get("search")[0]);
            String[] showColumns = {"Short description"};
            return ok(uuidlookup.render(uuidResult,Arrays.asList(showColumns)));
        }
        if (type.compareTo("location")==0) {
            List<String> locationtypes = Arrays.asList(params.get("locationTypes")[0].split(","));
            if (client == null) client = new RestClient("webtest", "entero");
            if (workspace == null || WorkspaceFactory.getWorkspaceName() != WorkspaceFactory.Endpoint.LOCATION)
                workspace = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.LOCATION);
            workspace.setClient(client);
            List<EnteroObject> uuidResult = workspace.searchLocations(params.get("search")[0], locationtypes);
            String[] showColumns = {"Type", "Location Description"};
            return ok(uuidlookup.render(uuidResult,Arrays.asList(showColumns)));
        }
        return badRequest();
    }
}

