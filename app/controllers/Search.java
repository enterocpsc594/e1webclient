package controllers;

import com.entero.generic.UserSession;
import com.entero.generic.UserSessionManager;
import com.entero.identifiers.ItemIdentifier;
import com.entero.identifiers.PriceIdentifier;
import com.entero.identifiers.InvoiceIdentifier;
import com.entero.workspace.Workspace;
import com.entero.workspace.WorkspaceFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Iterator;

import play.*;
import play.libs.F;
import play.libs.F.Promise;
import play.libs.ws.*;
import play.mvc.*;
import views.html.*;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.hssf.usermodel.*;
/**
 * Used for performing searches in workspaces
 */
public class Search extends Controller{


    public static Result itemSearchResultTable() throws Exception {
        UserSession userSession= UserSessionManager.getSession(session("uuid"));
        if (userSession==null) return redirect("/login");   //Redirect if the user is not logged in
        Map<String, String[]> params = request().body().asFormUrlEncoded();
        Workspace workspace = userSession.getWorkspace(params.get("ws_uid")[0]);
        if(workspace == null || WorkspaceFactory.getWorkspaceName() != WorkspaceFactory.Endpoint.ITEMS) {    //Create the workspace if it is not already created
            workspace = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.ITEMS);
            userSession.addWorkspace(params.get("ws_uid")[0], workspace);  //Add the created workspace to the user session
        }
        workspace.setClient(userSession.getRestClient()); //must be called
        Integer offset = Integer.parseInt(params.get("off")[0]);
        //This is the list with all of the itemGroups selected by the user
        List<String> itemGroupSearch = Arrays.asList(params.get("searchParam_itemGroups")[0].split(","));
        String itemType = params.get("searchParam_type")[0];

        workspace.searchItems(offset, itemType, itemGroupSearch, params.get("searchParam_description")[0]);
        String[] showColumns = ItemIdentifier
                .columnsOf(ItemIdentifier.table()); //Put here the columns we want to display on the table.
        return ok(searchresult.render(workspace.getTable(), Arrays.asList(showColumns),"","Item", offset));
    }

    /*
     * Essentially each item has a list of key value pairs (column name, column value)
     * according to the model created. Refer to Items.java (in com.entero.workspace)
     * for more details.
     */
    public static Result priceSearchResultTable() throws Exception {
        UserSession userSession= UserSessionManager.getSession(session("uuid"));
        if (userSession==null) return redirect("/login");   //Redirect if the user is not logged in
        Map<String, String[]> params = request().body().asFormUrlEncoded();
        Workspace workspace = userSession.getWorkspace(params.get("ws_uid")[0]);
        if(workspace == null || WorkspaceFactory.getWorkspaceName() != WorkspaceFactory.Endpoint.PRICE) {
            workspace = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.PRICE);
            userSession.addWorkspace(params.get("ws_uid")[0], workspace);  //Add the created workspace to the user session
        }
        workspace.setClient(userSession.getRestClient()); //must be called
        Integer offset = Integer.parseInt(params.get("off")[0]);
        workspace.searchPrice(offset, params.get("searchParam_sourceLocation")[0], params.get("searchParam_destinationLocation")[0], params.get("searchParam_itemDescription")[0], params.get("searchParam_description")[0]);

        //workspace.searchPrice("SOURCELOCATION", "Edmonton, AB", params.get("searchParam_itemDescription")[0], params.get("searchParam_description")[0]); //can only have one itemshortdesc accorsing to desktop application
        String[] showColumns = PriceIdentifier
                .columnsOf(PriceIdentifier.table()); //Put here the columns we want to display on the table.
        return ok(searchresult.render(workspace.getTable(), Arrays.asList(showColumns), "", "Price", offset));
    }

    /*
     * Essentially each item has a list of key value pairs (column name, column value)
     * according to the model created. Refer to Items.java (in com.entero.workspace)
     * for more details.
     */
    public static Result invoiceSearchResultTable() throws Exception {
        UserSession userSession= UserSessionManager.getSession(session("uuid"));
        if (userSession==null) return redirect("/login");   //Redirect if the user is not logged in
        Map<String, String[]> params = request().body().asFormUrlEncoded();
        Workspace workspace = userSession.getWorkspace(params.get("ws_uid")[0]);
        if(workspace == null || WorkspaceFactory.getWorkspaceName() != WorkspaceFactory.Endpoint.INVOICE) {
            workspace = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.INVOICE);
            userSession.addWorkspace(params.get("ws_uid")[0], workspace);  //Add the created workspace to the user session
        }
        workspace.setClient(userSession.getRestClient()); //must be called
        Integer offset = Integer.parseInt(params.get("off")[0]);
        System.out.println(offset);
        List<String> invoiceTypes = Arrays.asList(params.get("searchParam_type")[0].split(","));
        List<String> invoicestatuses = Arrays.asList(params.get("searchParam_status")[0].split(","));
        for (Iterator<String> itr = invoicestatuses.iterator(); itr.hasNext(); ) {
            System.out.println(itr.next());
        }
        // String payRec, Object invoiceTypeCode, Object invoiceStatus, String dateType, String fromDate, String toDate
        workspace.searchInvoice(offset, params.get("searchParam_invoiceref")[0],
        		params.get("searchParam_payrec")[0],
                invoiceTypes,
                invoicestatuses,
                params.get("searchParam_datetype")[0],
                params.get("searchParam_datefrom")[0],
                params.get("searchParam_dateto")[0]);
        //workspace.searchPriceNew(offset, params.get("searchParam_sourceLocation")[0], params.get("searchParam_destinationLocation")[0], params.get("searchParam_itemDescription")[0], params.get("searchParam_description")[0]);

        //workspace.searchPrice("SOURCELOCATION", "Edmonton, AB", params.get("searchParam_itemDescription")[0], params.get("searchParam_description")[0]); //can only have one itemshortdesc accorsing to desktop application
        String[] showColumns = InvoiceIdentifier
                .columnsOf(InvoiceIdentifier.table()); //Put here the columns we want to display on the table.
        return ok(searchresult.render(workspace.getTable(), Arrays.asList(showColumns), "", "Invoice", offset));
    }
}
