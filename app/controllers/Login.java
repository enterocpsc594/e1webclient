package controllers;

import com.entero.generic.UserSession;
import com.entero.generic.UserSessionManager;
import com.entero.rest.RestClient;
import play.*;
import play.libs.F;
import play.libs.F.Promise;
import play.libs.ws.*;
import play.mvc.*;
import views.html.*;

import java.util.Map;

/**
 * Created by brad on 3/3/15.
 */
public class Login extends Controller{

    /*
        Renders the login page so the user can type in their username and password
     */
    public static Result login() {
        return ok(login.render());
    }

    /*
        User is performing a login (posting credentials)
        Begins a session for the user is credentials are correct.
    */
    public static Result loginPost() {
        Map<String, String[]> params = request().body().asFormUrlEncoded(); //Get the POSTed parameters
        String user = params.get("username")[0];
        String pass = params.get("pass")[0];
        try {
            RestClient newClient = new RestClient(user,pass);	    //Create a new rest client using the credentials provided
            UserSession newSession = new UserSession();			    //Create a new User session for the user
            newSession.setRestClient(newClient);				    //Set the rest client for the user session
            String uuid = UserSessionManager.addSession(newSession);//Add the session to the session manager
            session("uuid", uuid);	                                //Store the UUID in a cookie (session ID)
        } catch (Exception e) {
            flash("msg","Invalid Credentials. Please try again.");
            return redirect("/login");                              //Redirect to the login page if the credentials were bad
        }
        return redirect("/");   //Redirect to the index page
    }
}
