package suites

import com.entero.generic.UserSession
import com.entero.generic.UserSessionManager
import com.entero.identifiers.ItemIdentifier
import com.entero.rest.RestClient
import com.entero.workspace.Workspace
import com.entero.workspace.WorkspaceFactory
import com.entero.workspace.WorkspaceFactory.Endpoint
import models.EnteroObject

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper

import play.api.test.Helpers.running
import play.api.test.FakeApplication

import collection.JavaConversions._
import collection.mutable._
import org.scalatest.junit.AssertionsForJUnit
import org.scalatest.PrivateMethodTester._
import org.scalatest._

import org.scalatest.mock.JMockCycle
import org.jmock.Expectations
import org.jmock.Expectations.returnValue
import org.scalatest.mock.JMockExpectations
import org.jmock.Mockery
import org.jmock.lib.legacy.ClassImposteriser
import org.jmock.lib.concurrent.Synchroniser

import org.scalatest.mock.{JMockCycleFixture, JMockCycle}

/**
 * READ ME (Last updated 24/11/2014):
 * The following tests uses ScalaTest in conjunction with Selenium to 
 * simulate a web browser to test the web app's behaviour, as well as
 * verifying the information. Tests are structured in FlatSpec format,
 * which follows BDD(Behavior-Driven Design) principles.
 * 
 * To compile these tests you need to have the following lines added to your SBT build file:
 * libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.1" % "test"
 * libraryDependencies += "org.seleniumhq.selenium" % "selenium-java" % "2.44.0" % "test"
 *
 * To run these tests you simply type "test" in your sbt terminal.
 *
 */
class ItemsBackendTest extends FlatSpec {
	val maxOffset = "50"
	val mapper = new ObjectMapper()

	/** This defines the fixture method to create mock RestClient objects!
	 * Search "Loan-Fixture" Method for more information
	 */
	def withRestClient(testCode: (RestClient, Mockery) => Any) {
		
		// This creates a context object from JMock
		var context = new Mockery
		context.setImposteriser(ClassImposteriser.INSTANCE)
		context.setThreadingPolicy(new Synchroniser()) //This prevents race conditions with mock objects!

		var mockClient = context.mock(classOf[RestClient])
		try {
			// Loans the mockClient to the tests
			testCode(mockClient, context)
		}
		finally {
			// Clean up objects
			mockClient = null
			context = null
		}
	}
	
	//=======================================================
	behavior of "Items Workspace"
	//=======================================================

	// The fake JSON replies are created using ObjectMapper:

    val fakeItemsJsonResponseSize = 11
    val fakeItemsJsonResponse = mapper.readTree(    // This fake json response Items, which contains multiple items
                                                    // but none of its details
    """ |[
        |    {
        |        "shortDescription": "10/20 Petcoke",
        |        "description": "Petcoke - 10/20 mm",
        |        "locationControl": false,
        |        "odorized": false,
        |        "carryInventoryQuality": false,
        |        "rinRequired": false,
        |        "qtyByComponent": false,
        |        "id": "800AAC53-6B64-4A84-A92E-BD367D87A462",
        |        "ref": "http://172.24.0.103:80/api/item/800AAC53-6B64-4A84-A92E-BD367D87A462?token=122a7d79-1848-4a6f-bc2e-57019c757567&format=json"
        |    },
        |    {
        |        "shortDescription": "10/20 Therm",
        |        "description": "Thermacite 10/20mm ",
        |        "locationControl": false,
        |        "odorized": false,
        |        "carryInventoryQuality": false,
        |        "rinRequired": false,
        |        "qtyByComponent": false,
        |        "id": "AAD6ED5E-FCF8-4138-A127-A99AE7F15FF6",
        |        "ref": "http://172.24.0.103:80/api/item/AAD6ED5E-FCF8-4138-A127-A99AE7F15FF6?token=122a7d79-1848-4a6f-bc2e-57019c757567&format=json"
        |    },
        |    {
        |        "shortDescription": "150/200 Asph",
        |        "description": "Asphalt 150/200",
        |        "locationControl": false,
        |        "odorized": false,
        |        "carryInventoryQuality": false,
        |        "rinRequired": false,
        |        "qtyByComponent": false,
        |        "id": "B48F5CFA-8B89-448E-B6FC-B02AAED74C1E",
        |        "ref": "http://172.24.0.103:80/api/item/B48F5CFA-8B89-448E-B6FC-B02AAED74C1E?token=122a7d79-1848-4a6f-bc2e-57019c757567&format=json"
        |    },
        |    {
        |        "shortDescription": "20/30 Petcoke",
        |        "description": "Petcoke - 20/30 mm",
        |        "locationControl": false,
        |        "odorized": false,
        |        "carryInventoryQuality": false,
        |        "rinRequired": false,
        |        "qtyByComponent": false,
        |        "id": "5E17EAC0-2631-4AE9-8F84-871EDAA3E2E8",
        |        "ref": "http://172.24.0.103:80/api/item/5E17EAC0-2631-4AE9-8F84-871EDAA3E2E8?token=122a7d79-1848-4a6f-bc2e-57019c757567&format=json"
        |    },
        |    {
        |        "shortDescription": "20/30 Therm",
        |        "description": "Thermacite 20/30mm ",
        |        "locationControl": false,
        |        "odorized": false,
        |        "carryInventoryQuality": false,
        |        "rinRequired": false,
        |        "qtyByComponent": false,
        |        "id": "2590BCD7-1E06-4319-947F-214A0025E4A4",
        |        "ref": "http://172.24.0.103:80/api/item/2590BCD7-1E06-4319-947F-214A0025E4A4?token=122a7d79-1848-4a6f-bc2e-57019c757567&format=json"
        |    },
        |    {
        |        "shortDescription": "200/300 Asph",
        |        "description": "Asphalt 200/300",
        |        "locationControl": false,
        |        "odorized": false,
        |        "carryInventoryQuality": false,
        |        "rinRequired": false,
        |        "qtyByComponent": false,
        |        "id": "C4BF8251-42FB-4A16-9F86-B1F6EEABCEF4",
        |        "ref": "http://172.24.0.103:80/api/item/C4BF8251-42FB-4A16-9F86-B1F6EEABCEF4?token=122a7d79-1848-4a6f-bc2e-57019c757567&format=json"
        |    },
        |    {
        |        "shortDescription": "AVGAS",
        |        "description": "Aviation Gas",
        |        "locationControl": false,
        |        "odorized": false,
        |        "carryInventoryQuality": false,
        |        "rinRequired": false,
        |        "qtyByComponent": false,
        |        "id": "6BC9A269-7EBB-43F1-8CBE-49EC2AFB454C",
        |        "ref": "http://172.24.0.103:80/api/item/6BC9A269-7EBB-43F1-8CBE-49EC2AFB454C?token=122a7d79-1848-4a6f-bc2e-57019c757567&format=json"
        |    },
        |    {
        |        "shortDescription": "Acid Gas",
        |        "description": "Acid Gas",
        |        "locationControl": false,
        |        "odorized": false,
        |        "carryInventoryQuality": false,
        |        "rinRequired": false,
        |        "qtyByComponent": false,
        |        "id": "1036e72a-5da5-449d-86b9-dc5cd8e5e32f",
        |        "ref": "http://172.24.0.103:80/api/item/1036e72a-5da5-449d-86b9-dc5cd8e5e32f?token=122a7d79-1848-4a6f-bc2e-57019c757567&format=json"
        |    },
        |    {
        |        "shortDescription": "Ammonia",
        |        "description": "Anhydrous Ammonia",
        |        "locationControl": false,
        |        "odorized": false,
        |        "carryInventoryQuality": false,
        |        "rinRequired": false,
        |        "qtyByComponent": false,
        |        "id": "F0DAD5ED-F638-4D35-92FE-26471CCE8AB3",
        |        "ref": "http://172.24.0.103:80/api/item/F0DAD5ED-F638-4D35-92FE-26471CCE8AB3?token=122a7d79-1848-4a6f-bc2e-57019c757567&format=json"
        |    },
        |    {
        |        "shortDescription": "Anthracite",
        |        "description": "Anthracite",
        |        "locationControl": false,
        |        "odorized": false,
        |        "carryInventoryQuality": false,
        |        "rinRequired": false,
        |        "qtyByComponent": false,
        |        "id": "53715AF6-E78F-4319-9A22-9C3FE3738A4F",
        |        "ref": "http://172.24.0.103:80/api/item/53715AF6-E78F-4319-9A22-9C3FE3738A4F?token=122a7d79-1848-4a6f-bc2e-57019c757567&format=json"
        |    },
        |    {
        |        "shortDescription": "BBL C2",
        |        "description": "BBL Ethane",
        |        "locationControl": false,
        |        "odorized": false,
        |        "carryInventoryQuality": false,
        |        "rinRequired": false,
        |        "qtyByComponent": false,
        |        "id": "df30518e-5835-47cf-a930-816826de62f1",
        |        "ref": "http://172.24.0.103:80/api/item/df30518e-5835-47cf-a930-816826de62f1?token=122a7d79-1848-4a6f-bc2e-57019c757567&format=json"
        |    }
        |]""".stripMargin)

    val fakeDetailedItemsJsonResponseSize = 9
    val fakeDetailedItemsJsonResponse = mapper.readTree("""
        |[
        |    {
        |        "shortDescription": "C4",
        |        "description": "Butane",
        |        "code": "18",
        |        "auditInfo": {
        |            "createUser": "base_v7",
        |            "createDate": "Jan 18, 2008 11:17:02 AM",
        |            "updateUser": "TammyRomeo",
        |            "updateDate": "May 31, 2010 01:39:02 PM"
        |        },
        |        "attributes": [],
        |        "type": {
        |            "description": "Product",
        |            "inventoryBalance": false,
        |            "id": "666A9009-82E2-403C-A60B-33FE451DD6EE",
        |            "ref": "http://172.24.0.103:80/api/itemtype/666A9009-82E2-403C-A60B-33FE451DD6EE?token=16e17f4b-2ef6-4d13-96fd-aa814d1a889e&format=json"
        |        },
        |        "itemGroup": {
        |            "code": "C4",
        |            "description": "Butanes",
        |            "id": "7B4AA572-0A8B-48BE-AC59-653932809915",
        |            "ref": "http://172.24.0.103:80/api/itemgroup/7B4AA572-0A8B-48BE-AC59-653932809915?token=16e17f4b-2ef6-4d13-96fd-aa814d1a889e&format=json"
        |        },
        |        "expiryDate": "Jan 1, 3000 12:00:00 AM",
        |        "locationControl": false,
        |        "tempCorrectionFactor": 0.999001,
        |        "odorized": false,
        |        "displayOrder": 109,
        |        "carryInventoryQuality": false,
        |        "rinRequired": false,
        |        "qtyByComponent": false,
        |        "allocationByComponent": "ALWAYS",
        |        "id": "1d0e13eb-b1f4-463d-9074-4a17c2396de7",
        |        "ref": "http://172.24.0.103:80/api/item/1d0e13eb-b1f4-463d-9074-4a17c2396de7?token=16e17f4b-2ef6-4d13-96fd-aa814d1a889e&format=json"
        |    },
        |    {
        |        "shortDescription": "C4-MX",
        |        "description": "Butane - Component in Mix",
        |        "code": "P17",
        |        "auditInfo": {
        |            "createUser": "QA_MARK",
        |            "createDate": "Jun 6, 2007 06:21:20 PM",
        |            "updateUser": "patpurcell",
        |            "updateDate": "Jun 16, 2014 12:42:53 PM"
        |        },
        |        "attributes": [],
        |        "type": {
        |            "description": "Product",
        |            "inventoryBalance": false,
        |            "id": "666A9009-82E2-403C-A60B-33FE451DD6EE",
        |            "ref": "http://172.24.0.103:80/api/itemtype/666A9009-82E2-403C-A60B-33FE451DD6EE?token=16e17f4b-2ef6-4d13-96fd-aa814d1a889e&format=json"
        |        },
        |        "itemGroup": {
        |            "code": "C4",
        |            "description": "Butanes",
        |            "id": "7B4AA572-0A8B-48BE-AC59-653932809915",
        |            "ref": "http://172.24.0.103:80/api/itemgroup/7B4AA572-0A8B-48BE-AC59-653932809915?token=16e17f4b-2ef6-4d13-96fd-aa814d1a889e&format=json"
        |        },
        |        "expiryDate": "Jan 1, 3000 12:00:00 AM",
        |        "locationControl": false,
        |        "tempCorrectionFactor": 0.999001,
        |        "odorized": false,
        |        "displayOrder": 1,
        |        "carryInventoryQuality": false,
        |        "rinRequired": false,
        |        "qtyByComponent": false,
        |        "lineColorValue": -26368,
        |        "allocationByComponent": "NEVER",
        |        "id": "64D16F47-0950-43D8-AD54-F811A58902CC",
        |        "ref": "http://172.24.0.103:80/api/item/64D16F47-0950-43D8-AD54-F811A58902CC?token=16e17f4b-2ef6-4d13-96fd-aa814d1a889e&format=json"
        |    },
        |    {
        |        "shortDescription": "C4=",
        |        "description": "Butane - Butylene Mix",
        |        "code": "P35",
        |        "auditInfo": {
        |            "createUser": "QA_MARK",
        |            "createDate": "Jun 6, 2007 06:21:20 PM",
        |            "updateUser": "base_v7",
        |            "updateDate": "Jul 17, 2008 03:46:00 PM"
        |        },
        |        "attributes": [],
        |        "type": {
        |            "description": "Product",
        |            "inventoryBalance": false,
        |            "id": "666A9009-82E2-403C-A60B-33FE451DD6EE",
        |            "ref": "http://172.24.0.103:80/api/itemtype/666A9009-82E2-403C-A60B-33FE451DD6EE?token=16e17f4b-2ef6-4d13-96fd-aa814d1a889e&format=json"
        |        },
        |        "itemGroup": {
        |            "code": "C4",
        |            "description": "Butanes",
        |            "id": "7B4AA572-0A8B-48BE-AC59-653932809915",
        |            "ref": "http://172.24.0.103:80/api/itemgroup/7B4AA572-0A8B-48BE-AC59-653932809915?token=16e17f4b-2ef6-4d13-96fd-aa814d1a889e&format=json"
        |        },
        |        "expiryDate": "Jan 1, 3000 12:00:00 AM",
        |        "locationControl": false,
        |        "tempCorrectionFactor": 0.999001,
        |        "odorized": false,
        |        "displayOrder": 1,
        |        "carryInventoryQuality": false,
        |        "rinRequired": false,
        |        "qtyByComponent": false,
        |        "allocationByComponent": "ALWAYS",
        |        "id": "34C2550A-7590-41B5-8EA7-FAA5B6D95525",
        |        "ref": "http://172.24.0.103:80/api/item/34C2550A-7590-41B5-8EA7-FAA5B6D95525?token=16e17f4b-2ef6-4d13-96fd-aa814d1a889e&format=json"
        |    },
        |    {
        |        "shortDescription": "FC4-MX",
        |        "description": "Field Grade Butane - Component in Mix",
        |        "code": "P17a",
        |        "auditInfo": {
        |            "createUser": "MitchBrown",
        |            "createDate": "Feb 7, 2012 11:50:02 AM",
        |            "updateUser": "patpurcell",
        |            "updateDate": "Jun 16, 2014 12:43:02 PM"
        |        },
        |        "attributes": [],
        |        "type": {
        |            "description": "Product",
        |            "inventoryBalance": false,
        |            "id": "666A9009-82E2-403C-A60B-33FE451DD6EE",
        |            "ref": "http://172.24.0.103:80/api/itemtype/666A9009-82E2-403C-A60B-33FE451DD6EE?token=16e17f4b-2ef6-4d13-96fd-aa814d1a889e&format=json"
        |        },
        |        "itemGroup": {
        |            "code": "C4",
        |            "description": "Butanes",
        |            "id": "7B4AA572-0A8B-48BE-AC59-653932809915",
        |            "ref": "http://172.24.0.103:80/api/itemgroup/7B4AA572-0A8B-48BE-AC59-653932809915?token=16e17f4b-2ef6-4d13-96fd-aa814d1a889e&format=json"
        |        },
        |        "expiryDate": "Jan 1, 3000 12:00:00 AM",
        |        "locationControl": false,
        |        "tempCorrectionFactor": 1,
        |        "odorized": false,
        |        "displayOrder": 1,
        |        "carryInventoryQuality": false,
        |        "rinRequired": false,
        |        "qtyByComponent": false,
        |        "allocationByComponent": "NEVER",
        |        "id": "b56d4b6f-96b2-4dbc-9579-35182b0a25b5",
        |        "ref": "http://172.24.0.103:80/api/item/b56d4b6f-96b2-4dbc-9579-35182b0a25b5?token=16e17f4b-2ef6-4d13-96fd-aa814d1a889e&format=json"
        |    },
        |    {
        |        "shortDescription": "IC4",
        |        "description": "Butane - Iso",
        |        "code": "P21",
        |        "auditInfo": {
        |            "createUser": "QA_MARK",
        |            "createDate": "Jun 6, 2007 06:21:20 PM",
        |            "updateUser": "steveremmington",
        |            "updateDate": "Sep 27, 2011 03:05:00 PM"
        |        },
        |        "attributes": [],
        |        "type": {
        |            "description": "Product",
        |            "inventoryBalance": false,
        |            "id": "666A9009-82E2-403C-A60B-33FE451DD6EE",
        |            "ref": "http://172.24.0.103:80/api/itemtype/666A9009-82E2-403C-A60B-33FE451DD6EE?token=16e17f4b-2ef6-4d13-96fd-aa814d1a889e&format=json"
        |        },
        |        "itemGroup": {
        |            "code": "C4",
        |            "description": "Butanes",
        |            "id": "7B4AA572-0A8B-48BE-AC59-653932809915",
        |            "ref": "http://172.24.0.103:80/api/itemgroup/7B4AA572-0A8B-48BE-AC59-653932809915?token=16e17f4b-2ef6-4d13-96fd-aa814d1a889e&format=json"
        |        },
        |        "expiryDate": "Jan 1, 3000 12:00:00 AM",
        |        "locationControl": false,
        |        "tempCorrectionFactor": 0.999001,
        |        "odorized": false,
        |        "displayOrder": 1,
        |        "carryInventoryQuality": false,
        |        "rinRequired": false,
        |        "qtyByComponent": false,
        |        "lineColorValue": -39424,
        |        "allocationByComponent": "ALWAYS",
        |        "id": "5ECE5EE7-80DA-4FD6-9FBD-8CF670A58896",
        |        "ref": "http://172.24.0.103:80/api/item/5ECE5EE7-80DA-4FD6-9FBD-8CF670A58896?token=16e17f4b-2ef6-4d13-96fd-aa814d1a889e&format=json"
        |    },
        |    {
        |        "shortDescription": "IC4-MX",
        |        "description": "Iso Butane - Component in Mix",
        |        "code": "P17b",
        |        "auditInfo": {
        |            "createUser": "MitchBrown",
        |            "createDate": "Feb 7, 2012 11:52:18 AM",
        |            "updateUser": "patpurcell",
        |            "updateDate": "Jun 16, 2014 12:43:06 PM"
        |        },
        |        "attributes": [],
        |        "type": {
        |            "description": "Product",
        |            "inventoryBalance": false,
        |            "id": "666A9009-82E2-403C-A60B-33FE451DD6EE",
        |            "ref": "http://172.24.0.103:80/api/itemtype/666A9009-82E2-403C-A60B-33FE451DD6EE?token=16e17f4b-2ef6-4d13-96fd-aa814d1a889e&format=json"
        |        },
        |        "itemGroup": {
        |            "code": "C4",
        |            "description": "Butanes",
        |            "id": "7B4AA572-0A8B-48BE-AC59-653932809915",
        |            "ref": "http://172.24.0.103:80/api/itemgroup/7B4AA572-0A8B-48BE-AC59-653932809915?token=16e17f4b-2ef6-4d13-96fd-aa814d1a889e&format=json"
        |        },
        |        "expiryDate": "Jan 1, 3000 12:00:00 AM",
        |        "locationControl": false,
        |        "tempCorrectionFactor": 1,
        |        "odorized": false,
        |        "displayOrder": 1,
        |        "carryInventoryQuality": false,
        |        "rinRequired": false,
        |        "qtyByComponent": false,
        |        "allocationByComponent": "NEVER",
        |        "id": "53f6ad05-41f0-43e2-b184-38940b74504e",
        |        "ref": "http://172.24.0.103:80/api/item/53f6ad05-41f0-43e2-b184-38940b74504e?token=16e17f4b-2ef6-4d13-96fd-aa814d1a889e&format=json"
        |    },
        |    {
        |        "shortDescription": "Iso-Butane",
        |        "description": "Iso-Butane Hierarchy",
        |        "code": "BH",
        |        "auditInfo": {
        |            "createUser": "ryanjarymy",
        |            "createDate": "Jun 14, 2013 05:26:19 PM",
        |            "updateUser": "ryanjarymy",
        |            "updateDate": "Aug 2, 2013 04:39:55 PM"
        |        },
        |        "attributes": [],
        |        "type": {
        |            "description": "Product",
        |            "inventoryBalance": false,
        |            "id": "666A9009-82E2-403C-A60B-33FE451DD6EE",
        |            "ref": "http://172.24.0.103:80/api/itemtype/666A9009-82E2-403C-A60B-33FE451DD6EE?token=16e17f4b-2ef6-4d13-96fd-aa814d1a889e&format=json"
        |        },
        |        "itemGroup": {
        |            "code": "C4",
        |            "description": "Butanes",
        |            "id": "7B4AA572-0A8B-48BE-AC59-653932809915",
        |            "ref": "http://172.24.0.103:80/api/itemgroup/7B4AA572-0A8B-48BE-AC59-653932809915?token=16e17f4b-2ef6-4d13-96fd-aa814d1a889e&format=json"
        |        },
        |        "expiryDate": "Jan 1, 3000 12:00:00 AM",
        |        "locationControl": false,
        |        "tempCorrectionFactor": 1,
        |        "odorized": false,
        |        "carryInventoryQuality": false,
        |        "rinRequired": false,
        |        "qtyByComponent": false,
        |        "allocationByComponent": "NEVER",
        |        "id": "63fe2640-4343-4eec-bd51-73e8d21da477",
        |        "ref": "http://172.24.0.103:80/api/item/63fe2640-4343-4eec-bd51-73e8d21da477?token=16e17f4b-2ef6-4d13-96fd-aa814d1a889e&format=json"
        |    },
        |    {
        |        "shortDescription": "NC4",
        |        "description": "Butane - Normal",
        |        "code": "P23",
        |        "auditInfo": {
        |            "createUser": "QA_MARK",
        |            "createDate": "Jun 6, 2007 06:21:20 PM",
        |            "updateUser": "steveremmington",
        |            "updateDate": "Sep 27, 2011 03:05:45 PM"
        |        },
        |        "attributes": [],
        |        "type": {
        |            "description": "Product",
        |            "inventoryBalance": false,
        |            "id": "666A9009-82E2-403C-A60B-33FE451DD6EE",
        |            "ref": "http://172.24.0.103:80/api/itemtype/666A9009-82E2-403C-A60B-33FE451DD6EE?token=16e17f4b-2ef6-4d13-96fd-aa814d1a889e&format=json"
        |        },
        |        "itemGroup": {
        |            "code": "C4",
        |            "description": "Butanes",
        |            "id": "7B4AA572-0A8B-48BE-AC59-653932809915",
        |            "ref": "http://172.24.0.103:80/api/itemgroup/7B4AA572-0A8B-48BE-AC59-653932809915?token=16e17f4b-2ef6-4d13-96fd-aa814d1a889e&format=json"
        |        },
        |        "expiryDate": "Jan 1, 3000 12:00:00 AM",
        |        "locationControl": false,
        |        "tempCorrectionFactor": 0.999001,
        |        "odorized": false,
        |        "displayOrder": 1,
        |        "carryInventoryQuality": false,
        |        "rinRequired": false,
        |        "qtyByComponent": false,
        |        "lineColorValue": -26368,
        |        "allocationByComponent": "ALWAYS",
        |        "id": "6F2B4DF6-CD54-4A7F-8851-220490ECF27E",
        |        "ref": "http://172.24.0.103:80/api/item/6F2B4DF6-CD54-4A7F-8851-220490ECF27E?token=16e17f4b-2ef6-4d13-96fd-aa814d1a889e&format=json"
        |    },
        |    {
        |        "shortDescription": "NC4-MX",
        |        "description": "Normal Butane - Component in Mix",
        |        "code": "P17c",
        |        "auditInfo": {
        |            "createUser": "MitchBrown",
        |            "createDate": "Feb 7, 2012 11:55:06 AM",
        |            "updateUser": "patpurcell",
        |            "updateDate": "Jun 16, 2014 12:43:14 PM"
        |        },
        |        "attributes": [],
        |        "type": {
        |            "description": "Product",
        |            "inventoryBalance": false,
        |            "id": "666A9009-82E2-403C-A60B-33FE451DD6EE",
        |            "ref": "http://172.24.0.103:80/api/itemtype/666A9009-82E2-403C-A60B-33FE451DD6EE?token=16e17f4b-2ef6-4d13-96fd-aa814d1a889e&format=json"
        |        },
        |        "itemGroup": {
        |            "code": "C4",
        |            "description": "Butanes",
        |            "id": "7B4AA572-0A8B-48BE-AC59-653932809915",
        |            "ref": "http://172.24.0.103:80/api/itemgroup/7B4AA572-0A8B-48BE-AC59-653932809915?token=16e17f4b-2ef6-4d13-96fd-aa814d1a889e&format=json"
        |        },
        |        "expiryDate": "Jan 1, 3000 12:00:00 AM",
        |        "locationControl": false,
        |        "tempCorrectionFactor": 1,
        |        "odorized": false,
        |        "displayOrder": 1,
        |        "carryInventoryQuality": false,
        |        "rinRequired": false,
        |        "qtyByComponent": false,
        |        "allocationByComponent": "NEVER",
        |        "id": "c6dc7f65-0cbc-430b-80bd-ab3f5ddd6079",
        |        "ref": "http://172.24.0.103:80/api/item/c6dc7f65-0cbc-430b-80bd-ab3f5ddd6079?token=16e17f4b-2ef6-4d13-96fd-aa814d1a889e&format=json"
        |    }
        |]""".stripMargin)

	val fakeItemGroupJsonResponse = mapper.readTree( // This fake response is for getting the ItemGroups
	 """|[
		|	{
        |		"code": "UNDF",
        |		"description": "Undefined",
        |		"id": "6DB23A00-89F9-4907-8278-C6E69EFF51C3",
        |		"ref": "http://184.150.226.173/e1RestServer/itemgroup/6DB23A00-89F9-4907-8278-C6E69EFF51C3.json?token=a188583d-624d-4def-bf12-dc6efaddf117"
    	|	},
    	|	{
        |		"code": "AMON",
        |		"description": "Ammonia",
        |		"id": "C3FAC269-BC54-47BC-89BD-50800B5E6D22",
        |		"ref": "http://184.150.226.173/e1RestServer/itemgroup/C3FAC269-BC54-47BC-89BD-50800B5E6D22.json?token=0410b000-9570-4866-911c-efb8d5831308"
    	|	},
    	|	{
        |		"code": "BAR",
        |		"description": "Barges",
        |		"id": "CD1FA9E4-AD19-4CA9-AE02-55ECB74627BA",
        |		"ref": "http://184.150.226.173/e1RestServer/itemgroup/CD1FA9E4-AD19-4CA9-AE02-55ECB74627BA.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
    	|	},
    	|	{
        |		"code": "BLDG",
        |		"description": "Buildings",
        |		"id": "870D9337-4C25-48B2-8329-ACE788C8A01B",
        |		"ref": "http://184.150.226.173/e1RestServer/itemgroup/870D9337-4C25-48B2-8329-ACE788C8A01B.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
    	|	},
        |   {
        |       "code": "C3",
        |       "description": "Propanes",
        |        "id": "8B4AA572-0A8B-48BE-AC59-653932809915",
        |        "ref": "http://184.150.226.173/e1RestServer/itemgroup/8B4AA572-0A8B-48BE-AC59-653932809915.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
        |   },
    	|	{
        |		"code": "C4",
        |		"description": "Butanes",
		|        "id": "7B4AA572-0A8B-48BE-AC59-653932809915",
		|        "ref": "http://184.150.226.173/e1RestServer/itemgroup/7B4AA572-0A8B-48BE-AC59-653932809915.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
		|   },
		|	{
        |		"code": "RGP",
        |		"description": "Raw Gas",
        |		"id": "2F226DE1-F285-4612-AEFB-7C17DCE2F852",
        |		"ref": "http://184.150.226.173/e1RestServer/itemgroup/2F226DE1-F285-4612-AEFB-7C17DCE2F852.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
    	|	},
    	|	{
        |		"code": "WP",
        |		"description": "Water",
        |		"id": "4C7804BE-3D84-4A8F-9B44-49CD26B56E15",
        |		"ref": "http://184.150.226.173/e1RestServer/itemgroup/4C7804BE-3D84-4A8F-9B44-49CD26B56E15.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
    	|	},
		|	{
        |		"code": "VES",
        |		"description": "Vessels",
        |		"id": "8A0D67BC-4729-4A1B-A7E5-049C546B6978",
        |		"ref": "http://184.150.226.173/e1RestServer/itemgroup/8A0D67BC-4729-4A1B-A7E5-049C546B6978.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
    	|	}
    	|]""".stripMargin)

	val fakeDetailedItemGroupJsonResponse = mapper.readTree( // The subsequent responses are more detailed and is for getting/filtering the actual Items!
	 """|[
		|	{
        |		"code": "UNDF",
        |		"description": "Undefined",
        |		"items": [
        |			{
        |				"shortDescription": "All Items",
        |				"description": "All Items",
        |				"id": "b9795dad-b755-49ba-af55-d75a473cad3a",
        |				"ref": "http://184.150.226.173/e1RestServer/item/b9795dad-b755-49ba-af55-d75a473cad3a.json?token=a188583d-624d-4def-bf12-dc6efaddf117"
        |			},
        |			{
        |				"shortDescription": "Undefined Item",
        |				"description": "Undefined Item",
        |				"id": "23A53BBC-C495-4CCA-9EEF-70AC901A1848",
        |				"ref": "http://184.150.226.173/e1RestServer/item/23A53BBC-C495-4CCA-9EEF-70AC901A1848.json?token=a188583d-624d-4def-bf12-dc6efaddf117"
        |			}
        |		],
        |		"id": "6DB23A00-89F9-4907-8278-C6E69EFF51C3",
        |		"ref": "http://184.150.226.173/e1RestServer/itemgroup/6DB23A00-89F9-4907-8278-C6E69EFF51C3.json?token=a188583d-624d-4def-bf12-dc6efaddf117"
    	|	},
    	|	{
        |		"code": "AMON",
        |		"description": "Ammonia",
        |		"items": [
        |			{
        |				"shortDescription": "Ammonia",
        |				"description": "Anhydrous Ammonia",
        |				"id": "F0DAD5ED-F638-4D35-92FE-26471CCE8AB3",
        |				"ref": "http://184.150.226.173/e1RestServer/itemgroup/6DB23A00-89F9-4907-8278-C6E69EFF51C3.json?token=0410b000-9570-4866-911c-efb8d5831308"
        |			}
        |		],
        |		"id": "C3FAC269-BC54-47BC-89BD-50800B5E6D22",
        |		"ref": "http://184.150.226.173/e1RestServer/itemgroup/C3FAC269-BC54-47BC-89BD-50800B5E6D22.json?token=0410b000-9570-4866-911c-efb8d5831308"
    	|	},
    	|	{
        |		"code": "BAR",
        |		"description": "Barges",
        |		"items": [
	    |        	{
    	|	            "shortDescription": "Barge",
        |        		"description": "Barge",
		|                "id": "8FEA70D9-9D21-4012-83E9-98FB6FA577DE",
        |        		"ref": "http://184.150.226.173/e1RestServer/item/8FEA70D9-9D21-4012-83E9-98FB6FA577DE.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
        |    		}
        |		],
        |		"id": "CD1FA9E4-AD19-4CA9-AE02-55ECB74627BA",
        |		"ref": "http://184.150.226.173/e1RestServer/itemgroup/CD1FA9E4-AD19-4CA9-AE02-55ECB74627BA.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
    	|	},
    	|	{
        |		"code": "BLDG",
        |		"description": "Buildings",
        |		"items": [],
        |		"id": "870D9337-4C25-48B2-8329-ACE788C8A01B",
        |		"ref": "http://184.150.226.173/e1RestServer/itemgroup/870D9337-4C25-48B2-8329-ACE788C8A01B.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
    	|	},
        |   {
        |       "code": "C3",
        |       "description": "Propanes",
        |       "items": [
        |           {
        |               "shortDescription": "C3",
        |               "description": "Propane",
        |               "id": "1d0e13eb-b1f4-463d-9074-4a17c2396de7",
        |               "ref": "http://184.150.226.173/e1RestServer/item/1d0e13eb-b1f4-463d-9074-4a17c2396de7.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
        |           },
        |           {
        |               "shortDescription": "C3-MX - Should be filtered out!",
        |               "description": "Propane - Component in Mix",
        |               "id": "64D16F47-0950-43D8-AD54-F811A58902CC",
        |               "ref": "http://184.150.226.173/e1RestServer/item/64D16F47-0950-43D8-AD54-F811A58902CC.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
        |           },
        |           {
        |               "shortDescription": "TEST item - Should be filtered out!",
        |               "description": "Test - Component in Mix",
        |               "id": "64D16F47-0950-43D8-AD54-F811A58902CC",
        |               "ref": "http://184.150.226.173/e1RestServer/item/64D16F47-0950-43D8-AD54-F811A58902CC.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
        |           }
        |        ],
        |        "id": "8B4AA572-0A8B-48BE-AC59-653932809915",
        |        "ref": "http://184.150.226.173/e1RestServer/itemgroup/8B4AA572-0A8B-48BE-AC59-653932809915.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
        |   },
    	|	{
        |		"code": "C4",
        |		"description": "Butanes",
        |		"items": [
        |	   		{
        |        		"shortDescription": "C4",
        |        		"description": "Butane",
        |        		"id": "1d0e13eb-b1f4-463d-9074-4a17c2396de7",
        |        		"ref": "http://184.150.226.173/e1RestServer/item/1d0e13eb-b1f4-463d-9074-4a17c2396de7.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
        |    		},
        |    		{
        |        		"shortDescription": "C4-MX",
        |        		"description": "Butane - Component in Mix",
        |        		"id": "64D16F47-0950-43D8-AD54-F811A58902CC",
        |        		"ref": "http://184.150.226.173/e1RestServer/item/64D16F47-0950-43D8-AD54-F811A58902CC.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
        |    		},
		|            {
		|                "shortDescription": "C4=",
		|                "description": "Butane - Butylene Mix",
		|                "id": "34C2550A-7590-41B5-8EA7-FAA5B6D95525",
		|                "ref": "http://184.150.226.173/e1RestServer/item/34C2550A-7590-41B5-8EA7-FAA5B6D95525.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
		|            },
		|            {
		|                "shortDescription": "FC4-MX",
		|                "description": "Field Grade Butane - Component in Mix",
		|                "id": "b56d4b6f-96b2-4dbc-9579-35182b0a25b5",
		|                "ref": "http://184.150.226.173/e1RestServer/item/b56d4b6f-96b2-4dbc-9579-35182b0a25b5.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
		|            },
		|            {
		|                "shortDescription": "IC4",
		|                "description": "Butane - Iso",
		|                "id": "5ECE5EE7-80DA-4FD6-9FBD-8CF670A58896",
		|                "ref": "http://184.150.226.173/e1RestServer/item/5ECE5EE7-80DA-4FD6-9FBD-8CF670A58896.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
		|            },
		|            {
		|                "shortDescription": "IC4-MX",
		|                "description": "Iso Butane - Component in Mix",
		|                "id": "53f6ad05-41f0-43e2-b184-38940b74504e",
		|                "ref": "http://184.150.226.173/e1RestServer/item/53f6ad05-41f0-43e2-b184-38940b74504e.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
		|            },
		|            {
		|                "shortDescription": "Iso-Butane",
		|                "description": "Iso-Butane Hierarchy",
		|                "id": "63fe2640-4343-4eec-bd51-73e8d21da477",
		|                "ref": "http://184.150.226.173/e1RestServer/item/63fe2640-4343-4eec-bd51-73e8d21da477.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
		|            },
		|            {
		|                "shortDescription": "NC4",
		|                "description": "Butane - Normal",
		|                "id": "6F2B4DF6-CD54-4A7F-8851-220490ECF27E",
		|                "ref": "http://184.150.226.173/e1RestServer/item/6F2B4DF6-CD54-4A7F-8851-220490ECF27E.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
		|            },
		|            {
		|                "shortDescription": "NC4-MX",
		|                "description": "Normal Butane - Component in Mix",
		|                "id": "c6dc7f65-0cbc-430b-80bd-ab3f5ddd6079",
		|                "ref": "http://184.150.226.173/e1RestServer/item/c6dc7f65-0cbc-430b-80bd-ab3f5ddd6079.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
		|            },
		|            {
		|                "shortDescription": "Normal-Butane",
		|                "description": "Normal-Butane Hierarchy",
		|                "id": "d98f67fb-3006-47b1-b0f2-56590f893207",
		|                "ref": "http://184.150.226.173/e1RestServer/item/d98f67fb-3006-47b1-b0f2-56590f893207.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
		|            }
		|        ],
		|        "id": "7B4AA572-0A8B-48BE-AC59-653932809915",
		|        "ref": "http://184.150.226.173/e1RestServer/itemgroup/7B4AA572-0A8B-48BE-AC59-653932809915.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
		|   },
		|	{
        |		"code": "RGP",
        |		"description": "Raw Gas",
        |		"items": [
        |	   		{
        |        		"shortDescription": "Acid Gas",
        |        		"description": "Acid Gas",
        |        		"id": "1036e72a-5da5-449d-86b9-dc5cd8e5e32f",
        |        		"ref": "http://184.150.226.173/e1RestServer/item/1036e72a-5da5-449d-86b9-dc5cd8e5e32f.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
        |    		},
        |    		{
        |        		"shortDescription": "Raw Gas",
        |        		"description": "Raw Gas",
        |        		"id": "BE9DB849-A661-43A4-A503-70A9DEEE8BD9",
        |        		"ref": "http://184.150.226.173/e1RestServer/item/BE9DB849-A661-43A4-A503-70A9DEEE8BD9.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
        |    		}
        |		],
        |		"id": "2F226DE1-F285-4612-AEFB-7C17DCE2F852",
        |		"ref": "http://184.150.226.173/e1RestServer/itemgroup/2F226DE1-F285-4612-AEFB-7C17DCE2F852.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
    	|	},
    	|	{
        |		"code": "WP",
        |		"description": "Water",
        |		"items": [
        |    		{
        |        		"shortDescription": "Steam",
        |        		"description": "Steam",
        |        		"id": "cc243a14-79c3-4ccd-9b22-2a15a96ff525",
        |        		"ref": "http://184.150.226.173/e1RestServer/item/cc243a14-79c3-4ccd-9b22-2a15a96ff525.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
        |    		},
        |    		{
        |        		"shortDescription": "Water",
        |        		"description": "Water",
        |        		"id": "4be23eaa-6c32-4b87-9442-57f39df0ca51",
        |        		"ref": "http://184.150.226.173/e1RestServer/item/4be23eaa-6c32-4b87-9442-57f39df0ca51.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
        |    		},
        |    		{
        |        		"shortDescription": "Water - Fresh",
        |        		"description": "Water - Fresh",
        |        		"id": "d0c38fdd-a70a-4fdb-a905-e8ad11029c2b",
        |        		"ref": "http://184.150.226.173/e1RestServer/item/d0c38fdd-a70a-4fdb-a905-e8ad11029c2b.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
        |    		}
        |		],
        |		"id": "4C7804BE-3D84-4A8F-9B44-49CD26B56E15",
        |		"ref": "http://184.150.226.173/e1RestServer/itemgroup/4C7804BE-3D84-4A8F-9B44-49CD26B56E15.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
    	|	},
		|	{
        |		"code": "VES",
        |		"description": "Vessels",
        |		"items": [],
        |		"id": "8A0D67BC-4729-4A1B-A7E5-049C546B6978",
        |		"ref": "http://184.150.226.173/e1RestServer/itemgroup/8A0D67BC-4729-4A1B-A7E5-049C546B6978.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
    	|	}
    	|]""".stripMargin)

	val NUM_OF_ITEMGROUPS = 9
	val NUM_OF_ITEMS = 22

    val fakeBargeDetailsJsonResponse = mapper.readTree(
    """ |[
            {
        |        "shortDescription": "Barge",
        |        "description": "Barge",
        |        "code": "BARGE",
        |        "auditInfo": {
        |            "createUser": "QA_MARK",
        |            "createDate": "Jun 6, 2007 06:21:20 PM",
        |            "updateUser": "steveremmington",
        |            "updateDate": "Feb 19, 2011 01:38:57 PM"
        |        },
        |        "attributes": [
        |            {
        |                "item": {
        |                    "shortDescription": "Barge",
        |                    "description": "Barge",
        |                    "locationControl": false,
        |                    "odorized": false,
        |                    "carryInventoryQuality": false,
        |                    "rinRequired": false,
        |                    "qtyByComponent": false,
        |                    "id": "8FEA70D9-9D21-4012-83E9-98FB6FA577DE",
        |                    "ref": "http://172.24.0.103:80/api/item/8FEA70D9-9D21-4012-83E9-98FB6FA577DE?token=ae208a86-a32c-4c47-b19d-37481be5f718&format=json"
        |                },
        |                "attribute": {
        |                    "code": "CAPACITY",
        |                    "description": "Capacity",
        |                    "type": "LOGISTICS",
        |                    "dataType": "NUMBER",
        |                    "id": "4BE8281B4B9A4E75B7F0AA871F74EA9E",
        |                    "ref": "http://172.24.0.103:80/api/attribute/4BE8281B4B9A4E75B7F0AA871F74EA9E?token=ae208a86-a32c-4c47-b19d-37481be5f718&format=json"
        |                },
        |                "requirementRule": "REQUIRED_WARNING",
        |                "unitOfMeasure": {
        |                    "code": "UNDEF",
        |                    "shortDescription": "UNDEF",
        |                    "description": "Undefined",
        |                    "id": "82B632D1-7A78-409F-836E-C7351CBD0DFC",
        |                    "ref": "http://172.24.0.103:80/api/unitofmeasure/82B632D1-7A78-409F-836E-C7351CBD0DFC?token=ae208a86-a32c-4c47-b19d-37481be5f718&format=json"
        |                },
        |                "id": "36c72f5c-2d97-4951-85d5-82b5e3759762",
        |                "ref": "http://172.24.0.103:80/api/itemattribute/36c72f5c-2d97-4951-85d5-82b5e3759762?token=ae208a86-a32c-4c47-b19d-37481be5f718&format=json"
        |            }
        |        ],
        |        "type": {
        |            "description": "Plant Equipment",
        |            "inventoryBalance": false,
        |            "id": "AC684149-A2FB-430F-9A59-FCCD199AF2C5",
        |            "ref": "http://172.24.0.103:80/api/itemtype/AC684149-A2FB-430F-9A59-FCCD199AF2C5?token=ae208a86-a32c-4c47-b19d-37481be5f718&format=json"
        |        },
        |        "itemGroup": {
        |            "code": "BAR",
        |            "description": "Barges",
        |            "id": "CD1FA9E4-AD19-4CA9-AE02-55ECB74627BA",
        |            "ref": "http://172.24.0.103:80/api/itemgroup/CD1FA9E4-AD19-4CA9-AE02-55ECB74627BA?token=ae208a86-a32c-4c47-b19d-37481be5f718&format=json"
        |        },
        |        "expiryDate": "Jan 1, 3000 12:00:00 AM",
        |        "locationControl": false,
        |        "tempCorrectionFactor": 1,
        |        "odorized": false,
        |        "displayOrder": 1,
        |        "carryInventoryQuality": false,
        |        "rinRequired": false,
        |        "qtyByComponent": false,
        |        "allocationByComponent": "ALWAYS",
        |        "id": "8FEA70D9-9D21-4012-83E9-98FB6FA577DE",
        |        "ref": "http://172.24.0.103:80/api/item/8FEA70D9-9D21-4012-83E9-98FB6FA577DE?token=ae208a86-a32c-4c47-b19d-37481be5f718&format=json"
        |    }
        |]""".stripMargin)

    val fakeItemAttributeJsonResponse = mapper.readTree(    // A fake json response containing ONE item containing TWO attributes
    """ |[
        |    {
        |        "item": {
        |            "shortDescription": "Barge",
        |            "description": "Barge",
        |            "locationControl": false,
        |            "odorized": false,
        |            "carryInventoryQuality": false,
        |            "rinRequired": false,
        |            "qtyByComponent": false,
        |            "id": "8FEA70D9-9D21-4012-83E9-98FB6FA577DE",
        |            "ref": "http://172.24.0.103:80/api/item/8FEA70D9-9D21-4012-83E9-98FB6FA577DE?token=8e62bd72-aca3-4c08-8397-03ecef95de46&format=json"
        |        },
        |        "attribute": {
        |            "code": "CAPACITY",
        |            "description": "Capacity",
        |            "type": "LOGISTICS",
        |            "dataType": "NUMBER",
        |            "id": "4BE8281B4B9A4E75B7F0AA871F74EA9E",
        |            "ref": "http://172.24.0.103:80/api/attribute/4BE8281B4B9A4E75B7F0AA871F74EA9E?token=8e62bd72-aca3-4c08-8397-03ecef95de46&format=json"
        |        },
        |        "requirementRule": "REQUIRED_WARNING",
        |        "unitOfMeasure": {
        |            "code": "UNDEF",
        |            "shortDescription": "UNDEF",
        |            "description": "Undefined",
        |            "id": "82B632D1-7A78-409F-836E-C7351CBD0DFC",
        |            "ref": "http://172.24.0.103:80/api/unitofmeasure/82B632D1-7A78-409F-836E-C7351CBD0DFC?token=8e62bd72-aca3-4c08-8397-03ecef95de46&format=json"
        |        },
        |        "id": "36c72f5c-2d97-4951-85d5-82b5e3759762",
        |        "ref": "http://172.24.0.103:80/api/itemattribute/36c72f5c-2d97-4951-85d5-82b5e3759762?token=8e62bd72-aca3-4c08-8397-03ecef95de46&format=json"
        |    }
        |]""".stripMargin)

    val fakeItemTypeJsonResponse = mapper.readTree(     // A fake JSON response containing all the item types
    """ |[
        |    {
        |        "description": "Undefined",
        |        "auditInfo": {
        |            "createUser": "INSTALL",
        |            "createDate": "Jan 1, 2005 12:00:00 AM",
        |            "updateUser": "INSTALL",
        |            "updateDate": "Jan 1, 2005 12:00:00 AM"
        |        },
        |        "itemTypeCode": "UN",
        |        "inventoryBalance": true,
        |        "itemCategory": "UNDEFINED",
        |        "id": "74422B84-E82F-4B3D-9F73-1DEDA5A3447D",
        |        "ref": "http://172.24.0.103:80/api/itemtype/74422B84-E82F-4B3D-9F73-1DEDA5A3447D?token=6d3985b3-809e-4738-97ec-c0a59ffb1460&format=json"
        |    },
        |    {
        |        "description": "Composite Product",
        |        "auditInfo": {
        |            "createUser": "INSTALL",
        |            "createDate": "Jan 1, 2005 12:00:00 AM",
        |            "updateUser": "INSTALL",
        |            "updateDate": "Jan 1, 2005 12:00:00 AM"
        |        },
        |        "itemTypeCode": "CP",
        |        "inventoryBalance": true,
        |        "itemCategory": "PRODUCT",
        |        "id": "F5A2145D-6CF6-4053-9D26-302B61747441",
        |        "ref": "http://172.24.0.103:80/api/itemtype/F5A2145D-6CF6-4053-9D26-302B61747441?token=6d3985b3-809e-4738-97ec-c0a59ffb1460&format=json"
        |    },
        |    {
        |        "description": "Product",
        |        "auditInfo": {
        |            "createUser": "INSTALL",
        |            "createDate": "Jan 1, 2005 12:00:00 AM",
        |            "updateUser": "INSTALL",
        |            "updateDate": "Jan 1, 2005 12:00:00 AM"
        |        },
        |        "itemTypeCode": "PD",
        |        "inventoryBalance": true,
        |        "itemCategory": "PRODUCT",
        |        "id": "666A9009-82E2-403C-A60B-33FE451DD6EE",
        |        "ref": "http://172.24.0.103:80/api/itemtype/666A9009-82E2-403C-A60B-33FE451DD6EE?token=6d3985b3-809e-4738-97ec-c0a59ffb1460&format=json"
        |    },
        |    {
        |        "description": "Meter Formula",
        |        "auditInfo": {
        |            "createUser": "BASE_V7",
        |            "createDate": "Jun 5, 2008 04:36:16 PM",
        |            "updateUser": "BASE_V7",
        |            "updateDate": "Jun 5, 2008 04:36:16 PM"
        |        },
        |        "itemTypeCode": "METER_FORMULA",
        |        "inventoryBalance": false,
        |        "itemCategory": "FIELD_EQUIPMENT",
        |        "id": "92C7C6EE94904080846CAFC3BE992576",
        |        "ref": "http://172.24.0.103:80/api/itemtype/92C7C6EE94904080846CAFC3BE992576?token=6d3985b3-809e-4738-97ec-c0a59ffb1460&format=json"
        |    },
        |    {
        |        "description": "Truck Equipment",
        |        "auditInfo": {
        |            "createUser": "ENTERO",
        |            "createDate": "Oct 10, 2008 08:51:10 AM",
        |            "updateUser": "ENTERO",
        |            "updateDate": "Oct 10, 2008 08:51:10 AM"
        |        },
        |        "itemTypeCode": "TRUCK",
        |        "inventoryBalance": false,
        |        "itemCategory": "LOGISTICS",
        |        "id": "75A16CE1-DB52-42F3-B0A0-AF48F5DCF9EC",
        |        "ref": "http://172.24.0.103:80/api/itemtype/75A16CE1-DB52-42F3-B0A0-AF48F5DCF9EC?token=6d3985b3-809e-4738-97ec-c0a59ffb1460&format=json"
        |    },
        |    {
        |        "description": "Barge",
        |        "auditInfo": {
        |            "createUser": "ENTERO",
        |            "createDate": "Oct 10, 2008 08:51:10 AM",
        |            "updateUser": "ENTERO",
        |            "updateDate": "Oct 10, 2008 08:51:10 AM"
        |        },
        |        "itemTypeCode": "BARGE",
        |        "inventoryBalance": false,
        |        "itemCategory": "LOGISTICS",
        |        "id": "F99D3011-E83B-4F23-90DF-0F2C8AC07000",
        |        "ref": "http://172.24.0.103:80/api/itemtype/F99D3011-E83B-4F23-90DF-0F2C8AC07000?token=6d3985b3-809e-4738-97ec-c0a59ffb1460&format=json"
        |    },
        |    {
        |        "description": "Machinery",
        |        "auditInfo": {
        |            "createUser": "ENTERO",
        |            "createDate": "Oct 10, 2008 08:51:11 AM",
        |            "updateUser": "ENTERO",
        |            "updateDate": "Oct 10, 2008 08:51:11 AM"
        |        },
        |        "itemTypeCode": "MACHINERY",
        |        "inventoryBalance": false,
        |        "itemCategory": "PLANT_EQUIPMENT",
        |        "id": "9185CBAD-79E7-4B56-936B-C06307040E49",
        |        "ref": "http://172.24.0.103:80/api/itemtype/9185CBAD-79E7-4B56-936B-C06307040E49?token=6d3985b3-809e-4738-97ec-c0a59ffb1460&format=json"
        |    },
        |    {
        |        "description": "Meter Backcalculated",
        |        "auditInfo": {
        |            "createUser": "BASE_V7",
        |            "createDate": "Oct 10, 2008 08:52:21 AM",
        |            "updateUser": "BASE_V7",
        |            "updateDate": "Oct 10, 2008 08:52:21 AM"
        |        },
        |        "itemTypeCode": "METER_BACKCALCULATED",
        |        "inventoryBalance": false,
        |        "itemCategory": "FIELD_EQUIPMENT",
        |        "id": "43F6BB65BE8049CAA9016DA7C9B3AA5F",
        |        "ref": "http://172.24.0.103:80/api/itemtype/43F6BB65BE8049CAA9016DA7C9B3AA5F?token=6d3985b3-809e-4738-97ec-c0a59ffb1460&format=json"
        |    },
        |    {
        |        "description": "Physical Inventory Group",
        |        "auditInfo": {
        |            "createUser": "INSTALL",
        |            "createDate": "Sep 6, 2013 06:48:16 AM",
        |            "updateUser": "INSTALL",
        |            "updateDate": "Sep 6, 2013 06:48:16 AM"
        |        },
        |        "itemTypeCode": "PHYSICAL_INVENTORY_GROUP",
        |        "inventoryBalance": false,
        |        "itemCategory": "FIELD_EQUIPMENT",
        |        "id": "E11347F4-DD38-466E-905A-132593A08AA8",
        |        "ref": "http://172.24.0.103:80/api/itemtype/E11347F4-DD38-466E-905A-132593A08AA8?token=6d3985b3-809e-4738-97ec-c0a59ffb1460&format=json"
        |    }
        |]""".stripMargin)


    it should "return the Price workspace's name" in {
        running(FakeApplication()){

            val itemWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.ITEMS)
            assert(itemWS.getWSName() == "Item")
        }
    }

	it should "return a list if searching all ItemGroups with no item parameters specified" in withRestClient { (mockClient, context) =>
		running(FakeApplication()){

			// This uses ScalaTest's JMockCycle to wrap the Mockery object
			val cycle = new JMockCycle
			import cycle._

			// Set the context of the mock
			context.checking(
				new Expectations() {
                    // Mock client.setHolder()
                    allowing(mockClient).setHolder("/itemGroup")
                    allowing(mockClient).setHolder("/item")
					
					// Query /itemGroup endpoint to grab all itemGroup ID's
					exactly(2).of(mockClient).getResponseAsJson()	     //NOTE: You need to keep track the exact amount of times the methods
						will(returnValue(fakeItemGroupJsonResponse))	 //  	are called in BOTH THE TESTS AND INTERNAL SYSTEM METHODS

                    // Query /item endpoint to grab all items
                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeDetailedItemsJsonResponse))

                    // Adds selected itemGroups to a list to be filtered
                    // locallly
                    oneOf(mockClient).setFlag("itemGroup", "2F226DE1-F285-4612-AEFB-7C17DCE2F852")
                    oneOf(mockClient).setFlag("itemGroup", "6DB23A00-89F9-4907-8278-C6E69EFF51C3")
                    oneOf(mockClient).setFlag("itemGroup", "C3FAC269-BC54-47BC-89BD-50800B5E6D22")
                    oneOf(mockClient).setFlag("itemGroup", "CD1FA9E4-AD19-4CA9-AE02-55ECB74627BA")
                    oneOf(mockClient).setFlag("itemGroup", "870D9337-4C25-48B2-8329-ACE788C8A01B")
                    oneOf(mockClient).setFlag("itemGroup", "7B4AA572-0A8B-48BE-AC59-653932809915")
                    oneOf(mockClient).setFlag("itemGroup", "8B4AA572-0A8B-48BE-AC59-653932809915")
                    oneOf(mockClient).setFlag("itemGroup", "4C7804BE-3D84-4A8F-9B44-49CD26B56E15")
                    oneOf(mockClient).setFlag("itemGroup", "8A0D67BC-4729-4A1B-A7E5-049C546B6978")

                    oneOf(mockClient).setFlag("offset", "0")
                    oneOf(mockClient).setFlag("max", maxOffset)

                    oneOf (mockClient).setFlag("detail", "itemGroup")
                    oneOf (mockClient).setFlag("detail", "type")
				}
			)

			// Test the system:
			val itemWS = WorkspaceFactory.createWorkspace(Endpoint.ITEMS)
			itemWS.setClient(mockClient)

            val offset = 0
			val itemGroups: java.util.List[String] = itemWS.getItemGroups()
			itemWS.searchItems(offset, "", itemGroups, "")
			val itemTable = itemWS.getTable()
		
			// Expect a list of EnteroObject's
            assert(itemTable.size() == fakeDetailedItemsJsonResponseSize)
            assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMTYPE.columnName())=="Product")
            assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMGROUPID.columnName())=="7B4AA572-0A8B-48BE-AC59-653932809915")
            assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMLONGDESC.columnName())=="Butane")
            assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMCODE.columnName())=="C4")
            assert(itemTable.get(0).getProperty(ItemIdentifier.ID.columnName())=="1d0e13eb-b1f4-463d-9074-4a17c2396de7")
            assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMSHORTDESC.columnName())=="C4")
            assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMTYPEID.columnName())=="666A9009-82E2-403C-A60B-33FE451DD6EE")
            assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMGROUPDESCRIPTION.columnName())=="Butanes")
		}
	}
	
	it should "return a list when searching by item description" in withRestClient { (mockClient, context) =>
		running(FakeApplication()){

			// This uses ScalaTest's JMockCycle to wrap the Mockery object
			val cycle = new JMockCycle
			import cycle._

			// Set the context of the mock
			context.checking(
				new Expectations() {
					// mock client.setHolder(string)
					allowing(mockClient).setHolder("/itemGroup")
                    allowing(mockClient).setHolder("/item")
					
					// Query /itemGroup endpoint to grab all itemGroup ID's
                    exactly(2).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeItemGroupJsonResponse))

                    // Query /item endpoint with &description=Barge
                    oneOf (mockClient).setFlag("description", "Barge")

					exactly(1).of(mockClient).getResponseAsJson()
						will(returnValue(fakeBargeDetailsJsonResponse))

                    // Adds request for itemGroups selected (which is all in this case)
                    oneOf(mockClient).setFlag("itemGroup", "2F226DE1-F285-4612-AEFB-7C17DCE2F852")
                    oneOf(mockClient).setFlag("itemGroup", "6DB23A00-89F9-4907-8278-C6E69EFF51C3")
                    oneOf(mockClient).setFlag("itemGroup", "C3FAC269-BC54-47BC-89BD-50800B5E6D22")
                    oneOf(mockClient).setFlag("itemGroup", "CD1FA9E4-AD19-4CA9-AE02-55ECB74627BA")
                    oneOf(mockClient).setFlag("itemGroup", "870D9337-4C25-48B2-8329-ACE788C8A01B")
                    oneOf(mockClient).setFlag("itemGroup", "7B4AA572-0A8B-48BE-AC59-653932809915")
                    oneOf(mockClient).setFlag("itemGroup", "8B4AA572-0A8B-48BE-AC59-653932809915")
                    oneOf(mockClient).setFlag("itemGroup", "4C7804BE-3D84-4A8F-9B44-49CD26B56E15")
                    oneOf(mockClient).setFlag("itemGroup", "8A0D67BC-4729-4A1B-A7E5-049C546B6978")

                    oneOf(mockClient).setFlag("offset", "0")
                    oneOf(mockClient).setFlag("max", maxOffset)

                    oneOf (mockClient).setFlag("detail", "itemGroup")
                    oneOf (mockClient).setFlag("detail", "type")
				}
			)

			// Test the system:
			val itemWS = WorkspaceFactory.createWorkspace(Endpoint.ITEMS)
			itemWS.setClient(mockClient)

            val offset = 0
			val itemGroups: java.util.List[String] = itemWS.getItemGroups()
			itemWS.searchItems(offset, "", itemGroups, "Barge")
			val itemTable = itemWS.getTable()
		
            //println(itemTable.get(0).getPropertyMap())
			assert(itemTable.size() == 1)
            assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMTYPE.columnName())=="Plant Equipment")
            assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMGROUPID.columnName())=="CD1FA9E4-AD19-4CA9-AE02-55ECB74627BA")
            assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMLONGDESC.columnName())=="Barge")
            assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMCODE.columnName())=="BAR")
            assert(itemTable.get(0).getProperty(ItemIdentifier.ID.columnName())=="8FEA70D9-9D21-4012-83E9-98FB6FA577DE")
            assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMSHORTDESC.columnName())=="Barge")
            assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMTYPEID.columnName())=="AC684149-A2FB-430F-9A59-FCCD199AF2C5")
            assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMGROUPDESCRIPTION.columnName())=="Barges")
		}
	}
	
	it should "return an empty list when searching for a non-existing item description" in withRestClient { (mockClient, context) =>
		running(FakeApplication()){

            // This uses ScalaTest's JMockCycle to wrap the Mockery object
            val cycle = new JMockCycle
            import cycle._

            // Set the context of the mock
            context.checking(
                new Expectations() {
                    // mock client.setHolder(string)
                    allowing(mockClient).setHolder("/itemGroup")
                    allowing(mockClient).setHolder("/item")
                    
                    // Query /itemGroup endpoint to grab all itemGroup ID's
                    exactly(2).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeItemGroupJsonResponse))

                    // Query /item endpoint with &description=Barge
                    oneOf (mockClient).setFlag("description", "NON-EXISTING ITEM")

                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(mapper.readTree("[]")))

                    // Adds request for itemGroups selected (which is all in this case)
                    oneOf(mockClient).setFlag("itemGroup", "2F226DE1-F285-4612-AEFB-7C17DCE2F852")
                    oneOf(mockClient).setFlag("itemGroup", "6DB23A00-89F9-4907-8278-C6E69EFF51C3")
                    oneOf(mockClient).setFlag("itemGroup", "C3FAC269-BC54-47BC-89BD-50800B5E6D22")
                    oneOf(mockClient).setFlag("itemGroup", "CD1FA9E4-AD19-4CA9-AE02-55ECB74627BA")
                    oneOf(mockClient).setFlag("itemGroup", "870D9337-4C25-48B2-8329-ACE788C8A01B")
                    oneOf(mockClient).setFlag("itemGroup", "7B4AA572-0A8B-48BE-AC59-653932809915")
                    oneOf(mockClient).setFlag("itemGroup", "8B4AA572-0A8B-48BE-AC59-653932809915")
                    oneOf(mockClient).setFlag("itemGroup", "4C7804BE-3D84-4A8F-9B44-49CD26B56E15")
                    oneOf(mockClient).setFlag("itemGroup", "8A0D67BC-4729-4A1B-A7E5-049C546B6978")

                    oneOf(mockClient).setFlag("offset", "0")
                    oneOf(mockClient).setFlag("max", maxOffset)

                    oneOf (mockClient).setFlag("detail", "itemGroup")
                    oneOf (mockClient).setFlag("detail", "type")
                }
            )

            // Test the system:
            val itemWS = WorkspaceFactory.createWorkspace(Endpoint.ITEMS)
            itemWS.setClient(mockClient)

            val offset = 0
            val itemGroups: java.util.List[String] = itemWS.getItemGroups()
            itemWS.searchItems(offset, "", itemGroups, "NON-EXISTING ITEM")
            val itemTable = itemWS.getTable()
        
            assert(itemTable.size() == 0)
        }
	}
	
	it should "return a list when searching in one itemGroup" in withRestClient { (mockClient, context) =>
		running(FakeApplication()){
			val cycle = new JMockCycle   // Use ScalaTest's JMockCycle to wrap the Mockery object
			import cycle._

			context.checking(            // Set the context of the mock
				new Expectations() {
                    allowing(mockClient).setHolder("/itemGroup")
                    allowing(mockClient).setHolder("/item")

                    // Query /itemGroup endpoint to grab all itemGroup ID's
                    exactly(2).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeItemGroupJsonResponse))
                    
                    // Adds request for itemGroups selected
                    oneOf(mockClient).setFlag("itemGroup", "CD1FA9E4-AD19-4CA9-AE02-55ECB74627BA")

                    oneOf(mockClient).setFlag("offset", "0")
                    oneOf(mockClient).setFlag("max", maxOffset)

                    oneOf (mockClient).setFlag("detail", "itemGroup")
                    oneOf (mockClient).setFlag("detail", "type")
				}
			)

			// Test the system:
			val itemWS = WorkspaceFactory.createWorkspace(Endpoint.ITEMS)
			itemWS.setClient(mockClient)
			
			// Search for items in the "Ammonia" itemGroup
            val offset = 0
			val itemGroup: java.util.List[String] = ArrayBuffer("Barges")
			itemWS.searchItems(offset, "", itemGroup, "")
			val itemTable = itemWS.getTable()
			
			//assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMLONGDESC.columnName()) == "Anhydrous Ammonia") 
            //assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMSHORTDESC.columnName())=="Ammonia")
		}
	}
	
	it should "return a list when searching in multiple itemGroups" in withRestClient { (mockClient, context) =>
		running(FakeApplication()){	
			val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
			import cycle._

			// Set the context of the mock
			context.checking(
				new Expectations() {
					allowing(mockClient).setHolder("/itemGroup")
                    allowing(mockClient).setHolder("/item")

                    // Query /itemGroup endpoint to grab all itemGroup ID's
                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeItemGroupJsonResponse))

                    // Adds request for itemGroups selected
                    oneOf(mockClient).setFlag("itemGroup", "CD1FA9E4-AD19-4CA9-AE02-55ECB74627BA")
                    oneOf(mockClient).setFlag("itemGroup", "7B4AA572-0A8B-48BE-AC59-653932809915")

                    oneOf(mockClient).setFlag("offset", "0")
                    oneOf(mockClient).setFlag("max", maxOffset)

                    oneOf (mockClient).setFlag("detail", "itemGroup")
                    oneOf (mockClient).setFlag("detail", "type")

                    // Query /item endpoint to grab the items in the selected itemsGroup
                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeBargeDetailsJsonResponse))
				}
			)

			// Test the system:
			val itemWS = WorkspaceFactory.createWorkspace(Endpoint.ITEMS)
			itemWS.setClient(mockClient)
			
			// Expect that items from two itemGroups are returned
            val offset = 0
			val itemGroup: java.util.List[String] = ArrayBuffer("Barges", "Butanes")
			itemWS.searchItems(offset, "", itemGroup, "") 
			val itemTable = itemWS.getTable()
			
			//println(itemTable.get(0).getPropertyMap())
            assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMTYPE.columnName())=="Plant Equipment")
            assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMGROUPID.columnName())=="CD1FA9E4-AD19-4CA9-AE02-55ECB74627BA")
            assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMLONGDESC.columnName())=="Barge")
            assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMCODE.columnName())=="BAR")
            assert(itemTable.get(0).getProperty(ItemIdentifier.ID.columnName())=="8FEA70D9-9D21-4012-83E9-98FB6FA577DE")
            assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMSHORTDESC.columnName())=="Barge")
            assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMTYPEID.columnName())=="AC684149-A2FB-430F-9A59-FCCD199AF2C5")
            assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMGROUPDESCRIPTION.columnName())=="Barges")
		}
	}
	
	it should "return an empty list when searching with no itemGroup selected" in withRestClient { (mockClient, context) =>
		running(FakeApplication()) {
			
			val cycle = new JMockCycle
			import cycle._

			// Set the context of the mock
			context.checking(
				new Expectations() {
					allowing(mockClient).setHolder("/itemGroup")
                    allowing(mockClient).setHolder("/item")

                    // Query /itemGroup endpoint to grab all itemGroup ID's
                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeItemGroupJsonResponse))

                    oneOf(mockClient).setFlag("offset", "0")
                    oneOf(mockClient).setFlag("max", maxOffset)

                    oneOf (mockClient).setFlag("detail", "itemGroup")
                    oneOf (mockClient).setFlag("detail", "type")
				}
			)

			// Test the system:
			val itemWS = WorkspaceFactory.createWorkspace(Endpoint.ITEMS)
			itemWS.setClient(mockClient)

            val offset = 0
			val emptyList: java.util.List[String] = ArrayBuffer("")
			itemWS.searchItems(offset, "", emptyList,"")
			val itemTable = itemWS.getTable()
		
			assert(itemTable.size() == 0)
		}
	}

    it should "return a list when searching by ItemType" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){
            val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
            import cycle._

            // Set the context of the mock
            context.checking(
                new Expectations() {
                    allowing(mockClient).setHolder("/itemGroup")                   
                    exactly(1).of(mockClient).getResponseAsJson()   
                        will(returnValue(fakeItemGroupJsonResponse))

                    allowing(mockClient).setHolder("/itemType")
                    exactly(1).of(mockClient).getResponseAsJson()   
                        will(returnValue(fakeItemTypeJsonResponse)) 

                    exactly(1).of(mockClient).getResponseAsJson()   
                        will(returnValue(fakeItemGroupJsonResponse))

                    allowing(mockClient).setHolder("/item")

                    // Adds request for itemGroups selected (which is all in this case)
                    oneOf(mockClient).setFlag("itemGroup", "2F226DE1-F285-4612-AEFB-7C17DCE2F852")
                    oneOf(mockClient).setFlag("itemGroup", "6DB23A00-89F9-4907-8278-C6E69EFF51C3")
                    oneOf(mockClient).setFlag("itemGroup", "C3FAC269-BC54-47BC-89BD-50800B5E6D22")
                    oneOf(mockClient).setFlag("itemGroup", "CD1FA9E4-AD19-4CA9-AE02-55ECB74627BA")
                    oneOf(mockClient).setFlag("itemGroup", "870D9337-4C25-48B2-8329-ACE788C8A01B")
                    oneOf(mockClient).setFlag("itemGroup", "7B4AA572-0A8B-48BE-AC59-653932809915")
                    oneOf(mockClient).setFlag("itemGroup", "8B4AA572-0A8B-48BE-AC59-653932809915")
                    oneOf(mockClient).setFlag("itemGroup", "4C7804BE-3D84-4A8F-9B44-49CD26B56E15")
                    oneOf(mockClient).setFlag("itemGroup", "8A0D67BC-4729-4A1B-A7E5-049C546B6978")

                    // Adds request for the specified itemType
                    oneOf(mockClient).setFlag("itemType", "92C7C6EE94904080846CAFC3BE992576")

                    oneOf(mockClient).setFlag("offset", "0")
                    oneOf(mockClient).setFlag("max", maxOffset)

                    oneOf(mockClient).setFlag("detail", "itemGroup")
                    oneOf(mockClient).setFlag("detail", "type")

                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeBargeDetailsJsonResponse))
                }
            )

            // Test the system:
            val itemWS = WorkspaceFactory.createWorkspace(Endpoint.ITEMS)
            itemWS.setClient(mockClient)

            val offset = 0
            val itemGroups: java.util.List[String] = itemWS.getItemGroups()
            itemWS.searchItems(offset, "Meter Formula", itemGroups, "")
            val itemTable = itemWS.getTable()
        
            assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMTYPE.columnName())=="Plant Equipment")
            assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMGROUPID.columnName())=="CD1FA9E4-AD19-4CA9-AE02-55ECB74627BA")
            assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMLONGDESC.columnName())=="Barge")
            assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMCODE.columnName())=="BAR")
            assert(itemTable.get(0).getProperty(ItemIdentifier.ID.columnName())=="8FEA70D9-9D21-4012-83E9-98FB6FA577DE")
            assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMSHORTDESC.columnName())=="Barge")
            assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMTYPEID.columnName())=="AC684149-A2FB-430F-9A59-FCCD199AF2C5")
            assert(itemTable.get(0).getProperty(ItemIdentifier.ITEMGROUPDESCRIPTION.columnName())=="Barges")
        }
    }

    it should "return detailed information for the specified item \"Barge\"" in withRestClient { (mockClient, context) =>
        running(FakeApplication()) {
            val cycle = new JMockCycle
            import cycle._

            context.checking(
                new Expectations() {
                    // mock client.setHolder(string)
                    allowing(mockClient).setHolder("/itemGroup")
                    allowing(mockClient).setHolder("/item")
                    
                    // Mock client.getResponseAsJson()
                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeItemGroupJsonResponse))

                    oneOf (mockClient).setFlag("id", "8FEA70D9-9D21-4012-83E9-98FB6FA577DE")
                    oneOf (mockClient).setFlag("detail", "")

                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeBargeDetailsJsonResponse))
                }
            )

            // Test the system:
            val itemWS = WorkspaceFactory.createWorkspace(Endpoint.ITEMS)
            itemWS.setClient(mockClient)

            // We need to call .getItemGroups() in order for Items.java to initialize JsonResponseHandler....
            val itemGroups: java.util.List[String] = itemWS.getItemGroups()

            val bargeDetails = itemWS.filterDetails("8FEA70D9-9D21-4012-83E9-98FB6FA577DE")
        
            assert(bargeDetails.getProperty(ItemIdentifier.ITEMCODE.columnName())=="BAR")
            assert(bargeDetails.getProperty(ItemIdentifier.ITEMGROUPDESCRIPTION.columnName())=="Barges")
            assert(bargeDetails.getProperty(ItemIdentifier.ITEMLONGDESC.columnName())=="Barge")
            assert(bargeDetails.getProperty(ItemIdentifier.ITEMSHORTDESC.columnName())=="Barge")
        }
    }
	
	/*it should "sort the Items table by columns alphabetically in both A-Z and Z-A" in withRestClient { (mockClient, context) =>
		running(FakeApplication()){
			// This uses ScalaTest's JMockCycle to wrap the Mockery object
			val cycle = new JMockCycle
			import cycle._

			// Set the context of the mock
			context.checking(
				new Expectations() {
                    // Mock client.setHolder()
                    allowing(mockClient).setHolder("/itemGroup")
                    allowing(mockClient).setHolder("/item")
                    
                    // Query /itemGroup endpoint to grab all itemGroup ID's
                    exactly(2).of(mockClient).getResponseAsJson()        
                        will(returnValue(fakeItemGroupJsonResponse))    

                    // Query /item endpoint to grab all items
                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeDetailedItemsJsonResponse))

                    // Adds selected itemGroups to a list to be filtered
                    // locallly
                    oneOf(mockClient).setFlag("itemGroup", "2F226DE1-F285-4612-AEFB-7C17DCE2F852")
                    oneOf(mockClient).setFlag("itemGroup", "6DB23A00-89F9-4907-8278-C6E69EFF51C3")
                    oneOf(mockClient).setFlag("itemGroup", "C3FAC269-BC54-47BC-89BD-50800B5E6D22")
                    oneOf(mockClient).setFlag("itemGroup", "CD1FA9E4-AD19-4CA9-AE02-55ECB74627BA")
                    oneOf(mockClient).setFlag("itemGroup", "870D9337-4C25-48B2-8329-ACE788C8A01B")
                    oneOf(mockClient).setFlag("itemGroup", "7B4AA572-0A8B-48BE-AC59-653932809915")
                    oneOf(mockClient).setFlag("itemGroup", "8B4AA572-0A8B-48BE-AC59-653932809915")
                    oneOf(mockClient).setFlag("itemGroup", "4C7804BE-3D84-4A8F-9B44-49CD26B56E15")
                    oneOf(mockClient).setFlag("itemGroup", "8A0D67BC-4729-4A1B-A7E5-049C546B6978")

                    oneOf(mockClient).setFlag("offset", "0")
                    oneOf(mockClient).setFlag("max", "100")

                    oneOf (mockClient).setFlag("detail", "itemGroup")
                    oneOf (mockClient).setFlag("detail", "type")
				}
			)

			// Test the system:
			val itemWS = WorkspaceFactory.createWorkspace(Endpoint.ITEMS)
			itemWS.setClient(mockClient)

            val offset = 0
			val itemGroups: java.util.List[String] = itemWS.getItemGroups()
			itemWS.searchItems(offset, "", itemGroups, "")
			val itemTable = itemWS.getTable()
			itemWS.sort("Item code")	// This should sort the table A-Z
println(itemTable.get(0).getPropertyMap())
			var asciiChar = ((itemTable.get(0).getProperty("Item code")).charAt(0)).toInt
			assert(((asciiChar>=65)&&(asciiChar<=68)) 			//We check the sorted table starts between A-D
					|| ((asciiChar>=97)&&(asciiChar<=100)))

			itemWS.sort("Item code")	// This should sort the table Z-A

			asciiChar = ((itemTable.get(0).getProperty("Item code")).charAt(0)).toInt
			assert(((asciiChar>=82)&&(asciiChar<=90)) 			//We check the sorted table starts between R-Z
					|| ((asciiChar>=114)&&(asciiChar<=122)))
		}
	}*/

    it should "return a list of items for the popup menu (when searching items inside other workspaces)" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){

            // This uses ScalaTest's JMockCycle to wrap the Mockery object
            val cycle = new JMockCycle
            import cycle._

            // Set the context of the mock
            context.checking(
                new Expectations() {

                    // Note: The function being tested behaves EXACTLY like doing any item search by description
                    allowing(mockClient).setHolder("/itemGroup")
                    
                    // Query /itemGroup endpoint to grab all itemGroup ID's
                    exactly(2).of(mockClient).getResponseAsJson()     
                        will(returnValue(fakeItemGroupJsonResponse))     

                    // Adds selected itemGroups to a list to be filtered
                    // locallly
                    oneOf(mockClient).setFlag("itemGroup", "2F226DE1-F285-4612-AEFB-7C17DCE2F852")
                    oneOf(mockClient).setFlag("itemGroup", "6DB23A00-89F9-4907-8278-C6E69EFF51C3")
                    oneOf(mockClient).setFlag("itemGroup", "C3FAC269-BC54-47BC-89BD-50800B5E6D22")
                    oneOf(mockClient).setFlag("itemGroup", "CD1FA9E4-AD19-4CA9-AE02-55ECB74627BA")
                    oneOf(mockClient).setFlag("itemGroup", "870D9337-4C25-48B2-8329-ACE788C8A01B")
                    oneOf(mockClient).setFlag("itemGroup", "7B4AA572-0A8B-48BE-AC59-653932809915")
                    oneOf(mockClient).setFlag("itemGroup", "8B4AA572-0A8B-48BE-AC59-653932809915")
                    oneOf(mockClient).setFlag("itemGroup", "4C7804BE-3D84-4A8F-9B44-49CD26B56E15")
                    oneOf(mockClient).setFlag("itemGroup", "8A0D67BC-4729-4A1B-A7E5-049C546B6978")

                    oneOf (mockClient).setFlag("description", "b")

                    oneOf(mockClient).setFlag("offset", "0")
                    oneOf(mockClient).setFlag("max", "32766")

                    oneOf (mockClient).setFlag("detail", "itemGroup")
                    oneOf (mockClient).setFlag("detail", "type")

                    allowing(mockClient).setHolder("/item")
                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeBargeDetailsJsonResponse))
                }
            )

            // Test the system:
            val itemWS = WorkspaceFactory.createWorkspace(Endpoint.ITEMS)
            itemWS.setClient(mockClient)

            val shortList = itemWS.getItemListWindow("b") // Search for items starting with "b"
        
            assert(shortList.size() == 1)
            assert(shortList.get(0).getProperty(ItemIdentifier.ITEMTYPE.columnName())=="Plant Equipment")
            assert(shortList.get(0).getProperty(ItemIdentifier.ITEMGROUPID.columnName())=="CD1FA9E4-AD19-4CA9-AE02-55ECB74627BA")
            assert(shortList.get(0).getProperty(ItemIdentifier.ITEMLONGDESC.columnName())=="Barge")
            assert(shortList.get(0).getProperty(ItemIdentifier.ITEMCODE.columnName())=="BAR")
            assert(shortList.get(0).getProperty(ItemIdentifier.ID.columnName())=="8FEA70D9-9D21-4012-83E9-98FB6FA577DE")
            assert(shortList.get(0).getProperty(ItemIdentifier.ITEMSHORTDESC.columnName())=="Barge")
            assert(shortList.get(0).getProperty(ItemIdentifier.ITEMTYPEID.columnName())=="AC684149-A2FB-430F-9A59-FCCD199AF2C5")
            assert(shortList.get(0).getProperty(ItemIdentifier.ITEMGROUPDESCRIPTION.columnName())=="Barges")
        }
    }

    it should "retrieve an item's attributes" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){
            val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
            import cycle._

            context.checking(           // Set the context of the mock
                new Expectations() {
                    allowing(mockClient).setHolder("/itemAttribute") 

                    // Adds request to get attributes of the selected item
                    oneOf(mockClient).setFlag("itemUUID", "8FEA70D9-9D21-4012-83E9-98FB6FA577DE")
                    
                    exactly(1).of(mockClient).getResponseAsJson()   
                        will(returnValue(fakeItemAttributeJsonResponse))
                }
            )

            // Test the system:
            val itemWS = WorkspaceFactory.createWorkspace(Endpoint.ITEMS)
            itemWS.setClient(mockClient)


            val attributesData = itemWS.filterAttributes("8FEA70D9-9D21-4012-83E9-98FB6FA577DE") // .filterAttributes(objectUUID)
        
            // Assert that the attributes are correctly processed
            //println(attributesData.get(0).getPropertyMap())
            assert(attributesData.get(0).getProperty(ItemIdentifier.itemAttributes()(0).columnName())=="Capacity") //.itemAttributes contains: ATTRIBUTE,ATTRIBUTEUOM,REQUIREMENTRULE,DISPLAYORDER 
            assert(attributesData.get(0).getProperty(ItemIdentifier.ATTRIBUTE.columnName())=="Capacity")
            assert(attributesData.get(0).getProperty(ItemIdentifier.ATTRIBUTEUOM.columnName())=="UNDEF")
            assert(attributesData.get(0).getProperty(ItemIdentifier.REQUIREMENTRULE.columnName())=="REQUIRED_WARNING")
            assert(attributesData.get(0).getProperty(ItemIdentifier.DISPLAYORDER.columnName())=="")
        }
    }
}