package suites

import com.entero.identifiers.InvoiceIdentifier
import com.entero.rest.RestClient
import com.entero.workspace.Workspace
import com.entero.workspace.WorkspaceFactory
import com.entero.workspace.WorkspaceFactory.Endpoint
import models.EnteroObject

// Used to create fake JSON responses
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper

// Play testing tools
import play.api.test.Helpers.running
import play.api.test.FakeApplication

// ScalaTest utils
import collection.JavaConversions._
import collection.mutable._
import org.scalatest.junit.AssertionsForJUnit
import org.scalatest.PrivateMethodTester._
import org.scalatest._
import org.scalatest.mock.{JMockCycleFixture, JMockCycle}

// Jmock tools
import org.scalatest.mock.JMockCycle
import org.jmock.Expectations
import org.jmock.Expectations.returnValue
import org.scalatest.mock.JMockExpectations
import org.jmock.Mockery
import org.jmock.lib.legacy.ClassImposteriser
import org.jmock.lib.concurrent.Synchroniser

/**
 * READ ME (Last updated 24/11/2014):
 * The following tests uses ScalaTest in conjunction with Selenium to 
 * simulate a web browser to test the web app's behaviour, as well as
 * verifying the information. Tests are structured in FlatSpec format,
 * which follows BDD(Behavior-Driven Design) principles.
 * 
 * To compile these tests you need to have the following lines added to your SBT build file:
 * libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.1" % "test"
 * libraryDependencies += "org.seleniumhq.selenium" % "selenium-java" % "2.44.0" % "test"
 *
 * To run these tests you simply type "test" in your sbt terminal.
 *
 */
class InvoiceBackendTest extends FlatSpec {
    
    //--------------Global Variables------------------
    val maxOffset = "50"    // This represents the maximum number of results to return
    val mapper = new ObjectMapper()
    //------------------------------------------------

    /** This defines the fixture method to create mock RestClient objects!
     *  Search "Loan-Fixture" Method for more information.
     */
    def withRestClient(testCode: (RestClient, Mockery) => Any) {
        
        // This creates a context object from JMock
        var context = new Mockery
        context.setImposteriser(ClassImposteriser.INSTANCE)
        context.setThreadingPolicy(new Synchroniser()) //This prevents race conditions with mock objects!

        var mockClient = context.mock(classOf[RestClient])
        try {
            // Loans the mockClient to the tests
            testCode(mockClient, context)
        }
        finally {
            // Clean up objects
            mockClient = null
            context = null
        }
    }
    
    //=======================================================
    behavior of "Invoice Workspace"
    //=======================================================
    val fakeInvoiceItemJsonResponse = mapper.readTree( // Fake Invoice JSON response for ONE price object
    """ |[
        |    {
        |        "payRec": "Receivable",
        |        "type": "Sales",
        |        "counterparty": {
        |            "contact": {
        |                "organizationName": "Bartells",
        |                "departmentName": "Rusty",
        |                "description": "Bartells, Rusty",
        |                "type": "Individual",
        |                "stakeholderId": 500051,
        |                "id": "22ba6e59-09cf-438e-8b1c-e551cfdf7cae",
        |                "ref": "http://172.24.0.103:80/api/stakeholder/22ba6e59-09cf-438e-8b1c-e551cfdf7cae"
        |            },
        |            "address": {
        |                "line1": "13105 Northwest Freeway ",
        |                "description": "13105 Northwest Freeway ",
        |                "type": "Office",
        |                "id": "5b0ad9b3-1e2c-45f2-8ed7-fb5193bf680a",
        |                "ref": "http://172.24.0.103:80/api/location/5b0ad9b3-1e2c-45f2-8ed7-fb5193bf680a"
        |            },
        |            "organizationName": "ZEROX",
        |            "description": "Amerigas Propane Incorporated",
        |            "type": "External Organization",
        |            "stakeholderId": 500045,
        |            "id": "7fd1c30e-a1be-468e-be84-c45ca6a23f5b",
        |            "ref": "http://172.24.0.103:80/api/organization/7fd1c30e-a1be-468e-be84-c45ca6a23f5b"
        |        },
        |        "businessUnit": {
        |            "contact": {
        |                "organizationName": "Sky",
        |                "departmentName": "Tammy",
        |                "description": "Sky, Tammy",
        |                "type": "Individual",
        |                "stakeholderId": 122,
        |                "id": "D7620DF9-72C1-4EC2-992E-98AE0A9C4F9F",
        |                "ref": "http://172.24.0.103:80/api/stakeholder/D7620DF9-72C1-4EC2-992E-98AE0A9C4F9F"
        |            },
        |            "address": {
        |                "line1": "440 - 2nd Ave. SW",
        |                "description": "440 - 2nd Ave. SW Suite 1500 Ernst & Young Tower Calgary, AB T2P 5E9",
        |                "type": "Office",
        |                "id": "6EDD95F2-EAC5-4EDB-94EF-CFE0679A4713",
        |                "ref": "http://172.24.0.103:80/api/location/6EDD95F2-EAC5-4EDB-94EF-CFE0679A4713"
        |            },
        |            "organizationName": "Northern Gas Liquids Marketing",
        |            "description": "Northern Gas Liquids Marketing",
        |            "type": "Operating Business Unit",
        |            "stakeholderId": 14,
        |            "id": "F3BC5F74-FF45-4AC4-B964-3E842127765F",
        |            "ref": "http://172.24.0.103:80/api/organization/F3BC5F74-FF45-4AC4-B964-3E842127765F"
        |        },
        |        "account": "Accounts Receivable $US",
        |        "currency": {
        |            "description": "U.S. Dollars",
        |            "currencyCode": "USD",
        |            "id": "60E8783A-FCB0-4395-91F3-1B771D480159",
        |            "ref": "http://172.24.0.103:80/api/currency/60E8783A-FCB0-4395-91F3-1B771D480159?token=e932cbd5-ea1b-40cb-a2b9-28bf0fb4cd2c&format=json"
        |        },
        |        "paymentTerms": "1% 10 Net 15",
        |        "format": "Standard Layout (S)",
        |        "invoiceMode": "Hard Copy",
        |        "issuedDate": "Nov 7, 1999 12:00:00 AM",
        |        "dueDate": "Nov 21, 2000 12:00:00 AM",
        |        "discountDate": "Nov 17, 2008 12:00:00 AM",
        |        "daysOverDue": 2338,
        |        "invoiceId": 500008,
        |        "invoiceRef": "NE Mktg-1",
        |        "status": "Paid",
        |        "invoiceStatus": "PAID",
        |        "paymentMode": "Check",
        |        "netAmount": 920326.39,
        |        "taxAmount": 3903.02,
        |        "adjustmentAmount": 0,
        |        "discountAmount": 9242.29,
        |        "totalAmount": 924229.41,
        |        "selfAssessmentAmount": 0,
        |        "outstandingAmount": 0,
        |        "totalPaymentAmount": 914987.12,
        |        "lastPaymentDate": "Nov 14, 2008 12:00:00 AM",
        |        "lastPaymentAmount": 914987.12,
        |        "lastPaymentRef": "500032",
        |        "remitAddress": {
        |            "line1": "440 - 2nd Ave. SW",
        |            "description": "440 - 2nd Ave. SW Suite 1500 Ernst & Young Tower Calgary, AB T2P 5E9",
        |            "type": "Office",
        |            "id": "6EDD95F2-EAC5-4EDB-94EF-CFE0679A4713",
        |            "ref": "http://172.24.0.103:80/api/location/6EDD95F2-EAC5-4EDB-94EF-CFE0679A4713?token=e932cbd5-ea1b-40cb-a2b9-28bf0fb4cd2c&format=json"
        |        },
        |        "id": "403b6385-4f5f-4f69-abaf-c13cd206b79f",
        |        "ref": "http://172.24.0.103:80/api/invoice/403b6385-4f5f-4f69-abaf-c13cd206b79f?token=e932cbd5-ea1b-40cb-a2b9-28bf0fb4cd2c&format=json"
        |    }
        |]""".stripMargin)

    val fakeInvoiceItemsJsonResponseSize = 4 // Represents the number of price items in this mock response
    val fakeInvoiceItemsJsonResponse = mapper.readTree( // Fake Invoice json response for 4 price objects
    """ |[
        |    {
        |        "payRec": "Receivable",
        |        "type": "Sales",
        |        "counterparty": {
        |            "contact": {
        |                "organizationName": "Bartells",
        |                "departmentName": "Rusty",
        |                "description": "Bartells, Rusty",
        |                "type": "Individual",
        |                "stakeholderId": 500051,
        |                "id": "22ba6e59-09cf-438e-8b1c-e551cfdf7cae",
        |                "ref": "http://172.24.0.103:80/api/stakeholder/22ba6e59-09cf-438e-8b1c-e551cfdf7cae"
        |            },
        |            "address": {
        |                "line1": "13105 Northwest Freeway ",
        |                "description": "13105 Northwest Freeway ",
        |                "type": "Office",
        |                "id": "5b0ad9b3-1e2c-45f2-8ed7-fb5193bf680a",
        |                "ref": "http://172.24.0.103:80/api/location/5b0ad9b3-1e2c-45f2-8ed7-fb5193bf680a"
        |            },
        |            "organizationName": "Amerigas Propane Incorporated",
        |            "description": "Amerigas Propane Incorporated",
        |            "type": "External Organization",
        |            "stakeholderId": 500045,
        |            "id": "7fd1c30e-a1be-468e-be84-c45ca6a23f5b",
        |            "ref": "http://172.24.0.103:80/api/organization/7fd1c30e-a1be-468e-be84-c45ca6a23f5b"
        |        },
        |        "businessUnit": {
        |            "contact": {
        |                "organizationName": "Sky",
        |                "departmentName": "Tammy",
        |                "description": "Sky, Tammy",
        |                "type": "Individual",
        |                "stakeholderId": 122,
        |                "id": "D7620DF9-72C1-4EC2-992E-98AE0A9C4F9F",
        |                "ref": "http://172.24.0.103:80/api/stakeholder/D7620DF9-72C1-4EC2-992E-98AE0A9C4F9F"
        |            },
        |            "address": {
        |                "line1": "440 - 2nd Ave. SW",
        |                "description": "440 - 2nd Ave. SW Suite 1500 Ernst & Young Tower Calgary, AB T2P 5E9",
        |                "type": "Office",
        |                "id": "6EDD95F2-EAC5-4EDB-94EF-CFE0679A4713",
        |                "ref": "http://172.24.0.103:80/api/location/6EDD95F2-EAC5-4EDB-94EF-CFE0679A4713"
        |            },
        |            "organizationName": "Northern Gas Liquids Marketing",
        |            "description": "Northern Gas Liquids Marketing",
        |            "type": "Operating Business Unit",
        |            "stakeholderId": 14,
        |            "id": "F3BC5F74-FF45-4AC4-B964-3E842127765F",
        |            "ref": "http://172.24.0.103:80/api/organization/F3BC5F74-FF45-4AC4-B964-3E842127765F"
        |        },
        |        "account": "Accounts Receivable $US",
        |        "currency": {
        |            "description": "U.S. Dollars",
        |            "currencyCode": "USD",
        |            "id": "60E8783A-FCB0-4395-91F3-1B771D480159",
        |            "ref": "http://172.24.0.103:80/api/currency/60E8783A-FCB0-4395-91F3-1B771D480159?token=e932cbd5-ea1b-40cb-a2b9-28bf0fb4cd2c&format=json"
        |        },
        |        "paymentTerms": "1% 10 Net 15",
        |        "format": "Standard Layout (S)",
        |        "invoiceMode": "Hard Copy",
        |        "issuedDate": "Dec 5, 2008 12:00:00 AM",
        |        "dueDate": "Dec 20, 2008 12:00:00 AM",
        |        "discountDate": "Dec 15, 2008 12:00:00 AM",
        |        "daysOverDue": 2309,
        |        "invoiceId": 500012,
        |        "invoiceRef": "NE Mktg-8",
        |        "status": "Issued",
        |        "invoiceStatus": "PAST_DUE",
        |        "paymentMode": "Check",
        |        "netAmount": 399616.52,
        |        "taxAmount": 1950.85,
        |        "adjustmentAmount": 0,
        |        "discountAmount": 0,
        |        "totalAmount": 401567.37,
        |        "selfAssessmentAmount": 0,
        |        "outstandingAmount": 401567.37,
        |        "totalPaymentAmount": 0,
        |        "lastPaymentDate": "Nov 14, 2008 12:00:00 AM",
        |        "lastPaymentAmount": 52500,
        |        "lastPaymentRef": "500021",
        |        "remitAddress": {
        |            "line1": "440 - 2nd Ave. SW",
        |            "description": "440 - 2nd Ave. SW Suite 1500 Ernst & Young Tower Calgary, AB T2P 5E9",
        |            "type": "Office",
        |            "id": "6EDD95F2-EAC5-4EDB-94EF-CFE0679A4713",
        |            "ref": "http://172.24.0.103:80/api/location/6EDD95F2-EAC5-4EDB-94EF-CFE0679A4713?token=e932cbd5-ea1b-40cb-a2b9-28bf0fb4cd2c&format=json"
        |        },
        |        "id": "65730d76-9870-4162-85cb-a8a10f6f8cca",
        |        "ref": "http://172.24.0.103:80/api/invoice/65730d76-9870-4162-85cb-a8a10f6f8cca?token=e932cbd5-ea1b-40cb-a2b9-28bf0fb4cd2c&format=json"
        |    },
        |    {
        |        "payRec": "Receivable",
        |        "type": "Sales",
        |        "counterparty": {
        |            "contact": {
        |                "organizationName": "Fleming ",
        |                "departmentName": "Mark ",
        |                "description": "Fleming , Mark ",
        |                "type": "Individual",
        |                "stakeholderId": 500000,
        |                "id": "d908b963-f6ac-4fe3-b5ec-0d77ed09e0ec",
        |                "ref": "http://172.24.0.103:80/api/stakeholder/d908b963-f6ac-4fe3-b5ec-0d77ed09e0ec"
        |            },
        |            "address": {
        |                "line1": "55 Riverside Avenue",
        |                "description": "55 Riverside Avenue Suite 2500 Houston, TX 20155",
        |                "type": "Office",
        |                "id": "ce87abb3-747c-4342-84ab-ac536d467832",
        |                "ref": "http://172.24.0.103:80/api/location/ce87abb3-747c-4342-84ab-ac536d467832"
        |            },
        |            "organizationName": "Wisconsin Power Corporation ",
        |            "description": "Wisconsin Power Corporation ",
        |            "type": "External Organization",
        |            "stakeholderId": 500017,
        |            "id": "02a719f3-5f5a-4d61-86cf-5d7a66057f21",
        |            "ref": "http://172.24.0.103:80/api/organization/02a719f3-5f5a-4d61-86cf-5d7a66057f21"
        |        },
        |        "businessUnit": {
        |            "contact": {
        |                "organizationName": "Doyle",
        |                "departmentName": "Joe",
        |                "description": "Doyle, Joe",
        |                "type": "Individual",
        |                "stakeholderId": 500034,
        |                "id": "cb035df3-511f-4e4a-9261-bd8e0fc6c988",
        |                "ref": "http://172.24.0.103:80/api/stakeholder/cb035df3-511f-4e4a-9261-bd8e0fc6c988"
        |            },
        |            "address": {
        |                "line1": "1200 1st Avenue",
        |                "description": "1200 1st Avenue Moose Jaw, SK S0X 1A1",
        |                "type": "Office",
        |                "id": "DC268216-F486-4BC2-91FB-CADCBCB85433",
        |                "ref": "http://172.24.0.103:80/api/location/DC268216-F486-4BC2-91FB-CADCBCB85433"
        |            },
        |            "organizationName": "Southern Carbon Marketing",
        |            "description": "Southern Carbon Marketing",
        |            "type": "Operating Business Unit",
        |            "stakeholderId": 10,
        |            "id": "116271A4-48BA-479C-9E84-6DE1574EE470",
        |            "ref": "http://172.24.0.103:80/api/organization/116271A4-48BA-479C-9E84-6DE1574EE470"
        |        },
        |        "account": "Accounts Receivable $US",
        |        "currency": {
        |            "description": "U.S. Dollars",
        |            "currencyCode": "USD",
        |            "id": "60E8783A-FCB0-4395-91F3-1B771D480159",
        |            "ref": "http://172.24.0.103:80/api/currency/60E8783A-FCB0-4395-91F3-1B771D480159?token=e932cbd5-ea1b-40cb-a2b9-28bf0fb4cd2c&format=json"
        |        },
        |        "paymentTerms": "Net 30 Days Date of Invoice",
        |        "format": "Shipper Fee Purchase Stmt",
        |        "invoiceMode": "Hard Copy",
        |        "issuedDate": "Nov 1, 2008 12:00:00 AM",
        |        "dueDate": "Dec 1, 2008 12:00:00 AM",
        |        "daysOverDue": 2328,
        |        "invoiceId": 500049,
        |        "invoiceRef": "SE Mktg-4",
        |        "status": "Paid",
        |        "invoiceStatus": "PAID",
        |        "paymentMode": "Check",
        |        "netAmount": 52500,
        |        "taxAmount": 0,
        |        "adjustmentAmount": 0,
        |        "discountAmount": 0,
        |        "totalAmount": 52500,
        |        "selfAssessmentAmount": 0,
        |        "outstandingAmount": 0,
        |        "totalPaymentAmount": 52500,
        |        "lastPaymentDate": "Nov 14, 2008 12:00:00 AM",
        |        "lastPaymentAmount": 52500,
        |        "lastPaymentRef": "500021",
        |        "remitAddress": {
        |            "line1": "1200 1st Avenue",
        |            "description": "1200 1st Avenue Moose Jaw, SK S0X 1A1",
        |            "type": "Office",
        |            "id": "DC268216-F486-4BC2-91FB-CADCBCB85433",
        |            "ref": "http://172.24.0.103:80/api/location/DC268216-F486-4BC2-91FB-CADCBCB85433?token=e932cbd5-ea1b-40cb-a2b9-28bf0fb4cd2c&format=json"
        |        },
        |        "id": "4cff48a6-705b-47dc-817c-7f0722af40ac",
        |        "ref": "http://172.24.0.103:80/api/invoice/4cff48a6-705b-47dc-817c-7f0722af40ac?token=e932cbd5-ea1b-40cb-a2b9-28bf0fb4cd2c&format=json"
        |    },
        |    {
        |        "payRec": "Payable",
        |        "type": "Purchase",
        |        "counterparty": {
        |            "contact": {
        |                "organizationName": "Wang",
        |                "departmentName": "Holly",
        |                "description": "Wang, Holly",
        |                "type": "Individual",
        |                "stakeholderId": 2204,
        |                "id": "1462FE5F-B455-491E-AA89-738E3EADB744",
        |                "ref": "http://172.24.0.103:80/api/stakeholder/1462FE5F-B455-491E-AA89-738E3EADB744"
        |            },
        |            "address": {
        |                "line1": "639 5th Ave. SW",
        |                "description": "639 5th Ave. SW Suite 2300 Standard Life Building Calgary, AB T2P 0M9",
        |                "type": "Office",
        |                "id": "125B39A3-FA91-4270-A6B1-895BB5962A8A",
        |                "ref": "http://172.24.0.103:80/api/location/125B39A3-FA91-4270-A6B1-895BB5962A8A"
        |            },
        |            "organizationName": "Dome Petroleum Limited",
        |            "description": "Dome Petroleum Limited",
        |            "type": "External Organization",
        |            "stakeholderId": 2255,
        |            "id": "1AA7C562-2674-4948-A124-0B50AAE11E1E",
        |            "ref": "http://172.24.0.103:80/api/organization/1AA7C562-2674-4948-A124-0B50AAE11E1E"
        |        },
        |        "businessUnit": {
        |            "contact": {
        |                "organizationName": "Sky",
        |                "departmentName": "Tammy",
        |                "description": "Sky, Tammy",
        |                "type": "Individual",
        |                "stakeholderId": 122,
        |                "id": "D7620DF9-72C1-4EC2-992E-98AE0A9C4F9F",
        |                "ref": "http://172.24.0.103:80/api/stakeholder/D7620DF9-72C1-4EC2-992E-98AE0A9C4F9F"
        |            },
        |            "address": {
        |                "line1": "440 - 2nd Ave. SW",
        |                "description": "440 - 2nd Ave. SW Suite 1500 Ernst & Young Tower Calgary, AB T2P 5E9",
        |                "type": "Office",
        |                "id": "6EDD95F2-EAC5-4EDB-94EF-CFE0679A4713",
        |                "ref": "http://172.24.0.103:80/api/location/6EDD95F2-EAC5-4EDB-94EF-CFE0679A4713"
        |            },
        |            "organizationName": "Northern Gas Liquids Marketing",
        |            "description": "Northern Gas Liquids Marketing",
        |            "type": "Operating Business Unit",
        |            "stakeholderId": 14,
        |            "id": "F3BC5F74-FF45-4AC4-B964-3E842127765F",
        |            "ref": "http://172.24.0.103:80/api/organization/F3BC5F74-FF45-4AC4-B964-3E842127765F"
        |        },
        |        "account": "Accounts Payable $CAD",
        |        "currency": {
        |            "description": "Canadian Dollars",
        |            "currencyCode": "CAD",
        |            "id": "B051DBB6-F3B2-44CF-84E5-CE98268FB68C",
        |            "ref": "http://172.24.0.103:80/api/currency/B051DBB6-F3B2-44CF-84E5-CE98268FB68C?token=e932cbd5-ea1b-40cb-a2b9-28bf0fb4cd2c&format=json"
        |        },
        |        "paymentTerms": "Due 25th of the Following Month, Cheq Exchange",
        |        "format": "Shipper Fee Purchase Stmt",
        |        "invoiceMode": "Hard Copy",
        |        "issuedDate": "Nov 11, 2008 12:00:00 AM",
        |        "dueDate": "Dec 24, 2008 12:00:00 AM",
        |        "receiptDate": "Nov 11, 2009 12:00:00 AM",
        |        "daysOverDue": 0,
        |        "invoiceId": 500103,
        |        "invoiceRef": "500103",
        |        "status": "Issued",
        |        "invoiceStatus": "PAST_DUE",
        |        "paymentMode": "Check",
        |        "netAmount": -160740,
        |        "taxAmount": -8037,
        |        "adjustmentAmount": 0,
        |        "discountAmount": 0,
        |        "totalAmount": -168777,
        |        "selfAssessmentAmount": 0,
        |        "outstandingAmount": -168777,
        |        "totalPaymentAmount": 0,
        |        "remitAddress": {
        |            "line1": "639 5th Ave. SW",
        |            "description": "639 5th Ave. SW Suite 2300 Standard Life Building Calgary, AB T2P 0M9",
        |            "type": "Office",
        |            "id": "125B39A3-FA91-4270-A6B1-895BB5962A8A",
        |            "ref": "http://172.24.0.103:80/api/location/125B39A3-FA91-4270-A6B1-895BB5962A8A?token=e932cbd5-ea1b-40cb-a2b9-28bf0fb4cd2c&format=json"
        |        },
        |        "id": "0cfe13f4-9302-4c54-a05f-ec51790e0de1",
        |        "ref": "http://172.24.0.103:80/api/invoice/0cfe13f4-9302-4c54-a05f-ec51790e0de1?token=e932cbd5-ea1b-40cb-a2b9-28bf0fb4cd2c&format=json"
        |    },
        |    {
        |        "payRec": "Receivable",
        |        "type": "Sales",
        |        "counterparty": {
        |            "contact": {
        |                "organizationName": "Bartells",
        |                "departmentName": "Rusty",
        |                "description": "Bartells, Rusty",
        |                "type": "Individual",
        |                "stakeholderId": 500051,
        |                "id": "22ba6e59-09cf-438e-8b1c-e551cfdf7cae",
        |                "ref": "http://172.24.0.103:80/api/stakeholder/22ba6e59-09cf-438e-8b1c-e551cfdf7cae"
        |            },
        |            "address": {
        |                "line1": "13105 Northwest Freeway ",
        |                "description": "13105 Northwest Freeway ",
        |                "type": "Office",
        |                "id": "5b0ad9b3-1e2c-45f2-8ed7-fb5193bf680a",
        |                "ref": "http://172.24.0.103:80/api/location/5b0ad9b3-1e2c-45f2-8ed7-fb5193bf680a"
        |            },
        |            "organizationName": "ZEROX",
        |            "description": "Amerigas Propane Incorporated",
        |            "type": "External Organization",
        |            "stakeholderId": 500045,
        |            "id": "7fd1c30e-a1be-468e-be84-c45ca6a23f5b",
        |            "ref": "http://172.24.0.103:80/api/organization/7fd1c30e-a1be-468e-be84-c45ca6a23f5b"
        |        },
        |        "businessUnit": {
        |            "contact": {
        |                "organizationName": "Sky",
        |                "departmentName": "Tammy",
        |                "description": "Sky, Tammy",
        |                "type": "Individual",
        |                "stakeholderId": 122,
        |                "id": "D7620DF9-72C1-4EC2-992E-98AE0A9C4F9F",
        |                "ref": "http://172.24.0.103:80/api/stakeholder/D7620DF9-72C1-4EC2-992E-98AE0A9C4F9F"
        |            },
        |            "address": {
        |                "line1": "440 - 2nd Ave. SW",
        |                "description": "440 - 2nd Ave. SW Suite 1500 Ernst & Young Tower Calgary, AB T2P 5E9",
        |                "type": "Office",
        |                "id": "6EDD95F2-EAC5-4EDB-94EF-CFE0679A4713",
        |                "ref": "http://172.24.0.103:80/api/location/6EDD95F2-EAC5-4EDB-94EF-CFE0679A4713"
        |            },
        |            "organizationName": "Northern Gas Liquids Marketing",
        |            "description": "Northern Gas Liquids Marketing",
        |            "type": "Operating Business Unit",
        |            "stakeholderId": 14,
        |            "id": "F3BC5F74-FF45-4AC4-B964-3E842127765F",
        |            "ref": "http://172.24.0.103:80/api/organization/F3BC5F74-FF45-4AC4-B964-3E842127765F"
        |        },
        |        "account": "Accounts Receivable $US",
        |        "currency": {
        |            "description": "U.S. Dollars",
        |            "currencyCode": "USD",
        |            "id": "60E8783A-FCB0-4395-91F3-1B771D480159",
        |            "ref": "http://172.24.0.103:80/api/currency/60E8783A-FCB0-4395-91F3-1B771D480159?token=e932cbd5-ea1b-40cb-a2b9-28bf0fb4cd2c&format=json"
        |        },
        |        "paymentTerms": "1% 10 Net 15",
        |        "format": "Standard Layout (S)",
        |        "invoiceMode": "Hard Copy",
        |        "issuedDate": "Nov 7, 1999 12:00:00 AM",
        |        "dueDate": "Nov 21, 2000 12:00:00 AM",
        |        "discountDate": "Nov 17, 2008 12:00:00 AM",
        |        "daysOverDue": 2338,
        |        "invoiceId": 500008,
        |        "invoiceRef": "NE Mktg-1",
        |        "status": "Paid",
        |        "invoiceStatus": "PAID",
        |        "paymentMode": "Check",
        |        "netAmount": 920326.39,
        |        "taxAmount": 3903.02,
        |        "adjustmentAmount": 0,
        |        "discountAmount": 9242.29,
        |        "totalAmount": 924229.41,
        |        "selfAssessmentAmount": 0,
        |        "outstandingAmount": 0,
        |        "totalPaymentAmount": 914987.12,
        |        "lastPaymentDate": "Nov 14, 2008 12:00:00 AM",
        |        "lastPaymentAmount": 914987.12,
        |        "lastPaymentRef": "500032",
        |        "remitAddress": {
        |            "line1": "440 - 2nd Ave. SW",
        |            "description": "440 - 2nd Ave. SW Suite 1500 Ernst & Young Tower Calgary, AB T2P 5E9",
        |            "type": "Office",
        |            "id": "6EDD95F2-EAC5-4EDB-94EF-CFE0679A4713",
        |            "ref": "http://172.24.0.103:80/api/location/6EDD95F2-EAC5-4EDB-94EF-CFE0679A4713?token=e932cbd5-ea1b-40cb-a2b9-28bf0fb4cd2c&format=json"
        |        },
        |        "id": "403b6385-4f5f-4f69-abaf-c13cd206b79f",
        |        "ref": "http://172.24.0.103:80/api/invoice/403b6385-4f5f-4f69-abaf-c13cd206b79f?token=e932cbd5-ea1b-40cb-a2b9-28bf0fb4cd2c&format=json"
        |    }
        |]""".stripMargin)

val fakeDetailedInvoiceItemJsonResponse = mapper.readTree( // Fake invoice response for ONE price object WITH DETAILS
    """ |{
        |    "payRec": "Receivable",
        |    "type": "Sales",
        |    "counterparty": {
        |        "contact": {
        |            "organizationName": "Bartells",
        |            "departmentName": "Rusty",
        |            "description": "Bartells, Rusty",
        |            "type": "Individual",
        |            "stakeholderId": 500051,
        |            "id": "22ba6e59-09cf-438e-8b1c-e551cfdf7cae",
        |            "ref": "http://172.24.0.103:80/api/stakeholder/22ba6e59-09cf-438e-8b1c-e551cfdf7cae"
        |        },
        |        "address": {
        |            "line1": "13105 Northwest Freeway ",
        |            "description": "13105 Northwest Freeway ",
        |            "type": "Office",
        |            "id": "5b0ad9b3-1e2c-45f2-8ed7-fb5193bf680a",
        |            "ref": "http://172.24.0.103:80/api/location/5b0ad9b3-1e2c-45f2-8ed7-fb5193bf680a"
        |        },
        |        "organizationName": "Amerigas Propane Incorporated",
        |        "description": "Amerigas Propane Incorporated",
        |        "type": "External Organization",
        |        "stakeholderId": 500045,
        |        "id": "7fd1c30e-a1be-468e-be84-c45ca6a23f5b",
        |        "ref": "http://172.24.0.103:80/api/organization/7fd1c30e-a1be-468e-be84-c45ca6a23f5b"
        |    },
        |    "businessUnit": {
        |        "contact": {
        |            "organizationName": "Sky",
        |            "departmentName": "Tammy",
        |            "description": "Sky, Tammy",
        |            "type": "Individual",
        |            "stakeholderId": 122,
        |            "id": "D7620DF9-72C1-4EC2-992E-98AE0A9C4F9F",
        |            "ref": "http://172.24.0.103:80/api/stakeholder/D7620DF9-72C1-4EC2-992E-98AE0A9C4F9F"
        |        },
        |        "address": {
        |            "line1": "440 - 2nd Ave. SW",
        |            "description": "440 - 2nd Ave. SW Suite 1500 Ernst & Young Tower Calgary, AB T2P 5E9",
        |            "type": "Office",
        |            "id": "6EDD95F2-EAC5-4EDB-94EF-CFE0679A4713",
        |            "ref": "http://172.24.0.103:80/api/location/6EDD95F2-EAC5-4EDB-94EF-CFE0679A4713"
        |        },
        |        "organizationName": "Northern Gas Liquids Marketing",
        |        "description": "Northern Gas Liquids Marketing",
        |        "type": "Operating Business Unit",
        |        "stakeholderId": 14,
        |        "id": "F3BC5F74-FF45-4AC4-B964-3E842127765F",
        |        "ref": "http://172.24.0.103:80/api/organization/F3BC5F74-FF45-4AC4-B964-3E842127765F"
        |    },
        |    "account": "Accounts Receivable $US",
        |    "currency": {
        |        "description": "U.S. Dollars",
        |        "currencyCode": "USD",
        |        "id": "60E8783A-FCB0-4395-91F3-1B771D480159",
        |        "ref": "http://172.24.0.103:80/api/currency/60E8783A-FCB0-4395-91F3-1B771D480159?token=a0274be3-8ee4-491f-a5ff-06c2786379e8&format=json"
        |    },
        |    "paymentTerms": "1% 10 Net 15",
        |    "format": "Standard Layout (S)",
        |    "invoiceMode": "Hard Copy",
        |    "issuedDate": "Dec 5, 2008 12:00:00 AM",
        |    "dueDate": "Dec 20, 2008 12:00:00 AM",
        |    "discountDate": "Dec 15, 2008 12:00:00 AM",
        |    "daysOverDue": 2309,
        |    "invoiceId": 500012,
        |    "invoiceRef": "NE Mktg-8",
        |    "status": "Issued",
        |    "invoiceStatus": "PAST_DUE",
        |    "paymentMode": "Check",
        |    "netAmount": 399616.52,
        |    "taxAmount": 1950.85,
        |    "adjustmentAmount": 0,
        |    "discountAmount": 0,
        |    "totalAmount": 401567.37,
        |    "selfAssessmentAmount": 0,
        |    "outstandingAmount": 401567.37,
        |    "totalPaymentAmount": 0,
        |    "auditInfo": {
        |        "createUser": "modupeokusanya",
        |        "createDate": "May 28, 2010 02:39:23 PM",
        |        "updateUser": "TammyRomeo",
        |        "updateDate": "Jun 26, 2011 08:47:37 AM"
        |    },
        |    "remitAddress": {
        |        "line1": "440 - 2nd Ave. SW",
        |        "description": "440 - 2nd Ave. SW Suite 1500 Ernst & Young Tower Calgary, AB T2P 5E9",
        |        "type": "Office",
        |        "id": "6EDD95F2-EAC5-4EDB-94EF-CFE0679A4713",
        |        "ref": "http://172.24.0.103:80/api/location/6EDD95F2-EAC5-4EDB-94EF-CFE0679A4713?token=a0274be3-8ee4-491f-a5ff-06c2786379e8&format=json"
        |    },
        |    "payments": [],
        |    "details": [
        |        {
        |            "type": "Contract Price",
        |            "item": {
        |                "shortDescription": "C3o",
        |                "description": "Propane - HD5 Odorized",
        |                "locationControl": false,
        |                "odorized": false,
        |                "carryInventoryQuality": false,
        |                "rinRequired": false,
        |                "qtyByComponent": false,
        |                "id": "FAFE9242-E5CE-4B64-B9B0-6A4E64874155",
        |                "ref": "http://172.24.0.103:80/api/item/FAFE9242-E5CE-4B64-B9B0-6A4E64874155?token=a0274be3-8ee4-491f-a5ff-06c2786379e8&format=json"
        |            },
        |            "sourceItem": {
        |                "shortDescription": "C3",
        |                "description": "Propane - HD5 Not Odorized",
        |                "locationControl": false,
        |                "odorized": false,
        |                "carryInventoryQuality": false,
        |                "rinRequired": false,
        |                "qtyByComponent": false,
        |                "id": "E7B8241A-6484-4464-AE6C-5049620A6685",
        |                "ref": "http://172.24.0.103:80/api/item/E7B8241A-6484-4464-AE6C-5049620A6685?token=a0274be3-8ee4-491f-a5ff-06c2786379e8&format=json"
        |            },
        |            "destinationItem": {
        |                "shortDescription": "C3o",
        |                "description": "Propane - HD5 Odorized",
        |                "locationControl": false,
        |                "odorized": false,
        |                "carryInventoryQuality": false,
        |                "rinRequired": false,
        |                "qtyByComponent": false,
        |                "id": "FAFE9242-E5CE-4B64-B9B0-6A4E64874155",
        |                "ref": "http://172.24.0.103:80/api/item/FAFE9242-E5CE-4B64-B9B0-6A4E64874155?token=a0274be3-8ee4-491f-a5ff-06c2786379e8&format=json"
        |            },
        |            "invoiceDocumentItemDescriptionOverride": "Propane - HD5 Odorized",
        |            "shipDate": "Dec 1, 2008 12:00:00 AM",
        |            "accountingDate": "Dec 1, 2008 12:00:00 AM",
        |            "bolTicket": "500250",
        |            "refNumber": "45342",
        |            "sourceLocation": {
        |                "line1": "Bend",
        |                "description": "Bend, OR",
        |                "type": "Jurisdiction",
        |                "id": "89A342F1-A7A4-428C-BED8-3EFF7F88CEDC",
        |                "ref": "http://172.24.0.103:80/api/location/89A342F1-A7A4-428C-BED8-3EFF7F88CEDC?token=a0274be3-8ee4-491f-a5ff-06c2786379e8&format=json"
        |            },
        |            "contractDescription": "Amerigas C3 - NGL Mtkg : 500047 : 1",
        |            "contractReference": "1",
        |            "agreementId": 500002,
        |            "dealId": 500047,
        |            "dealLineId": 500281,
        |            "quantityDelivered": {
        |                "uom": {
        |                    "code": "TC",
        |                    "shortDescription": "Rail Car",
        |                    "description": "Rail Car",
        |                    "id": "9DEBBD53-D89E-4655-8A50-E67EA0E97ADC",
        |                    "ref": "http://172.24.0.103:80/api/unitofmeasure/9DEBBD53-D89E-4655-8A50-E67EA0E97ADC"
        |                },
        |                "quantity": 1
        |            },
        |            "currency": {
        |                "description": "U.S. Dollars",
        |                "currencyCode": "USD",
        |                "id": "60E8783A-FCB0-4395-91F3-1B771D480159",
        |                "ref": "http://172.24.0.103:80/api/currency/60E8783A-FCB0-4395-91F3-1B771D480159?token=a0274be3-8ee4-491f-a5ff-06c2786379e8&format=json"
        |            },
        |            "unitCost": 4515,
        |            "invoiceAmount": 4515,
        |            "id": "bd677709-261c-4abb-b2cf-15f77188c31a",
        |            "ref": "http://172.24.0.103:80/api/invoicedetail/bd677709-261c-4abb-b2cf-15f77188c31a?token=a0274be3-8ee4-491f-a5ff-06c2786379e8&format=json"
        |        }
        |    ],
        |    "id": "65730d76-9870-4162-85cb-a8a10f6f8cca",
        |    "ref": "http://172.24.0.103:80/api/invoice/65730d76-9870-4162-85cb-a8a10f6f8cca?token=a0274be3-8ee4-491f-a5ff-06c2786379e8&format=json"
        |}""".stripMargin)
    
    it should "return the Invoice workspace's name" in {
        running(FakeApplication()){

            val invoiceWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.INVOICE)
            assert(invoiceWS.getWSName() == "Invoice")
        }
    }
                
    it should "return a list when blank-searching Invoice (no parameters specified)" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){

            val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
            import cycle._

            context.checking( // Set the context of the mock
                new Expectations() {
                    allowing(mockClient).setHolder("/invoice")

                    oneOf (mockClient).setFlag("offset", "0")
                    oneOf (mockClient).setFlag("max", maxOffset)

                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeInvoiceItemsJsonResponse))
                }
            )

            // Test the system:
            val invoiceWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.INVOICE)
            invoiceWS.setClient(mockClient)

            // Perform a blank search
            val offset = 0
            // .searchInvoice(currentOffset, invoiceReference, payRec, invoiceTypeCode, invoiceStatus, dateType, fromDate, toDate)
            invoiceWS.searchInvoice(offset, "", "", "", "", "", "", "")
            val invoiceTable = invoiceWS.getTable()

            //println(invoiceTable.get(0).getPropertyMap())
            assert(invoiceTable.size() == fakeInvoiceItemsJsonResponseSize)
            assert(invoiceTable.get(0).getProperty(InvoiceIdentifier.ACCOUNT.columnName()).contains("Accounts Receivable"))
            assert(invoiceTable.get(0).getProperty(InvoiceIdentifier.BUSINESSUNIT.columnName()).contains("Northern Gas"))
            assert(invoiceTable.get(0).getProperty(InvoiceIdentifier.INVOICEREF.columnName()).contains("NE"))
            assert(invoiceTable.get(0).getProperty(InvoiceIdentifier.DISCOUNTDATE.columnName()).contains("Dec 15"))
            assert(invoiceTable.get(0).getProperty(InvoiceIdentifier.LASTPAYMENTAMOUNT.columnName()).contains("52500"))
            assert(invoiceTable.get(0).getProperty(InvoiceIdentifier.INVOICEPAYREC.columnName()).contains("Receivable"))
            assert(invoiceTable.get(0).getProperty(InvoiceIdentifier.FORMAT.columnName()).contains("Standard Layout"))
            assert(invoiceTable.get(0).getProperty(InvoiceIdentifier.COUNTERPARTY.columnName()).contains("Amerigas"))
            assert(invoiceTable.get(0).getProperty(InvoiceIdentifier.PRETAXTOTAL.columnName()).contains("399616.52"))
            assert(invoiceTable.get(0).getProperty(InvoiceIdentifier.CURRENCY.columnName()).contains("U.S. Dollars"))
            assert(invoiceTable.get(0).getProperty(InvoiceIdentifier.ID.columnName()).contains("65730d76-9870-4162-85cb-a8a10f6f8cca"))
            assert(invoiceTable.get(0).getProperty(InvoiceIdentifier.PAYMENTMODE.columnName()).contains("Check"))
            assert(invoiceTable.get(0).getProperty(InvoiceIdentifier.DUEDATE.columnName()).contains("Dec 20"))
            assert(invoiceTable.get(0).getProperty(InvoiceIdentifier.ADJUSTMENTAMOUNT.columnName()).contains("0"))
            //assert(invoiceTable.get(0).getProperty(InvoiceIdentifier.RECEIPTDATE.columnName()).contains(""))
            assert(invoiceTable.get(0).getProperty(InvoiceIdentifier.LASTPAYMENTREF.columnName()).contains("500021"))
            assert(invoiceTable.get(0).getProperty(InvoiceIdentifier.INVOICEID.columnName()).contains("500012"))
            assert(invoiceTable.get(0).getProperty(InvoiceIdentifier.TAXTOTAL.columnName()).contains("1950.85"))
            assert(invoiceTable.get(0).getProperty(InvoiceIdentifier.OUTSTANDINGINVOICEAMOUNT.columnName()).contains("401567.37"))
            assert(invoiceTable.get(0).getProperty(InvoiceIdentifier.PAYMENTTERMS.columnName()).contains("1%"))
            assert(invoiceTable.get(0).getProperty(InvoiceIdentifier.INVOICETYPE.columnName()).contains("Sales"))
            assert(invoiceTable.get(0).getProperty(InvoiceIdentifier.TOTAL.columnName()).contains("401567.37"))
            assert(invoiceTable.get(0).getProperty(InvoiceIdentifier.ISSUEDDATE.columnName()).contains("Dec 5"))
            assert(invoiceTable.get(0).getProperty(InvoiceIdentifier.CURRENTSTATUS.columnName()).contains("Issued"))
            assert(invoiceTable.get(0).getProperty(InvoiceIdentifier.SELFASSESSEDTOTAL.columnName()).contains("0"))
        }
    }

    it should "return a list when searching by invoice Ref" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){
            val invoiceRef = "http://172.24.0.103:80/api/invoice/403b6385-4f5f-4f69-abaf-c13cd206b79f?token=e932cbd5-ea1b-40cb-a2b9-28bf0fb4cd2c&format=json" // This is used to grab details of a price!

            val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
            import cycle._

            context.checking( // Set the context of the mock
                new Expectations() {
                    allowing(mockClient).setHolder("/invoice")

                    oneOf(mockClient).setFlag("invoiceReference", invoiceRef)

                    oneOf(mockClient).setFlag("offset", "0")
                    oneOf(mockClient).setFlag("max", maxOffset)

                     exactly(1).of(mockClient).getResponseAsJson()
                         will(returnValue(fakeInvoiceItemsJsonResponse))
                }
            )

            // Test the system:
            val invoiceWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.INVOICE)
            invoiceWS.setClient(mockClient)

            val offset = 0
            // .searchInvoice(currentOffset, invoiceReference, payRec, invoiceTypeCode, invoiceStatus, dateType, fromDate, toDate)
            invoiceWS.searchInvoice(offset, invoiceRef, "", "", "", "", "", "")
            val invoiceTable = invoiceWS.getTable()

            // This test is concerned with server queries as oppose to the returned results 
            // (the blank-search test case handles this) so no assertions are needed
        }
    }

    it should "return a list when searching for \"Payment\" or \"Receive\" invoices" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){

            val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
            import cycle._

            context.checking( // Set the context of the mock
                new Expectations() {
                    allowing(mockClient).setHolder("/invoice")

                    oneOf(mockClient).setFlag("payRecType", "PAY")

                    oneOf(mockClient).setFlag("offset", "0")
                    oneOf(mockClient).setFlag("max", maxOffset)

                     exactly(1).of(mockClient).getResponseAsJson()
                         will(returnValue(fakeInvoiceItemsJsonResponse))
                }
            )

            // Test the system:
            val invoiceWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.INVOICE)
            invoiceWS.setClient(mockClient)

            val offset = 0
            // .searchInvoice(currentOffset, invoiceReference, payRec, invoiceTypeCode, invoiceStatus, dateType, fromDate, toDate)
            invoiceWS.searchInvoice(offset, "", "PAY", "", "", "", "", "")
            val invoiceTable = invoiceWS.getTable()

            // This test is concerned with server queries as oppose to the returned results 
            // (the blank-search test case handles this) so no assertions are needed
        }
    }

    it should "return a list when searching by invoice Type" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){
            val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
            import cycle._

            context.checking( // Set the context of the mock
                new Expectations() {
                    allowing(mockClient).setHolder("/invoice")

                    oneOf(mockClient).setFlag("invoiceTypeCode", InvoiceIdentifier.invoiceTypes()(0).toString())

                    oneOf(mockClient).setFlag("offset", "0")
                    oneOf(mockClient).setFlag("max", maxOffset)

                     exactly(1).of(mockClient).getResponseAsJson()
                         will(returnValue(fakeInvoiceItemsJsonResponse))
                }
            )

            // Test the system:
            val invoiceWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.INVOICE)
            invoiceWS.setClient(mockClient)

            val offset = 0
            val invoiceType = InvoiceIdentifier.invoiceTypes()(0)   // Retrieves an invoice type from the identifier's methods
            // .searchInvoice(currentOffset, invoiceReference, payRec, invoiceTypeCode, invoiceStatus, dateType, fromDate, toDate)
            invoiceWS.searchInvoice(offset, "", "", invoiceType, "", "", "", "")
            val invoiceTable = invoiceWS.getTable()

            // This test is concerned with server queries as oppose to the returned results 
            // (the blank-search test case handles this) so no assertions are needed
        }
    }

    it should "return a list when searching by invoice Status" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){
            val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
            import cycle._

            context.checking( // Set the context of the mock
                new Expectations() {
                    allowing(mockClient).setHolder("/invoice")

                    oneOf(mockClient).setFlag("invoiceStatus", InvoiceIdentifier.invoiceStatus()(0).toString())

                    oneOf(mockClient).setFlag("offset", "0")
                    oneOf(mockClient).setFlag("max", maxOffset)

                     exactly(1).of(mockClient).getResponseAsJson()
                         will(returnValue(fakeInvoiceItemsJsonResponse))
                }
            )

            // Test the system:
            val invoiceWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.INVOICE)
            invoiceWS.setClient(mockClient)

            val offset = 0
            val invoiceStatus = InvoiceIdentifier.invoiceStatus()(0)   // Retrieves an invoice status from the identifier's methods
            // .searchInvoice(currentOffset, invoiceReference, payRec, invoiceTypeCode, invoiceStatus, dateType, fromDate, toDate)
            invoiceWS.searchInvoice(offset, "", "", "", invoiceStatus, "", "", "")
            val invoiceTable = invoiceWS.getTable()

            // This test is concerned with server queries as oppose to the returned results 
            // (the blank-search test case handles this) so no assertions are needed
        }
    }

    it should "return a list when searching by invoice Date Type" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){
            val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
            import cycle._

            context.checking( // Set the context of the mock
                new Expectations() {
                    allowing(mockClient).setHolder("/invoice")

                    oneOf(mockClient).setFlag("dateType", InvoiceIdentifier.invoiceDateTypes()(0).toString())

                    oneOf(mockClient).setFlag("offset", "0")
                    oneOf(mockClient).setFlag("max", maxOffset)

                     exactly(1).of(mockClient).getResponseAsJson()
                         will(returnValue(fakeInvoiceItemsJsonResponse))
                }
            )

            // Test the system:
            val invoiceWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.INVOICE)
            invoiceWS.setClient(mockClient)

            val offset = 0
            val invoiceDateType = InvoiceIdentifier.invoiceDateTypes()(0).toString()   // Retrieves an invoice date type from the identifier's methods
            // .searchInvoice(currentOffset, invoiceReference, payRec, invoiceTypeCode, invoiceStatus, dateType, fromDate, toDate)
            invoiceWS.searchInvoice(offset, "", "", "", "", invoiceDateType, "", "")
            val invoiceTable = invoiceWS.getTable()

            // This test is concerned with server queries as oppose to the returned results 
            // (the blank-search test case handles this) so no assertions are needed
        }
    }

    it should "return a list when searching invoices in between specified dates" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){
            val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
            import cycle._

            context.checking( // Set the context of the mock
                new Expectations() {
                    allowing(mockClient).setHolder("/invoice")

                    oneOf(mockClient).setFlag("fromDate", "Jan 01, 2005 12:00:00 AM")
                    oneOf(mockClient).setFlag("toDate", "Jan 01, 2015 12:00:00 AM")

                    oneOf(mockClient).setFlag("offset", "0")
                    oneOf(mockClient).setFlag("max", maxOffset)

                     exactly(1).of(mockClient).getResponseAsJson()
                         will(returnValue(fakeInvoiceItemsJsonResponse))
                }
            )

            // Test the system:
            val invoiceWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.INVOICE)
            invoiceWS.setClient(mockClient)

            val offset = 0
            val fromDate = "01/01/2005"
            val toDate = "01/01/2015"
            // .searchInvoice(currentOffset, invoiceReference, payRec, invoiceTypeCode, invoiceStatus, dateType, fromDate, toDate)
            invoiceWS.searchInvoice(offset, "", "", "", "", "", fromDate, toDate)
            val invoiceTable = invoiceWS.getTable()

            // This test is concerned with server queries as oppose to the returned results 
            // (the blank-search test case handles this) so no assertions are needed
        }
    }

    // it should "return a list when searching by invoice Counterparty" in withRestClient { (mockClient, context) =>
    //     running(FakeApplication()){

    //         val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
    //         import cycle._

    //         context.checking( // Set the context of the mock
    //             new Expectations() {
    //                 //insert expectations
    //             }
    //         )
    //     }
    // }

    // it should "return a list when searching by invoice Business Unit" in withRestClient { (mockClient, context) =>
    //     running(FakeApplication()){

    //         val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
    //         import cycle._

    //         context.checking( // Set the context of the mock
    //             new Expectations() {
    //                 //insert expectations
    //             }
    //         )
    //     }
    // }

    // it should "return a list when searching by invoice ID" in withRestClient { (mockClient, context) =>
    //     running(FakeApplication()){

    //         val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
    //         import cycle._

    //         context.checking( // Set the context of the mock
    //             new Expectations() {
    //                 //insert expectations
    //             }
    //         )
    //     }
    // }

    // it should "return a list when searching by invoice BOL/Ticket #" in withRestClient { (mockClient, context) =>
    //     running(FakeApplication()){

    //         val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
    //         import cycle._

    //         context.checking( // Set the context of the mock
    //             new Expectations() {
    //                 //insert expectations
    //             }
    //         )
    //     }
    // }

    // it should "return a list when searching by invoice PO #" in withRestClient { (mockClient, context) =>
    //     running(FakeApplication()){

    //         val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
    //         import cycle._

    //         context.checking( // Set the context of the mock
    //             new Expectations() {
    //                 //insert expectations
    //             }
    //         )
    //     }
    // }

    // it should "return a list when searching invoice by Item" in withRestClient { (mockClient, context) =>
    //     running(FakeApplication()){

    //         val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
    //         import cycle._

    //         context.checking( // Set the context of the mock
    //             new Expectations() {
    //                 //insert expectations
    //             }
    //         )
    //     }
    // }

    // it should "return a list when searching by invoice Deal" in withRestClient { (mockClient, context) =>
    //     running(FakeApplication()){

    //         val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
    //         import cycle._

    //         context.checking( // Set the context of the mock
    //             new Expectations() {
    //                 //insert expectations
    //             }
    //         )
    //     }
    // }

    // it should "return a list when searching by invoice Deal Line" in withRestClient { (mockClient, context) =>
    //     running(FakeApplication()){

    //         val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
    //         import cycle._

    //         context.checking( // Set the context of the mock
    //             new Expectations() {
    //                 //insert expectations
    //             }
    //         )
    //     }
    // }

    // it should "return a list when searching by invoice Account" in withRestClient { (mockClient, context) =>
    //     running(FakeApplication()){

    //         val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
    //         import cycle._

    //         context.checking( // Set the context of the mock
    //             new Expectations() {
    //                 //insert expectations
    //             }
    //         )
    //     }
    // }

    // it should "return a list when searching by invoice Currency" in withRestClient { (mockClient, context) =>
    //     running(FakeApplication()){

    //         val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
    //         import cycle._

    //         context.checking( // Set the context of the mock
    //             new Expectations() {
    //                 //insert expectations
    //             }
    //         )
    //     }
    // }

    // it should "return a list when searching by invoice Mode" in withRestClient { (mockClient, context) =>
    //     running(FakeApplication()){

    //         val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
    //         import cycle._

    //         context.checking( // Set the context of the mock
    //             new Expectations() {
    //                 //insert expectations
    //             }
    //         )
    //     }
    // }

    // it should "return a list when searching by invoice Payment Mode" in withRestClient { (mockClient, context) =>
    //     running(FakeApplication()){

    //         val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
    //         import cycle._

    //         context.checking( // Set the context of the mock
    //             new Expectations() {
    //                 //insert expectations
    //             }
    //         )
    //     }
    // }

    // it should "return a list when searching by invoice's Total \"to\"" in withRestClient { (mockClient, context) =>
    //     running(FakeApplication()){

    //         val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
    //         import cycle._

    //         context.checking( // Set the context of the mock
    //             new Expectations() {
    //                 //insert expectations
    //             }
    //         )
    //     }
    // }

    // it should "return a list when searching by invoice's Total \"from\"" in withRestClient { (mockClient, context) =>
    //     running(FakeApplication()){

    //         val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
    //         import cycle._

    //         context.checking( // Set the context of the mock
    //             new Expectations() {
    //                 //insert expectations
    //             }
    //         )
    //     }
    // }

    it should "sort the Invoice table by columns alphabetically in both A-Z and Z-A" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){
            val cycle = new JMockCycle
            import cycle._

            context.checking(
                new Expectations() {
                    allowing(mockClient).setHolder("/invoice")

                    oneOf(mockClient).setFlag("offset", "0")
                    oneOf(mockClient).setFlag("max", maxOffset)

                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeInvoiceItemsJsonResponse))
                }
            )

            // Test the system:
            val invoiceWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.INVOICE)
            invoiceWS.setClient(mockClient)

            val offset = 0
            // .searchInvoice(currentOffset, invoiceReference, payRec, invoiceTypeCode, invoiceStatus, dateType, fromDate, toDate)
            invoiceWS.searchInvoice(offset, "", "", "", "", "", "", "")
            //val invoiceTable = invoiceWS.getTable()

            // Sort the Counter Party column
            var invoiceTable = invoiceWS.sort(InvoiceIdentifier.COUNTERPARTY.columnName())    // This should sort the table A-Z
            var asciiChar = ((invoiceTable.get(0).getProperty(InvoiceIdentifier.COUNTERPARTY.columnName()).charAt(0))).toInt
            assert(((asciiChar>=65)&&(asciiChar<=70))           //We check the sorted table starts between A-F
                    || ((asciiChar>=97)&&(asciiChar<=102)))

            invoiceTable = invoiceWS.sort(InvoiceIdentifier.COUNTERPARTY.columnName())    // This should sort the table Z-A
            asciiChar = ((invoiceTable.get(0).getProperty(InvoiceIdentifier.COUNTERPARTY.columnName())).charAt(0)).toInt
            assert(((asciiChar>=82)&&(asciiChar<=90))           //We check the sorted table starts between R-Z
                    || ((asciiChar>=114)&&(asciiChar<=122)))
        }
    }

    it should "display the middle details pane" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){
            val invoiceObjID = "65730d76-9870-4162-85cb-a8a10f6f8cca"

            val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
            import cycle._

            context.checking( // Set the context of the mock
                new Expectations() {
                    allowing(mockClient).setHolder("/invoice/"+invoiceObjID)

                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeInvoiceItemJsonResponse))
                }
            )

            // Test the system:
            val invoiceWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.INVOICE)
            invoiceWS.setClient(mockClient)

            // Fetch the details of the specified invoice object using its ID
            val invoiceDetails = invoiceWS.filterDetails(invoiceObjID)

            // Ensure the details are correctly parsed
            //println(invoiceDetails.getPropertyMap())
            assert(invoiceDetails.getProperty(InvoiceIdentifier.BUADDRESSDESCRIPTION.columnName()).contains("440 - 2nd Ave."))
            assert(invoiceDetails.getProperty(InvoiceIdentifier.ACCOUNT.columnName()).contains("Accounts"))
            assert(invoiceDetails.getProperty(InvoiceIdentifier.DISCOUNTAMOUNT.columnName()).contains("9242.29"))
            assert(invoiceDetails.getProperty(InvoiceIdentifier.REMITADDRESS.columnName()).contains("440 - 2nd"))
            assert(invoiceDetails.getProperty(InvoiceIdentifier.INVOICEREF.columnName()).contains("NE Mktg-1"))
            assert(invoiceDetails.getProperty(InvoiceIdentifier.DISCOUNTDATE.columnName()).contains("Nov 17"))
            assert(invoiceDetails.getProperty(InvoiceIdentifier.BUCONTACT.columnName()).contains("Sky"))
            assert(invoiceDetails.getProperty(InvoiceIdentifier.INVOICEPAYREC.columnName()).contains("Receivable"))
            assert(invoiceDetails.getProperty(InvoiceIdentifier.FORMAT.columnName()).contains("Standard"))
            assert(invoiceDetails.getProperty(InvoiceIdentifier.BU.columnName()).contains("Northern Gas"))
            assert(invoiceDetails.getProperty(InvoiceIdentifier.PRETAXTOTAL.columnName()).contains("920326.39"))
            assert(invoiceDetails.getProperty(InvoiceIdentifier.CURRENCY.columnName()).contains("U.S. Dollars"))
            //assert(invoiceDetails.getProperty(InvoiceIdentifier.CREATEUSER.columnName()).contains(""))
            assert(invoiceDetails.getProperty(InvoiceIdentifier.PAYMENTMODE.columnName()).contains("Check"))
            assert(invoiceDetails.getProperty(InvoiceIdentifier.DUEDATE.columnName()).contains("Nov 21, 2000"))
            assert(invoiceDetails.getProperty(InvoiceIdentifier.CPADDRESS.columnName()).contains("13105 Northwest Freeway"))
            //assert(invoiceDetails.getProperty(InvoiceIdentifier.RECEIPTDATE.columnName()).contains(""))
            assert(invoiceDetails.getProperty(InvoiceIdentifier.ADJUSTMENTAMOUNT.columnName()).contains("0"))
            assert(invoiceDetails.getProperty(InvoiceIdentifier.MODE.columnName()).contains("Hard Copy"))
            assert(invoiceDetails.getProperty(InvoiceIdentifier.INVOICEID.columnName()).contains("500008"))
            assert(invoiceDetails.getProperty(InvoiceIdentifier.CPDESCRIPTION.columnName()).contains("Amerigas"))
            assert(invoiceDetails.getProperty(InvoiceIdentifier.TAXTOTAL.columnName()).contains("3903.02"))
            assert(invoiceDetails.getProperty(InvoiceIdentifier.PAYMENTTERMS.columnName()).contains("1%"))
            assert(invoiceDetails.getProperty(InvoiceIdentifier.INVOICETYPE.columnName()).contains("Sales"))
            assert(invoiceDetails.getProperty(InvoiceIdentifier.TOTAL.columnName()).contains("924229.41"))
            assert(invoiceDetails.getProperty(InvoiceIdentifier.ISSUEDDATE.columnName()).contains("Nov 7, 1999"))
            assert(invoiceDetails.getProperty(InvoiceIdentifier.CURRENTSTATUS.columnName()).contains("Paid"))
            assert(invoiceDetails.getProperty(InvoiceIdentifier.CPCONTACT.columnName()).contains("Bartells"))
            assert(invoiceDetails.getProperty(InvoiceIdentifier.SELFASSESSEDTOTAL.columnName()).contains("0"))
        }
    }

    it should "display the bottom-most details pane" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){
            val invoiceObjID = "65730d76-9870-4162-85cb-a8a10f6f8cca"

            val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
            import cycle._

            context.checking( // Set the context of the mock
                new Expectations() {
                    allowing(mockClient).setHolder("/invoice/"+invoiceObjID)

                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeDetailedInvoiceItemJsonResponse))
                }
            )

            // Test the system:
            val invoiceWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.INVOICE)
            invoiceWS.setClient(mockClient)

            // Fetch the details of the specified invoice object using its ID
            val invoiceDetails = invoiceWS.filterDetailsBottomPane(invoiceObjID)

            //println(invoiceDetails.get(0).getPropertyMap())
            assert(invoiceDetails.get(0).getProperty(InvoiceIdentifier.SOURCEITEM.columnName()).contains("C3o"))
            //TODO: assert the rest of the fields
        }
    }
}