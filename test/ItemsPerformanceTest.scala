package suites

import org.scalatest.junit.AssertionsForJUnit
import org.scalatest._
import org.junit.Assert._
import java.util.concurrent.TimeUnit
import org.openqa.selenium._
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.support.ui.Select
import scala.util.control.Breaks._

/**
 * READ ME (Last updated 16/3/2015):
 * The following tests uses ScalaTest in conjunction with Selenium to 
 * simulate a web browser to test the web app's behaviour, as well as
 * verifying the information. Tests are structured in FlatSpec format,
 * which follows BDD(Behavior-Driven Design) principles.
 * 
 * To compile these tests you need to have the following lines added to your SBT build file:
 * libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.1" % "test"
 * libraryDependencies += "org.seleniumhq.selenium" % "selenium-java" % "2.44.0" % "test"
 * 
 * As well you need the FireFox Selenium IDE plugin: http://docs.seleniumhq.org/download/ ver.2.9.0 or later
 *
 * To run these tests you simply type <sbt "test-only suites.PerformanceTest"> while the program is running in the background.
 * NOTE: You need to have quotation marks or else the whole suite will run!
 *
 */ 


 /**
  * The following sets up the setup and teardown methods before tests begin
  * and after all tests are complete.
  */
trait ItemsDriver extends BeforeAndAfterAll { this: Suite =>
	val driver: WebDriver = new FirefoxDriver()
	val baseUrl = "http://localhost:9000/"
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS)

	override def beforeAll() {
		super.beforeAll()
	}
	
	override def afterAll() {
		try super.afterAll()catch{case e:Exception=>}
		driver.quit()
	}
} 
 
class ItemsPerformanceTest extends FlatSpec with ItemsDriver{
	
	/**
	 * The following method checks to see if an HTML element is  present.
	 * This method was taken out of Selenium 2.0, but existed in earlier versions.
	 * It is used to find elements that xpath cannot find, such as the data table!
	 */
	def isElementPresent(id: By) : Boolean = {
		try {
			driver.findElement(id)
		} catch {
			case e:Exception => return false
		}
		return true
	}
		
	//=======================================================
	behavior of "Worst-Case:"
	//=======================================================
	it should "Log the user into application" in {
		
		// Opens up the website and auto-logs the user in
		driver.get(baseUrl + "login")
    	driver.findElement(By.name("username")).clear()
   		driver.findElement(By.name("username")).sendKeys("webtest")
    	driver.findElement(By.name("pass")).clear()
    	driver.findElement(By.name("pass")).sendKeys("entero")
    	driver.findElement(By.cssSelector("input.login-button")).click()

		assert(true)
	}

	it should "search all ItemGroups for a non-existing Item" in {
		
		driver.get(baseUrl)
		var start = System.currentTimeMillis() // Time the execution in milliseconds

		// Open up the Item workspace and search for a non-existing Item
		driver.findElement(By.cssSelector("img[alt=\"Item\"]")).click()
		driver.findElement(By.id("ws-item-searchparam-description")).clear()
    	driver.findElement(By.id("ws-item-searchparam-description")).sendKeys("DoesNotExist")
    	driver.findElement(By.xpath("(//input[@value='Search'])[3]")).click()
				
		// Checks if a blank table is returned
		var itemExists = false
		var second = 0
		while ((second <= 120) && (!itemExists)) {
			try { if ("Item code Item group Long description Short description".equals(driver.findElement(By.id("tab-content-0")).getText()))itemExists=true}catch{case e:Exception =>}
			Thread sleep 500
			second = second + 1
		}
		if(itemExists==false) assert(false)
		else {
			driver.findElement(By.id("ws-item-searchparam-description")).clear()
			var finish = System.currentTimeMillis()
			var totalTime = (finish - start)/1000.0
			System.out.print("Time: "+totalTime+"secs ")
			Thread sleep 100
			assert(true)
		}
	}
	
	it should "search all but one ItemGroup" in {
	
		driver.get(baseUrl)
		var start = System.currentTimeMillis() // Time the execution in milliseconds

		driver.findElement(By.xpath("//div[3]/span")).click()
		
		// We wait and verify that the ItemGroup checkbox-menu is fully loaded
		var itemExists = false
		var second = 0
		while ((second <= 120) && (!itemExists)) {
			if (isElementPresent(By.xpath("//div[@id='itemGroupParams']/table/tbody/tr/td[2]/div"))) {itemExists = true}
			Thread sleep 500
			second = second + 1
		}
		
		// De-select an ItemGroup and begin searching
		driver.findElement(By.xpath("//div[@id='itemGroupParams']/table/tbody/tr/td[2]/div")).click()
    	driver.findElement(By.id("multi-param-1")).click()
    	driver.findElement(By.xpath("(//input[@value='Search'])[3]")).click()
		
		// Wait for the table to load
		itemExists = false
		second = 0
		while ((second <= 120) && (!itemExists)) {
			try{if(isElementPresent(By.xpath("//div[@id='tab-content-0']/table/tbody/tr/td[2]"))) itemExists = true}catch{case e:Exception =>}
			Thread sleep 500
			second = second + 1
		}
		if(itemExists==false){assert(false)}
		else{
			var finish = System.currentTimeMillis()
			var totalTime = (finish - start)/1000.0
			System.out.print("Time: "+totalTime+"secs ")
			Thread sleep 100
			assert(true)	
		}
	}

	it should "search all but one ItemGroup for a non-existing item" in {
		
		driver.get(baseUrl)
		var start = System.currentTimeMillis() // Time the execution in milliseconds
		
		driver.findElement(By.xpath("//div[3]/span")).click()
		
		// We wait and verify that the ItemGroup checkbox-menu is fully loaded
		var itemExists = false
		var second = 0
		while ((second <= 120) && (!itemExists)) {
			if (isElementPresent(By.xpath("//div[@id='itemGroupParams']/table/tbody/tr/td[2]/div"))) {itemExists = true}
			Thread sleep 500
			second = second + 1
		}
		
		// De-select an ItemGroup, type a description and begin searching
		driver.findElement(By.id("ws-item-searchparam-description")).clear()
    	driver.findElement(By.id("ws-item-searchparam-description")).sendKeys("DoesNotExist")
		driver.findElement(By.xpath("//div[@id='itemGroupParams']/table/tbody/tr/td[2]/div")).click()
    	driver.findElement(By.id("multi-param-1")).click()
    	driver.findElement(By.xpath("(//input[@value='Search'])[3]")).click()
		
		// Wait for the empty table to load
		itemExists = false
		second = 0
		while ((second <= 120) && (!itemExists)) {
			try { if (isElementPresent(By.id("tab-content-0")))itemExists=true}catch{case e:Exception =>}
			Thread sleep 500
			second = second + 1
		}
		if(itemExists==false){assert(false)}
		else{
			var finish = System.currentTimeMillis()
			var totalTime = (finish - start)/1000.0
			System.out.print("Time: "+totalTime+"secs ")
			Thread sleep 100
			assert(true)	
		}
	}
	
	it should "search all but one ItemGroup and load up the last item in the search result table" in {
		
		driver.get(baseUrl)
		var start = System.currentTimeMillis() // Time the execution in milliseconds
		
		driver.findElement(By.xpath("//div[3]/span")).click()
		
		// Wait and verify that the ItemGroup checkbox-menu is fully loaded
		var itemExists = false
		var second = 0
		while ((second <= 120) && (!itemExists)) {
			if (isElementPresent(By.xpath("//div[@id='itemGroupParams']/table/tbody/tr/td[2]/div"))) {itemExists = true}
			Thread sleep 500
			second = second + 1
		}
		
		// De-select an ItemGroup and begin searching
		driver.findElement(By.xpath("//div[@id='itemGroupParams']/table/tbody/tr/td[2]/div")).click()
    	driver.findElement(By.id("multi-param-1")).click()
    	driver.findElement(By.xpath("(//input[@value='Search'])[3]")).click()
		
		// Wait for the table to load and find the 191th item in the search result list
		itemExists = false
		second = 0
		while ((second <= 120) && (!itemExists)) {
			if (isElementPresent(By.xpath("//div[@id='tab-content-0']/table/tbody/tr[190]/td[2]"))) {itemExists = true}
			Thread sleep 500
			second = second + 1
		}
		if(itemExists==false){assert(false)}
		else{
			driver.findElement(By.id("ws-item-searchparam-description")).clear()
			var finish = System.currentTimeMillis()
			var totalTime = (finish - start)/1000.0
			System.out.print("Time: "+totalTime+"secs ")
			Thread sleep 100
			assert(true)	
		}
	}
	
	it should "search all but one ItemGroup and sort the returned list of items from the server alphabetically" in {
		
		driver.get(baseUrl)
		var start = System.currentTimeMillis() // Time the execution in milliseconds

		driver.findElement(By.xpath("//div[3]/span")).click()

		// Wait and verify that the ItemGroup checkbox-menu is fully loaded
		var itemExists = false
		var second = 0
		while ((second <= 120) && (!itemExists)) {
			if (isElementPresent(By.xpath("//div[@id='itemGroupParams']/table/tbody/tr/td[2]/div"))) {itemExists = true}
			Thread sleep 500
			second = second + 1
		}
		
		// De-select an ItemGroup and begin searching
		driver.findElement(By.xpath("//div[@id='itemGroupParams']/table/tbody/tr/td[2]/div")).click()
    	driver.findElement(By.id("multi-param-1")).click()
    	driver.findElement(By.xpath("(//input[@value='Search'])[3]")).click()

    	// Wait for the table to load and find the first element of the list
		itemExists = false
		second = 0
		while ((second <= 120) && (!itemExists)) {
			if (isElementPresent(By.id("tab-content-0"))) {itemExists = true}
			Thread sleep 500
			second = second + 1
		}

		// We check that the third item from the top starts between "A" to "C"
		var asciiChar = (driver.findElement(By.xpath("//div[@id='tab-content-0']/table/tbody/tr[3]/td[2]")).getText()).charAt(0).toInt
		if(((asciiChar<68)&&(asciiChar>64))||((asciiChar<100)&&(asciiChar>96))){assert(true)}
			else{assert(false)}

		// We sort the items' ItemCode in descending order (Z-A)
		driver.findElement(By.xpath("//th[@onclick=\"getWorkspaceManager().getActiveWorkspace().sortBy('Item code');\"]")).click()

		// Wait for the table to load and find the first element of the list
		itemExists = false
		second = 0
		while ((second <= 120) && (!itemExists)) {
			if (driver.findElement(By.xpath("//div[@id='tab-content-0']/table/tbody/tr/td[2]")).getText()!="") {itemExists = true}
			else {driver.findElement(By.xpath("//th[@onclick=\"getWorkspaceManager().getActiveWorkspace().sortBy('Item code');\"]")).click()}
			Thread sleep 500
			second = second + 1
		}

		// We check if the item at the top of the list starts from "P" to "Z"
		asciiChar = (driver.findElement(By.xpath("//div[@id='tab-content-0']/table/tbody/tr/td[2]")).getText()).charAt(0).toInt
		itemExists = false
		second = 0
		while ((second <= 120) && (!itemExists)) {
			if(((asciiChar<91)&&(asciiChar>80))||((asciiChar<123)&&(asciiChar>112))){itemExists=true}
			Thread sleep 500
			second = second + 1
		}
		if(itemExists==false){assert(false)}
		else{
			var finish = System.currentTimeMillis()
			var totalTime = (finish - start)/1000.0
			System.out.print("Time: "+totalTime+"secs ")
			Thread sleep 100
			assert(true)	
		}
	}

	it should "search all but one ItemGroup, load up the last item in the search result table and display the item details" in {
		
		driver.get(baseUrl)
		var start = System.currentTimeMillis() // Time the execution in milliseconds
		
		driver.findElement(By.xpath("//div[3]/span")).click()
		
		// Wait and verify that the ItemGroup checkbox-menu is fully loaded
		var itemExists = false
		var second = 0
		while ((second <= 120) && (!itemExists)) {
			if (isElementPresent(By.xpath("//div[@id='itemGroupParams']/table/tbody/tr/td[2]/div"))) {itemExists = true}
			Thread sleep 500
			second = second + 1
		}
		
		// De-select an ItemGroup and begin searching
		driver.findElement(By.xpath("//div[@id='itemGroupParams']/table/tbody/tr/td[2]/div")).click()
    	driver.findElement(By.id("multi-param-1")).click()
    	driver.findElement(By.xpath("(//input[@value='Search'])[3]")).click()
		
		// Wait for the table to load and find the 190th item in the search result list
		itemExists = false
		second = 0
		while ((second <= 120) && (!itemExists)) {
			if (isElementPresent(By.xpath("//div[@id='tab-content-0']/table/tbody/tr[190]/td[2]"))) {itemExists = true}
			Thread sleep 500
			second = second + 1
		}

		// Select the bottom-most item
		driver.findElement(By.xpath("//div[@id='tab-content-0']/table/tbody/tr[190]/td[2]")).click()

		// Wait and verify that the details are properly loaded
		itemExists = false
		second = 0
		while ((second <= 120) && (!itemExists)) {
			if (driver.findElement(By.cssSelector("input.styled-input")).getAttribute("value")!="") {itemExists = true}
			Thread sleep 500
			second = second + 1
		}
		if(itemExists==false){assert(false)}
		else{
			driver.findElement(By.id("ws-item-searchparam-description")).clear()
			var finish = System.currentTimeMillis()
			var totalTime = (finish - start)/1000.0
			System.out.print("Time: "+totalTime+"secs ")
			Thread sleep 100
			assert(true)	
		}
	}
}