package suites

import com.entero.identifiers.LocationIdentifier
import com.entero.rest.RestClient
import com.entero.workspace.Workspace
import com.entero.workspace.WorkspaceFactory
import com.entero.workspace.WorkspaceFactory.Endpoint
import models.EnteroObject

// Used to create fake JSON responses
import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper

// Play testing tools
import play.api.test.Helpers.running
import play.api.test.FakeApplication

// ScalaTest utils
import collection.JavaConversions._
import collection.mutable._
import org.scalatest.junit.AssertionsForJUnit
import org.scalatest.PrivateMethodTester._
import org.scalatest._
import org.scalatest.mock.{JMockCycleFixture, JMockCycle}

// Jmock tools
import org.scalatest.mock.JMockCycle
import org.jmock.Expectations
import org.jmock.Expectations.returnValue
import org.scalatest.mock.JMockExpectations
import org.jmock.Mockery
import org.jmock.lib.legacy.ClassImposteriser
import org.jmock.lib.concurrent.Synchroniser

/**
 * READ ME (Last updated 24/11/2014):
 * The following tests uses ScalaTest in conjunction with Selenium to 
 * simulate a web browser to test the web app's behaviour, as well as
 * verifying the information. Tests are structured in FlatSpec format,
 * which follows BDD(Behavior-Driven Design) principles.
 * 
 * To compile these tests you need to have the following lines added to your SBT build file:
 * libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.1" % "test"
 * libraryDependencies += "org.seleniumhq.selenium" % "selenium-java" % "2.44.0" % "test"
 *
 * To run these tests you simply type "test" in your sbt terminal.
 *
 */
class LocationBackendTest extends FlatSpec {
    
    val maxOffset = "100"   // Represents the maximum number of results for searching
    val mapper = new ObjectMapper()

    /** This defines the fixture method to create mock RestClient objects!
     *  Search "Loan-Fixture" Method for more information.
     */
    def withRestClient(testCode: (RestClient, Mockery) => Any) {
        
        // This creates a context object from JMock
        var context = new Mockery
        context.setImposteriser(ClassImposteriser.INSTANCE)
        context.setThreadingPolicy(new Synchroniser()) //This prevents race conditions with mock objects!

        var mockClient = context.mock(classOf[RestClient])
        try {
            // Loans the mockClient to the tests
            testCode(mockClient, context)
        }
        finally {
            // Clean up objects
            mockClient = null
            context = null
        }
    }
    
    //=======================================================
    behavior of "Location Workspace"
    //=======================================================
    val fakeLocationItemJsonResponse = mapper.readTree(    // Fake response containing "Alberta"
    """ |[
        |    {
        |        "line1": "Alberta",
        |        "description": "Alberta",
        |        "type": "Jurisdiction",
        |        "id": "7BC3FF2E-5379-4BEB-9045-2D08DFB5D2D6",
        |        "ref": "http://172.24.0.103:80/api/location/7BC3FF2E-5379-4BEB-9045-2D08DFB5D2D6?token=a0274be3-8ee4-491f-a5ff-06c2786379e8&format=json"
        |    },
        |    {
        |        "line1": "Alberta BT 0054678",
        |        "description": "Alberta BT 0054678",
        |        "type": "Facility",
        |        "id": "3f75ea06-49ed-4ad8-8d76-f5e3a7937fe8",
        |        "ref": "http://172.24.0.103:80/api/location/3f75ea06-49ed-4ad8-8d76-f5e3a7937fe8?token=a0274be3-8ee4-491f-a5ff-06c2786379e8&format=json"
        |    },
        |    {
        |        "line1": "Alberta Beach",
        |        "description": "Alberta Beach, AB",
        |        "type": "Jurisdiction",
        |        "id": "0E774825-530A-4C0F-A3CA-2E1DE9C41C66",
        |        "ref": "http://172.24.0.103:80/api/location/0E774825-530A-4C0F-A3CA-2E1DE9C41C66?token=a0274be3-8ee4-491f-a5ff-06c2786379e8&format=json"
        |    },
        |    {
        |        "line1": "Alberta Ethane Gathering System (AEGS)",
        |        "description": "Alberta Ethane Gathering System (AEGS), AB",
        |        "type": "Facility",
        |        "id": "4e1852eb-7343-495f-a0ba-3534f7a4ca1e",
        |        "ref": "http://172.24.0.103:80/api/location/4e1852eb-7343-495f-a0ba-3534f7a4ca1e?token=a0274be3-8ee4-491f-a5ff-06c2786379e8&format=json"
        |    },
        |    {
        |        "line1": "Alberta Gas Pipeline Region",
        |        "description": "Alberta Gas Pipeline Region",
        |        "type": "Region",
        |        "id": "6b2243c8-a9e5-4806-8600-b0eb5282e2af",
        |        "ref": "http://172.24.0.103:80/api/location/6b2243c8-a9e5-4806-8600-b0eb5282e2af?token=a0274be3-8ee4-491f-a5ff-06c2786379e8&format=json"
        |    },
        |    {
        |        "line1": "Alberta MSW Feeder P/L Graphing Region",
        |        "description": "Alberta MSW Feeder P/L Graphing Region",
        |        "type": "Region",
        |        "id": "5f21c166-8d1c-4a61-afe7-81a4bc95358e",
        |        "ref": "http://172.24.0.103:80/api/location/5f21c166-8d1c-4a61-afe7-81a4bc95358e?token=a0274be3-8ee4-491f-a5ff-06c2786379e8&format=json"
        |    },
        |    {
        |        "line1": "Alberta",
        |        "description": "Alberta, VA",
        |        "type": "Jurisdiction",
        |        "id": "733084EF-E1C7-43BC-8B61-912A3A0A8480",
        |        "ref": "http://172.24.0.103:80/api/location/733084EF-E1C7-43BC-8B61-912A3A0A8480?token=a0274be3-8ee4-491f-a5ff-06c2786379e8&format=json"
        |    }
        |]""".stripMargin)

    it should "return a list of location Types" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){
            val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
            import cycle._

            context.checking( // Set the context of the mock
                new Expectations() {

                }
            )

            // Test the system:
            val locationWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.LOCATION)
            locationWS.setClient(mockClient)

            val locationTypes = locationWS.getLocationTypes()
            assert(locationTypes(0).toString().contains(LocationIdentifier.locationTypes()(0).toString()))
        }
    }

    it should "return an empty list of locations when blank searching" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){
            val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
            import cycle._

            context.checking( // Set the context of the mock
                new Expectations() {
                    //insert expectations
                }
            )

            // Test the system:
            val locationWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.LOCATION)
            locationWS.setClient(mockClient)
            
            val locations = locationWS.searchLocations("", ArrayBuffer(""))    // .searchLocations(locationDescription, selectedLocationType)
            assert(locations.size()==0)
        }
    }

    it should "return a list of locations when searching by location Description" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){
            val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
            import cycle._

            context.checking( // Set the context of the mock
                new Expectations() {
                    allowing(mockClient).setHolder("/location")

                    oneOf(mockClient).setFlag("offset", "0")
                    oneOf(mockClient).setFlag("max", maxOffset)

                    oneOf(mockClient).setFlag("locationType", "")
                    oneOf(mockClient).setFlag("description", "Alberta")

                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeLocationItemJsonResponse))
                }
            )

            // Test the system:
            val locationWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.LOCATION)
            locationWS.setClient(mockClient)
            
            val locations = locationWS.searchLocations("Alberta", ArrayBuffer(""))    // .searchLocations(locationDescription, selectedLocationType)
            //println(locations.get(0).getPropertyMap())
            assert(locations.get(0).getProperty(LocationIdentifier.LOCATIONDESC.columnName()).contains("Alberta"))
            assert(locations.get(0).getProperty(LocationIdentifier.ID.columnName()).contains("7BC3FF2E-5379-4BEB-9045-2D08DFB5D2D6"))
            assert(locations.get(0).getProperty(LocationIdentifier.LOCATIONTYPE.columnName()).contains("Jurisdiction"))
        }
    }

    it should "return a list of locations when searching by location Type" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){

            val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
            import cycle._

            context.checking( // Set the context of the mock
                new Expectations() {
                    //insert expectations
                }
            )

            // Test the system:
            val locationWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.LOCATION)
            locationWS.setClient(mockClient)
            
            // Search by one location type
            val locationTypes: java.util.List[String] = ArrayBuffer(LocationIdentifier.locationTypes()(0))
            val locations = locationWS.searchLocations("", locationTypes)   // .searchLocations(locationDescription, selectedLocationType)
            //println(locations.get(0).getPropertyMap())
            // assert(locations.get(0).getProperty(LocationIdentifier.LOCATIONDESC.columnName()).contains("Alberta"))
            // assert(locations.get(0).getProperty(LocationIdentifier.ID.columnName()).contains("7BC3FF2E-5379-4BEB-9045-2D08DFB5D2D6"))
            // assert(locations.get(0).getProperty(LocationIdentifier.LOCATIONTYPE.columnName()).contains("Jurisdiction"))
        }
    }

    // it should "return an empty list when searching for a non-existing location" in withRestClient { (mockClient, context) =>
    //     running(FakeApplication()){

    //         val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
    //         import cycle._

    //         context.checking( // Set the context of the mock
    //             new Expectations() {
    //                 //insert expectations
    //             }
    //         )
    //     }
    // }
}