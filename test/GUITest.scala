package guisuite

import org.scalatest.junit.AssertionsForJUnit
import org.scalatest._
import org.junit.Assert._
import org.openqa.selenium._
import org.openqa.selenium.firefox._
import org.openqa.selenium.support.ui.Select
import scala.util.control.Breaks._
import org.openqa.selenium.JavascriptExecutor

/**
 * READ ME (Last updated 24/11/2014):
 * The following tests uses ScalaTest in conjunction with Selenium to 
 * simulate a web browser to test the web app's behaviour, as well as
 * verifying the information. Tests are structured in FlatSpec format,
 * which follows BDD(Behavior-Driven Design) principles.
 * 
 * To compile these tests you need to have the following lines added to your SBT build file:
 * libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.1" % "test"
 * libraryDependencies += "org.seleniumhq.selenium" % "selenium-java" % "2.44.0" % "test"
 *
 * To run these tests you simply type "test" while the program is running in the background.
 *
 */ 
 
 /**
  * The following sets up the setup and teardown methods before tests begin
  * and after all tests are complete.
  */
trait Driver extends BeforeAndAfterAll { this: Suite =>
	val driver: WebDriver = new FirefoxDriver
	val baseUrl = "http://localhost:9000/"

	override def beforeAll() {
		super.beforeAll()
	}
	
	override def afterAll() {
		try super.afterAll()catch{case e:Exception=>}
		driver.quit()
	}
}
 
class GUITest extends FlatSpec with Driver{
	
	/**
	 * The following method checks to see if an HTML element is  present.
	 * This method was taken out of Selenium 2.0, but existed in earlier versions.
	 */
	def isElementPresent(id: By) : Boolean = {
		try {
			driver.findElement(id)
		} catch {
			case e:Exception => return false
		}
		return true
	}
	
	//=======================================================
	behavior of "Front-End: RestClient's Login System"
	//=======================================================
	it should "have the correct title" in {
		driver.get(baseUrl)
		assert("Entero One Web Application" == driver.getTitle())
	}
	
	it should "show an error message when using wrong credentials" in {
    	driver.get(baseUrl + "login")
   		driver.findElement(By.name("username")).clear()
    	driver.findElement(By.name("username")).sendKeys("wrongUsername")
    	driver.findElement(By.name("pass")).clear()
    	driver.findElement(By.name("pass")).sendKeys("invalidpassword")
    	driver.findElement(By.cssSelector("input.login-button")).click()
    	
    	assertEquals("Invalid Credentials. Please try again.", driver.findElement(By.cssSelector("div.login-msg")).getText())
	}
	
	it should "log a user in when using correct credentials" in {
		driver.get(baseUrl + "login");
   		driver.findElement(By.name("username")).clear()
   		driver.findElement(By.name("username")).sendKeys("webtest")
    	driver.findElement(By.name("pass")).clear()
    	driver.findElement(By.name("pass")).sendKeys("entero")
    	driver.findElement(By.cssSelector("input.login-button")).click()

    	//Verify toolbar is loaded
    	//try{assert(isElementPresent(By.xpath("(//input[@value='Search'])[3]")))}catch{case e: Exception => assert(false)}
    	assertEquals(baseUrl, driver.getCurrentUrl())
		//assert(false)
	}

	//it should "handle an expired token" in {
		//TODO: Return a fake expired JSON message and verify if system
		//		asks for login information and/or error message.
		
		//assert(false)
	//}
	
	//it should "handle logging a user out" in {
		//TODO: Login and logout. Verify if at login screen.
		
		//assert(false)
	//}
	
	//it should "show the user's <Role> in the system" in {
		//TODO: Login and verify if user's role is correct
		
		//assert(false)
	//}
	
	
	//=======================================================
	//behavior of "Front-End: Overall Workspace"
	//=======================================================
	//it should "have the correct title" in {
		//assert(webDriver.getTitle() === "Entero One Web Application")
	//}
	
	//it should "have a functioning <Save> button" in {
		//TODO: check if save button works and if tooltip is correct
		//assert(false);
	//}
	
	//it should "have a functioning <Print> button" in {
		//TODO: check if print button works and if tooltip is correct
		//assert(false);
	//}
	
	//it should "have a functioning <New> button" in {
		//TODO: check if new button works and if tooltip is correct
		//assert(false);
	//}
	
	//DO THE OTHER BUTTONS (paste, undo, cut, duplicate, zoom)
	
	//=======================================================
	behavior of "Front-End: Item Search Workspace"
	//=======================================================	
	/*it should "have an Item Search Pane" in {
		driver.get(baseUrl);
		assert("Items" == driver.findElement(By.xpath("//div[3]/span")).getText())
	}*/
	
	it should "search with no parameters" in {
		driver.get(baseUrl);
		driver.findElement(By.xpath("//div[3]/span")).click();
		
		//Checks if Search button exists and clicks it
		try{assert(isElementPresent(By.xpath("(//input[@value='Search'])[3]")))}catch{case e: Exception => assert(false)}
		Thread sleep 500
		driver.findElement(By.xpath("(//input[@value='Search'])[3]")).click()
		
		//Checks if Items tab is opened
		assertEquals("Items", driver.findElement(By.cssSelector("div.tab.selected")).getText())
		
		//Checks if table is populated with items
		var itemExists = false
		var second = 0
		while ((second <= 60) && (!itemExists)) {
			try{if(isElementPresent(By.cssSelector("tr.search-result-item > td"))) itemExists = true}catch{case e:Exception =>}
			Thread sleep 1000
			second = second + 1
		}
		if(itemExists==false) assert(false)
		else assert(true)
		
	}
	
	it should "handle closing search tabs" in {
		driver.get(baseUrl)
		
		//We open up an item search and verify that a tab is indeed opened
		driver.findElement(By.xpath("//div[3]/span")).click()
		Thread sleep 500
		driver.findElement(By.xpath("(//input[@value='Search'])[3]")).click()
		assertEquals("Items", driver.findElement(By.cssSelector("div.tab.selected")).getText())
		
		//We wait until the table is loaded. Test fails if timeout!
		var itemExists = false
		var second = 0
		while ((second <= 60) && (!itemExists)) {
			try{if(isElementPresent(By.cssSelector("tr.search-result-item > td"))) itemExists = true}catch{case e:Exception =>}
			Thread sleep 1000
			second = second + 1
		} 
		if(itemExists==false){assert(false)}

		//Close the tab by clicking the close button and verify it no longer exists
		//by expecting an exception when looking for the missing tab.
    	driver.findElement(By.cssSelector(".tab-close-button")).click()
    	Thread sleep 500

    	intercept[NoSuchElementException] {
    		driver.findElement(By.cssSelector("div.tab.selected")).getText()
    	}
    	
	}
	
	it should "handle searching by one \"Item Group\"" in {
		driver.get(baseUrl)
		
		//We open up the Item Search Pane and select ONLY Ammonia item group
		driver.findElement(By.xpath("//div[3]/span")).click()
		Thread sleep 500
		
		//We wait and verify that the Item Group sub-menu is fully loaded
		var itemExists = false
		var second = 0
		while ((second <= 60) && (!itemExists)) {
			if (isElementPresent(By.xpath("//div[@id='itemGroupParams']/table/tbody/tr/td[2]/div"))) {itemExists = true}
			Thread sleep 1000
			second = second + 1
		}
		
		//We open the Item Group submenu and select only Ammonia sub-group
		driver.findElement(By.xpath("//div[@id='itemGroupParams']/table/tbody/tr/td[2]/div")).click()
		Thread sleep 500
		driver.findElement(By.cssSelector("button.toggleAll")).click()
		driver.findElement(By.id("multi-param-58")).click()
		driver.findElement(By.xpath("(//input[@value='Search'])[3]")).click()
		
		//We wait until the table is loaded. Test fails if timeout!
		itemExists = false
		second = 0
		while ((second <= 60) && (!itemExists)) {
			try{if(isElementPresent(By.cssSelector("tr.search-result-item > td"))) itemExists = true}catch{case e:Exception =>}
			Thread sleep 1000
			second = second + 1
		} 
		if(itemExists==false){assert(false)}
		
		//We verify the table's results, which should contain "Anhydrous Ammonia"
		try {
			assertEquals("Anhydrous Ammonia", driver.findElement(By.cssSelector("tr.search-result-item > td:nth-child(5)")).getText())
		}catch{
			case e:Exception => println("Anhydrous Ammonia is not found!")
			fail()
		}
	}
	
	it should "handle searching by Multiple \"Item Group\"'s" in {
		driver.get(baseUrl)
		
		//We open up the Item Search Pane and select ONLY Ammonia item group
		driver.findElement(By.xpath("//div[3]/span")).click()
		Thread sleep 500
		
		//We wait and verify that the Item Group sub-menu is fully loaded
		var itemExists = false
		var second = 0
		while ((second <= 60) && (!itemExists)) {
			if (isElementPresent(By.xpath("//div[@id='itemGroupParams']/table/tbody/tr/td[2]/div"))) {itemExists = true}
			Thread sleep 1000
			second = second + 1
		}
		
		//We open the Item Group submenu and select Methane, Petroleum Coke,
		//and Propane sub-groups
		driver.findElement(By.xpath("//div[@id='itemGroupParams']/table/tbody/tr/td[2]/div")).click()
		Thread sleep 500
		driver.findElement(By.cssSelector("button.toggleAll")).click()
		driver.findElement(By.id("multi-param-31")).click()
    	driver.findElement(By.id("multi-param-49")).click()
    	driver.findElement(By.id("multi-param-38")).click()
		driver.findElement(By.xpath("(//input[@value='Search'])[3]")).click()
		
		//We wait until the table is loaded. Test fails if timeout!
		itemExists = false
		second = 0
		while ((second <= 60) && (!itemExists)) {
			try{if(isElementPresent(By.cssSelector("tr.search-result-item > td"))) itemExists = true}catch{case e:Exception =>}
			Thread sleep 1000
			second = second + 1
		} 
		if(itemExists==false){assert(false)}
		
		//We verify the table's results, which should contain "Petcoke - 20/30 mm, Methane and
		//Propane - HD5 Not Odorized under the Long description column"
		assertEquals("Petcoke - 10/20 mm", driver.findElement(By.cssSelector("tr.search-result-item:nth-child(1) > td:nth-child(5)")).getText())
		assertEquals("Methane", driver.findElement(By.cssSelector("tr.search-result-item:nth-child(3) > td:nth-child(5)")).getText())
		assertEquals("Propane - HD5 Not Odorized", driver.findElement(By.cssSelector("tr.search-result-item:nth-child(5) > td:nth-child(5)")).getText())
		
	}
	
	it should "handle searching by both Description and Multiple \"Item Group\"'s" in {
		driver.get(baseUrl)
		
		//We open up the Item Search Pane and select ONLY Ammonia item group
		driver.findElement(By.xpath("//div[3]/span")).click()
		Thread sleep 500
		
		//We wait and verify that the Item Group sub-menu is fully loaded
		var itemExists = false
		var second = 0
		while ((second <= 60) && (!itemExists)) {
			if (isElementPresent(By.xpath("//div[@id='itemGroupParams']/table/tbody/tr/td[2]/div"))) {itemExists = true}
			Thread sleep 1000
			second = second + 1
		}
		
		//We open the Item Group submenu and select Methane, Petroleum Coke,
		//and Propane sub-groups
		driver.findElement(By.xpath("//div[@id='itemGroupParams']/table/tbody/tr/td[2]/div")).click()
		Thread sleep 500
		driver.findElement(By.cssSelector("button.toggleAll")).click()
		driver.findElement(By.id("multi-param-31")).click()
    	driver.findElement(By.id("multi-param-49")).click()
    	driver.findElement(By.id("multi-param-38")).click()

    	//We type the description "Pet" in the Descrition box
    	driver.findElement(By.id("ws-item-searchparam-description")).clear();
    	driver.findElement(By.id("ws-item-searchparam-description")).sendKeys("Pet");
    	driver.findElement(By.xpath("(//input[@value='Search'])[3]")).click();

    	//We wait until the table is loaded. Test fails if timeout!
		itemExists = false
		second = 0
		while ((second <= 60) && (!itemExists)) {
			try{if(isElementPresent(By.cssSelector("tr.search-result-item > td"))) itemExists = true}catch{case e:Exception =>}
			Thread sleep 1000
			second = second + 1
		} 
		if(itemExists==false){assert(false)}
		
		//We verify the table's results, which should contain entries of Item code "PCOK"
		assertEquals("PCOK", driver.findElement(By.cssSelector("tr.search-result-item:nth-child(1) > td:nth-child(2)")).getText())
		assertEquals("PCOK", driver.findElement(By.cssSelector("tr.search-result-item:nth-child(2) > td:nth-child(2)")).getText())
	}

	it should "sort by Item code when searching by Item groups(Methanes, Propanes and Petroleum coke)" in {
		driver.get(baseUrl)
		
		//We open up the Item Search Pane and select ONLY Ammonia item group
		driver.findElement(By.xpath("//div[3]/span")).click()
		Thread sleep 500
		
		//We wait and verify that the Item Group sub-menu is fully loaded
		var itemExists = false
		var second = 0
		while ((second <= 60) && (!itemExists)) {
			if (isElementPresent(By.xpath("//div[@id='itemGroupParams']/table/tbody/tr/td[2]/div"))) {itemExists = true}
			Thread sleep 1000
			second = second + 1
		}
		
		//We open the Item Group submenu and select Methane, Petroleum Coke,
		//and Propane sub-groups
		driver.findElement(By.xpath("//div[@id='itemGroupParams']/table/tbody/tr/td[2]/div")).click()
		Thread sleep 500
		driver.findElement(By.cssSelector("button.toggleAll")).click()
		driver.findElement(By.id("multi-param-31")).click()
    	driver.findElement(By.id("multi-param-49")).click()
    	driver.findElement(By.id("multi-param-38")).click()
		driver.findElement(By.xpath("(//input[@value='Search'])[3]")).click()
		
		//We wait until the table is loaded. Test fails if timeout!
		itemExists = false
		second = 0
		while ((second <= 60) && (!itemExists)) {
			try{if(isElementPresent(By.cssSelector("tr.search-result-item > td"))) itemExists = true}catch{case e:Exception =>}
			Thread sleep 1000
			second = second + 1
		} 
		if(itemExists==false){assert(false)}

		//Click on Item code to sort in Descending order
		driver.findElement(By.xpath("//th[@onclick=\"getWorkspaceManager().getActiveWorkspace().sortBy('Item code');\"]")).click()

		//Verify that table results are sorted by Item code
		if("C1" == driver.findElement(By.cssSelector("tr.search-result-item:nth-child(1) > td:nth-child(2)")).getText())
			{assert(true)}
		else if ("PCOK" == driver.findElement(By.cssSelector("tr.search-result-item:nth-child(1) > td:nth-child(2)")).getText())
			{assert(true)}
		else {assert(false)}

		if("PCOK" == driver.findElement(By.cssSelector("tr.search-result-item:nth-child(12) > td:nth-child(2)")).getText())
			{assert(true)}
		else if("C1" == driver.findElement(By.cssSelector("tr.search-result-item:nth-child(12) > td:nth-child(2)")).getText())
			{assert(true)}
		else {assert(false)}
	}

	it should "sort by Item group Ascending and Descending when searching by Item groups(Methanes, Propanes and Petroleum coke)" in {
		driver.get(baseUrl)
		
		//We open up the Item Search Pane and select ONLY Ammonia item group
		driver.findElement(By.xpath("//div[3]/span")).click()
		Thread sleep 500
		
		//We wait and verify that the Item Group sub-menu is fully loaded
		var itemExists = false
		var second = 0
		while ((second <= 60) && (!itemExists)) {
			if (isElementPresent(By.xpath("//div[@id='itemGroupParams']/table/tbody/tr/td[2]/div"))) {itemExists = true}
			Thread sleep 1000
			second = second + 1
		}
		
		//We open the Item Group submenu and select Methane, Petroleum Coke,
		//and Propane sub-groups
		driver.findElement(By.xpath("//div[@id='itemGroupParams']/table/tbody/tr/td[2]/div")).click()
		Thread sleep 500
		driver.findElement(By.cssSelector("button.toggleAll")).click()
		driver.findElement(By.id("multi-param-31")).click()
    	driver.findElement(By.id("multi-param-49")).click()
    	driver.findElement(By.id("multi-param-38")).click()
		driver.findElement(By.xpath("(//input[@value='Search'])[3]")).click()
		
		//We wait until the table is loaded. Test fails if timeout!
		itemExists = false
		second = 0
		while ((second <= 60) && (!itemExists)) {
			try{if(isElementPresent(By.cssSelector("tr.search-result-item > td"))) itemExists = true}catch{case e:Exception =>}
			Thread sleep 1000
			second = second + 1
		} 
		if(itemExists==false){assert(false)}

		//Click on Item code to sort in Descending order
		driver.findElement(By.xpath("//th[@onclick=\"getWorkspaceManager().getActiveWorkspace().sortBy('Item group');\"]")).click()

		//Verify that table results are sorted by Item group
		if("Methanes" == driver.findElement(By.cssSelector("tr.search-result-item:nth-child(1) > td:nth-child(3)")).getText())
			{assert(true)}
		else if ("Propanes" == driver.findElement(By.cssSelector("tr.search-result-item:nth-child(1) > td:nth-child(3)")).getText())
			{assert(true)}
		else {assert(false)}

		if("Propanes" == driver.findElement(By.cssSelector("tr.search-result-item:nth-child(12) > td:nth-child(3)")).getText())
			{assert(true)}
		else if("Methanes" == driver.findElement(By.cssSelector("tr.search-result-item:nth-child(12) > td:nth-child(3)")).getText())
			{assert(true)}
		else {assert(false)}
		}

	it should "sort by Long description when searching by Item groups(Methanes, Propanes and Petroleum coke)" in {
		driver.get(baseUrl)
		
		//We open up the Item Search Pane and select ONLY Ammonia item group
		driver.findElement(By.xpath("//div[3]/span")).click()
		Thread sleep 500
		
		//We wait and verify that the Item Group sub-menu is fully loaded
		var itemExists = false
		var second = 0
		while ((second <= 60) && (!itemExists)) {
			if (isElementPresent(By.xpath("//div[@id='itemGroupParams']/table/tbody/tr/td[2]/div"))) {itemExists = true}
			Thread sleep 1000
			second = second + 1
		}
		
		//We open the Item Group submenu and select Methane, Petroleum Coke,
		//and Propane sub-groups
		driver.findElement(By.xpath("//div[@id='itemGroupParams']/table/tbody/tr/td[2]/div")).click()
		Thread sleep 500
		driver.findElement(By.cssSelector("button.toggleAll")).click()
		driver.findElement(By.id("multi-param-31")).click()
    	driver.findElement(By.id("multi-param-49")).click()
    	driver.findElement(By.id("multi-param-38")).click()
		driver.findElement(By.xpath("(//input[@value='Search'])[3]")).click()
		
		//We wait until the table is loaded. Test fails if timeout!
		itemExists = false
		second = 0
		while ((second <= 60) && (!itemExists)) {
			try{if(isElementPresent(By.cssSelector("tr.search-result-item > td"))) itemExists = true}catch{case e:Exception =>}
			Thread sleep 1000
			second = second + 1
		} 
		if(itemExists==false){assert(false)}

		//Click on Item code to sort in Descending order
		driver.findElement(By.xpath("//th[@onclick=\"getWorkspaceManager().getActiveWorkspace().sortBy('Long description');\"]")).click()

		//Verify that table results are sorted by Long description
		if("High Sulfur Petcoke" == driver.findElement(By.cssSelector("tr.search-result-item:nth-child(1) > td:nth-child(5)")).getText())
			{assert(true)}
		else if ("Propane Hierarchy" == driver.findElement(By.cssSelector("tr.search-result-item:nth-child(1) > td:nth-child(5)")).getText())
			{assert(true)}
		else {assert(false)}

		if("Propane Hierarchy" == driver.findElement(By.cssSelector("tr.search-result-item:nth-child(12) > td:nth-child(5)")).getText())
			{assert(true)}
		else if("High Sulfur Petcoke" == driver.findElement(By.cssSelector("tr.search-result-item:nth-child(12) > td:nth-child(5)")).getText())
			{assert(true)}
		else {assert(false)}

	}

	it should "sort by Short description when searching by Item groups(Methanes, Propanes and Petroleum coke)" in {
		driver.get(baseUrl)
		
		//We open up the Item Search Pane and select ONLY Ammonia item group
		driver.findElement(By.xpath("//div[3]/span")).click()
		Thread sleep 500
		
		//We wait and verify that the Item Group sub-menu is fully loaded
		var itemExists = false
		var second = 0
		while ((second <= 60) && (!itemExists)) {
			if (isElementPresent(By.xpath("//div[@id='itemGroupParams']/table/tbody/tr/td[2]/div"))) {itemExists = true}
			Thread sleep 1000
			second = second + 1
		}
		
		//We open the Item Group submenu and select Methane, Petroleum Coke,
		//and Propane sub-groups
		driver.findElement(By.xpath("//div[@id='itemGroupParams']/table/tbody/tr/td[2]/div")).click()
		Thread sleep 500
		driver.findElement(By.cssSelector("button.toggleAll")).click()
		driver.findElement(By.id("multi-param-31")).click()
    	driver.findElement(By.id("multi-param-49")).click()
    	driver.findElement(By.id("multi-param-38")).click()
		driver.findElement(By.xpath("(//input[@value='Search'])[3]")).click()
		
		//We wait until the table is loaded. Test fails if timeout!
		itemExists = false
		second = 0
		while ((second <= 60) && (!itemExists)) {
			try{if(isElementPresent(By.cssSelector("tr.search-result-item > td"))) itemExists = true}catch{case e:Exception =>}
			Thread sleep 1000
			second = second + 1
		} 
		if(itemExists==false){assert(false)}

		//Click on Item code to sort in Descending order
		driver.findElement(By.xpath("//th[@onclick=\"getWorkspaceManager().getActiveWorkspace().sortBy('Short description');\"]")).click()

		//Verify that table results are sorted by Short description
		if("10/20 Petcoke" == driver.findElement(By.cssSelector("tr.search-result-item:nth-child(1) > td:nth-child(6)")).getText())
			{assert(true)}
		else if ("Propane Hierarchy" == driver.findElement(By.cssSelector("tr.search-result-item:nth-child(1) > td:nth-child(6)")).getText())
			{assert(true)}
		else {assert(false)}

		if("Propane Hierarchy" == driver.findElement(By.cssSelector("tr.search-result-item:nth-child(12) > td:nth-child(6)")).getText())
			{assert(true)}
		else if("10/20 Petcoke" == driver.findElement(By.cssSelector("tr.search-result-item:nth-child(12) > td:nth-child(6)")).getText())
			{assert(true)}
		else {assert(false)}
	
	}

	it should "handle searching by \"Item Type\"" in {
		driver.get(baseUrl)
		
		//We open up the Item Search Pane and select ONLY Ammonia item group
		driver.findElement(By.xpath("//div[3]/span")).click()
		Thread sleep 500
		
		//We wait and verify that the Item type sub-menu is fully loaded
		var itemExists = false
		var second = 0
		while ((second <= 60) && (!itemExists)) {
			if (isElementPresent(By.id("#ws-item-searchparam-type"))) {itemExists = true}
			Thread sleep 1000
			second = second + 1
		}
		
		//We open the Item Type submenu and select the "Meter label"
		new Select(driver.findElement(By.id("ws-item-searchparam-type"))).selectByVisibleText("Meter")
		driver.findElement(By.xpath("(//input[@value='Search'])[3]")).click()
		
		//We wait until the table is loaded. Test fails if timeout!
		itemExists = false
		second = 0
		while ((second <= 60) && (!itemExists)) {
			try{if(isElementPresent(By.cssSelector("tr.search-result-item > td"))) itemExists = true}catch{case e:Exception =>}
			Thread sleep 1000
			second = second + 1
		} 
		if(itemExists==false){assert(false)}
	}


	//=======================================================
	behavior of "Front-End: Item Detail Workspace"
	//=======================================================
	it should "show details of a selected Item when searching by Item groups(Methanes, Propanes and Petroleum coke)" in {
		driver.get(baseUrl)
		
		//We open up the Item Search Pane and select ONLY Ammonia item group
		driver.findElement(By.xpath("//div[3]/span")).click()
		Thread sleep 500
		
		//We wait and verify that the Item Group sub-menu is fully loaded
		var itemExists = false
		var second = 0
		while ((second <= 60) && (!itemExists)) {
			if (isElementPresent(By.xpath("//div[@id='itemGroupParams']/table/tbody/tr/td[2]/div"))) {itemExists = true}
			Thread sleep 1000
			second = second + 1
		}
		
		//We open the Item Group submenu and select Methane, Petroleum Coke,
		//and Propane sub-groups
		driver.findElement(By.xpath("//div[@id='itemGroupParams']/table/tbody/tr/td[2]/div")).click()
		Thread sleep 500
		driver.findElement(By.cssSelector("button.toggleAll")).click()
		driver.findElement(By.id("multi-param-31")).click()
    	driver.findElement(By.id("multi-param-49")).click()
    	driver.findElement(By.id("multi-param-38")).click()
		driver.findElement(By.xpath("(//input[@value='Search'])[3]")).click()
		
		//We wait until the table is loaded. Test fails if timeout!
		itemExists = false
		second = 0
		while ((second <= 60) && (!itemExists)) {
			try{if(isElementPresent(By.cssSelector("tr.search-result-item > td"))) itemExists = true}catch{case e:Exception =>}
			Thread sleep 1000
			second = second + 1
		} 
		if(itemExists==false){assert(false)}

		//Select an Item in the Search results pane
		driver.findElement(By.cssSelector("tr.search-result-item:nth-child(1)")).click()
		
		//We wait for details else timeout!
		itemExists = false
		second = 0
		while ((second <= 60) && (!itemExists)) {
			try{if(isElementPresent(By.cssSelector(".detail-view-properties-table"))) itemExists = true}catch{case e:Exception =>}
			Thread sleep 1000
			second = second + 1
		} 
		if(itemExists==false){assert(false)}

		//Verify results returned in Details pane
		assertEquals("PCOK", driver.findElement(By.cssSelector(".detail-view-properties-table > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(2) > input:nth-child(1)")).getAttribute("value"))
		assertEquals("10/20 Petcoke", driver.findElement(By.cssSelector(".detail-view-properties-table > tbody:nth-child(1) > tr:nth-child(6) > td:nth-child(2) > input:nth-child(1)")).getAttribute("value"))
	}

	//it should "handle searching by \"Item Hierarchy\"" in {
		//TODO: Login, click on <Price>, click on "Item Hierarchy", enter "propane
		//		and verify search results
		
		//assert(false)
	//} 
	
	
	//it should "handle searching by \"Show Expired\" date" in {
		//TODO: Login, enter Expired date
		//		and verify search results
		
		//assert(false)
	//}
	
	//it should "handle searching by multiple criterias" in {
		//TODO: Login, enter Expired date
		//		and verify search results
		
		//assert(false)
	//}
	
	//it should "have additional options" in {
		//TODO: Login, click options and verify menu
		
		//assert(false)
	//}
	
	//=======================================================
	behavior of "Front-End: Price Search Workspace"
	//=======================================================
	it should "handle closing search tabs in the price workspace" in {
		driver.get(baseUrl)
		
		//We open up an price search and verify that a tab is indeed opened
		driver.findElement(By.cssSelector("img[alt=\"Price\"]")).click()
		Thread sleep 500

		driver.findElement(By.cssSelector("input.search-parameter-input.search-parameter-search-button")).click()
		assertEquals("Price", driver.findElement(By.cssSelector("div.tab.selected")).getText())
		
		//We wait until the table is loaded. Test fails if timeout!
		var itemExists = false
		var second = 0
		while ((second <= 60) && (!itemExists)) {
			try{if(isElementPresent(By.cssSelector("tr.search-result-item > td"))) itemExists = true}catch{case e:Exception =>}
			Thread sleep 1000
			second = second + 1
		} 
		if(itemExists==false){assert(false)}

		//Close the tab by clicking the close button and verify it no longer exists
		//by expecting an exception when looking for the missing tab.
    	driver.findElement(By.cssSelector(".tab-close-button")).click()
    	Thread sleep 500

    	intercept[NoSuchElementException] {
    		driver.findElement(By.cssSelector("div.tab.selected")).getText()
    	}
	}

	it should "handle searching with no description" in {
		driver.get(baseUrl);
		driver.findElement(By.cssSelector("img[alt=\"Price\"]")).click()
		
		//Checks if Search button exists and clicks it
		try{assert(isElementPresent(By.cssSelector("input.search-parameter-input.search-parameter-search-button")))}catch{case e: Exception => assert(false)}
		Thread sleep 500
		driver.findElement(By.cssSelector("input.search-parameter-input.search-parameter-search-button")).click()
		
		//Checks if Items tab is opened
		assertEquals("Price", driver.findElement(By.cssSelector("div.tab.selected")).getText())
		
		//Checks if table is populated with items
		var itemExists = false
		var second = 0
		while ((second <= 60) && (!itemExists)) {
			try{if(isElementPresent(By.cssSelector("tr.search-result-item > td"))) itemExists = true}catch{case e:Exception =>}
			Thread sleep 1000
			second = second + 1
		}
		if(itemExists==false) assert(false)
		else assert(true)
	}
/*	
	it should "handle searching with a description \"Mankato\"" in {
		driver.get(baseUrl);
		driver.findElement(By.cssSelector("img[alt=\"Price\"]")).click();
		
		//Enter description into search field
		driver.findElement(By.id("ws-price-searchparam-description")).clear();
    	driver.findElement(By.id("ws-price-searchparam-description")).sendKeys("Mankato");

		//Checks if Search button exists and clicks it
		try{assert(isElementPresent(By.cssSelector("input.search-parameter-input.search-parameter-search-button")))}catch{case e: Exception => assert(false)}
		Thread sleep 500
		driver.findElement(By.cssSelector("input.search-parameter-input.search-parameter-search-button")).click()
		
		//Checks if Items tab is opened
		assertEquals("Price", driver.findElement(By.cssSelector("div.tab.selected")).getText())
		
		//Checks if table is populated with items
		var itemExists = false
		var second = 0
		while ((second <= 60) && (!itemExists)) {
			try{if(isElementPresent(By.cssSelector("tr.search-result-item > td"))) itemExists = true}catch{case e:Exception =>}
			Thread sleep 1000
			second = second + 1
		}
		//if(itemExists==false) assert(false)
		//else assert(true)

		//Verify that table results are sorted by Item group
		if("Mankato Terminal (Cochin) - C3 - Intercompany Transfer Price" == driver.findElement(By.xpath("//table[@id='JColResizer3']/tbody/tr/td[2]/td[1]")).getText())
			{assert(true)}
		else assert(false)
	}
	
*/
	 it should "return no results when searching for a non-existing price description" in{
		driver.get(baseUrl);
		driver.findElement(By.cssSelector("img[alt=\"Price\"]")).click();
		
		//Enter description into search field
		driver.findElement(By.id("ws-price-searchparam-description")).clear();
    	driver.findElement(By.id("ws-price-searchparam-description")).sendKeys("non-existing");

		//Checks if Search button exists and clicks it
		try{assert(isElementPresent(By.cssSelector("input.search-parameter-input.search-parameter-search-button")))}catch{case e: Exception => assert(false)}
		Thread sleep 500
		driver.findElement(By.cssSelector("input.search-parameter-input.search-parameter-search-button")).click()
		
		//Checks if Items tab is opened
		assertEquals("Price", driver.findElement(By.cssSelector("div.tab.selected")).getText())
		
		//Checks if table is populated with items
		var itemExists = false
		var second = 0
		while ((second <= 20) && (!itemExists)) {
			try{if(isElementPresent(By.cssSelector("tr.search-result-item > td"))) itemExists = true}catch{case e:Exception =>}
			Thread sleep 1000
			second = second + 1
		}
		if(itemExists==false) assert(true)
		else assert(false)
	 }


/*	
	it should "sort the Price table by columns alphabetically in both Ascending and Descending" in {
		driver.get(baseUrl)
		
		//We open up the Price Search Pane and click the search button besides item.
		driver.findElement(By.cssSelector("img[alt=\"Price\"]")).click()
		Thread sleep 500
		driver.findElement(By.xpath("//div[@onclick=\"UUIDparameterSearch('location', 'ws-price-searchparam-sourcelocationuuid'); return false;\"]")).click()

		//We wait and verify that the Item pop up window is fully loaded and the search parameter exists
		var itemExists = false
		var second = 0
		while ((second <= 60) && (!itemExists)) {
			if (isElementPresent(By.id("uuid-searchparam"))) {itemExists = true}
			Thread sleep 1000
			second = second + 1
		}

		driver.findElement(By.id("uuid-searchparam")).clear()
    	driver.findElement(By.id("uuid-searchparam")).sendKeys("Alberta")
    	driver.findElement(By.cssSelector(".main > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(2) > div:nth-child(1)")).click()
   		
    	//Wait for items to load in the table
   		itemExists = false
   		second = 0
   		while ((second <= 60) && (!itemExists)) {
			if (isElementPresent(By.xpath("//table[@id='JColResizer0']/tbody/tr/td[2]"))) {itemExists = true}
			Thread sleep 1000
			second = second + 1
		}
    	driver.findElement(By.xpath("//table[@id='JColResizer0']/tbody/tr/td[2]")).click()
    	driver.findElement(By.cssSelector("div.modal-ok-button")).click()
    	
    	//Click search
    	driver.findElement(By.cssSelector("input.search-parameter-input.search-parameter-search-button")).click()

    	//We wait until the table is loaded. Test fails if timeout!
		itemExists = false
		second = 0
		while ((second <= 60) && (!itemExists)) {
			try{if(isElementPresent(By.cssSelector("tr.search-result-item > td"))) itemExists = true}catch{case e:Exception =>}
			Thread sleep 1000
			second = second + 1
		} 
		if(itemExists==false){assert(false)}

    	//Click on Description to sort in Ascending/Descending order
		driver.findElement(By.xpath("//th[@onclick=\"getWorkspaceManager().getActiveWorkspace().sortBy('Description');\"]")).click()

    	//Verify that table results are sorted by Item group
		if("Southbend Transportation Fee ($0.15/GJ)" == driver.findElement(By.cssSelector("tr.search-result-item:nth-child(1) > td:nth-child(1)")).getText())
			{assert(true)}
		else if ("02-44 Oil Battery #2 - Bergen - Posted Price" == driver.findElement(By.cssSelector("tr.search-result-item:nth-child(1) > td:nth-child(1)")).getText())
			{assert(true)}
		else {assert(false)}

		if("02-44 Oil Battery #2 - Bergen - Posted Price" == driver.findElement(By.cssSelector("tr.search-result-item:nth-child(41) > td:nth-child(1)")).getText())
			{assert(true)}
		else if("Southbend Transportation Fee ($0.15/GJ)" == driver.findElement(By.cssSelector("tr.search-result-item:nth-child(41) > td:nth-child(1)")).getText())
			{assert(true)}
		else {assert(false)}
	}
*/

	it should "handle searching by \"Source Location\"" in {
		driver.get(baseUrl)
		
		//We open up the Price Search Pane and click the search button besides item.
		driver.findElement(By.cssSelector("img[alt=\"Price\"]")).click()
		Thread sleep 500
		driver.findElement(By.xpath("//div[@onclick=\"UUIDparameterSearch('location', 'ws-price-searchparam-sourcelocationuuid'); return false;\"]")).click()

		//We wait and verify that the Item pop up window is fully loaded and the search parameter exists
		var itemExists = false
		var second = 0
		while ((second <= 60) && (!itemExists)) {
			if (isElementPresent(By.id("uuid-searchparam"))) {itemExists = true}
			Thread sleep 1000
			second = second + 1
		}

		driver.findElement(By.id("uuid-searchparam")).clear()
    	driver.findElement(By.id("uuid-searchparam")).sendKeys("Alberta")
    	driver.findElement(By.cssSelector(".main > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(2) > div:nth-child(1)")).click()
   		
    	//Wait for items to load in the table
   		itemExists = false
   		second = 0
   		while ((second <= 60) && (!itemExists)) {
			if (isElementPresent(By.cssSelector(".uuidlookup > tbody:nth-child(2) > tr:nth-child(1)"))) {itemExists = true}
			Thread sleep 1000
			second = second + 1
		}
    	driver.findElement(By.cssSelector(".uuidlookup > tbody:nth-child(2) > tr:nth-child(1)")).click()
    	driver.findElement(By.cssSelector("div.modal-ok-button")).click()
    	
    	//Click search
    	driver.findElement(By.cssSelector("input.search-parameter-input.search-parameter-search-button")).click()

    	//We wait until the table is loaded. Test fails if timeout!
		itemExists = false
		second = 0
		while ((second <= 60) && (!itemExists)) {
			try{if(isElementPresent(By.cssSelector("tr.search-result-item > td"))) itemExists = true}catch{case e:Exception =>}
			Thread sleep 1000
			second = second + 1
		} 
		if(itemExists==false){assert(false)}
	}
	
	it should "handle searching by \"Destination Location\"" in {
		driver.get(baseUrl)
		
		//We open up the Price Search Pane and click the search button besides item.
		driver.findElement(By.cssSelector("img[alt=\"Price\"]")).click()
		Thread sleep 500
		driver.findElement(By.xpath("//div[@onclick=\"UUIDparameterSearch('location', 'ws-price-searchparam-destinationlocationuuid'); return false;\"]")).click()
		//We wait and verify that the Item pop up window is fully loaded and the search parameter exists
		var itemExists = false
		var second = 0
		while ((second <= 60) && (!itemExists)) {
			if (isElementPresent(By.id("uuid-searchparam"))) {itemExists = true}
			Thread sleep 1000
			second = second + 1
		}

		driver.findElement(By.id("uuid-searchparam")).clear()
    	driver.findElement(By.id("uuid-searchparam")).sendKeys("Alberta")
    	driver.findElement(By.cssSelector(".main > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(2) > div:nth-child(1)")).click()
   		
    	//Wait for items to load in the table
   		itemExists = false
   		second = 0
   		while ((second <= 60) && (!itemExists)) {
			if (isElementPresent(By.cssSelector(".uuidlookup > tbody:nth-child(2) > tr:nth-child(1)"))) {itemExists = true}
			Thread sleep 1000
			second = second + 1
		}
    	driver.findElement(By.cssSelector(".uuidlookup > tbody:nth-child(2) > tr:nth-child(1)")).click()
    	driver.findElement(By.cssSelector("div.modal-ok-button")).click()
    	
    	//Click search
    	driver.findElement(By.cssSelector("input.search-parameter-input.search-parameter-search-button")).click()

    	//We wait until the table is loaded. Test fails if timeout!
		itemExists = false
		second = 0
		while ((second <= 60) && (!itemExists)) {
			try{if(isElementPresent(By.cssSelector("tr.search-result-item > td"))) itemExists = true}catch{case e:Exception =>}
			Thread sleep 1000
			second = second + 1
		} 
		if(itemExists==false){assert(false)}
	}
	
	it should "handle searching by \"Item\"" in {
		driver.get(baseUrl)
		
		//We open up the Price Search Pane and click the search button besides item.
		driver.findElement(By.cssSelector("img[alt=\"Price\"]")).click()
		Thread sleep 500
		driver.findElement(By.xpath("//div[@onclick=\"UUIDparameterSearch('item', 'ws-price-searchparam-itemuuid'); return false;\"]")).click()
		
		//We wait and verify that the Item pop up window is fully loaded and the search parameter exists
		var itemExists = false
		var second = 0
		while ((second <= 60) && (!itemExists)) {
			if (isElementPresent(By.id("uuid-searchparam"))) {itemExists = true}
			Thread sleep 1000
			second = second + 1
		}
		
		//We then choose "Propane Heirachy" after typing "p" in the search box and click ok
		driver.findElement(By.id("uuid-searchparam")).clear()
   		driver.findElement(By.id("uuid-searchparam")).sendKeys("ammonia")
   		driver.findElement(By.cssSelector(".main > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(2) > div:nth-child(1)")).click()
   		//Wait for items to load in the table
   		itemExists = false
   		second = 0
   		while ((second <= 60) && (!itemExists)) {
			if (isElementPresent(By.cssSelector(".main > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(2) > div:nth-child(1)"))) {itemExists = true}
			Thread sleep 1000
			second = second + 1
		}
   		driver.findElement(By.cssSelector(".main > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(2) > div:nth-child(1)")).click()
   		driver.findElement(By.cssSelector("div.modal-ok-button")).click()

   		//Click search
   		driver.findElement(By.cssSelector("input.search-parameter-input.search-parameter-search-button")).click()

		//We wait until the table is loaded. Test fails if timeout!
		itemExists = false
		second = 0
		while ((second <= 60) && (!itemExists)) {
			try{if(isElementPresent(By.cssSelector("tr.search-result-item > td"))) itemExists = true}catch{case e:Exception =>}
			Thread sleep 1000
			second = second + 1
		} 
		if(itemExists==false){assert(false)}
	}
	
	
	//it should "handle searching by \"Sub-Group\"" in {
		//TODO: Login, enter source Sub-Group name
		//		and verify search results
		
		//assert(false)
	//}
	
	//it should "handle searching by \"Show Expired\" date" in {
		//TODO: Login, enter Expired date
		//		and verify search results
		
		//assert(false)
	//}
	
	//it should "handle searching by multiple criterias" in {
		//TODO: Login, enter Expired date
		//		and verify search results
		
		//assert(false)
	//}
	
	//it should "have additional options" in {
		//TODO: Login, click options and verify menu
		
		//assert(false)
	//}
	
	//=======================================================
	behavior of "Front-End: Invoice Search Workspace"
	//=======================================================
		it should "handle searching by \"Pay/Rec\"" in {
		//Test case is covered in "handle a multi parameter search by \"Invoice\"
		
		//assert(false)
		}

		it should "handle searching by \"Invoice Type\"" in {
		//Test case is covered in "handle a multi parameter search by \"Invoice\"
		
		//assert(false)
		}

		it should "handle a multi parameter search by \"Invoice\"" in {
			driver.get(baseUrl)
    		
    		driver.findElement(By.xpath("//div[2]/span")).click()
    		Thread sleep 500

    		new Select(driver.findElement(By.id("ws-invoice-searchparam-payrec"))).selectByVisibleText("REC")
    		new Select(driver.findElement(By.id("invoiceTypeParams"))).selectByVisibleText("SALES")
    		new Select(driver.findElement(By.id("invoiceStatusParams"))).selectByVisibleText("OUTSTANDING")
    		driver.findElement(By.id("ws-invoice-searchparam-datefrom")).click()

    		//We wait and verify that the datepicker Ui is loaded up
			var itemExists = false
			var second = 0
			while ((second <= 60) && (!itemExists)) {
				if (isElementPresent(By.id("ui-datepicker-div"))) {itemExists = true}
				Thread sleep 1000
				second = second + 1
			}

    		driver.asInstanceOf[JavascriptExecutor].executeScript("document.getElementById('ws-invoice-searchparam-datefrom').removeAttribute('readonly',0);")
    		driver.findElement(By.id("ws-invoice-searchparam-datefrom")).clear()
    		driver.findElement(By.id("ws-invoice-searchparam-datefrom")).sendKeys("04/01/2005")
    		driver.findElement(By.xpath("(//input[@value='Search'])[2]")).click()

    		//We wait until the table is loaded. Test fails if timeout!
 			itemExists = false
 			second = 0
			while ((second <= 60) && (!itemExists)) {
				try{if(isElementPresent(By.cssSelector("tr.search-result-item > td"))) itemExists = true}catch{case e:Exception =>}
				Thread sleep 1000
				second = second + 1
			} 
			if(itemExists==false){assert(false)}
			
			//Verify invoice search results are loaded correctly
			if("Amerigas Propane Incorporated" == driver.findElement(By.cssSelector("tr.search-result-item:nth-child(1) > td:nth-child(4)")).getText())
				{assert(true)}
		}

		it should "handle searching by \"Invoice Status\"" in {
		//Test case is covered in "handle a multi parameter search by \"Invoice\"
		
		//assert(false)
		
		}
		it should "handle searching by \"Date between\"" in {
		//Test case is covered in "handle a multi parameter search by \"Invoice\"
		
		//assert(false)
		}

		//it should "handle searching by \"Invoice Ref\"" in {
		//TODO: Login, enter Group name
		//		and verify search results
		
		//assert(false)


	//=======================================================
	//behavior of "Front-End: Invoice Detail Workspace"
	//=======================================================
	it should "Ensure invoice details is loaded correctly" in {
		driver.get(baseUrl)
    		
    		driver.findElement(By.xpath("//div[2]/span")).click()
    		Thread sleep 500

    		new Select(driver.findElement(By.id("ws-invoice-searchparam-payrec"))).selectByVisibleText("REC")
    		new Select(driver.findElement(By.id("invoiceTypeParams"))).selectByVisibleText("SALES")
    		new Select(driver.findElement(By.id("invoiceStatusParams"))).selectByVisibleText("OUTSTANDING")
    		driver.findElement(By.id("ws-invoice-searchparam-datefrom")).click()

    		//We wait and verify that the datepicker Ui is loaded up
			var itemExists = false
			var second = 0
			while ((second <= 60) && (!itemExists)) {
				if (isElementPresent(By.id("ui-datepicker-div"))) {itemExists = true}
				Thread sleep 1000
				second = second + 1
			}

    		driver.asInstanceOf[JavascriptExecutor].executeScript("document.getElementById('ws-invoice-searchparam-datefrom').removeAttribute('readonly',0);")
    		driver.findElement(By.id("ws-invoice-searchparam-datefrom")).clear()
    		driver.findElement(By.id("ws-invoice-searchparam-datefrom")).sendKeys("04/01/2005")
    		driver.findElement(By.xpath("(//input[@value='Search'])[2]")).click()

    		//We wait until the table is loaded. Test fails if timeout!
 			itemExists = false
 			second = 0
			while ((second <= 60) && (!itemExists)) {
				try{if(isElementPresent(By.cssSelector("tr.search-result-item > td"))) itemExists = true}catch{case e:Exception =>}
				Thread sleep 1000
				second = second + 1
			} 
			if(itemExists==false){assert(false)}
			
			//Verify invoice search results are loaded correctly
			if("Amerigas Propane Incorporated" == driver.findElement(By.cssSelector("tr.search-result-item:nth-child(1) > td:nth-child(4)")).getText())
				{assert(true)}
			
			//Click on the first item in the search results
			driver.findElement(By.cssSelector("tr.search-result-item:nth-child(1) > td:nth-child(4)")).click()

			//We wait until the innvoice details is loaded. Test fails if timeout!
 			itemExists = false
 			second = 0
			while ((second <= 60) && (!itemExists)) {
				try{if(isElementPresent(By.cssSelector(".detail-view-properties-table"))) itemExists = true}catch{case e:Exception =>}
				Thread sleep 1000
				second = second + 1
			} 
			if(itemExists==false){assert(false)}

			/****Check if Invoice details is rendered properly***/
			//Check if Pay/Rec 
			try{new Select(driver.findElement(By.cssSelector(".detail-view-properties-table > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(2) > select:nth-child(1)"))).selectByVisibleText("Receivable")}catch{case e:Exception =>}
			
			//Check if CP
			if(!(driver.findElement(By.cssSelector(".detail-view-properties-table > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(2) > input:nth-child(1)")).getAttribute("value") == "Amerigas Propane Incorporated"))
			{
				assert(false)
			}

			//Check if CP Contract 
			if(!(driver.findElement(By.cssSelector(".detail-view-properties-table > tbody:nth-child(1) > tr:nth-child(3) > td:nth-child(2) > input:nth-child(1)")).getAttribute("value") == "Bartells, Rusty"))
			{
				assert(false)
			}

			//Check if CP Mail Addr

			if(!(driver.findElement(By.cssSelector(".detail-view-properties-table > tbody:nth-child(1) > tr:nth-child(4) > td:nth-child(2) > input:nth-child(1)")).getAttribute("value") == "13105 Northwest Freeway "))
			{
				assert(false)
			}

			//Check BU
			if(!(driver.findElement(By.cssSelector(".detail-view-properties-table > tbody:nth-child(1) > tr:nth-child(5) > td:nth-child(2) > input:nth-child(1)")).getAttribute("value") == "Northern Gas Liquids Marketing"))
			{
				assert(false)
			}

			//Check BU Contact
			if(!(driver.findElement(By.cssSelector(".detail-view-properties-table > tbody:nth-child(1) > tr:nth-child(6) > td:nth-child(2) > input:nth-child(1)")).getAttribute("value") == "Sky, Tammy"))
			{
				assert(false)
			}

			//Check BU Addr
			if(!(driver.findElement(By.cssSelector(".detail-view-properties-table > tbody:nth-child(1) > tr:nth-child(7) > td:nth-child(2) > input:nth-child(1)")).getAttribute("value") == "440 - 2nd Ave. SW Suite 1500 Ernst & Young Tower Calgary, AB T2P 5E9"))
			{
				assert(false)
			}

			//Check Remit Addr
			if(!(driver.findElement(By.cssSelector(".detail-view-properties-table > tbody:nth-child(1) > tr:nth-child(8) > td:nth-child(2) > input:nth-child(1)")).getAttribute("value") == "440 - 2nd Ave. SW Suite 1500 Ernst & Young Tower Calgary, AB T2P 5E9"))
			{
				assert(false)
			}

			//Check Account
			try{new Select(driver.findElement(By.cssSelector(".detail-view-properties-table > tbody:nth-child(1) > tr:nth-child(9) > td:nth-child(2) > select:nth-child(1)"))).selectByVisibleText("Accounts Receivable $US")}catch{case e:Exception =>}
			
			//Check currency
			try{new Select(driver.findElement(By.cssSelector(".detail-view-properties-table > tbody:nth-child(1) > tr:nth-child(10) > td:nth-child(2) > select:nth-child(1)"))).selectByVisibleText("U.S. Dollars")}catch{case e:Exception =>}
			
			//Check Pay Terms
			try{new Select(driver.findElement(By.cssSelector(".detail-view-properties-table > tbody:nth-child(1) > tr:nth-child(11) > td:nth-child(2) > select:nth-child(1)"))).selectByVisibleText("1% 10 Net 15")}catch{case e:Exception =>}
			
			//Check Formart
			try{new Select(driver.findElement(By.cssSelector(".detail-view-properties-table > tbody:nth-child(1) > tr:nth-child(12) > td:nth-child(2) > select:nth-child(1)"))).selectByVisibleText("Standard Layout(S)")}catch{case e:Exception =>}
			
			//Check Mode
			try{new Select(driver.findElement(By.cssSelector(".detail-view-properties-table > tbody:nth-child(1) > tr:nth-child(13) > td:nth-child(2) > select:nth-child(1)"))).selectByVisibleText("Hard Copy")}catch{case e:Exception =>}
			
			//Check Issued
			try{new Select(driver.findElement(By.cssSelector(".detail-view-properties-table > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(4) > select:nth-child(1)"))).selectByVisibleText("")}catch{case e:Exception =>}
			
			//Check Due
			try{new Select(driver.findElement(By.cssSelector(".detail-view-properties-table > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(4) > select:nth-child(1)"))).selectByVisibleText("")}catch{case e:Exception =>}
			
			//Check Discount
			try{new Select(driver.findElement(By.cssSelector(".detail-view-properties-table > tbody:nth-child(1) > tr:nth-child(3) > td:nth-child(4) > select:nth-child(1)"))).selectByVisibleText("")}catch{case e:Exception =>}
			
			//Invoice ID
			if(!(driver.findElement(By.cssSelector(".detail-view-properties-table > tbody:nth-child(1) > tr:nth-child(5) > td:nth-child(4) > input:nth-child(1)")).getAttribute("value") == "500012"))
			{
				assert(false)
			}
			//Invoice Ref
			if(!(driver.findElement(By.cssSelector(".detail-view-properties-table > tbody:nth-child(1) > tr:nth-child(6) > td:nth-child(4) > input:nth-child(1)")).getAttribute("value") == "NE Mktg-8"))
			{
				assert(false)
			}


		}
}