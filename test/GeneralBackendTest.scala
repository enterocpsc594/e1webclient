package suites

import controllers.Application
import controllers.Excel
import controllers.Login
import com.entero.identifiers.ItemIdentifier
import com.entero.identifiers.PriceIdentifier
import com.entero.identifiers.InvoiceIdentifier
import com.entero.generic.UserSession
import com.entero.generic.UserSessionManager
import com.entero.rest.RestClient
import com.entero.workspace.Workspace
import com.entero.workspace.WorkspaceFactory
import com.entero.workspace.WorkspaceFactory.Endpoint
import com.entero.workspace.WSHelper
import models.EnteroObject

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper

import play.api.data._
import play.api.data.Forms._
import play.api.test.Helpers.running
import play.api.test.FakeApplication
import play.mvc._
import play.test.FakeRequest
import play.test.Helpers

import java.util.Collections

//tests 
import scala.concurrent.Future

import collection.JavaConversions._
import collection.mutable._
import org.scalatest.junit.AssertionsForJUnit
import org.scalatest.PrivateMethodTester._
import org.scalatest._

import org.scalatest.mock.JMockCycle
import org.jmock.Expectations
import org.jmock.Expectations.returnValue
import org.scalatest.mock.JMockExpectations
import org.jmock.Mockery
import org.jmock.lib.legacy.ClassImposteriser
import org.jmock.lib.concurrent.Synchroniser

import org.scalatest.mock.{JMockCycleFixture, JMockCycle}

/**
 * READ ME (Last updated 24/11/2014):
 * The following tests uses ScalaTest in conjunction with Selenium to 
 * simulate a web browser to test the web app's behaviour, as well as
 * verifying the information. Tests are structured in FlatSpec format,
 * which follows BDD(Behavior-Driven Design) principles.
 * 
 * To compile these tests you need to have the following lines added to your SBT build file:
 * libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.1" % "test"
 * libraryDependencies += "org.seleniumhq.selenium" % "selenium-java" % "2.44.0" % "test"
 *
 * To run these tests you simply type "test" in your sbt terminal.
 *
 */
class GeneralBackendTest extends FlatSpec {
    
    val mapper = new ObjectMapper() // ObjectMapper is used to generate fake JSON response from text
    val timeout = 10000L    //Represents timeout value for http request tests

    /** This defines the fixture method to create mock RestClient objects!
     * Search "Loan-Fixture" Method for more information
     */
    def withRestClient(testCode: (RestClient, Mockery) => Any) {
        
        // This creates a context object from JMock
        var context = new Mockery
        context.setImposteriser(ClassImposteriser.INSTANCE)
        context.setThreadingPolicy(new Synchroniser()) //This prevents race conditions with mock objects!

        var mockClient = context.mock(classOf[RestClient])
        try {
            // Loans the mockClient to the tests
            testCode(mockClient, context)
        }
        finally {
            // Clean up objects
            mockClient = null
            context = null
        }
    }

    /**
     * This defines a fake workspace used to fake workspace behaviour such as table results (used in excel test)
     */
    def withWorkspace(testCode: (Workspace, Mockery) => Any) {
        
        // This creates a context object from JMock
        var context = new Mockery
        context.setImposteriser(ClassImposteriser.INSTANCE)
        context.setThreadingPolicy(new Synchroniser()) //This prevents race conditions with mock objects!

        var mockWorkspace = context.mock(classOf[Workspace])
        try {
            // Loans the mockClient to the tests
            testCode(mockWorkspace, context)
        }
        finally {
            // Clean up objects
            mockWorkspace = null
            context = null
        }
    }



    /**
     * This mocks the RequestHeader class, which is needed to test the Application.java
     */
    // def withRequestHeader(testCode: (RequestHeader, Mockery) => Any) {

    //  // This creates a context object from JMock
    //  var context = new Mockery
    //  context.setImposteriser(ClassImposteriser.INSTANCE)
    //  context.setThreadingPolicy(new Synchorniser()) // Prevents race-conditions with mock objects!

    //  var mockRequestHeader = context.mock(classOf[RequestHeader])
    //  try {
    //          // Loans the mockRequestHeader to the tests
    //          testCode(mockRequestHeader, context)

    //      } finally {
    //          // Clean up objects
    //          mockRequestHeader = null
    //          context = null
    //      }
    //  val flashData: java.util.Map[String, String] = Collections.emptyMap()
    //  val argData: java.util.Map[String, String] = Collections.emptyMap()
    //  Long id = 2L

    //  //play.api.mvc.RequestHeader header = mock(play.api.mvc.RequestHeader.class)
    //  //Http.Context context = new Http.Context(id, header, request, flashData, flashData, argData)
    //  //Http.Context.current.set(context)
    // }

    /**
     * This fixture sets up the http context required to test Application.java
     */
     def httpContext =
        new {
            // Create empty values to initialize Http context
            val flashData: java.util.Map[String, String] = Collections.emptyMap()
            val argData: java.util.Map[String, Object] = Collections.emptyMap()
            val id = 2L

            val session: java.util.Map[String, String] = HashMap("uuid"->"testUUID")

            // Create a fake RequestHeader
            var mockContext = new Mockery
            mockContext.setImposteriser(ClassImposteriser.INSTANCE)
            mockContext.setThreadingPolicy(new Synchroniser())
            var mockHeader = mockContext.mock(classOf[play.api.mvc.RequestHeader])

            // Create a fake Request
            var mockContext2 = new Mockery
            mockContext2.setImposteriser(ClassImposteriser.INSTANCE)
            mockContext2.setThreadingPolicy(new Synchroniser())
            var mockRequest = mockContext2.mock(classOf[play.mvc.Http.Request])

            var context = new Http.Context(id, mockHeader, mockRequest, session, flashData, argData)
            Http.Context.current.set(context)
         }

        //def read(html:HTML):String = io.Source.fromFile(html).mkString

    //=======================================================
    behavior of "Back-End: RestClient.java"
    //=======================================================
    it should "receive a token given correct username and password" in {
        running(FakeApplication()){
            val client = new RestClient("Webtest", "entero")
            val getAuthToken = PrivateMethod[String]('getAuthToken)
            
            // Un-commenting the following will print out the token!
            //val token = (client invokePrivate getAuthToken())
            //println("Token received: " + token)
            
            assert((client invokePrivate getAuthToken()).length > 0)
        }
    }
    
    //it should "handle incorrect user credentials" in {
        //TODO: Code login functions in RestClient to handle
        //      different login errors, such as "User does not exist"
        //      "Password was not correct", etc.
        //assert(false)
    //}
    
    //it should "erase user's login information upon logout" in {
        //TODO: Code logout function in RestClient and GUI and
        //      then check if password variable is cleared after logout
        //assert(false)
    //}
    
    //=======================================================
    behavior of "Back-End: EnteroObject.java"
    //=======================================================
    it should "store and retrieve at least one property" in {
        val enteroObject = new EnteroObject()
        enteroObject.setProperty("Test Property 1", "Attribute 1")
        assert(enteroObject.getProperty("Test Property 1") == "Attribute 1")
    }
    
    it should "store and retrieve multiple properties" in {
        val enteroObject = new EnteroObject()
        enteroObject.setProperty("Test Property 1", "Attribute 1")
        enteroObject.setProperty("Test Property 2", "Attribute 2")
        assert(enteroObject.getProperty("Test Property 1") == "Attribute 1")
        assert(enteroObject.getProperty("Test Property 2") == "Attribute 2")
    }

    //=======================================================
    behavior of "Back-End: UserSession.java"
    //=======================================================
    it should "store and retrieve an instance of RestClient for the user's session" in withRestClient { (mockClient, context) =>
        running(FakeApplication()) {
            val session = new UserSession()
            session.setRestClient(mockClient)
            assert(session.getRestClient()==mockClient)
        }
    }

    it should "store and retrieve an instance of Workspace for the user's session" in withRestClient { (mockClient, context) =>
        running(FakeApplication()) {
            val workspace = WorkspaceFactory.createWorkspace(Endpoint.ITEMS)
            val session = new UserSession()
            session.addWorkspace("testID", workspace)
            assert(session.getWorkspace("testID")==workspace)
        }
    }

    it should "store and retrieve multiple instances of Workspace for the user's session" in withRestClient { (mockClient, context) =>
        running(FakeApplication()) {
            val workspace1 = WorkspaceFactory.createWorkspace(Endpoint.ITEMS)
            val workspace2 = WorkspaceFactory.createWorkspace(Endpoint.PRICE)
            val session = new UserSession()
            session.addWorkspace("testID1", workspace1)
            session.addWorkspace("testID2", workspace2)
            assert(session.getWorkspace("testID1")==workspace1)
        }
    }

    //=======================================================
    behavior of "Back-End: UserSessionManager.java"
    //=======================================================
    it should "store and retrieve a user session" in {
        running(FakeApplication()) {
            val session = new UserSession()
            val uuid = UserSessionManager.addSession(session)
            assert(UserSessionManager.getSession(uuid)==session)
        }
    }

    it should "store and retrieve multiple user sessions" in {
        running(FakeApplication()) {
            val session = new UserSession()
            val uuid = UserSessionManager.addSession(session)
            assert(UserSessionManager.getSession(uuid)==session)
        }
    }

    //=======================================================
    behavior of "Back-End: Login.java"
    //=======================================================

    it should "allow the users to enter and submit credentials" in {
        running(FakeApplication()) {
            val context = httpContext
            val login = Login.login()
            val loginHtml = views.html.login.render()

            // Create fake session by logging into the system:
            val loginInfo: java.util.Map[String, String] = HashMap("username"->"webtest", "pass"->"entero")
            val fakeLogin = new FakeRequest(play.test.Helpers.POST, "/login/post").withFormUrlEncodedBody(loginInfo)
            play.test.Helpers.routeAndCall(fakeLogin, timeout)
        }
    }

    it should "alert the users when they enter and submit the wrong credentials" in {
        running(FakeApplication()) {
            val context = httpContext
            val login = Login.login()
            val loginHtml = views.html.login.render()

            // Create fake session by logging into the system:
            val loginInfo: java.util.Map[String, String] = HashMap("username"->"wrongusername", "pass"->"wrongpass")
            val fakeLogin = new FakeRequest(play.test.Helpers.POST, "/login/post").withFormUrlEncodedBody(loginInfo)
            play.test.Helpers.routeAndCall(fakeLogin, timeout)
            //assert(Helpers.status(result) == (Http.Status.OK))
        }
    }

    //=======================================================
    behavior of "Back-End: WSHelper.java"
    //=======================================================
    /*it should "check if a List object contains a specific string" in {
        running(FakeApplication()) {
            val fakeList: java.util.List[Object] = ArrayBuffer("testString")
            val helper = new WSHelper()
            val results = helper.contains(fakeList, "testString")

            //assert(results)
        }
    }*/

    //=======================================================
    behavior of "Back-End: Search.java"
    //=======================================================
    it should "display the Items search result table \n   (using an existing session that contains the Items workspace)" in {
        running(FakeApplication()) {     
            // Create a fake UserSession and add the Items workspace to it
            val userSession = new UserSession()
            val itemsWS = WorkspaceFactory.createWorkspace(Endpoint.ITEMS)
            userSession.addWorkspace("testws_uid", itemsWS)                 // Add the ItemsWS with "testws_uid" as the key
            val uuid = UserSessionManager.addSession(userSession)           // Calling this method adds a session to the play framework's system

            // Create a fake route request for the items search result table
            val tableParam: java.util.Map[String, String] = HashMap(
                                                                "off" -> "0",
                                                                "ws_uid"->"testws_uid", 
                                                                "searchParam_itemGroups"->"",
                                                                "searchParam_type"->"", 
                                                                "searchParam_description"->"")
            val fakeTableSortRequest = new FakeRequest(play.test.Helpers.POST, "/itemSearchResultTable")
                .withSession("uuid",uuid).withFormUrlEncodedBody(tableParam)// Note: must append ".withSession()" to let controllers retrieve the proper session!
            play.test.Helpers.routeAndCall(fakeTableSortRequest, timeout)
        }
    }

    it should "display the Items search result table \n   (using a fresh session containing no workspace)" in {
        running(FakeApplication()) {     
            // Create a fake UserSession and add the Items workspace to it
            val userSession = new UserSession()
            val uuid = UserSessionManager.addSession(userSession)   // Calling this method adds a session to the play framework's system

            // Create a fake route request for the items search result table
            val tableParam: java.util.Map[String, String] = HashMap(
                                                                "off" -> "0",
                                                                "ws_uid"->"testws_uid", 
                                                                "searchParam_itemGroups"->"",
                                                                "searchParam_type"->"", 
                                                                "searchParam_description"->"")
            val fakeTableSortRequest = new FakeRequest(play.test.Helpers.POST, "/itemSearchResultTable")
                .withSession("uuid",uuid).withFormUrlEncodedBody(tableParam)    // Note: must append ".withSession()" to let controllers retrieve the proper session!
            play.test.Helpers.routeAndCall(fakeTableSortRequest, timeout)
        }
    }

    it should "display the Price search result table \n   (using an existing session that contains the Price workspace)" in {
        running(FakeApplication()) {     
            // Create a fake UserSession and add the price workspace to it
            val userSession = new UserSession()
            val client = new RestClient("webtest", "entero")
            userSession.setRestClient(client)                               // Price filter implementation requires usage of RestClient...
            val priceWS = WorkspaceFactory.createWorkspace(Endpoint.PRICE)
            userSession.addWorkspace("testws_uid", priceWS)                 // Add the ItemsWS with "testws_uid" as the key
            val uuid = UserSessionManager.addSession(userSession)           // Calling this method adds a session to the play framework's system

            // Create a fake route request for the price search result table
            val tableParam: java.util.Map[String, String] = HashMap(
                "off" -> "0",
                "ws_uid"->"testws_uid", 
                "searchParam_sourceLocation"->"", 
                "searchParam_destinationLocation"->"", 
                "searchParam_itemDescription"->"", 
                "searchParam_description"->"")
            val fakeTableSortRequest = new FakeRequest(play.test.Helpers.POST, "/priceSearchResultTable")
                .withSession("uuid",uuid).withFormUrlEncodedBody(tableParam)// Note: must append ".withSession()" to let controllers retrieve the proper session!
            play.test.Helpers.routeAndCall(fakeTableSortRequest, timeout)
        }
    }

    it should "display the Price search result table \n   (using a fresh session containing no workspace)" in {
        running(FakeApplication()) {     
            // Create a fake UserSession and add the price workspace to it
            val userSession = new UserSession()
            val client = new RestClient("webtest", "entero")
            userSession.setRestClient(client)                       // Price filter implementation requires usage of RestClient...
            val uuid = UserSessionManager.addSession(userSession)   // Calling this method adds a session to the play framework's system

            // Create a fake route request for the price search result table
            val tableParam: java.util.Map[String, String] = HashMap(
                "off" -> "0",
                "ws_uid"->"testws_uid", 
                "searchParam_sourceLocation"->"", 
                "searchParam_destinationLocation"->"", 
                "searchParam_itemDescription"->"", 
                "searchParam_description"->"")
            val fakeTableSortRequest = new FakeRequest(play.test.Helpers.POST, "/priceSearchResultTable")
                .withSession("uuid",uuid).withFormUrlEncodedBody(tableParam)    // Note: must append ".withSession()" to let controllers retrieve the proper session!
            play.test.Helpers.routeAndCall(fakeTableSortRequest, timeout)
        }
    }

    it should "display the Invoice search result table \n   (using an existing session that contains the Price workspace)" in {
        running(FakeApplication()) {     
            // Create a fake UserSession and add the invoice workspace to it
            val userSession = new UserSession()
            val client = new RestClient("webtest", "entero")
            userSession.setRestClient(client)                               // Price filter implementation requires usage of RestClient...
            val priceWS = WorkspaceFactory.createWorkspace(Endpoint.PRICE)
            userSession.addWorkspace("testws_uid", priceWS)                 // Add the ItemsWS with "testws_uid" as the key
            val uuid = UserSessionManager.addSession(userSession)           // Calling this method adds a session to the play framework's system

            // Create a fake route request for the invoice search result table
            val tableParam: java.util.Map[String, String] = HashMap(
                "off" -> "0",
                "ws_uid"->"testws_uid", 
                "searchParam_type"->"", 
                "searchParam_status"->"", 
                "searchParam_invoiceref"->"", 
                "searchParam_payrec"->"",
                "searchParam_datetype"->"",
                "searchParam_datefrom"->"",
                "searchParam_dateto"->"")
            val fakeTableSortRequest = new FakeRequest(play.test.Helpers.POST, "/invoiceSearchResultTable")
                .withSession("uuid",uuid).withFormUrlEncodedBody(tableParam)// Note: must append ".withSession()" to let controllers retrieve the proper session!
            play.test.Helpers.routeAndCall(fakeTableSortRequest, timeout)
        }
    }

    //=======================================================
    behavior of "Back-End: Application.java"
    // Test to see that methods are being called correctly through Play's Router
    //=======================================================
    it should "return the index page containing the main application content" in {
        running(FakeApplication()) {
            // Create a fake UserSession and add the Items workspace to it
            val userSession = new UserSession()
            val client = new RestClient("webtest", "entero")
            userSession.setRestClient(client)
            val priceWS = WorkspaceFactory.createWorkspace(Endpoint.ITEMS)
            userSession.addWorkspace("testws_uid", priceWS)
            val uuid = UserSessionManager.addSession(userSession)           // Calling this method adds a session to the play framework's system

            val fakeRequest = new FakeRequest(play.test.Helpers.GET, "/")
                .withSession("uuid",uuid)//.withFormUrlEncodedBody(tableParam)
            play.test.Helpers.routeAndCall(fakeRequest, timeout)
        }
    }

    it should "call sorting the Items search results table" in {
        running(FakeApplication()) {
            // Create a fake UserSession and add the Items workspace to it
            val userSession = new UserSession()
            val client = new RestClient("webtest", "entero")
            userSession.setRestClient(client)
            val priceWS = WorkspaceFactory.createWorkspace(Endpoint.ITEMS)
            userSession.addWorkspace("testws_uid", priceWS)
            val uuid = UserSessionManager.addSession(userSession)

            // Table containing fake data to be sorted
            val tableParam: java.util.Map[String, String] = HashMap(
                "off" -> "0",
                "ws_uid"->"testws_uid", 
                "column_sort"->ItemIdentifier.ITEMCODE.columnName().toString())

            val fakeRequest = new FakeRequest(play.test.Helpers.POST, "/sortSearchResultTableItems")
                .withSession("uuid",uuid).withFormUrlEncodedBody(tableParam)
            play.test.Helpers.routeAndCall(fakeRequest, timeout)
        }
    }

    it should "call sorting the Price search results table" in {
        running(FakeApplication()) {
            // Create a fake UserSession and add the Items workspace to it
            val userSession = new UserSession()
            val client = new RestClient("webtest", "entero")
            userSession.setRestClient(client)
            val priceWS = WorkspaceFactory.createWorkspace(Endpoint.PRICE)
            userSession.addWorkspace("testws_uid", priceWS)
            val uuid = UserSessionManager.addSession(userSession)

            // Table containing fake data to be sorted
            val tableParam: java.util.Map[String, String] = HashMap(
                "off" -> "0",
                "ws_uid"->"testws_uid", 
                "column_sort"->PriceIdentifier.PRICEDESCRIPTION.columnName().toString())

            val fakeRequest = new FakeRequest(play.test.Helpers.POST, "/sortSearchResultTablePrice")
                .withSession("uuid",uuid).withFormUrlEncodedBody(tableParam)
            play.test.Helpers.routeAndCall(fakeRequest, timeout)
        }
    }

    it should "call sorting the Invoice search results table" in {
        running(FakeApplication()) {
            // Create a fake UserSession and add the Items workspace to it
            val userSession = new UserSession()
            val client = new RestClient("webtest", "entero")
            userSession.setRestClient(client)
            val priceWS = WorkspaceFactory.createWorkspace(Endpoint.INVOICE)
            userSession.addWorkspace("testws_uid", priceWS)
            val uuid = UserSessionManager.addSession(userSession)

            // Table containing fake data to be sorted
            val tableParam: java.util.Map[String, String] = HashMap(
                "off" -> "0",
                "ws_uid"->"testws_uid", 
                "column_sort"->InvoiceIdentifier.COUNTERPARTY.columnName().toString())

            val fakeRequest = new FakeRequest(play.test.Helpers.POST, "/sortSearchResultTableInvoice")
                .withSession("uuid",uuid).withFormUrlEncodedBody(tableParam)
            play.test.Helpers.routeAndCall(fakeRequest, timeout)
        }
    }

    it should "call getting the Items search parameters" in {
        running(FakeApplication()) {
            // Create a fake UserSession and add the Items workspace to it
            val userSession = new UserSession()
            val client = new RestClient("webtest", "entero")
            userSession.setRestClient(client)
            val priceWS = WorkspaceFactory.createWorkspace(Endpoint.ITEMS)
            userSession.addWorkspace("testws_uid", priceWS)
            val uuid = UserSessionManager.addSession(userSession)

            val fakeRequest = new FakeRequest(play.test.Helpers.GET, "/multiParameterList/itemgroup")
                .withSession("uuid",uuid)
            play.test.Helpers.routeAndCall(fakeRequest, timeout)
        }
    }

    it should "call getting the Location type search parameters" in {
        running(FakeApplication()) {
            // Create a fake UserSession and add the Items workspace to it
            val userSession = new UserSession()
            val client = new RestClient("webtest", "entero")
            userSession.setRestClient(client)
            val priceWS = WorkspaceFactory.createWorkspace(Endpoint.ITEMS)
            userSession.addWorkspace("testws_uid", priceWS)
            val uuid = UserSessionManager.addSession(userSession)

            val fakeRequest = new FakeRequest(play.test.Helpers.GET, "/multiParameterList/locationtype")
                .withSession("uuid",uuid)
            play.test.Helpers.routeAndCall(fakeRequest, timeout)
        }
    }

    it should "call getting the Invoice type search parameters" in {
        running(FakeApplication()) {
            // Create a fake UserSession and add the Items workspace to it
            val userSession = new UserSession()
            val client = new RestClient("webtest", "entero")
            userSession.setRestClient(client)
            val priceWS = WorkspaceFactory.createWorkspace(Endpoint.ITEMS)
            userSession.addWorkspace("testws_uid", priceWS)
            val uuid = UserSessionManager.addSession(userSession)

            val fakeRequest = new FakeRequest(play.test.Helpers.GET, "/multiParameterList/invoicetype")
                .withSession("uuid",uuid)
            play.test.Helpers.routeAndCall(fakeRequest, timeout)
        }
    }

    it should "call getting the Invoice status search parameters" in {
        running(FakeApplication()) {
            // Create a fake UserSession and add the Items workspace to it
            val userSession = new UserSession()
            val client = new RestClient("webtest", "entero")
            userSession.setRestClient(client)
            val priceWS = WorkspaceFactory.createWorkspace(Endpoint.ITEMS)
            userSession.addWorkspace("testws_uid", priceWS)
            val uuid = UserSessionManager.addSession(userSession)

            val fakeRequest = new FakeRequest(play.test.Helpers.GET, "/multiParameterList/invoicestatus")
                .withSession("uuid",uuid)
            play.test.Helpers.routeAndCall(fakeRequest, timeout)
        }
    }

    it should "look up the uuid for an Item" in {
        running(FakeApplication()) {

            // Table containing fake data
            val tableParam: java.util.Map[String, String] = HashMap(
                "locationTypes" -> "location",
                "search"->"p")

            val fakeRequest = new FakeRequest(play.test.Helpers.POST, "/uuidlookup/item")
                .withFormUrlEncodedBody(tableParam)
            play.test.Helpers.routeAndCall(fakeRequest, timeout)
        }
    }

    it should "look up the uuid for a location" in {
        running(FakeApplication()) {

            // Table containing fake data
            val tableParam: java.util.Map[String, String] = HashMap(
                "locationTypes" -> "location",
                "search"->"Alberta")

            val fakeRequest = new FakeRequest(play.test.Helpers.POST, "/uuidlookup/location")
                .withFormUrlEncodedBody(tableParam)
            play.test.Helpers.routeAndCall(fakeRequest, timeout)
        }
    }

    it should "export Items table search result table as and Excel spreadsheet" in withRestClient { (mockClient, context) =>
        running(FakeApplication()) {

             // This uses ScalaTest's JMockCycle to wrap the Mockery object
            val cycle = new JMockCycle
            import cycle._

            // Set the context of the mock
            context.checking(
                new Expectations() {

                }
            )                    
            
            // Create fake session
            //val mockSession = new FakeRequest(play.test.Helpers.GET, "/").withSession("username", "webtest")
            //Context.current.set(new Context(mockRequest, new HashMap[String,String]()))

            val jsonTableData = mapper.readTree(
            """["",""],["",""]""".stripMargin)

            val fakeRequest = new FakeRequest(play.test.Helpers.POST, "/itemGenerateExcel").withJsonBody(jsonTableData)
            val result = play.test.Helpers.routeAndCall(fakeRequest, timeout)
        }
    }

    it should "export Price table search result table as and Excel spreadsheet" in withWorkspace { (mockWorkspace, context) =>
        running(FakeApplication()) {

             // This uses ScalaTest's JMockCycle to wrap the Mockery object
            val cycle = new JMockCycle
            import cycle._

            // Set the context of the mock
            context.checking(
                new Expectations() {

                }
            )                    
            
            // Create a fake session with Items workspace opened
            val session = new UserSession()
            session.addWorkspace("Item", mockWorkspace)

            val uuid = UserSessionManager.addSession(session)
            
            play.mvc.Controller.session("uuid", uuid)

            val jsonTableData = mapper.readTree(
            """[{},{}]""".stripMargin)

            val fakeRequest = new FakeRequest(play.test.Helpers.POST, "/datatableExcelExport").withJsonBody(jsonTableData)
            val result = play.test.Helpers.route(fakeRequest)   
        }
    }


    //=======================================================
    behavior of "Back-End: Router"
    //=======================================================
    it should "handle GET request for homepage" in {
        running(FakeApplication()) {
            val fakeRequest = new FakeRequest(play.test.Helpers.GET, "/")
            val result= play.test.Helpers.routeAndCall(fakeRequest, timeout)

            assert(result!=null)
        }
    }

    it should "handle GET request for login" in {
        running(FakeApplication()) {
            val fakeRequest = new FakeRequest(play.test.Helpers.GET, "/login")
            val result= play.test.Helpers.routeAndCall(fakeRequest, timeout)

            assert(result!=null)
        }
    }

    it should "handle POST request for logging in" in {
        running(FakeApplication()) {
            val fakeRequest = new FakeRequest(play.test.Helpers.POST, "/login/post")
            val fakeCredentials: java.util.Map[String, String] = HashMap("username"->"wrongusername", "pass"->"wrongpassword")
            fakeRequest.withFormUrlEncodedBody(fakeCredentials)
            val result= play.test.Helpers.routeAndCall(fakeRequest, timeout)

            assert(result!=null)
        }
    }

    it should "handle POST request for Items search results table" in {
        running(FakeApplication()) {
            val fakeRequest = new FakeRequest(play.test.Helpers.POST, "/itemSearchResultTable")
            val result= play.test.Helpers.routeAndCall(fakeRequest, timeout)

            assert(result!=null)
        }
    }

    // it should "handle POST request for sorting the Items search results table" in {
    //  running(FakeApplication()) {
    //      val fakeRequest = new FakeRequest(play.test.Helpers.POST, "/sortSearchResultTableItems")
    //      val result= play.test.Helpers.routeAndCall(fakeRequest, timeout)

    //      assert(result!=null)
    //  }
    // }

    it should "handle POST request for generating excel" in {
        running(FakeApplication()) {
            val fakeRequest = new FakeRequest(play.test.Helpers.POST, "/itemGenerateExcel")
            val result= play.test.Helpers.routeAndCall(fakeRequest, timeout)

            assert(result!=null)
        }
    }

    it should "handle POST request for Price search results table" in {
     running(FakeApplication()) {
         val fakeRequest = new FakeRequest(play.test.Helpers.POST, "/priceSearchResultTable")
         val result= play.test.Helpers.routeAndCall(fakeRequest, timeout)

         assert(result!=null)
     }
    }

    // it should "handle POST request for sorting the Price search results table" in {
    //  running(FakeApplication()) {
    //      val fakeRequest = new FakeRequest(play.test.Helpers.POST, "/sortSearchResultTablePrice")
    //      val result= play.test.Helpers.routeAndCall(fakeRequest, timeout)

    //      assert(result!=null)
    //  }
    // }

    // it should "handle GET request for sorting the Price search results table" in {
    //  running(FakeApplication()) {
    //      val fakeRequest = new FakeRequest(play.test.Helpers.GET, "/sortSearchResultTablePrice")
    //      val result= play.test.Helpers.routeAndCall(fakeRequest, timeout)

    //      assert(result!=null)
    //  }
    // }

    //=======================================================
    behavior of "Back-End: Views Rendering"
    // Please refer to the route file for more information on how to intialize these pages!
    //=======================================================
    val fakeInvoiceItemJsonResponse = mapper.readTree( // Fake Invoice JSON response for ONE price object
    """ |[
        |    {
        |        "payRec": "Receivable",
        |        "type": "Sales",
        |        "counterparty": {
        |            "contact": {
        |                "organizationName": "Bartells",
        |                "departmentName": "Rusty",
        |                "description": "Bartells, Rusty",
        |                "type": "Individual",
        |                "stakeholderId": 500051,
        |                "id": "22ba6e59-09cf-438e-8b1c-e551cfdf7cae",
        |                "ref": "http://172.24.0.103:80/api/stakeholder/22ba6e59-09cf-438e-8b1c-e551cfdf7cae"
        |            },
        |            "address": {
        |                "line1": "13105 Northwest Freeway ",
        |                "description": "13105 Northwest Freeway ",
        |                "type": "Office",
        |                "id": "5b0ad9b3-1e2c-45f2-8ed7-fb5193bf680a",
        |                "ref": "http://172.24.0.103:80/api/location/5b0ad9b3-1e2c-45f2-8ed7-fb5193bf680a"
        |            },
        |            "organizationName": "ZEROX",
        |            "description": "Amerigas Propane Incorporated",
        |            "type": "External Organization",
        |            "stakeholderId": 500045,
        |            "id": "7fd1c30e-a1be-468e-be84-c45ca6a23f5b",
        |            "ref": "http://172.24.0.103:80/api/organization/7fd1c30e-a1be-468e-be84-c45ca6a23f5b"
        |        },
        |        "businessUnit": {
        |            "contact": {
        |                "organizationName": "Sky",
        |                "departmentName": "Tammy",
        |                "description": "Sky, Tammy",
        |                "type": "Individual",
        |                "stakeholderId": 122,
        |                "id": "D7620DF9-72C1-4EC2-992E-98AE0A9C4F9F",
        |                "ref": "http://172.24.0.103:80/api/stakeholder/D7620DF9-72C1-4EC2-992E-98AE0A9C4F9F"
        |            },
        |            "address": {
        |                "line1": "440 - 2nd Ave. SW",
        |                "description": "440 - 2nd Ave. SW Suite 1500 Ernst & Young Tower Calgary, AB T2P 5E9",
        |                "type": "Office",
        |                "id": "6EDD95F2-EAC5-4EDB-94EF-CFE0679A4713",
        |                "ref": "http://172.24.0.103:80/api/location/6EDD95F2-EAC5-4EDB-94EF-CFE0679A4713"
        |            },
        |            "organizationName": "Northern Gas Liquids Marketing",
        |            "description": "Northern Gas Liquids Marketing",
        |            "type": "Operating Business Unit",
        |            "stakeholderId": 14,
        |            "id": "F3BC5F74-FF45-4AC4-B964-3E842127765F",
        |            "ref": "http://172.24.0.103:80/api/organization/F3BC5F74-FF45-4AC4-B964-3E842127765F"
        |        },
        |        "account": "Accounts Receivable $US",
        |        "currency": {
        |            "description": "U.S. Dollars",
        |            "currencyCode": "USD",
        |            "id": "60E8783A-FCB0-4395-91F3-1B771D480159",
        |            "ref": "http://172.24.0.103:80/api/currency/60E8783A-FCB0-4395-91F3-1B771D480159?token=e932cbd5-ea1b-40cb-a2b9-28bf0fb4cd2c&format=json"
        |        },
        |        "paymentTerms": "1% 10 Net 15",
        |        "format": "Standard Layout (S)",
        |        "invoiceMode": "Hard Copy",
        |        "issuedDate": "Nov 7, 1999 12:00:00 AM",
        |        "dueDate": "Nov 21, 2000 12:00:00 AM",
        |        "discountDate": "Nov 17, 2008 12:00:00 AM",
        |        "daysOverDue": 2338,
        |        "invoiceId": 500008,
        |        "invoiceRef": "NE Mktg-1",
        |        "status": "Paid",
        |        "invoiceStatus": "PAID",
        |        "paymentMode": "Check",
        |        "netAmount": 920326.39,
        |        "taxAmount": 3903.02,
        |        "adjustmentAmount": 0,
        |        "discountAmount": 9242.29,
        |        "totalAmount": 924229.41,
        |        "selfAssessmentAmount": 0,
        |        "outstandingAmount": 0,
        |        "totalPaymentAmount": 914987.12,
        |        "lastPaymentDate": "Nov 14, 2008 12:00:00 AM",
        |        "lastPaymentAmount": 914987.12,
        |        "lastPaymentRef": "500032",
        |        "remitAddress": {
        |            "line1": "440 - 2nd Ave. SW",
        |            "description": "440 - 2nd Ave. SW Suite 1500 Ernst & Young Tower Calgary, AB T2P 5E9",
        |            "type": "Office",
        |            "id": "6EDD95F2-EAC5-4EDB-94EF-CFE0679A4713",
        |            "ref": "http://172.24.0.103:80/api/location/6EDD95F2-EAC5-4EDB-94EF-CFE0679A4713?token=e932cbd5-ea1b-40cb-a2b9-28bf0fb4cd2c&format=json"
        |        },
        |        "id": "403b6385-4f5f-4f69-abaf-c13cd206b79f",
        |        "ref": "http://172.24.0.103:80/api/invoice/403b6385-4f5f-4f69-abaf-c13cd206b79f?token=e932cbd5-ea1b-40cb-a2b9-28bf0fb4cd2c&format=json"
        |    }
        |]""".stripMargin)

    it should "render the login page that prompts for user credentials" in {
        running(FakeApplication()) {
            val context = httpContext
            val login = Login.login()

            val loginHtml = views.html.login.render()
            assert(loginHtml.toString().contains("login-panel"))
        }
    }

    it should "render the index page that displays the three panels for the workspaces" in {
        running(FakeApplication()) {
            val context = httpContext

            val indexHtml = views.html.index.render(
                ArrayBuffer("itemTestType1", "itemTestType2"),
                ArrayBuffer("payRec1", "payRec2"),
                ArrayBuffer("invoiceTestType1", "invoiceTestType2"),
                ArrayBuffer("invoiceStatus1", "invoiceStatus2"),
                ArrayBuffer("testtype"))
            assert(indexHtml.toString().contains("split-component-1"))
            assert(indexHtml.toString().contains("split-pane-vertical"))
            assert(indexHtml.toString().contains("split-component-2"))
            assert(indexHtml.toString().contains("tab-navigation"))
            assert(indexHtml.toString().contains("search-results-panel"))
        }
    }

    it should "render the multi-search parameter pane" in {
        running(FakeApplication()) {
            val context = httpContext

            val searchPanelHtml = views.html.multiSelectParameter.render(ArrayBuffer("Items", "Invoice", "Price"))
            assert(searchPanelHtml.toString().contains("search-parameter"))
        }
    }

    it should "render the search results pane" in {
        running(FakeApplication()) {
            val context = httpContext

            val listEnteroObject: java.util.List[EnteroObject] = ArrayBuffer(new EnteroObject())
            val columnsToDisplay = ArrayBuffer("Description")
            val offset = 0
            val resultsPanelHtml = views.html.searchresult.render(listEnteroObject, columnsToDisplay, "", "Items WS", offset)
            assert(resultsPanelHtml.toString().contains("datatable"))
        }
    }

    it should "show the item's UUID in the search results pane" in {
        running(FakeApplication()) {
            val context = httpContext

            val listEnteroObject: java.util.List[EnteroObject] = ArrayBuffer(new EnteroObject())
            val columnsToDisplay = ArrayBuffer("Description")
            val resultsPanelHtml = views.html.uuidlookup.render(listEnteroObject, columnsToDisplay)
            assert(resultsPanelHtml.toString().contains("datatable uuidlookup"))
        }
    }

    it should "render the pane that displays items details" in {
        running(FakeApplication()) {
            val context = httpContext

            val detailPaneHtml = views.html.detailviewItems.render(new EnteroObject())
            assert(detailPaneHtml.toString().contains("detail-view"))
        }
    }

    it should "render the panes that displays price details" in {
        running(FakeApplication()) {
            val context = httpContext

            val listEnteroObject: java.util.List[EnteroObject] = ArrayBuffer(new EnteroObject())
            val detailPaneHtml = views.html.detailviewPrice.render(new EnteroObject(), listEnteroObject)
            assert(detailPaneHtml.toString().contains("detail-view"))
        }
    }

    it should "render the panes that displays invoice details" in withRestClient { (mockClient, context) =>
        val invoiceObjID = "65730d76-9870-4162-85cb-a8a10f6f8cca"

        val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
        import cycle._

        context.checking( // Set the context of the mock
            new Expectations() {
                allowing(mockClient).setHolder("/invoice/"+invoiceObjID)

                exactly(1).of(mockClient).getResponseAsJson()
                    will(returnValue(fakeInvoiceItemJsonResponse))
            }
        )

        // Test the system:
        val invoiceWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.INVOICE)
        invoiceWS.setClient(mockClient)

        // Fetch the details of the specified invoice object using its ID
        val invoiceDetails = invoiceWS.filterDetails(invoiceObjID)
        val listEnteroObject: java.util.List[EnteroObject] = ArrayBuffer(new EnteroObject())
        val detailPaneHtml = views.html.detailviewInvoice.render(invoiceDetails, listEnteroObject)
        //println(detailPaneHtml.toString())
        assert(detailPaneHtml.toString().contains("detail-view"))
        }
    
}