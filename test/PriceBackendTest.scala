package suites

import com.entero.generic.UserSession
import com.entero.generic.UserSessionManager
import com.entero.identifiers.PriceIdentifier
import com.entero.identifiers.ItemIdentifier
import com.entero.rest.RestClient
import com.entero.workspace.Workspace
import com.entero.workspace.WorkspaceFactory
import com.entero.workspace.WorkspaceFactory.Endpoint
import models.EnteroObject

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper

import play.api.test.Helpers.running
import play.api.test.FakeApplication

import collection.JavaConversions._
import collection.mutable._
import org.scalatest.junit.AssertionsForJUnit
import org.scalatest.PrivateMethodTester._
import org.scalatest._

import org.scalatest.mock.JMockCycle
import org.jmock.Expectations
import org.jmock.Expectations.returnValue
import org.scalatest.mock.JMockExpectations
import org.jmock.Mockery
import org.jmock.lib.legacy.ClassImposteriser
import org.jmock.lib.concurrent.Synchroniser

import org.scalatest.mock.{JMockCycleFixture, JMockCycle}

/**
 * READ ME (Last updated 24/11/2014):
 * The following tests uses ScalaTest in conjunction with Selenium to 
 * simulate a web browser to test the web app's behaviour, as well as
 * verifying the information. Tests are structured in FlatSpec format,
 * which follows BDD(Behavior-Driven Design) principles.
 * 
 * To compile these tests you need to have the following lines added to your SBT build file:
 * libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.1" % "test"
 * libraryDependencies += "org.seleniumhq.selenium" % "selenium-java" % "2.44.0" % "test"
 *
 * To run these tests you simply type "test" in your sbt terminal.
 *
 */
class PriceBackendTest extends FlatSpec {
    val maxOffset = "50"
    val mapper = new ObjectMapper()

    /** This defines the fixture method to create mock RestClient objects!
     * Search "Loan-Fixture" Method for more information
     */
    def withRestClient(testCode: (RestClient, Mockery) => Any) {
        
        // This creates a context object from JMock
        var context = new Mockery
        context.setImposteriser(ClassImposteriser.INSTANCE)
        context.setThreadingPolicy(new Synchroniser()) //This prevents race conditions with mock objects!

        var mockClient = context.mock(classOf[RestClient])
        try {
            // Loans the mockClient to the tests
            testCode(mockClient, context)
        }
        finally {
            // Clean up objects
            mockClient = null
            context = null
        }
    }
    
    //=======================================================
    behavior of "Price Workspace"
    //=======================================================
    val fakePriceItemJsonResponse = mapper.readTree(   // This fake response represents ONE fake price item
    """ |[
        |   {
        |        "description": "Mankato Terminal (Cochin) - C3 - Intercompany Transfer Price",
        |        "item": {
        |            "shortDescription": "C3",
        |            "description": "Propane - HD5 Not Odorized",
        |            "locationControl": false,
        |            "odorized": false,
        |            "carryInventoryQuality": false,
        |            "rinRequired": false,
        |            "qtyByComponent": false,
        |            "id": "E7B8241A-6484-4464-AE6C-5049620A6685",
        |            "ref": "http://172.24.0.103:80/api/item/E7B8241A-6484-4464-AE6C-5049620A6685?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
        |        },
        |        "sourceLocation": {
        |            "line1": "Mankato Terminal (Cochin)",
        |            "description": "Mankato Terminal (Cochin)",
        |            "type": "Facility",
        |            "id": "2756402c-5a82-43b5-8456-ecd4cbd201f2",
        |            "ref": "http://172.24.0.103:80/api/location/2756402c-5a82-43b5-8456-ecd4cbd201f2?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
        |        },
        |        "destinationLocation": {
        |            "line1": "Mankato Terminal (Cochin)",
        |            "description": "Mankato Terminal (Cochin)",
        |            "type": "Facility",
        |            "id": "2756402c-5a82-43b5-8456-ecd4cbd201f2",
        |            "ref": "http://172.24.0.103:80/api/location/2756402c-5a82-43b5-8456-ecd4cbd201f2?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
        |        },
        |        "priceCategory": "Product - Formula Price",
        |        "currency": {
        |            "description": "Canadian Dollars",
        |            "currencyCode": "CAD",
        |            "id": "B051DBB6-F3B2-44CF-84E5-CE98268FB68C",
        |            "ref": "http://172.24.0.103:80/api/currency/B051DBB6-F3B2-44CF-84E5-CE98268FB68C?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
        |        },
        |        "uom": {
        |            "code": "M3",
        |            "shortDescription": "m3",
        |            "description": "Cubic Meters",
        |            "id": "2A144658-BACB-46D4-97C9-1864B15B445C",
        |            "ref": "http://172.24.0.103:80/api/unitofmeasure/2A144658-BACB-46D4-97C9-1864B15B445C?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
        |        },
        |        "id": "e48ca5d0-50a8-49a4-8a7f-a3c43b93dcf9",
        |        "ref": "http://172.24.0.103:80/api/price/e48ca5d0-50a8-49a4-8a7f-a3c43b93dcf9?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
        |    }
        |]""".stripMargin)
    
    val fakePriceItemsJsonResponseSize = 5   // Represents the number of price objects in the fake response
    val fakePriceItemsJsonResponse = mapper.readTree(   // Fake price JSON response that DOESN'T contain price details 
    """ |[
        |    {
        |        "description": "Fixed Amount",
        |        "item": {
        |            "shortDescription": "Undefined Item",
        |            "description": "Undefined Item",
        |            "locationControl": false,
        |            "odorized": false,
        |            "carryInventoryQuality": false,
        |            "rinRequired": false,
        |            "qtyByComponent": false,
        |            "id": "23A53BBC-C495-4CCA-9EEF-70AC901A1848",
        |            "ref": "http://172.24.0.103:80/api/item/23A53BBC-C495-4CCA-9EEF-70AC901A1848?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |        },
        |        "sourceLocation": {
        |            "line1": "Undefined",
        |            "description": "Undefined",
        |            "type": "Internal Type for Undefined",
        |            "id": "D7DBAC63-04D9-4BA7-9C12-BD88AE7E552B",
        |            "ref": "http://172.24.0.103:80/api/location/D7DBAC63-04D9-4BA7-9C12-BD88AE7E552B?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |        },
        |        "destinationLocation": {
        |            "line1": "Undefined",
        |            "description": "Undefined",
        |            "type": "Internal Type for Undefined",
        |            "id": "D7DBAC63-04D9-4BA7-9C12-BD88AE7E552B",
        |            "ref": "http://172.24.0.103:80/api/location/D7DBAC63-04D9-4BA7-9C12-BD88AE7E552B?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |        },
        |        "priceCategory": "Product - Published Reference Price",
        |        "currency": {
        |            "description": "Canadian Dollars",
        |            "currencyCode": "CAD",
        |            "id": "B051DBB6-F3B2-44CF-84E5-CE98268FB68C",
        |            "ref": "http://172.24.0.103:80/api/currency/B051DBB6-F3B2-44CF-84E5-CE98268FB68C?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |        },
        |        "uom": {
        |            "code": "LI",
        |            "shortDescription": "L",
        |            "description": "Liters",
        |            "id": "B7FF11F4-790F-4346-BFB0-F84C336A1319",
        |            "ref": "http://172.24.0.103:80/api/unitofmeasure/B7FF11F4-790F-4346-BFB0-F84C336A1319?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |        },
        |        "id": "550E41EB-14FA-4C59-B9EF-D1B73AD5FFCD",
        |        "ref": "http://172.24.0.103:80/api/price/550E41EB-14FA-4C59-B9EF-D1B73AD5FFCD?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |    },
        |    {
        |        "description": "Gibson Terminaling Fee ",
        |        "item": {
        |            "shortDescription": "C3",
        |            "description": "Propane - HD5 Not Odorized",
        |            "locationControl": false,
        |            "odorized": false,
        |            "carryInventoryQuality": false,
        |            "rinRequired": false,
        |            "qtyByComponent": false,
        |            "id": "E7B8241A-6484-4464-AE6C-5049620A6685",
        |            "ref": "http://172.24.0.103:80/api/item/E7B8241A-6484-4464-AE6C-5049620A6685?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |        },
        |        "sourceLocation": {
        |            "line1": "Gibson Terminal Edmonton ",
        |            "description": "Gibson Terminal Edmonton ",
        |            "type": "Facility",
        |            "id": "f2925573-5065-4626-a03b-cac999d569ce",
        |            "ref": "http://172.24.0.103:80/api/location/f2925573-5065-4626-a03b-cac999d569ce?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |        },
        |        "destinationLocation": {
        |            "line1": "Gibson Terminal Edmonton ",
        |            "description": "Gibson Terminal Edmonton ",
        |            "type": "Facility",
        |            "id": "f2925573-5065-4626-a03b-cac999d569ce",
        |            "ref": "http://172.24.0.103:80/api/location/f2925573-5065-4626-a03b-cac999d569ce?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |        },
        |        "priceCategory": "Terminalling Fee",
        |        "currency": {
        |            "description": "Canadian Dollars",
        |            "currencyCode": "CAD",
        |            "id": "B051DBB6-F3B2-44CF-84E5-CE98268FB68C",
        |            "ref": "http://172.24.0.103:80/api/currency/B051DBB6-F3B2-44CF-84E5-CE98268FB68C?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |        },
        |        "uom": {
        |            "code": "M3",
        |            "shortDescription": "m3",
        |            "description": "Cubic Meters",
        |            "id": "2A144658-BACB-46D4-97C9-1864B15B445C",
        |            "ref": "http://172.24.0.103:80/api/unitofmeasure/2A144658-BACB-46D4-97C9-1864B15B445C?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |        },
        |        "id": "fe608b4b-0d8d-4890-a6ad-8c1bce33d346",
        |        "ref": "http://172.24.0.103:80/api/price/fe608b4b-0d8d-4890-a6ad-8c1bce33d346?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |    },
        |    {
        |        "description": "Mankato Terminal (Cochin) - C3 - Intercompany Transfer Price",
        |        "item": {
        |            "shortDescription": "C3",
        |            "description": "Propane - HD5 Not Odorized",
        |            "locationControl": false,
        |            "odorized": false,
        |            "carryInventoryQuality": false,
        |            "rinRequired": false,
        |            "qtyByComponent": false,
        |            "id": "E7B8241A-6484-4464-AE6C-5049620A6685",
        |            "ref": "http://172.24.0.103:80/api/item/E7B8241A-6484-4464-AE6C-5049620A6685?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |        },
        |        "sourceLocation": {
        |            "line1": "Mankato Terminal (Cochin)",
        |            "description": "Mankato Terminal (Cochin)",
        |            "type": "Facility",
        |            "id": "2756402c-5a82-43b5-8456-ecd4cbd201f2",
        |            "ref": "http://172.24.0.103:80/api/location/2756402c-5a82-43b5-8456-ecd4cbd201f2?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |        },
        |        "destinationLocation": {
        |            "line1": "Mankato Terminal (Cochin)",
        |            "description": "Mankato Terminal (Cochin)",
        |            "type": "Facility",
        |            "id": "2756402c-5a82-43b5-8456-ecd4cbd201f2",
        |            "ref": "http://172.24.0.103:80/api/location/2756402c-5a82-43b5-8456-ecd4cbd201f2?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |        },
        |        "priceCategory": "Product - Formula Price",
        |        "currency": {
        |            "description": "Canadian Dollars",
        |            "currencyCode": "CAD",
        |            "id": "B051DBB6-F3B2-44CF-84E5-CE98268FB68C",
        |            "ref": "http://172.24.0.103:80/api/currency/B051DBB6-F3B2-44CF-84E5-CE98268FB68C?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |        },
        |        "uom": {
        |            "code": "M3",
        |            "shortDescription": "m3",
        |            "description": "Cubic Meters",
        |            "id": "2A144658-BACB-46D4-97C9-1864B15B445C",
        |            "ref": "http://172.24.0.103:80/api/unitofmeasure/2A144658-BACB-46D4-97C9-1864B15B445C?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |        },
        |        "id": "e48ca5d0-50a8-49a4-8a7f-a3c43b93dcf9",
        |        "ref": "http://172.24.0.103:80/api/price/e48ca5d0-50a8-49a4-8a7f-a3c43b93dcf9?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |    },
        |    {
        |        "description": "WTI Bonus - A",
        |        "item": {
        |            "shortDescription": "WTI",
        |            "description": "West Texas Intermediate Crude",
        |            "locationControl": false,
        |            "odorized": false,
        |            "carryInventoryQuality": false,
        |            "rinRequired": false,
        |            "qtyByComponent": false,
        |            "id": "B4E5F05C-D05D-46C3-B0EE-2293BE9E096A",
        |            "ref": "http://172.24.0.103:80/api/item/B4E5F05C-D05D-46C3-B0EE-2293BE9E096A?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |        },
        |        "sourceLocation": {
        |            "line1": "Lease 1",
        |            "description": "Lease 1",
        |            "type": "Well",
        |            "id": "88200e8d-d5b2-48c7-a8a8-e886b76c061a",
        |            "ref": "http://172.24.0.103:80/api/location/88200e8d-d5b2-48c7-a8a8-e886b76c061a?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |        },
        |        "destinationLocation": {
        |            "line1": "Lease 1",
        |            "description": "Lease 1",
        |            "type": "Well",
        |            "id": "88200e8d-d5b2-48c7-a8a8-e886b76c061a",
        |            "ref": "http://172.24.0.103:80/api/location/88200e8d-d5b2-48c7-a8a8-e886b76c061a?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |        },
        |        "priceCategory": "Product - Base Price",
        |        "currency": {
        |            "description": "U.S. Dollars",
        |            "currencyCode": "USD",
        |            "id": "60E8783A-FCB0-4395-91F3-1B771D480159",
        |            "ref": "http://172.24.0.103:80/api/currency/60E8783A-FCB0-4395-91F3-1B771D480159?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |        },
        |        "uom": {
        |            "code": "BBL",
        |            "shortDescription": "BBL",
        |            "description": "Barrels",
        |            "id": "31FBE848-3609-43DE-AA1F-AC073946F445",
        |            "ref": "http://172.24.0.103:80/api/unitofmeasure/31FBE848-3609-43DE-AA1F-AC073946F445?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |        },
        |        "id": "c5f47c01-ab3a-4a90-a98e-6c0fe74c49ab",
        |        "ref": "http://172.24.0.103:80/api/price/c5f47c01-ab3a-4a90-a98e-6c0fe74c49ab?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |    },
        |    {
        |        "description": "WTI Bonus - D",
        |        "item": {
        |            "shortDescription": "WTI",
        |            "description": "West Texas Intermediate Crude",
        |            "locationControl": false,
        |            "odorized": false,
        |            "carryInventoryQuality": false,
        |            "rinRequired": false,
        |            "qtyByComponent": false,
        |            "id": "B4E5F05C-D05D-46C3-B0EE-2293BE9E096A",
        |            "ref": "http://172.24.0.103:80/api/item/B4E5F05C-D05D-46C3-B0EE-2293BE9E096A?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |        },
        |        "sourceLocation": {
        |            "line1": "Lease 1",
        |            "description": "Lease 1",
        |            "type": "Well",
        |            "id": "88200e8d-d5b2-48c7-a8a8-e886b76c061a",
        |            "ref": "http://172.24.0.103:80/api/location/88200e8d-d5b2-48c7-a8a8-e886b76c061a?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |        },
        |        "destinationLocation": {
        |            "line1": "Lease 1",
        |            "description": "Lease 1",
        |            "type": "Well",
        |            "id": "88200e8d-d5b2-48c7-a8a8-e886b76c061a",
        |            "ref": "http://172.24.0.103:80/api/location/88200e8d-d5b2-48c7-a8a8-e886b76c061a?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |        },
        |        "priceCategory": "Product - Base Price",
        |        "currency": {
        |            "description": "U.S. Dollars",
        |            "currencyCode": "USD",
        |            "id": "60E8783A-FCB0-4395-91F3-1B771D480159",
        |            "ref": "http://172.24.0.103:80/api/currency/60E8783A-FCB0-4395-91F3-1B771D480159?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |        },
        |        "uom": {
        |            "code": "BBL",
        |            "shortDescription": "BBL",
        |            "description": "Barrels",
        |            "id": "31FBE848-3609-43DE-AA1F-AC073946F445",
        |            "ref": "http://172.24.0.103:80/api/unitofmeasure/31FBE848-3609-43DE-AA1F-AC073946F445?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |        },
        |        "id": "136fa5a9-7a01-4800-ac26-bf79df79a539",
        |        "ref": "http://172.24.0.103:80/api/price/136fa5a9-7a01-4800-ac26-bf79df79a539?token=1ba8ecd1-3a0b-4157-86ff-a6de9a7f56fd&format=json"
        |    }
        |]""".stripMargin)


    // val fakePriceDescJsonResponse = mapper.readTree(   // This fake response represents price descriptions
    // """ |[
    //     |    {
    //     |        "description": "Fixed Amount",
    //     |        "item": {
    //     |            "shortDescription": "Undefined Item",
    //     |            "description": "Undefined Item",
    //     |            "locationControl": false,
    //     |            "odorized": false,
    //     |            "carryInventoryQuality": false,
    //     |            "rinRequired": false,
    //     |            "qtyByComponent": false,
    //     |            "id": "23A53BBC-C495-4CCA-9EEF-70AC901A1848",
    //     |            "ref": "http://172.24.0.103:80/api/item/23A53BBC-C495-4CCA-9EEF-70AC901A1848?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |        },
    //     |        "sourceLocation": {
    //     |            "line1": "Undefined",
    //     |            "description": "Undefined",
    //     |            "type": "Internal Type for Undefined",
    //     |            "id": "D7DBAC63-04D9-4BA7-9C12-BD88AE7E552B",
    //     |            "ref": "http://172.24.0.103:80/api/location/D7DBAC63-04D9-4BA7-9C12-BD88AE7E552B?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |        },
    //     |        "destinationLocation": {
    //     |            "line1": "Undefined",
    //     |            "description": "Undefined",
    //     |            "type": "Internal Type for Undefined",
    //     |            "id": "D7DBAC63-04D9-4BA7-9C12-BD88AE7E552B",
    //     |            "ref": "http://172.24.0.103:80/api/location/D7DBAC63-04D9-4BA7-9C12-BD88AE7E552B?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |        },
    //     |        "priceCategory": "Product - Published Reference Price",
    //     |        "currency": {
    //     |            "description": "Canadian Dollars",
    //     |            "currencyCode": "CAD",
    //     |            "id": "B051DBB6-F3B2-44CF-84E5-CE98268FB68C",
    //     |            "ref": "http://172.24.0.103:80/api/currency/B051DBB6-F3B2-44CF-84E5-CE98268FB68C?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |        },
    //     |        "uom": {
    //     |            "code": "LI",
    //     |            "shortDescription": "L",
    //     |            "description": "Liters",
    //     |            "id": "B7FF11F4-790F-4346-BFB0-F84C336A1319",
    //     |            "ref": "http://172.24.0.103:80/api/unitofmeasure/B7FF11F4-790F-4346-BFB0-F84C336A1319?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |        },
    //     |        "id": "550E41EB-14FA-4C59-B9EF-D1B73AD5FFCD",
    //     |        "ref": "http://172.24.0.103:80/api/price/550E41EB-14FA-4C59-B9EF-D1B73AD5FFCD?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |    },
    //     |    {
    //     |        "description": "Gibson Terminaling Fee ",
    //     |        "item": {
    //     |            "shortDescription": "C3",
    //     |            "description": "Propane - HD5 Not Odorized",
    //     |            "locationControl": false,
    //     |            "odorized": false,
    //     |            "carryInventoryQuality": false,
    //     |            "rinRequired": false,
    //     |            "qtyByComponent": false,
    //     |            "id": "E7B8241A-6484-4464-AE6C-5049620A6685",
    //     |            "ref": "http://172.24.0.103:80/api/item/E7B8241A-6484-4464-AE6C-5049620A6685?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |        },
    //     |        "sourceLocation": {
    //     |            "line1": "Gibson Terminal Edmonton ",
    //     |            "description": "Gibson Terminal Edmonton ",
    //     |            "type": "Facility",
    //     |            "id": "f2925573-5065-4626-a03b-cac999d569ce",
    //     |            "ref": "http://172.24.0.103:80/api/location/f2925573-5065-4626-a03b-cac999d569ce?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |        },
    //     |        "destinationLocation": {
    //     |            "line1": "Gibson Terminal Edmonton ",
    //     |            "description": "Gibson Terminal Edmonton ",
    //     |            "type": "Facility",
    //     |            "id": "f2925573-5065-4626-a03b-cac999d569ce",
    //     |            "ref": "http://172.24.0.103:80/api/location/f2925573-5065-4626-a03b-cac999d569ce?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |        },
    //     |        "priceCategory": "Terminalling Fee",
    //     |        "currency": {
    //     |            "description": "Canadian Dollars",
    //     |            "currencyCode": "CAD",
    //     |            "id": "B051DBB6-F3B2-44CF-84E5-CE98268FB68C",
    //     |            "ref": "http://172.24.0.103:80/api/currency/B051DBB6-F3B2-44CF-84E5-CE98268FB68C?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |        },
    //     |        "uom": {
    //     |            "code": "M3",
    //     |            "shortDescription": "m3",
    //     |            "description": "Cubic Meters",
    //     |            "id": "2A144658-BACB-46D4-97C9-1864B15B445C",
    //     |            "ref": "http://172.24.0.103:80/api/unitofmeasure/2A144658-BACB-46D4-97C9-1864B15B445C?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |        },
    //     |        "id": "fe608b4b-0d8d-4890-a6ad-8c1bce33d346",
    //     |        "ref": "http://172.24.0.103:80/api/price/fe608b4b-0d8d-4890-a6ad-8c1bce33d346?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |    },
    //     |    {
    //     |        "description": "Mankato Terminal (Cochin) - C3 - Intercompany Transfer Price",
    //     |        "item": {
    //     |            "shortDescription": "C3",
    //     |            "description": "Propane - HD5 Not Odorized",
    //     |            "locationControl": false,
    //     |            "odorized": false,
    //     |            "carryInventoryQuality": false,
    //     |            "rinRequired": false,
    //     |            "qtyByComponent": false,
    //     |            "id": "E7B8241A-6484-4464-AE6C-5049620A6685",
    //     |            "ref": "http://172.24.0.103:80/api/item/E7B8241A-6484-4464-AE6C-5049620A6685?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |        },
    //     |        "sourceLocation": {
    //     |            "line1": "Mankato Terminal (Cochin)",
    //     |            "description": "Mankato Terminal (Cochin)",
    //     |            "type": "Facility",
    //     |            "id": "2756402c-5a82-43b5-8456-ecd4cbd201f2",
    //     |            "ref": "http://172.24.0.103:80/api/location/2756402c-5a82-43b5-8456-ecd4cbd201f2?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |        },
    //     |        "destinationLocation": {
    //     |            "line1": "Mankato Terminal (Cochin)",
    //     |            "description": "Mankato Terminal (Cochin)",
    //     |            "type": "Facility",
    //     |            "id": "2756402c-5a82-43b5-8456-ecd4cbd201f2",
    //     |            "ref": "http://172.24.0.103:80/api/location/2756402c-5a82-43b5-8456-ecd4cbd201f2?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |        },
    //     |        "priceCategory": "Product - Formula Price",
    //     |        "currency": {
    //     |            "description": "Canadian Dollars",
    //     |            "currencyCode": "CAD",
    //     |            "id": "B051DBB6-F3B2-44CF-84E5-CE98268FB68C",
    //     |            "ref": "http://172.24.0.103:80/api/currency/B051DBB6-F3B2-44CF-84E5-CE98268FB68C?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |        },
    //     |        "uom": {
    //     |            "code": "M3",
    //     |            "shortDescription": "m3",
    //     |            "description": "Cubic Meters",
    //     |            "id": "2A144658-BACB-46D4-97C9-1864B15B445C",
    //     |            "ref": "http://172.24.0.103:80/api/unitofmeasure/2A144658-BACB-46D4-97C9-1864B15B445C?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |        },
    //     |        "id": "e48ca5d0-50a8-49a4-8a7f-a3c43b93dcf9",
    //     |        "ref": "http://172.24.0.103:80/api/price/e48ca5d0-50a8-49a4-8a7f-a3c43b93dcf9?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |    },
    //     |    {
    //     |        "description": "WTI Bonus - A",
    //     |        "item": {
    //     |            "shortDescription": "WTI",
    //     |            "description": "West Texas Intermediate Crude",
    //     |            "locationControl": false,
    //     |            "odorized": false,
    //     |            "carryInventoryQuality": false,
    //     |            "rinRequired": false,
    //     |            "qtyByComponent": false,
    //     |            "id": "B4E5F05C-D05D-46C3-B0EE-2293BE9E096A",
    //     |            "ref": "http://172.24.0.103:80/api/item/B4E5F05C-D05D-46C3-B0EE-2293BE9E096A?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |        },
    //     |        "sourceLocation": {
    //     |            "line1": "Lease 1",
    //     |            "description": "Lease 1",
    //     |            "type": "Well",
    //     |            "id": "88200e8d-d5b2-48c7-a8a8-e886b76c061a",
    //     |            "ref": "http://172.24.0.103:80/api/location/88200e8d-d5b2-48c7-a8a8-e886b76c061a?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |        },
    //     |        "destinationLocation": {
    //     |            "line1": "Lease 1",
    //     |            "description": "Lease 1",
    //     |            "type": "Well",
    //     |            "id": "88200e8d-d5b2-48c7-a8a8-e886b76c061a",
    //     |            "ref": "http://172.24.0.103:80/api/location/88200e8d-d5b2-48c7-a8a8-e886b76c061a?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |        },
    //     |        "priceCategory": "Product - Base Price",
    //     |        "currency": {
    //     |            "description": "U.S. Dollars",
    //     |            "currencyCode": "USD",
    //     |            "id": "60E8783A-FCB0-4395-91F3-1B771D480159",
    //     |            "ref": "http://172.24.0.103:80/api/currency/60E8783A-FCB0-4395-91F3-1B771D480159?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |        },
    //     |        "uom": {
    //     |            "code": "BBL",
    //     |            "shortDescription": "BBL",
    //     |            "description": "Barrels",
    //     |            "id": "31FBE848-3609-43DE-AA1F-AC073946F445",
    //     |            "ref": "http://172.24.0.103:80/api/unitofmeasure/31FBE848-3609-43DE-AA1F-AC073946F445?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |        },
    //     |        "id": "c5f47c01-ab3a-4a90-a98e-6c0fe74c49ab",
    //     |        "ref": "http://172.24.0.103:80/api/price/c5f47c01-ab3a-4a90-a98e-6c0fe74c49ab?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |    },
    //     |    {
    //     |        "description": "WTI Bonus - D",
    //     |        "item": {
    //     |            "shortDescription": "WTI",
    //     |            "description": "West Texas Intermediate Crude",
    //     |            "locationControl": false,
    //     |            "odorized": false,
    //     |            "carryInventoryQuality": false,
    //     |            "rinRequired": false,
    //     |            "qtyByComponent": false,
    //     |            "id": "B4E5F05C-D05D-46C3-B0EE-2293BE9E096A",
    //     |            "ref": "http://172.24.0.103:80/api/item/B4E5F05C-D05D-46C3-B0EE-2293BE9E096A?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |        },
    //     |        "sourceLocation": {
    //     |            "line1": "Lease 1",
    //     |            "description": "Lease 1",
    //     |            "type": "Well",
    //     |            "id": "88200e8d-d5b2-48c7-a8a8-e886b76c061a",
    //     |            "ref": "http://172.24.0.103:80/api/location/88200e8d-d5b2-48c7-a8a8-e886b76c061a?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |        },
    //     |        "destinationLocation": {
    //     |            "line1": "Lease 1",
    //     |            "description": "Lease 1",
    //     |            "type": "Well",
    //     |            "id": "88200e8d-d5b2-48c7-a8a8-e886b76c061a",
    //     |            "ref": "http://172.24.0.103:80/api/location/88200e8d-d5b2-48c7-a8a8-e886b76c061a?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |        },
    //     |        "priceCategory": "Product - Base Price",
    //     |        "currency": {
    //     |            "description": "U.S. Dollars",
    //     |            "currencyCode": "USD",
    //     |            "id": "60E8783A-FCB0-4395-91F3-1B771D480159",
    //     |            "ref": "http://172.24.0.103:80/api/currency/60E8783A-FCB0-4395-91F3-1B771D480159?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |        },
    //     |        "uom": {
    //     |            "code": "BBL",
    //     |            "shortDescription": "BBL",
    //     |            "description": "Barrels",
    //     |            "id": "31FBE848-3609-43DE-AA1F-AC073946F445",
    //     |            "ref": "http://172.24.0.103:80/api/unitofmeasure/31FBE848-3609-43DE-AA1F-AC073946F445?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |        },
    //     |        "id": "136fa5a9-7a01-4800-ac26-bf79df79a539",
    //     |        "ref": "http://172.24.0.103:80/api/price/136fa5a9-7a01-4800-ac26-bf79df79a539?token=a80e4b9d-ee46-4982-b1aa-ae1454144a8c&format=json"
    //     |    }
    //     |]""".stripMargin)

        //val NUM_OF_ITEMS = 5 //This number represents the total number of price items

    
    

    val fakeDetailedPriceItemJsonResponse = mapper.readTree(    //Fake json response for ONE price item with &detail=
                                                                //and is used for testing handling of price details!
    """ |[
        |    {
        |        "description": "Mankato Terminal (Cochin) - C3 - Intercompany Transfer Price",
        |        "item": {
        |            "shortDescription": "C3",
        |            "description": "Propane - HD5 Not Odorized",
        |            "locationControl": false,
        |            "odorized": false,
        |            "carryInventoryQuality": false,
        |            "rinRequired": false,
        |            "qtyByComponent": false,
        |            "id": "E7B8241A-6484-4464-AE6C-5049620A6685",
        |            "ref": "http://172.24.0.103:80/api/item/E7B8241A-6484-4464-AE6C-5049620A6685?token=d980c23d-789b-47eb-916c-03624cbe8d3e&format=json"
        |        },
        |        "sourceLocation": {
        |            "line1": "Mankato Terminal (Cochin)",
        |            "description": "Mankato Terminal (Cochin)",
        |            "type": "Facility",
        |            "id": "2756402c-5a82-43b5-8456-ecd4cbd201f2",
        |            "ref": "http://172.24.0.103:80/api/location/2756402c-5a82-43b5-8456-ecd4cbd201f2?token=d980c23d-789b-47eb-916c-03624cbe8d3e&format=json"
        |        },
        |        "destinationLocation": {
        |            "line1": "Mankato Terminal (Cochin)",
        |            "description": "Mankato Terminal (Cochin)",
        |            "type": "Facility",
        |            "id": "2756402c-5a82-43b5-8456-ecd4cbd201f2",
        |            "ref": "http://172.24.0.103:80/api/location/2756402c-5a82-43b5-8456-ecd4cbd201f2?token=d980c23d-789b-47eb-916c-03624cbe8d3e&format=json"
        |        },
        |        "priceCategory": "Product - Formula Price",
        |        "currency": {
        |            "description": "Canadian Dollars",
        |            "currencyCode": "CAD",
        |            "id": "B051DBB6-F3B2-44CF-84E5-CE98268FB68C",
        |            "ref": "http://172.24.0.103:80/api/currency/B051DBB6-F3B2-44CF-84E5-CE98268FB68C?token=d980c23d-789b-47eb-916c-03624cbe8d3e&format=json"
        |        },
        |        "uom": {
        |            "code": "M3",
        |            "shortDescription": "m3",
        |            "description": "Cubic Meters",
        |            "id": "2A144658-BACB-46D4-97C9-1864B15B445C",
        |            "ref": "http://172.24.0.103:80/api/unitofmeasure/2A144658-BACB-46D4-97C9-1864B15B445C?token=d980c23d-789b-47eb-916c-03624cbe8d3e&format=json"
        |        },
        |        "currentPrice": 336.51737472,
        |        "auditInfo": {
        |            "createUser": "mikeflaman",
        |            "createDate": "Apr 7, 2010 02:18:13 PM",
        |            "updateUser": "steveremmington",
        |            "updateDate": "Apr 28, 2010 09:16:57 AM"
        |        },
        |        "priceDetails": [
        |            {
        |                "effectiveDate": "Feb 1, 2010 12:00:00 AM",
        |                "priceAmount": 336.51737472,
        |                "priceCompleteDate": "Jul 1, 2009 12:00:00 AM",
        |                "type": "ACTUAL",
        |                "id": "9A0C3B94-EA22-40EA-A743-661CCF222AD8",
        |                "ref": "http://172.24.0.103:80/api/pricedetail/9A0C3B94-EA22-40EA-A743-661CCF222AD8"
        |            },
        |            {
        |                "effectiveDate": "Jan 1, 2010 12:00:00 AM",
        |                "priceAmount": 336.21880847,
        |                "priceCompleteDate": "Jul 1, 2009 12:00:00 AM",
        |                "type": "ACTUAL",
        |                "id": "F2836DB1-824C-42BA-AA20-EF4C17617120",
        |                "ref": "http://172.24.0.103:80/api/pricedetail/F2836DB1-824C-42BA-AA20-EF4C17617120"
        |            },
        |            {
        |                "effectiveDate": "Dec 1, 2009 12:00:00 AM",
        |                "priceAmount": 336.519516,
        |                "priceCompleteDate": "Jul 1, 2009 12:00:00 AM",
        |                "type": "ACTUAL",
        |                "id": "A144C081-425E-4259-B0F5-BCBBE7FD8250",
        |                "ref": "http://172.24.0.103:80/api/pricedetail/A144C081-425E-4259-B0F5-BCBBE7FD8250"
        |            },
        |            {
        |                "effectiveDate": "Nov 1, 2009 12:00:00 AM",
        |                "priceAmount": 336.60070947,
        |                "priceCompleteDate": "Jul 1, 2009 12:00:00 AM",
        |                "type": "ACTUAL",
        |                "id": "EA45DEB3-A2E1-4A7E-A1F4-C0E12E1E26B7",
        |                "ref": "http://172.24.0.103:80/api/pricedetail/EA45DEB3-A2E1-4A7E-A1F4-C0E12E1E26B7"
        |            },
        |            {
        |                "effectiveDate": "Oct 1, 2009 12:00:00 AM",
        |                "priceAmount": 336.52042944,
        |                "priceCompleteDate": "Jul 1, 2009 12:00:00 AM",
        |                "type": "ACTUAL",
        |                "id": "3DD23281-7B26-4193-9E25-FD87C584869D",
        |                "ref": "http://172.24.0.103:80/api/pricedetail/3DD23281-7B26-4193-9E25-FD87C584869D"
        |            },
        |            {
        |                "effectiveDate": "Sep 1, 2009 12:00:00 AM",
        |                "priceAmount": 337.13880259,
        |                "priceCompleteDate": "Jul 1, 2009 12:00:00 AM",
        |                "type": "ACTUAL",
        |                "id": "2D6DBB28-84BF-417E-A01A-29B33D6358D0",
        |                "ref": "http://172.24.0.103:80/api/pricedetail/2D6DBB28-84BF-417E-A01A-29B33D6358D0"
        |            },
        |            {
        |                "effectiveDate": "Aug 1, 2009 12:00:00 AM",
        |                "priceAmount": 337.3080357,
        |                "priceCompleteDate": "Jul 1, 2009 12:00:00 AM",
        |                "type": "ACTUAL",
        |                "id": "F633CF07-E205-4678-B5F3-757CA9F9B487",
        |                "ref": "http://172.24.0.103:80/api/pricedetail/F633CF07-E205-4678-B5F3-757CA9F9B487"
        |            },
        |            {
        |                "effectiveDate": "Jul 1, 2009 12:00:00 AM",
        |                "priceAmount": 338.09495776,
        |                "priceCompleteDate": "Jul 1, 2009 12:00:00 AM",
        |                "type": "ACTUAL",
        |                "id": "FCDD8C6E-B4E4-4494-9E48-E722A292F5D5",
        |                "ref": "http://172.24.0.103:80/api/pricedetail/FCDD8C6E-B4E4-4494-9E48-E722A292F5D5"
        |            },
        |            {
        |                "effectiveDate": "Jun 1, 2009 12:00:00 AM",
        |                "priceAmount": 336.60466473,
        |                "priceCompleteDate": "Jan 1, 2009 12:00:00 AM",
        |                "type": "ACTUAL",
        |                "id": "C063F92F-B620-4925-B2A8-4C07870C6AA0",
        |                "ref": "http://172.24.0.103:80/api/pricedetail/C063F92F-B620-4925-B2A8-4C07870C6AA0"
        |            },
        |            {
        |                "effectiveDate": "May 1, 2009 12:00:00 AM",
        |                "priceAmount": 337.11974609,
        |                "priceCompleteDate": "Jan 1, 2009 12:00:00 AM",
        |                "type": "ACTUAL",
        |                "id": "86A88B1A-986B-45B8-812C-3688842DF5A5",
        |                "ref": "http://172.24.0.103:80/api/pricedetail/86A88B1A-986B-45B8-812C-3688842DF5A5"
        |            },
        |            {
        |                "effectiveDate": "Apr 1, 2009 12:00:00 AM",
        |                "priceAmount": 338.80184591,
        |                "priceCompleteDate": "Jan 1, 2009 12:00:00 AM",
        |                "type": "ACTUAL",
        |                "id": "5D53CBF6-2CA8-4ECA-8AB9-55B86625FE85",
        |                "ref": "http://172.24.0.103:80/api/pricedetail/5D53CBF6-2CA8-4ECA-8AB9-55B86625FE85"
        |            },
        |            {
        |                "effectiveDate": "Mar 1, 2009 12:00:00 AM",
        |                "priceAmount": 339.67469995,
        |                "priceCompleteDate": "Jan 1, 2009 12:00:00 AM",
        |                "type": "ACTUAL",
        |                "id": "2751F17C-D62B-4113-83F4-384AB2929040",
        |                "ref": "http://172.24.0.103:80/api/pricedetail/2751F17C-D62B-4113-83F4-384AB2929040"
        |            },
        |            {
        |                "effectiveDate": "Feb 1, 2009 12:00:00 AM",
        |                "priceAmount": 339.25665863,
        |                "priceCompleteDate": "Jan 1, 2009 12:00:00 AM",
        |                "type": "ACTUAL",
        |                "id": "F0A24CC8-F8C8-4138-AD52-059C0C98B138",
        |                "ref": "http://172.24.0.103:80/api/pricedetail/F0A24CC8-F8C8-4138-AD52-059C0C98B138"
        |            },
        |            {
        |                "effectiveDate": "Jan 1, 2009 12:00:00 AM",
        |                "priceAmount": 338.8068914,
        |                "priceCompleteDate": "Jan 1, 2009 12:00:00 AM",
        |                "type": "ACTUAL",
        |                "id": "0D49D721-82AB-4245-BA70-EB4ED693BE9B",
        |                "ref": "http://172.24.0.103:80/api/pricedetail/0D49D721-82AB-4245-BA70-EB4ED693BE9B"
        |            },
        |            {
        |                "effectiveDate": "Dec 1, 2008 12:00:00 AM",
        |                "priceAmount": 279.92202207,
        |                "priceCompleteDate": "Dec 1, 2008 12:00:00 AM",
        |                "type": "ACTUAL",
        |                "id": "BCEFAB42-9592-43FA-8D91-F04367E56354",
        |                "ref": "http://172.24.0.103:80/api/pricedetail/BCEFAB42-9592-43FA-8D91-F04367E56354"
        |            },
        |            {
        |                "effectiveDate": "Nov 1, 2008 12:00:00 AM",
        |                "priceAmount": 325.90382905,
        |                "priceCompleteDate": "Nov 1, 2008 12:00:00 AM",
        |                "type": "ACTUAL",
        |                "id": "6EC9B92D-17C6-4E7C-B214-1478FEDDA85E",
        |                "ref": "http://172.24.0.103:80/api/pricedetail/6EC9B92D-17C6-4E7C-B214-1478FEDDA85E"
        |            },
        |            {
        |                "effectiveDate": "Oct 1, 2008 12:00:00 AM",
        |                "priceAmount": 327.49487272,
        |                "priceCompleteDate": "Oct 1, 2008 12:00:00 AM",
        |                "type": "ACTUAL",
        |                "id": "9F434F2E-238C-49E8-B660-40F093AABEDA",
        |                "ref": "http://172.24.0.103:80/api/pricedetail/9F434F2E-238C-49E8-B660-40F093AABEDA"
        |            }
        |        ],
        |        "id": "e48ca5d0-50a8-49a4-8a7f-a3c43b93dcf9",
        |        "ref": "http://172.24.0.103:80/api/price/e48ca5d0-50a8-49a4-8a7f-a3c43b93dcf9?token=d980c23d-789b-47eb-916c-03624cbe8d3e&format=json"
        |    }
        |]""".stripMargin)

    val fakeDetailedPriceItemsJsonResponse = mapper.readTree(   //fake json response for TWO price items with &detail=
                                                                    //which contains the same item, C3.
    """ |[
        |    {
        |        "description": "Mankato Terminal (Cochin) - C3 - Intercompany Transfer Price",
        |        "item": {
        |            "shortDescription": "C3",
        |            "description": "Propane - HD5 Not Odorized",
        |            "locationControl": false,
        |            "odorized": false,
        |            "carryInventoryQuality": false,
        |            "rinRequired": false,
        |            "qtyByComponent": false,
        |            "id": "E7B8241A-6484-4464-AE6C-5049620A6685",
        |            "ref": "http://172.24.0.103:80/api/item/E7B8241A-6484-4464-AE6C-5049620A6685?token=d980c23d-789b-47eb-916c-03624cbe8d3e&format=json"
        |        },
        |        "sourceLocation": {
        |            "line1": "Mankato Terminal (Cochin)",
        |            "description": "Mankato Terminal (Cochin)",
        |            "type": "Facility",
        |            "id": "2756402c-5a82-43b5-8456-ecd4cbd201f2",
        |            "ref": "http://172.24.0.103:80/api/location/2756402c-5a82-43b5-8456-ecd4cbd201f2?token=d980c23d-789b-47eb-916c-03624cbe8d3e&format=json"
        |        },
        |        "destinationLocation": {
        |            "line1": "Mankato Terminal (Cochin)",
        |            "description": "Mankato Terminal (Cochin)",
        |            "type": "Facility",
        |            "id": "2756402c-5a82-43b5-8456-ecd4cbd201f2",
        |            "ref": "http://172.24.0.103:80/api/location/2756402c-5a82-43b5-8456-ecd4cbd201f2?token=d980c23d-789b-47eb-916c-03624cbe8d3e&format=json"
        |        },
        |        "priceCategory": "Product - Formula Price",
        |        "currency": {
        |            "description": "Canadian Dollars",
        |            "currencyCode": "CAD",
        |            "id": "B051DBB6-F3B2-44CF-84E5-CE98268FB68C",
        |            "ref": "http://172.24.0.103:80/api/currency/B051DBB6-F3B2-44CF-84E5-CE98268FB68C?token=d980c23d-789b-47eb-916c-03624cbe8d3e&format=json"
        |        },
        |        "uom": {
        |            "code": "M3",
        |            "shortDescription": "m3",
        |            "description": "Cubic Meters",
        |            "id": "2A144658-BACB-46D4-97C9-1864B15B445C",
        |            "ref": "http://172.24.0.103:80/api/unitofmeasure/2A144658-BACB-46D4-97C9-1864B15B445C?token=d980c23d-789b-47eb-916c-03624cbe8d3e&format=json"
        |        },
        |        "currentPrice": 336.51737472,
        |        "auditInfo": {
        |            "createUser": "mikeflaman",
        |            "createDate": "Apr 7, 2010 02:18:13 PM",
        |            "updateUser": "steveremmington",
        |            "updateDate": "Apr 28, 2010 09:16:57 AM"
        |        },
        |        "priceDetails": [
        |            {
        |                "effectiveDate": "Feb 1, 2010 12:00:00 AM",
        |                "priceAmount": 336.51737472,
        |                "priceCompleteDate": "Jul 1, 2009 12:00:00 AM",
        |                "type": "ACTUAL",
        |                "id": "9A0C3B94-EA22-40EA-A743-661CCF222AD8",
        |                "ref": "http://172.24.0.103:80/api/pricedetail/9A0C3B94-EA22-40EA-A743-661CCF222AD8"
        |            },
        |            {
        |                "effectiveDate": "Jan 1, 2010 12:00:00 AM",
        |                "priceAmount": 336.21880847,
        |                "priceCompleteDate": "Jul 1, 2009 12:00:00 AM",
        |                "type": "ACTUAL",
        |                "id": "F2836DB1-824C-42BA-AA20-EF4C17617120",
        |                "ref": "http://172.24.0.103:80/api/pricedetail/F2836DB1-824C-42BA-AA20-EF4C17617120"
        |            }
        |        ],
        |        "id": "e48ca5d0-50a8-49a4-8a7f-a3c43b93dcf9",
        |        "ref": "http://172.24.0.103:80/api/price/e48ca5d0-50a8-49a4-8a7f-a3c43b93dcf9?token=d980c23d-789b-47eb-916c-03624cbe8d3e&format=json"
        |    },
        |    {
        |        "description": "Test Transfer",
        |        "item": {
        |            "shortDescription": "C3",
        |            "description": "Propane - HD5 Not Odorized",
        |            "locationControl": false,
        |            "odorized": false,
        |            "carryInventoryQuality": false,
        |            "rinRequired": false,
        |            "qtyByComponent": false,
        |            "id": "E7B8241A-6484-4464-AE6C-5049620A6685",
        |            "ref": "http://172.24.0.103:80/api/item/E7B8241A-6484-4464-AE6C-5049620A6685?token=d980c23d-789b-47eb-916c-03624cbe8d3e&format=json"
        |        },
        |        "sourceLocation": {
        |            "line1": "Mankato Terminal (Cochin)",
        |            "description": "TestLocation",
        |            "type": "Facility",
        |            "id": "2756402c-5a82-43b5-8456-ecd4cbd201f2",
        |            "ref": "http://172.24.0.103:80/api/location/2756402c-5a82-43b5-8456-ecd4cbd201f2?token=d980c23d-789b-47eb-916c-03624cbe8d3e&format=json"
        |        },
        |        "destinationLocation": {
        |            "line1": "Mankato Terminal (Cochin)",
        |            "description": "TestDestination",
        |            "type": "Facility",
        |            "id": "2756402c-5a82-43b5-8456-ecd4cbd201f2",
        |            "ref": "http://172.24.0.103:80/api/location/2756402c-5a82-43b5-8456-ecd4cbd201f2?token=d980c23d-789b-47eb-916c-03624cbe8d3e&format=json"
        |        },
        |        "priceCategory": "TestCategory",
        |        "currency": {
        |            "description": "TestCurrency",
        |            "currencyCode": "CAD",
        |            "id": "B051DBB6-F3B2-44CF-84E5-CE98268FB68C",
        |            "ref": "http://172.24.0.103:80/api/currency/B051DBB6-F3B2-44CF-84E5-CE98268FB68C?token=d980c23d-789b-47eb-916c-03624cbe8d3e&format=json"
        |        },
        |        "uom": {
        |            "code": "M3",
        |            "shortDescription": "m3",
        |            "description": "TestUnit",
        |            "id": "2A144658-BACB-46D4-97C9-1864B15B445C",
        |            "ref": "http://172.24.0.103:80/api/unitofmeasure/2A144658-BACB-46D4-97C9-1864B15B445C?token=d980c23d-789b-47eb-916c-03624cbe8d3e&format=json"
        |        },
        |        "currentPrice": 336.51737472,
        |        "auditInfo": {
        |            "createUser": "mikeflaman",
        |            "createDate": "Apr 7, 2010 02:18:13 PM",
        |            "updateUser": "steveremmington",
        |            "updateDate": "Apr 28, 2010 09:16:57 AM"
        |        },
        |        "priceDetails": [
        |            {
        |                "effectiveDate": "Feb 1, 2010 12:00:00 AM",
        |                "priceAmount": 336.51737472,
        |                "priceCompleteDate": "Jul 1, 2009 12:00:00 AM",
        |                "type": "ACTUAL",
        |                "id": "9A0C3B94-EA22-40EA-A743-661CCF222AD8",
        |                "ref": "http://172.24.0.103:80/api/pricedetail/9A0C3B94-EA22-40EA-A743-661CCF222AD8"
        |            },
        |            {
        |                "effectiveDate": "Jan 1, 2010 12:00:00 AM",
        |                "priceAmount": 336.21880847,
        |                "priceCompleteDate": "Jul 1, 2009 12:00:00 AM",
        |                "type": "ACTUAL",
        |                "id": "F2836DB1-824C-42BA-AA20-EF4C17617120",
        |                "ref": "http://172.24.0.103:80/api/pricedetail/F2836DB1-824C-42BA-AA20-EF4C17617120"
        |            }
        |        ],
        |        "id": "test-test-test-test-test",
        |        "ref": "http://172.24.0.103:80/api/price/e48ca5d0-50a8-49a4-8a7f-a3c43b93dcf9?token=d980c23d-789b-47eb-916c-03624cbe8d3e&format=json"
        |    }
        |]""".stripMargin)

        val fakeItemGroupJsonResponse = mapper.readTree( // This fake response is for getting the ItemGroups
     """|[
        |   {
        |       "code": "UNDF",
        |       "description": "Undefined",
        |       "id": "6DB23A00-89F9-4907-8278-C6E69EFF51C3",
        |       "ref": "http://184.150.226.173/e1RestServer/itemgroup/6DB23A00-89F9-4907-8278-C6E69EFF51C3.json?token=a188583d-624d-4def-bf12-dc6efaddf117"
        |   },
        |   {
        |       "code": "AMON",
        |       "description": "Ammonia",
        |       "id": "C3FAC269-BC54-47BC-89BD-50800B5E6D22",
        |       "ref": "http://184.150.226.173/e1RestServer/itemgroup/C3FAC269-BC54-47BC-89BD-50800B5E6D22.json?token=0410b000-9570-4866-911c-efb8d5831308"
        |   },
        |   {
        |       "code": "C3",
        |       "description": "Propanes",
        |        "id": "7B4AA572-0A8B-48BE-AC59-653932809915",
        |        "ref": "http://184.150.226.173/e1RestServer/itemgroup/7B4AA572-0A8B-48BE-AC59-653932809915.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
        |   }
        |]""".stripMargin)

        val fakeDetailedItemGroupJsonResponse = mapper.readTree( // A more detailed responses at /itemGroup endpoint that is used for getting/filtering the actual Items!
     """|[
        |   {
        |       "code": "UNDF",
        |       "description": "Undefined",
        |       "items": [
        |           {
        |               "shortDescription": "All Items",
        |               "description": "All Items",
        |               "id": "b9795dad-b755-49ba-af55-d75a473cad3a",
        |               "ref": "http://184.150.226.173/e1RestServer/item/b9795dad-b755-49ba-af55-d75a473cad3a.json?token=a188583d-624d-4def-bf12-dc6efaddf117"
        |           },
        |           {
        |               "shortDescription": "Undefined Item",
        |               "description": "Undefined Item",
        |               "id": "23A53BBC-C495-4CCA-9EEF-70AC901A1848",
        |               "ref": "http://184.150.226.173/e1RestServer/item/23A53BBC-C495-4CCA-9EEF-70AC901A1848.json?token=a188583d-624d-4def-bf12-dc6efaddf117"
        |           }
        |       ],
        |       "id": "6DB23A00-89F9-4907-8278-C6E69EFF51C3",
        |       "ref": "http://184.150.226.173/e1RestServer/itemgroup/6DB23A00-89F9-4907-8278-C6E69EFF51C3.json?token=a188583d-624d-4def-bf12-dc6efaddf117"
        |   },
        |   {
        |       "code": "AMON",
        |       "description": "Ammonia",
        |       "items": [
        |           {
        |               "shortDescription": "Ammonia",
        |               "description": "Anhydrous Ammonia",
        |               "id": "F0DAD5ED-F638-4D35-92FE-26471CCE8AB3",
        |               "ref": "http://184.150.226.173/e1RestServer/itemgroup/6DB23A00-89F9-4907-8278-C6E69EFF51C3.json?token=0410b000-9570-4866-911c-efb8d5831308"
        |           }
        |       ],
        |       "id": "C3FAC269-BC54-47BC-89BD-50800B5E6D22",
        |       "ref": "http://184.150.226.173/e1RestServer/itemgroup/C3FAC269-BC54-47BC-89BD-50800B5E6D22.json?token=0410b000-9570-4866-911c-efb8d5831308"
        |   },
        |   {
        |       "code": "C3",
        |       "description": "Propanes",
        |       "items": [
        |           {
        |               "shortDescription": "C3",
        |               "description": "Propane",
        |               "id": "1d0e13eb-b1f4-463d-9074-4a17c2396de7",
        |               "ref": "http://184.150.226.173/e1RestServer/item/1d0e13eb-b1f4-463d-9074-4a17c2396de7.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
        |           },
        |           {
        |               "shortDescription": "C3-MX - Should be filtered out!",
        |               "description": "Propane - Component in Mix",
        |               "id": "64D16F47-0950-43D8-AD54-F811A58902CC",
        |               "ref": "http://184.150.226.173/e1RestServer/item/64D16F47-0950-43D8-AD54-F811A58902CC.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
        |           },
        |           {
        |               "shortDescription": "TEST item - Should be filtered out!",
        |               "description": "Test - Component in Mix",
        |               "id": "64D16F47-0950-43D8-AD54-F811A58902CC",
        |               "ref": "http://184.150.226.173/e1RestServer/item/64D16F47-0950-43D8-AD54-F811A58902CC.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
        |           }
        |        ],
        |        "id": "7B4AA572-0A8B-48BE-AC59-653932809915",
        |        "ref": "http://184.150.226.173/e1RestServer/itemgroup/7B4AA572-0A8B-48BE-AC59-653932809915.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
        |   },
        |   {
        |       "code": "VES",
        |       "description": "Vessels",
        |       "items": [],
        |       "id": "8A0D67BC-4729-4A1B-A7E5-049C546B6978",
        |       "ref": "http://184.150.226.173/e1RestServer/itemgroup/8A0D67BC-4729-4A1B-A7E5-049C546B6978.json?token=75e2160f-12f0-4bba-b037-6658a847e652"
        |   }
        |]""".stripMargin)

    val fakeLocationJsonResponse = mapper.readTree( // A fake JSON response containing ONE fake location 
                                                    // Is used when searching price by location
    """ |[
        |    {
        |        "line1": "testLocation",
        |        "description": "testLocation, IA",
        |        "type": "Jurisdiction",
        |        "id": "2D701D5B-A389-4B98-8D31-8582B5C84DDE",
        |        "ref": "http://172.24.0.103:80/api/location/2D701D5B-A389-4B98-8D31-8582B5C84DDE?token=58f26800-8709-48ae-8d15-74e6ebb4fcb9&format=json"
        |    }
        |]""".stripMargin)


    val fakeLocationsJsonResponse = mapper.readTree( // A fake JSON response containing 3 fake locations 
                                                    // Is used when searching locations using the popup menu
    """ |[
        |    {
        |        "line1": "testLocation",
        |        "description": "testLocation",
        |        "type": "Facility",
        |        "id": "2756402c-5a82-43b5-8456-ecd4cbd201f2",
        |        "ref": "http://172.24.0.103:80/api/location/2756402c-5a82-43b5-8456-ecd4cbd201f2?token=58f26800-8709-48ae-8d15-74e6ebb4fcb9&format=json"
        |    },
        |    {
        |        "line1": "testLocation",
        |        "description": "testLocation, IA",
        |        "type": "Jurisdiction",
        |        "id": "2D701D5B-A389-4B98-8D31-8582B5C84DDE",
        |        "ref": "http://172.24.0.103:80/api/location/2D701D5B-A389-4B98-8D31-8582B5C84DDE?token=58f26800-8709-48ae-8d15-74e6ebb4fcb9&format=json"
        |    },
        |    {
        |        "line1": "testLocation",
        |        "description": "testLocation, MN",
        |        "type": "Jurisdiction",
        |        "id": "7ce8f85b-a036-427a-b71f-58aa427e0b47",
        |        "ref": "http://172.24.0.103:80/api/location/7ce8f85b-a036-427a-b71f-58aa427e0b47?token=58f26800-8709-48ae-8d15-74e6ebb4fcb9&format=json"
        |    }
        |]""".stripMargin)

    
    it should "return the Price workspace's name" in {
        running(FakeApplication()){

            val priceWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.PRICE)
            assert(priceWS.getWSName() == "Price")
        }
    }
                
    it should "return a list if no price parameters are specified" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){

            // This uses ScalaTest's JMockCycle to wrap the Mockery object
            val cycle = new JMockCycle
            import cycle._

            // Set the context of the mock
            context.checking(
                new Expectations() {

                    allowing(mockClient).setHolder("/price")
                    
                    oneOf(mockClient).setFlag("offset", "0")
                    oneOf(mockClient).setFlag("max", maxOffset)

                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakePriceItemsJsonResponse))
                }
            )

            // Test the system:
            val priceWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.PRICE)
            priceWS.setClient(mockClient)

            val offset = 0
            priceWS.searchPrice(offset, "", "", "", "") //.searchPrice(offset, selectedItemUUID, priceDescription)
            val priceTable = priceWS.getTable()
        
            //println(priceTable.get(1).getPropertyMap())
            assert(priceTable.size() == fakePriceItemsJsonResponseSize)
            assert(priceTable.get(1).getProperty(PriceIdentifier.PRICEDESCRIPTION.columnName())=="Gibson Terminaling Fee ")
            assert(priceTable.get(1).getProperty(PriceIdentifier.DESTINATIONLOCATIONDESC.columnName())=="Gibson Terminal Edmonton ")
            assert(priceTable.get(1).getProperty(PriceIdentifier.ITEM.columnName())=="C3")
            assert(priceTable.get(1).getProperty(PriceIdentifier.UOM.columnName())=="m3")
            assert(priceTable.get(1).getProperty(PriceIdentifier.SOURCELOCATION.columnName())=="Gibson Terminal Edmonton ")
            assert(priceTable.get(1).getProperty(PriceIdentifier.CURRENCY.columnName())=="Canadian Dollars")
            assert(priceTable.get(1).getProperty(PriceIdentifier.ID.columnName())=="fe608b4b-0d8d-4890-a6ad-8c1bce33d346")
            assert(priceTable.get(1).getProperty(PriceIdentifier.PRICECATEGORY.columnName())=="Terminalling Fee")
        }
    }

    it should "return a list when searching by price description" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){
            val cycle = new JMockCycle  // This uses ScalaTest's JMockCycle to wrap the Mockery object
            import cycle._
 
            context.checking(           // Set the context of the mock
                new Expectations() {

                    allowing(mockClient).setHolder("/price")
                    
                    oneOf (mockClient).setFlag(PriceIdentifier.PRICEDESCRIPTION.fieldName(), "Mankato")

                    oneOf(mockClient).setFlag("offset", "0")
                    oneOf(mockClient).setFlag("max", maxOffset)

                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakePriceItemJsonResponse))
                }
            )

            // Test the system:
            val priceWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.PRICE)
            priceWS.setClient(mockClient)
  
            val offset = 0
            priceWS.searchPrice(offset, "", "", "", "Mankato") //.searchPrice(offset, selectedItemUUID, priceDescription)
            val priceTable = priceWS.getTable()
        
            //println(priceTable.get(0).getPropertyMap())
            assert(priceTable.get(0).getProperty(PriceIdentifier.PRICEDESCRIPTION.columnName()).contains("Mankato"))
            assert(priceTable.get(0).getProperty(PriceIdentifier.DESTINATIONLOCATIONDESC.columnName()).contains("Mankato"))
            assert(priceTable.get(0).getProperty(PriceIdentifier.ITEM.columnName()).contains("C3"))
            assert(priceTable.get(0).getProperty(PriceIdentifier.UOM.columnName()).contains("m3"))
            assert(priceTable.get(0).getProperty(PriceIdentifier.SOURCELOCATION.columnName()).contains("Mankato"))
            assert(priceTable.get(0).getProperty(PriceIdentifier.CURRENCY.columnName()).contains("Canadian Dollars"))
            assert(priceTable.get(0).getProperty(PriceIdentifier.ID.columnName()).contains("e48ca5d0-50a8-49a4-8a7f-a3c43b93dcf9"))
            assert(priceTable.get(0).getProperty(PriceIdentifier.PRICECATEGORY.columnName()).contains("Formula"))
        }
    }
    
    it should "return an empty list when searching for a non-existing price description" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){
            val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
            import cycle._

            context.checking( // Set the context of the mock
                new Expectations() {

                    allowing(mockClient).setHolder("/price")
                    
                    oneOf (mockClient).setFlag(PriceIdentifier.PRICEDESCRIPTION.fieldName(), "non-existing")

                    oneOf(mockClient).setFlag("offset", "0")
                    oneOf(mockClient).setFlag("max", maxOffset)

                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(mapper.readTree("[]")))
                }
            )

            // Test the system:
            val priceWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.PRICE)
            priceWS.setClient(mockClient)

            val offset = 0
            priceWS.searchPrice(offset, "", "", "", "non-existing") //.searchPrice(offset, selectedItemUUID, priceDescription)
            val priceTable = priceWS.getTable()
        
            assert(priceTable.size() == 0)
        }
    }

    it should "return a list when searching by item's UUID inside the Price workspace" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){

            val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
            import cycle._

            context.checking( // Set the context of the mock
                new Expectations() {

                    allowing(mockClient).setHolder("/price")

                    oneOf (mockClient).setFlag("itemUUID", "testUUID")

                    oneOf(mockClient).setFlag("offset", "0")
                    oneOf(mockClient).setFlag("max", maxOffset)

                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakePriceItemsJsonResponse))
                }
            )

            // Test the system:
            val priceWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.PRICE)
            priceWS.setClient(mockClient)

            val offset = 0
            priceWS.searchPrice(offset, "", "", "testUUID", "")   //.searchPrice(offset, selectedItemUUID, priceDescription)
            val priceTable = priceWS.getTable()
        
            //println(priceTable.get(1).getPropertyMap())
            assert(priceTable.get(1).getProperty(PriceIdentifier.PRICEDESCRIPTION.columnName()).contains("Gibson"))
            assert(priceTable.get(1).getProperty(PriceIdentifier.DESTINATIONLOCATIONDESC.columnName()).contains("Gibson Terminal"))
            assert(priceTable.get(1).getProperty(PriceIdentifier.ITEM.columnName()).contains("C3"))
            assert(priceTable.get(1).getProperty(PriceIdentifier.UOM.columnName()).contains("m3"))
            assert(priceTable.get(1).getProperty(PriceIdentifier.SOURCELOCATION.columnName()).contains("Gibson Terminal"))
            assert(priceTable.get(1).getProperty(PriceIdentifier.CURRENCY.columnName()).contains("Canadian Dollars"))
            assert(priceTable.get(1).getProperty(PriceIdentifier.ID.columnName()).contains("fe608b4b-0d8d-4890-a6ad-8c1bce33d346"))
            assert(priceTable.get(1).getProperty(PriceIdentifier.PRICECATEGORY.columnName()).contains("Terminalling"))
        }
    }
    
    /*it should "return a list when searching by both price and item descriptions" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){

            val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
            import cycle._

            context.checking( // Set the context of the mock
                new Expectations() {

                    allowing(mockClient).setHolder("/itemGroup")    //Check /itemGroups to get all names

                    exactly(2).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeItemGroupJsonResponse))
                    
                    oneOf (mockClient).setFlag("detail", null)

                    exactly(1).of(mockClient).getResponseAsJson()               //Search through all itemGroups with &detail= to get
                        will(returnValue(fakeDetailedItemGroupJsonResponse))    //the item containing "C3" (two in mock object)

                    allowing(mockClient).setHolder("/price")        //Check /price and get ones which contains "C3"!

                    oneOf (mockClient).setFlag("itemUUID", "testUUID")

                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeDetailedPriceItemsJsonResponse))
                }
            )

            // Test the system:
            val priceWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.PRICE)
            priceWS.setClient(mockClient)

            //.searchPrice(locationSourceOrDestination, locationDesc, selectedItemShortDesc, priceDesc)
            val offset = 0
            priceWS.searchPrice(offset, "", "", "testUUID", "Test Transfer")
            val priceTable = priceWS.getTable()
        
            //Unlike the test above, if you filter out by price description, there will be only 2 valid price items to display!
            assert(priceTable.size() == 1)
            assert(priceTable.get(0).getProperty(PriceIdentifier.PRICEDESCRIPTION.columnName())=="Test Transfer")
            assert(priceTable.get(0).getProperty(PriceIdentifier.PRICECATEGORY.columnName())=="TestCategory")
            assert(priceTable.get(0).getProperty(PriceIdentifier.SOURCELOCATION.columnName())=="TestLocation")
            assert(priceTable.get(0).getProperty(PriceIdentifier.DESTINATIONLOCATION.columnName())=="TestDestination")
            assert(priceTable.get(0).getProperty(PriceIdentifier.ITEM.columnName())=="C3")
            assert(priceTable.get(0).getProperty(PriceIdentifier.UOM.columnName())=="TestUnit")
            assert(priceTable.get(0).getProperty(PriceIdentifier.CURRENCY.columnName())=="TestCurrency")
        }
    }*/
    
    //======================================================================================
    // TODO: PriceGroup is supported by the API, but is outside project scope (Mar 21, 2015)
    //it should "return a list when searching in one priceGroup" in withRestClient { (mockClient, context) =>
    //  running(FakeApplication()) {
    //      assert(true)
    //  }
    //}
    
    //it should "return a list when searching in multiple priceGroup's" in withRestClient { (mockClient, context) =>
    //  running(FakeApplication()) {
    //      assert(true)
    //  }
    //}
    
    //it should "return a list when searching for price items that doesn't belong in any priceGroup" in withRestClient { (mockClient, context) =>
    //  running(FakeApplication()) {
    //      assert(true)
    //  }
    //}
    //======================================================================================

    //====================================================
    //Searching by UUID is a private method (Mar 21, 2015)
    //and is called by searchPrice()
    //it should "return a specific price item when searching by itemUUID" in withRestClient { (mockClient, context) =>
    //  running(FakeApplication()) {
    //      assert(true)
    //  }
    //}
    
    //it should "return an empty list when searching for non-existing itemUUID" in withRestClient { (mockClient, context) =>
    //  running(FakeApplication()) {
    //      assert(true)
    //  }
    //}
    //====================================================
    
    //===================================================================
    //Filtering by LocationUUID is done in private methods (Mar 21, 2015)
    //and is called by searchPrice()
    //it should "return a specific price item when searching by full sourceLocationUUID" in withRestClient { (mockClient, context) =>
    //  running(FakeApplication()) {
    //      assert(true)
    //  }
    //}
    
    //it should "return an empty list when searching for non-existing sourceLocationUUID" in withRestClient { (mockClient, context) =>
    //  running(FakeApplication()) {
    //      assert(true)
    //  }
    //}
    
    //it should "return a specific price item when searching by full destinationLocationUUID" in withRestClient { (mockClient, context) =>
    //  running(FakeApplication()) {
    //      assert(true)
    //  }
    //}
    
    //it should "return an empty list when searching for non-existing destinationLocationUUID" in withRestClient { (mockClient, context) =>
    //  running(FakeApplication()) {
    //      assert(true)
    //  }
    //}
    //===================================================================

    // TODO: This test is to be moved to a different test file...
    /*it should "return a list of locations for the popup menu" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){

            val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
            import cycle._

            context.checking( // Set the context of the mock
                new Expectations() {

                    exactly(1).of(mockClient).setHolder("/price")

                    // Set endpoint to /location to search & retrieve list of locations
                    exactly(1).of(mockClient).setHolder("/location")

                    // Search in all location types:
                    /*oneOf(mockClient).setFlag("locationType", "BANKROUTING")
                    oneOf(mockClient).setFlag("locationType", "CASCADE")
                    oneOf(mockClient).setFlag("locationType", "EFT")
                    oneOf(mockClient).setFlag("locationType", "FACILITY")
                    oneOf(mockClient).setFlag("locationType", "FINANCIAL_TRADING")
                    oneOf(mockClient).setFlag("locationType", "FORMATION")
                    oneOf(mockClient).setFlag("locationType", "GRAPHING_REGION")
                    oneOf(mockClient).setFlag("locationType", "JURISDICTION")
                    oneOf(mockClient).setFlag("locationType", "MAILING")
                    oneOf(mockClient).setFlag("locationType", "OFFICE")
                    oneOf(mockClient).setFlag("locationType", "NOTIONAL")
                    oneOf(mockClient).setFlag("locationType", "PROPERTY")
                    oneOf(mockClient).setFlag("locationType", "REPORTING_REGION")
                    oneOf(mockClient).setFlag("locationType", "RESIDENCE")
                    oneOf(mockClient).setFlag("locationType", "RESERVOIR")
                    oneOf(mockClient).setFlag("locationType", "RISK_REGION")
                    oneOf(mockClient).setFlag("locationType", "TRACT")
                    oneOf(mockClient).setFlag("locationType", "WELL")
                    oneOf(mockClient).setFlag("locationType", "UNIT")
                    oneOf(mockClient).setFlag("locationType", "YARD")

                    // Sets the location description parameter
                    oneOf(mockClient).setFlag("description", "TestLocation")

                    oneOf(mockClient).setFlag("offset", "0")
                    oneOf(mockClient).setFlag("max", "100")

                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeLocationsJsonResponse))

                    exactly(1).of(mockClient).setHolder("/location")
                    exactly(1).of(mockClient).setHolder("/price")*/ 
                }
            )

            // Test the system:
            val priceWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.PRICE)
            priceWS.setClient(mockClient)

            //.searchPrice(locationSourceOrDestination, locationDesc, selectedItemShortDesc, priceDesc)
            val locations = priceWS.searchLocations("TestLocation", priceWS.getLocationTypes())
            
            // Should return list containing the three test locations
            assert(locations.size() == 3)
        }
    }

    it should "return an empty list of locations when searching with blank location description" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){

            val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
            import cycle._

            context.checking( // Set the context of the mock
                new Expectations() {

                    exactly(1).of(mockClient).setHolder("/price")

                    // Set endpoint to /location to search & retrieve list of locations
                    exactly(1).of(mockClient).setHolder("/location")

                    // Search in all location types:
                    oneOf(mockClient).setFlag("locationType", "BANKROUTING")
                    oneOf(mockClient).setFlag("locationType", "CASCADE")
                    oneOf(mockClient).setFlag("locationType", "EFT")
                    oneOf(mockClient).setFlag("locationType", "FACILITY")
                    oneOf(mockClient).setFlag("locationType", "FINANCIAL_TRADING")
                    oneOf(mockClient).setFlag("locationType", "FORMATION")
                    oneOf(mockClient).setFlag("locationType", "GRAPHING_REGION")
                    oneOf(mockClient).setFlag("locationType", "JURISDICTION")
                    oneOf(mockClient).setFlag("locationType", "MAILING")
                    oneOf(mockClient).setFlag("locationType", "OFFICE")
                    oneOf(mockClient).setFlag("locationType", "NOTIONAL")
                    oneOf(mockClient).setFlag("locationType", "PROPERTY")
                    oneOf(mockClient).setFlag("locationType", "REPORTING_REGION")
                    oneOf(mockClient).setFlag("locationType", "RESIDENCE")
                    oneOf(mockClient).setFlag("locationType", "RESERVOIR")
                    oneOf(mockClient).setFlag("locationType", "RISK_REGION")
                    oneOf(mockClient).setFlag("locationType", "TRACT")
                    oneOf(mockClient).setFlag("locationType", "WELL")
                    oneOf(mockClient).setFlag("locationType", "UNIT")
                    oneOf(mockClient).setFlag("locationType", "YARD")


                    oneOf(mockClient).setFlag("offset", "0")
                    oneOf(mockClient).setFlag("max", "100")

                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeLocationsJsonResponse))

                    exactly(1).of(mockClient).setHolder("/location")
                    exactly(1).of(mockClient).setHolder("/price") 
                }
            )

            // Test the system:
            val priceWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.PRICE)
            priceWS.setClient(mockClient)

            //.searchPrice(locationSourceOrDestination, locationDesc, selectedItemShortDesc, priceDesc)
            val locations = priceWS.searchLocations("", priceWS.getLocationTypes())
        
            assert(locations.size() == 0)
        }
    }*/

    it should "return a list when searching by price's source location description" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){

            val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
            import cycle._

            context.checking( // Set the context of the mock
                new Expectations() {

                    allowing(mockClient).setHolder("/price")

                    oneOf(mockClient).setFlag("sourceLocationUUID", "SOURCELOCATION")
                    oneOf(mockClient).setFlag("destinationLocationUUID", "testLocation, IA")

                    oneOf(mockClient).setFlag("offset", "0")
                    oneOf(mockClient).setFlag("max", maxOffset)

                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeDetailedPriceItemJsonResponse))

                    /*// Set endpoint to /location to search & retrieve list of locations
                    exactly(1).of(mockClient).setHolder("/location")

                    // Search in all location types:
                    oneOf(mockClient).setFlag("locationType", "BANKROUTING")
                    oneOf(mockClient).setFlag("locationType", "CASCADE")
                    oneOf(mockClient).setFlag("locationType", "EFT")
                    oneOf(mockClient).setFlag("locationType", "FACILITY")
                    oneOf(mockClient).setFlag("locationType", "FINANCIAL_TRADING")
                    oneOf(mockClient).setFlag("locationType", "FORMATION")
                    oneOf(mockClient).setFlag("locationType", "GRAPHING_REGION")
                    oneOf(mockClient).setFlag("locationType", "JURISDICTION")
                    oneOf(mockClient).setFlag("locationType", "MAILING")
                    oneOf(mockClient).setFlag("locationType", "OFFICE")
                    oneOf(mockClient).setFlag("locationType", "NOTIONAL")
                    oneOf(mockClient).setFlag("locationType", "PROPERTY")
                    oneOf(mockClient).setFlag("locationType", "REPORTING_REGION")
                    oneOf(mockClient).setFlag("locationType", "RESIDENCE")
                    oneOf(mockClient).setFlag("locationType", "RESERVOIR")
                    oneOf(mockClient).setFlag("locationType", "RISK_REGION")
                    oneOf(mockClient).setFlag("locationType", "TRACT")
                    oneOf(mockClient).setFlag("locationType", "WELL")
                    oneOf(mockClient).setFlag("locationType", "UNIT")
                    oneOf(mockClient).setFlag("locationType", "YARD")

                    
                 
                    

                    // Retrieves the location's UUID
                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeLocationJsonResponse))  

                    exactly(2).of(mockClient).setHolder("/location") 
                    exactly(1).of(mockClient).setFlag("detail", "")
                    exactly(1).of(mockClient).setFlag("sourceLocationUUID", "2D701D5B-A389-4B98-8D31-8582B5C84DDE")

                    

                    exactly(1).of(mockClient).setHolder("/price")*/
                }
            )

            // Test the system:
            val priceWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.PRICE)
            priceWS.setClient(mockClient)

            //.searchPrice(locationSourceOrDestination, locationLongDesc, selectedItemShortDesc, priceDesc)
            val offset = 0
            priceWS.searchPrice(offset, "SOURCELOCATION", "testLocation, IA", "", "")
            val priceTable = priceWS.getTable()
        
            assert(priceTable.size() == 1)
        }
    }

    /*it should "return a list when searching by price's destination location description" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){

            val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
            import cycle._

            context.checking( // Set the context of the mock
                new Expectations() {

                    exactly(1).of(mockClient).setHolder("/price")

                    // Set endpoint to /location to search & retrieve list of locations
                    exactly(1).of(mockClient).setHolder("/location")

                    // Search in all location types:
                    oneOf(mockClient).setFlag("locationType", "BANKROUTING")
                    oneOf(mockClient).setFlag("locationType", "CASCADE")
                    oneOf(mockClient).setFlag("locationType", "EFT")
                    oneOf(mockClient).setFlag("locationType", "FACILITY")
                    oneOf(mockClient).setFlag("locationType", "FINANCIAL_TRADING")
                    oneOf(mockClient).setFlag("locationType", "FORMATION")
                    oneOf(mockClient).setFlag("locationType", "GRAPHING_REGION")
                    oneOf(mockClient).setFlag("locationType", "JURISDICTION")
                    oneOf(mockClient).setFlag("locationType", "MAILING")
                    oneOf(mockClient).setFlag("locationType", "OFFICE")
                    oneOf(mockClient).setFlag("locationType", "NOTIONAL")
                    oneOf(mockClient).setFlag("locationType", "PROPERTY")
                    oneOf(mockClient).setFlag("locationType", "REPORTING_REGION")
                    oneOf(mockClient).setFlag("locationType", "RESIDENCE")
                    oneOf(mockClient).setFlag("locationType", "RESERVOIR")
                    oneOf(mockClient).setFlag("locationType", "RISK_REGION")
                    oneOf(mockClient).setFlag("locationType", "TRACT")
                    oneOf(mockClient).setFlag("locationType", "WELL")
                    oneOf(mockClient).setFlag("locationType", "UNIT")
                    oneOf(mockClient).setFlag("locationType", "YARD")

                    oneOf(mockClient).setFlag("offset", "0")
                    oneOf(mockClient).setFlag("max", "100")
                 
                    exactly(1).of(mockClient).setFlag("description", "testLocation, IA")

                    // Retrieves the location's UUID
                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeLocationJsonResponse))  

                    exactly(2).of(mockClient).setHolder("/location") 
                    exactly(1).of(mockClient).setFlag("detail", "")
                    exactly(1).of(mockClient).setFlag("destinationLocationUUID", "2D701D5B-A389-4B98-8D31-8582B5C84DDE")

                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeDetailedPriceItemJsonResponse))

                    exactly(1).of(mockClient).setHolder("/price")
                }
            )

            // Test the system:
            val priceWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.PRICE)
            priceWS.setClient(mockClient)

            //.searchPrice(locationSourceOrDestination, locationLongDesc, selectedItemShortDesc, priceDesc)
            val offset = 0
            priceWS.searchPrice(offset, "DESINTATIONLOCATION", "testLocation, IA", "", "")
            val priceTable = priceWS.getTable()
        
            assert(priceTable.size() == 1)
        }
    }

    /*it should "return a list when searching by both price destination & source location descriptions" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){

            val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
            import cycle._

            context.checking( // Set the context of the mock
                new Expectations() {

                    exactly(1).of(mockClient).setHolder("/price")

                    // Set endpoint to /location to search & retrieve list of locations
                    exactly(1).of(mockClient).setHolder("/location")

                    // Search in all location types:
                    oneOf(mockClient).setFlag("locationType", "BANKROUTING")
                    oneOf(mockClient).setFlag("locationType", "CASCADE")
                    oneOf(mockClient).setFlag("locationType", "EFT")
                    oneOf(mockClient).setFlag("locationType", "FACILITY")
                    oneOf(mockClient).setFlag("locationType", "FINANCIAL_TRADING")
                    oneOf(mockClient).setFlag("locationType", "FORMATION")
                    oneOf(mockClient).setFlag("locationType", "GRAPHING_REGION")
                    oneOf(mockClient).setFlag("locationType", "JURISDICTION")
                    oneOf(mockClient).setFlag("locationType", "MAILING")
                    oneOf(mockClient).setFlag("locationType", "OFFICE")
                    oneOf(mockClient).setFlag("locationType", "NOTIONAL")
                    oneOf(mockClient).setFlag("locationType", "PROPERTY")
                    oneOf(mockClient).setFlag("locationType", "REPORTING_REGION")
                    oneOf(mockClient).setFlag("locationType", "RESIDENCE")
                    oneOf(mockClient).setFlag("locationType", "RESERVOIR")
                    oneOf(mockClient).setFlag("locationType", "RISK_REGION")
                    oneOf(mockClient).setFlag("locationType", "TRACT")
                    oneOf(mockClient).setFlag("locationType", "WELL")
                    oneOf(mockClient).setFlag("locationType", "UNIT")
                    oneOf(mockClient).setFlag("locationType", "YARD")

                    oneOf(mockClient).setFlag("offset", "0")
                    oneOf(mockClient).setFlag("max", "100")
                 
                    exactly(1).of(mockClient).setFlag("description", "testLocation, IA")

                    // Retrieves the source location's UUID
                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeLocationJsonResponse))  

                    exactly(2).of(mockClient).setHolder("/location") 
                    exactly(1).of(mockClient).setFlag("detail", "")
                    exactly(1).of(mockClient).setFlag("sourceLocationUUID", "2D701D5B-A389-4B98-8D31-8582B5C84DDE")

                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeDetailedPriceItemJsonResponse))

                    exactly(1).of(mockClient).setHolder("/price")

                    // Retrieve the destination location's UUID
                    //exactly(2).of(mockClient).setHolder("/location")
                }
            )

            // Test the system:
            val priceWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.PRICE)
            priceWS.setClient(mockClient)

            //.searchPrice(locationSourceOrDestination, locationLongDesc, selectedItemShortDesc, priceDesc)
            priceWS.searchPrice("BOTH", "testLocation, IA", "", "")
            val priceTable = priceWS.getTable()
        
            assert(priceTable.size() == 1)
        }
    }*/

    it should "return a list when searching by both price locations and item descriptions" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){

            val cycle = new JMockCycle // This uses ScalaTest's JMockCycle to wrap the Mockery object
            import cycle._

            context.checking( // Set the context of the mock
                new Expectations() {

                    exactly(1).of(mockClient).setHolder("/price")

                    // Set endpoint to /location to search & retrieve list of locations
                    exactly(1).of(mockClient).setHolder("/location")

                    // Search in all location types:
                    oneOf(mockClient).setFlag("locationType", "BANKROUTING")
                    oneOf(mockClient).setFlag("locationType", "CASCADE")
                    oneOf(mockClient).setFlag("locationType", "EFT")
                    oneOf(mockClient).setFlag("locationType", "FACILITY")
                    oneOf(mockClient).setFlag("locationType", "FINANCIAL_TRADING")
                    oneOf(mockClient).setFlag("locationType", "FORMATION")
                    oneOf(mockClient).setFlag("locationType", "GRAPHING_REGION")
                    oneOf(mockClient).setFlag("locationType", "JURISDICTION")
                    oneOf(mockClient).setFlag("locationType", "MAILING")
                    oneOf(mockClient).setFlag("locationType", "OFFICE")
                    oneOf(mockClient).setFlag("locationType", "NOTIONAL")
                    oneOf(mockClient).setFlag("locationType", "PROPERTY")
                    oneOf(mockClient).setFlag("locationType", "REPORTING_REGION")
                    oneOf(mockClient).setFlag("locationType", "RESIDENCE")
                    oneOf(mockClient).setFlag("locationType", "RESERVOIR")
                    oneOf(mockClient).setFlag("locationType", "RISK_REGION")
                    oneOf(mockClient).setFlag("locationType", "TRACT")
                    oneOf(mockClient).setFlag("locationType", "WELL")
                    oneOf(mockClient).setFlag("locationType", "UNIT")
                    oneOf(mockClient).setFlag("locationType", "YARD")

                    oneOf(mockClient).setFlag("offset", "0")
                    oneOf(mockClient).setFlag("max", "100")
                 
                    exactly(1).of(mockClient).setFlag("description", "testLocation, IA")

                    // Retrieves the location's UUID
                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeLocationJsonResponse))  

                    exactly(2).of(mockClient).setHolder("/location") 
                    exactly(1).of(mockClient).setFlag("detail", "")
                    exactly(1).of(mockClient).setFlag("sourceLocationUUID", "2D701D5B-A389-4B98-8D31-8582B5C84DDE")

                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeDetailedPriceItemJsonResponse))

                    exactly(1).of(mockClient).setHolder("/price")
                }
            )

            // Test the system:
            val priceWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.PRICE)
            priceWS.setClient(mockClient)

            //.searchPrice(locationSourceOrDestination, locationLongDesc, selectedItemShortDesc, priceDesc)
            val offset = 0
            priceWS.searchPrice(offset, "SOURCELOCATION", "testLocation, IA", "C3", "Mankato")
            val priceTable = priceWS.getTable()
        
            assert(priceTable.size() == 1)
        }
    }*/

    it should "sort the Price table by columns alphabetically in both A-Z and Z-A" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){
            val cycle = new JMockCycle
            import cycle._

            context.checking(
                new Expectations() {
                    allowing(mockClient).setHolder("/price")
                    
                    oneOf(mockClient).setFlag("offset", "0")
                    oneOf(mockClient).setFlag("max", maxOffset)

                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakePriceItemsJsonResponse))
                }
            )

            // Test the system:
            val priceWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.PRICE)
            priceWS.setClient(mockClient)

            // Perform a blank search and try to sort the results
            val offset = 0
            priceWS.searchPrice(offset, "", "", "", "") //.searchPrice(offset, selectedItemUUID, priceDescription)
            //val priceTable = priceWS.getTable()

            var priceTable = priceWS.sort("Description")    // This should sort the table A-Z
            var asciiChar = ((priceTable.get(0).getProperty("Description")).charAt(0)).toInt
            assert(((asciiChar>=65)&&(asciiChar<=70))           //We check the sorted table starts between A-F
                    || ((asciiChar>=97)&&(asciiChar<=102)))

            priceTable = priceWS.sort("Description")    // This should sort the table Z-A
            asciiChar = ((priceTable.get(0).getProperty("Description")).charAt(0)).toInt
            assert(((asciiChar>=82)&&(asciiChar<=90))           //We check the sorted table starts between R-Z
                    || ((asciiChar>=114)&&(asciiChar<=122)))
        }
    }

    /*it should "return a list when searching with maxPriceDetails equal 1000" in withRestClient { (mockClient, context) =>
        //running(FakeApplication()) {
            assert(true)
        //}
    }
    
    it should "return an empty list when searching with maxPriceDetails equal 0" in withRestClient { (mockClient, context) =>
        //running(FakeApplication()) {
            assert(true)
        //}
    }
    
    it should "return a list when searching with with FromDate" in withRestClient { (mockClient, context) =>
        //running(FakeApplication()) {
            assert(true)
        //}
    }
    
    it should "return a list when searching with ToDate" in withRestClient { (mockClient, context) =>
        //running(FakeApplication()) {
            assert(true)
        //}
    }
    
    it should "sort the search results by \"most current first\"" in withRestClient { (mockClient, context) =>
        //running(FakeApplication()) {
            assert(true)
        //}
    }
    
    it should "sort the search results by \"most current last\"" in withRestClient { (mockClient, context) =>
        //running(FakeApplication()) {
            assert(true)
        //}
    }*/

    it should "return price information for the left-side details pane" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){

            // This uses ScalaTest's JMockCycle to wrap the Mockery object
            val cycle = new JMockCycle
            import cycle._

            // Set the context of the mock
            context.checking(
                new Expectations() {

                    allowing(mockClient).setHolder("/price")
                    
                    oneOf (mockClient).setFlag(PriceIdentifier.ID.fieldName(), "e48ca5d0-50a8-49a4-8a7f-a3c43b93dcf9")
                    oneOf (mockClient).setFlag("detail", "")
                    

                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeDetailedPriceItemJsonResponse))
                }
            )

            // Test the system:
            val priceWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.PRICE)
            priceWS.setClient(mockClient)

            val priceItemDetails = priceWS.filterDetails("e48ca5d0-50a8-49a4-8a7f-a3c43b93dcf9") //.filterDetails(priceID)
            assert(priceItemDetails.getProperty(PriceIdentifier.priceDetailsShort()(0).columnName())=="Product - Formula Price") //.priceDetailsShort() contains: PRICECATEGORY, PRICEDESCRIPTION
            assert(priceItemDetails.getProperty(PriceIdentifier.PRICECATEGORY.columnName())=="Product - Formula Price")
            assert(priceItemDetails.getProperty(PriceIdentifier.PRICEDESCRIPTION.columnName())=="Mankato Terminal (Cochin) - C3 - Intercompany Transfer Price")
            assert(priceItemDetails.getProperty(PriceIdentifier.priceAuditInfo()(0).columnName())=="mikeflaman") //.priceAuditInfo() contains: PRICECREATEUSR, PRICEUPDATEUSR, PRICEUPDATEDATE
            assert(priceItemDetails.getProperty(PriceIdentifier.PRICECREATEUSR.columnName())=="mikeflaman")
            assert(priceItemDetails.getProperty(PriceIdentifier.PRICEUPDATEUSR.columnName())=="steveremmington")
            assert(priceItemDetails.getProperty(PriceIdentifier.PRICEUPDATEDATE.columnName())=="Apr 28, 2010 09:16:57 AM")
            assert(priceItemDetails.getProperty(PriceIdentifier.ITEMSHORTDESC.columnName())=="C3")
            assert(priceItemDetails.getProperty(PriceIdentifier.SOURCELOCATION.columnName())=="Mankato Terminal (Cochin)")
            assert(priceItemDetails.getProperty(PriceIdentifier.DESTINATIONLOCATION.columnName())=="Mankato Terminal (Cochin)")
            assert(priceItemDetails.getProperty(PriceIdentifier.UOM.columnName())=="m3")
            assert(priceItemDetails.getProperty(PriceIdentifier.CURRENCY.columnName())=="Canadian Dollars")
        }
    }

    it should "return price information for the right-side details pane" in withRestClient { (mockClient, context) =>
        running(FakeApplication()){
            val cycle = new JMockCycle  // This uses ScalaTest's JMockCycle to wrap the Mockery object
            import cycle._

            
            context.checking(           // Set the context of the mock
                new Expectations() {
                    allowing(mockClient).setHolder("/price")

                    oneOf(mockClient).setFlag("id", "e48ca5d0-50a8-49a4-8a7f-a3c43b93dcf9")
                    oneOf(mockClient).setFlag("detail", "priceDetails")

                    exactly(1).of(mockClient).getResponseAsJson()
                        will(returnValue(fakeDetailedPriceItemJsonResponse))
                }
            )

            // Test the system:
            val priceWS = WorkspaceFactory.createWorkspace(WorkspaceFactory.Endpoint.PRICE)
            priceWS.setClient(mockClient)

            val priceRightPaneDetails = priceWS.filterDetailsRightPane("e48ca5d0-50a8-49a4-8a7f-a3c43b93dcf9") //.filterDetails(priceID)

            //println(priceRightPaneDetails.get(0).getPropertyMap())
            assert(priceRightPaneDetails.get(0).getProperty(PriceIdentifier.PRICEDETAILTYPE.columnName()).contains("ACTUAL"))
            assert(priceRightPaneDetails.get(0).getProperty(PriceIdentifier.PRICEDETAILREF.columnName()).contains("http://"))
            assert(priceRightPaneDetails.get(0).getProperty(PriceIdentifier.EFFECTIVEDATE.columnName()).contains("Feb 1, 2010 12:00:00 AM"))
            assert(priceRightPaneDetails.get(0).getProperty(PriceIdentifier.PRICEAMOUNT.columnName()).contains("336.51737472"))
            assert(priceRightPaneDetails.get(0).getProperty(PriceIdentifier.PRICEDETAILID.columnName()).contains("9A0C3"))
            assert(priceRightPaneDetails.get(0).getProperty(PriceIdentifier.PRICECOMPLETEDATE.columnName()).contains("Jul 1, 2009 12:00:00 AM"))            
        }
    }
}