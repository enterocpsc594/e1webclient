package suites

import org.scalatest.junit.AssertionsForJUnit
import org.scalatest._
import org.junit.Assert._
import java.util.concurrent.TimeUnit
import org.openqa.selenium._
import org.openqa.selenium.firefox.FirefoxDriver
import org.openqa.selenium.support.ui.Select
import scala.util.control.Breaks._

/**
 * READ ME (Last updated 16/3/2015):
 * The following tests uses ScalaTest in conjunction with Selenium to 
 * simulate a web browser to test the web app's behaviour, as well as
 * verifying the information. Tests are structured in FlatSpec format,
 * which follows BDD(Behavior-Driven Design) principles.
 * 
 * To compile these tests you need to have the following lines added to your SBT build file:
 * libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.1" % "test"
 * libraryDependencies += "org.seleniumhq.selenium" % "selenium-java" % "2.44.0" % "test"
 * 
 * As well you need the FireFox Selenium IDE plugin: http://docs.seleniumhq.org/download/ ver.2.9.0 or later
 *
 * To run these tests you simply type <sbt "test-only suites.PerformanceTest"> while the program is running in the background.
 * NOTE: You need to have quotation marks or else the whole suite will run!
 *
 */ 


 /**
  * The following sets up the setup and teardown methods before tests begin
  * and after all tests are complete.
  */
trait PriceDriver extends BeforeAndAfterAll { this: Suite =>
	val driver: WebDriver = new FirefoxDriver()
	val baseUrl = "http://localhost:9000/"
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS)

	override def beforeAll() {
		super.beforeAll()
	}
	
	override def afterAll() {
		try super.afterAll()catch{case e:Exception=>}
		driver.quit()
	}
} 
 
class PricePerformanceTest extends FlatSpec with PriceDriver{
	
	/**
	 * The following method checks to see if an HTML element is  present.
	 * This method was taken out of Selenium 2.0, but existed in earlier versions.
	 * It is used to find elements that xpath cannot find, such as the data table!
	 */
	def isElementPresent(id: By) : Boolean = {
		try {
			driver.findElement(id)
		} catch {
			case e:Exception => return false
		}
		return true
	}

	/**
	 * waitForElement() is a method that waits for the web element table to display.
	 * It takes in an By element similar to isElementPresent() function.
	 */
	def waitForElement(id: By) : Boolean = {
		var itemExists = false
		var second = 0
		while ((second <= 120) && (!itemExists)) {
			try{if(isElementPresent(id)) itemExists = true}catch{case e:Exception =>}
			Thread sleep 500
			second = second + 1
		}
		if(itemExists==false) {return false}
		else {return true}
	}
		
	//=======================================================
	behavior of "Worst-Case:"
	//=======================================================
	it should "Log the user into application" in {
		
		// Opens up the website and auto-logs the user in
		driver.get(baseUrl + "login")
    	driver.findElement(By.name("username")).clear()
   		driver.findElement(By.name("username")).sendKeys("webtest")
    	driver.findElement(By.name("pass")).clear()
    	driver.findElement(By.name("pass")).sendKeys("entero")
    	driver.findElement(By.cssSelector("input.login-button")).click()

		assert(true)
	}

	it should "search for a non-existing Price Item" in {
		
		driver.get(baseUrl)
		var start = System.currentTimeMillis() // Time the execution in milliseconds

		// Open up the Item workspace and search for a non-existing Item
		driver.findElement(By.cssSelector("img[alt=\"Price\"]")).click()
		driver.findElement(By.id("ws-price-searchparam-description")).clear()
    	driver.findElement(By.id("ws-price-searchparam-description")).sendKeys("DoesNotExist")
    	driver.findElement(By.cssSelector("input.search-parameter-input.search-parameter-search-button")).click()
				
		// Checks if a blank table is returned
		if(waitForElement(By.id("tab-content-0"))){assert(true)}
			else{assert(false)}

		driver.findElement(By.id("ws-price-searchparam-description")).clear()
		var finish = System.currentTimeMillis()
		var totalTime = (finish - start)/1000.0
		System.out.print("Time: "+totalTime+"secs ")
		Thread sleep 100
		assert(true)
	}
	
	it should """blank search, sort by column and sift through the entire 
					 |list to find the last item and load its details""".stripMargin in {
	
		driver.get(baseUrl)
		var start = System.currentTimeMillis() // Time the execution in milliseconds

		// Open up the Item workspace and perform blank search
		driver.findElement(By.cssSelector("img[alt=\"Price\"]")).click()
    	driver.findElement(By.cssSelector("input.search-parameter-input.search-parameter-search-button")).click()
		
		// Wait for the table to load
		if(waitForElement(By.xpath("//div[@id='tab-content-0']/table/tbody/tr/td[2]"))){assert(true)}
			else{assert(false)}

		// Sort the Price Items' "Price Category" in ascending order (A-Z)
		driver.findElement(By.xpath("//th[@onclick=\"getWorkspaceManager().getActiveWorkspace().sortBy('Price Category');\"]")).click()

		// Wait for the table to load
		if(waitForElement(By.xpath("//div[@id='tab-content-0']/table/tbody/tr/td[2]"))){assert(true)}
			else{assert(false)}

		// We check that the third item from the top starts between "A" to "C"
		var asciiChar = (driver.findElement(By.xpath("//div[@id='tab-content-0']/table/tbody/tr/td[6]")).getText()).charAt(0).toInt
		if(((asciiChar<68)&&(asciiChar>64))||((asciiChar<100)&&(asciiChar>96))){assert(true)}
			else{assert(false)}

		// Sort the Price Items' "Price Category" in descending order (Z-A)
		driver.findElement(By.xpath("//th[@onclick=\"getWorkspaceManager().getActiveWorkspace().sortBy('Price Category');\"]")).click()

		// Wait for the table to load
		if(waitForElement(By.xpath("//div[@id='tab-content-0']/table/tbody/tr/td[2]"))){assert(true)}
			else{assert(false)}

		// We check if the item at the top of the list starts from "P" to "Z"
		asciiChar = (driver.findElement(By.xpath("//div[@id='tab-content-0']/table/tbody/tr/td[6]")).getText()).charAt(0).toInt
		if(((asciiChar<91)&&(asciiChar>80))||((asciiChar<123)&&(asciiChar>112))){assert(true)}
			else{assert(false)}

		var finish = System.currentTimeMillis()
		var totalTime = (finish - start)/1000.0
		System.out.print("Time: "+totalTime+"secs ")
		Thread sleep 100
		assert(true)	

	}
	
	it should "search by location, items and price descriptions, and load up the details" in {
		
		driver.get(baseUrl)
		var start = System.currentTimeMillis() // Time the execution in milliseconds
		
		// Open up the Item workspace and enter description for price, source & destination location and items
		driver.findElement(By.cssSelector("img[alt=\"Price\"]")).click()
		driver.findElement(By.id("ws-price-searchparam-description")).clear()
    	driver.findElement(By.id("ws-price-searchparam-description")).sendKeys("New Hampshire")

    	// Enter the source location via pop-up window
    	driver.findElement(By.xpath("//div[@onclick=\"UUIDparameterSearch('location', 'ws-price-searchparam-sourcelocationuuid'); return false;\"]")).click()
    	driver.findElement(By.id("uuid-searchparam")).clear()
    	driver.findElement(By.id("uuid-searchparam")).sendKeys("New")
    	driver.findElement(By.cssSelector(".main > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(2) > div:nth-child(1)")).click()
    	driver.findElement(By.xpath("//table[@id='JColResizer0']/tbody/tr[47]/td[2]")).click()
    	driver.findElement(By.cssSelector("div.modal-ok-button")).click()

    	// Enter the destination location via pop-up window
    	driver.findElement(By.xpath("//div[@onclick=\"UUIDparameterSearch('location', 'ws-price-searchparam-destinationlocationuuid'); return false;\"]")).click()
	    driver.findElement(By.id("uuid-searchparam")).clear()
	    driver.findElement(By.id("uuid-searchparam")).sendKeys("New")
	    driver.findElement(By.cssSelector(".main > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(2) > div:nth-child(1)")).click()
	    driver.findElement(By.xpath("//table[@id='JColResizer1']/tbody/tr[47]/td[2]")).click()
	    driver.findElement(By.cssSelector("div.modal-ok-button")).click()

	    // Enter item's short description via pop-up window
	    driver.findElement(By.xpath("//div[@onclick=\"UUIDparameterSearch('item', 'ws-price-searchparam-itemuuid'); return false;\"]")).click()
	    driver.findElement(By.id("uuid-searchparam")).clear()
	    driver.findElement(By.id("uuid-searchparam")).sendKeys("C3o")
	    driver.findElement(By.cssSelector(".main > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(2) > div:nth-child(1)")).click()
	    driver.findElement(By.xpath("//table[@id='JColResizer2']/tbody/tr/td[2]")).click()
	    driver.findElement(By.cssSelector("div.modal-ok-button")).click()

	    // Begin search
	    driver.findElement(By.cssSelector("input.search-parameter-input.search-parameter-search-button")).click()

	    // Wait for the table to load
		if(waitForElement(By.xpath("//div[@id='tab-content-0']/table/tbody/tr/td[2]"))){assert(true)}
			else{assert(false)}

		// Select the item to load up its details and verify
		driver.findElement(By.xpath("//table[@id='JColResizer3']/tbody/tr/td[2]")).click();

		// Wait details to load
		if(waitForElement(By.cssSelector(".detail-view-properties-table"))){assert(true)}
			else{assert(false)}
					
		driver.findElement(By.id("ws-price-searchparam-description")).clear()
		var finish = System.currentTimeMillis()
		var totalTime = (finish - start)/1000.0
		System.out.print("Time: "+totalTime+"secs ")
		Thread sleep 100
		assert(true)	
		
	}

	/*it should "blank search, select the entire results list and export it as excel" in {
		Excel has yet to be implemented for price workspace
	}*/
}