name := """Workspace/e1"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.1"

libraryDependencies += "javax.inject" % "javax.inject" % "1"

libraryDependencies += "org.scalatest" % "scalatest_2.11" % "2.2.1" % "test"

libraryDependencies += "org.seleniumhq.selenium" % "selenium-java" % "2.44.0" % "test"

libraryDependencies += "org.jmock" % "jmock-junit4" % "2.6.0"

libraryDependencies += "org.jmock" % "jmock-legacy" % "2.6.0"

libraryDependencies += "org.apache.poi" % "poi" % "3.9"

jacoco.settings

parallelExecution in jacoco.Config := false

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache,
  javaWs
)