
/*
    "base class" Workspace Object. 
    Other workspaces (ie Items, Price, etc) are derived from this object.
*/
var Workspace = function() {
    this.name = "untitled";             //Name of the workspace
    this.searchResultsElement = null;   //jQuery element containing the search results
    this.tabObject = null;              //Tab object for the search results
    this.detailViewElement = null;      //jQuery element containing the detail view
    this.detailViewMessage = null;		//A Message to display in detail view
    this.parentWorkspaceManager = null; //Parent manager
    this.dataTable = null;				//The datatable containing the search results
    this.UID = 0;
    this.currentOffset = 0;
    this.lastFetchedOffset = null;
};

/*
	Workspace.open()
		Activate the workspace so it is shown on the screen (other workspaces will be hidden)
	Author: Brad Triebwasser
	Returns: Nothing
*/
Workspace.prototype.open = function() {
    this.tabObject.select(); //Select the tab for this workspace
    this.showDetailView();
    this.showMessage("Please select an item");
};

/*
	Workspace.getDataTable()
		Gets the dataTable (search results)
	Returns: DataTable for the workspace
*/

Workspace.prototype.getDataTable = function() {
	return this.dataTable;
}

/*
	Workspace.close()
		This will close (destroy) the workspace, and remove it from the workspace manager
	Author: Brad Triebwasser
	Returns: Nothing
*/
Workspace.prototype.close = function() {
    this.dataTable.saveColumnSizes();
	this.parentWorkspaceManager.closeWorkspace(this); //Tell the workspace manager to close us..
};
/*
	Workspace.getSelectedID();
		Gets the currently selected object ID for the workspace
	Returns: (String) the ID of the object (example: 8FEA70D9-9D21-4012-83E9-98FB6FA577DE)
*/
Workspace.prototype.getSelectedID = function() {
	return this.selectedID;
};


/*
	Workspace.showDetailView()
		Shows the detailview for this workspace in the Workspace managers detailView element. Hides any other
		Detail views that may be showing
	Author: Brad Triebwasser
	Returns: Nothing
*/
Workspace.prototype.showDetailView = function() {
    this.parentWorkspaceManager.detailView.children().css("display", "none");
    if (this.dataTable && this.dataTable.getSelectionCount()<1) {
    	this.showMessage("Please Select an Item.");
    }else if (this.dataTable && this.dataTable.getSelectionCount()>1) {	//More than one item was selected
		 this.showMessage("Multiple items are selected.");
	}else{
    	this.detailViewElement.css("display","");
    	this.detailViewElement.parent().find("#detail-view-msg").remove();
	}
    setFillElements();

}

/*
	Workspace.hideDetailView(msg)
		Hide the detail view and display a messahe
	msg: String of message to show
	Returns: nothing
*/

Workspace.prototype.showMessage = function(msg) {
	this.parentWorkspaceManager.detailView.append("<div id='detail-view-msg' style='margin-left: auto; margin-right: auto; margin-top: 64px;'>"+msg+"</div>");
	this.detailViewElement.css("display", "none");
	//this.parentWorkspaceManager.detailView.find("#detail-view-msg").remove();
}


Workspace.prototype.constructDetailView = function() {
};
Workspace.prototype.onOpen = function() {}; //Executed when the workspace is first opened
Workspace.prototype.onFocus = function() {}; //Executed when the workspace is focused (ie tab switched to it)
Workspace.prototype.onBlur = function() {}; //Executed when workspace loses focus (ie tab switched away from it)
Workspace.prototype.onClose = function() {//Executed when the workspace is closed
	//Cleanup our detail view
	this.detailViewElement.remove();
	this.tabObject.onBeforeClosed(null); //Reset the onBefore closed function
	this.tabObject.close(); //Close

}; 
//Called when the currently selected item is changed in the datatable
Workspace.prototype.onSelectedChanged = function() {
	this.showDetailView();
};

//Called when the search results is scrolled to the bottom. We should fetch more results from the server
Workspace.prototype.onScrolledBottom = function() {

};

//Perform a search on the workspace (opens new search results)
Workspace.prototype.performSearch = function(offset) {}; 


//Set the scroll callback for the search results element to detect when it is scrolled to the bottom
//When it is scrolled to the bottom, we will want to fetch more results from the server
Workspace.prototype.setScrollCallback = function() {
	var dt = this.searchResultsElement;
	var w = this;
	this.searchResultsElement.scroll(function() {
		if(dt.scrollTop() + dt.innerHeight() >= dt.get(0).scrollHeight) {
		    if (w.currentOffset!=w.lastFetchedOffset) {
		        w.performSearch(w.currentOffset);
		        w.lastFetchedOffset = w.currentOffset;
		    }
			//alert("End Reached.");
		}
	});
};



/*
	Sort the search results by the specified column

*/
Workspace.prototype.sortBy = function(column) {
	var tabContent = this.searchResultsElement;
    	var ws = this;
	$.post(
      "/sortSearchResultTable"+this.name,
      {
        column_sort: column,
	ws_uid: this.UID
      }
    )
      .done(function( data ) {
      		var table = $(data);
            ws.dataTable.setPersistenceID("ws-"+this.name+"-search");
            ws.dataTable.setTable(table);
            $(tabContent).html('');
            $(tabContent).append(table);
            ws.dataTable.setSelectionChangedCallback(
                function() { ws.onSelectedChanged(); }
            );
	    ws.setScrollCallback();


      });
};

/*
    Requests an Excel .xls file from the server for the selected search results in the workspace.
    The file will be downloaded when it returned from the server
*/
Workspace.prototype.fetchExcelExport = function() {
    if (this.dataTable.getSelectionCount()>0) {
        //Set the workspace ID and selected ID's as a JSON string to pass to the server.
        var str = JSON.stringify([this.UID, this.dataTable.getSelectedIDs()]);
        $.ajax( {
            type: "POST",
          url: "/itemGenerateExcel",
          data: str,
          contentType: "application/json; charset=utf-8",
        })
          .done(function( data ) {
            window.location.href = "/assets/"+data ;
          }).fail(function() {
            alert("Failed to generate Excel .xls file.");
          });
    }
};



/*
    	Instantiates a workspace object based off the workspace name
    	Basically this is a factory that creates a class based off a string
    Author: Brad Triebwasser
    workspaceName: Name of the workspace (ie "Item", "Price", etc.)
    Returns: the workspace object, or null if there is no workspace with that name
*/
function _instantiateWorkspace(workspaceName) {
    switch(workspaceName.toLowerCase()) {
        case "items":
            return new ItemWorkspace(); //Instantiate Item workspace
        break;
        case "price":
        	return new PriceWorkspace();
        case "invoice":
            return new InvoiceWorkspace();
        default:
            console.log("No Workspace can be instantiated with workspace name: "+workspaceName);
            return null;
    }
}

