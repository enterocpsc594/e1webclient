/*
    Items workspace. It is derived from a "Workspace" Object
*/

var PriceWorkspace = function() {
    this.name = "Price";
    this.tabView = null;
}
PriceWorkspace.prototype = new Workspace(); //PriceWorkspace inherits the Workspace object


/*
    PriceWorkspace.constructDetailView()
        Overrides Workspace.constructDetailView method. It constructs the detail view for the Items workspace
        by creating all of the tabs.
    Author: Brad Triebwasser
    Returns: Nothing
*/
PriceWorkspace.prototype.constructDetailView = function() { 
    this.detailViewElement  = $('<div id="ws-'+this.name+'-detailview-tabview" class="tab-view split-component-fill"><div class="tab-navigation"></div></div>');
    this.parentWorkspaceManager.detailView.append(this.detailViewElement);
    this.tabView = GetDefaultTabViewManager().addHTMLTabView(this.detailViewElement);
    var ws = this;
    //We want each tab to load it's content when it is selected ("onSelected event"). Currently it just fetches mockup (static) HTML, but will eventually be linked to back end
    this.tabView.addTab("Price",false).onSelected(function() { this.getTabContentObject().appendRemoteObject("/priceDetails/"+ws.UID+"/"+ws.dataTable.getSelectedID()); });
    this.tabView.addTab("Aliases",false).onSelected(function() { this.getTabContentObject().appendRemoteObject("/assets/staticHTML/aliases.html"); });
    this.tabView.addTab("Related Price",false).onSelected(function() { this.getTabContentObject().appendRemoteObject("/assets/staticHTML/relatedPrices.html"); });
    this.tabView.addTab("Future Prices",false).onSelected(function() { this.getTabContentObject().appendRemoteObject("/assets/staticHTML/futurePrices.html"); });
    this.tabView.addTab("Risk Locations",false).onSelected(function() { /*this.getTabContentObject().appendRemoteObject("/assets/staticHTML/aliases.html"); */});
    this.tabView.addTab("Graph",false).onSelected(function() { /*this.getTabContentObject().appendRemoteObject("/assets/staticHTML/attributes.html"); */});
    this.tabView.addTab("Cost Type Exclusion",false).onSelected(function() { /*this.getTabContentObject().appendRemoteObject("/assets/staticHTML/activities.html");*/ });

};

//Called when the currently selected item is changed in the datatable
PriceWorkspace.prototype.onSelectedChanged = function() {
	Workspace.prototype.onSelectedChanged.call(this);
    if (this.detailViewElement.css("display")=="none") return 0;
    //The selected item has changed, so we want to refresh the detail view tab. We can refresh it simply by selecting it again
    if (this.tabView.getSelectedTab()) //If a tab is selected, refresh it
        this.tabView.getSelectedTab().select();
    else {
        this.tabView.getTab(0).select(); //No tab was selected to refresh, so we just open the first tab
    }
};


/*
    PriceWorkspace.performSearch()
        Performs a search and displays the results. Uses any parameters currently entered into the parameters on the left side.
        Returns: Nothing
*/

PriceWorkspace.prototype.performSearch = function(offset) {
    var tabContent = this.searchResultsElement;

    var ws = this;
    if (offset==null) offset = 0;
    if (offset==0) { $(tabContent).html(ajaxLoadingHTML()); }
    /*

		PAramaters:
			ws-price-searchparam-description
			ws-price-searchparam-aliastype1uuid
			ws-price-searchparam-sourcelocationuuid
			ws-price-searchparam-destinationlocationuuid
			ws-price-searchparam-itemuuid
			ws-price-searchparam-group

    */
    $.post(
      "/priceSearchResultTable",
      {
        searchParam_description: $("#ws-price-searchparam-description").val(),
		searchParam_itemDescription: $("#ws-price-searchparam-itemuuid").data('uuid'),
          searchParam_sourceLocation: $("#ws-price-searchparam-sourcelocationuuid").data('uuid'),
          searchParam_destinationLocation: $("#ws-price-searchparam-destinationlocationuuid").data('uuid'),
		ws_uid: this.UID,
		off: offset
      }
    )
      .done(function( data ) {
       console.log("Offset"+offset);
      if (offset==0) {
            var table = $(data);
            ws.dataTable = new Datatable();
            ws.dataTable.setPersistenceID("ws-"+this.name+"-search");
            $(tabContent).html('');
            $(tabContent).append(table);
            ws.dataTable.setTable(table);
            ws.dataTable.setSelectionChangedCallback(
                function() { ws.onSelectedChanged(); }
            );
	    ws.setScrollCallback();
	     }else{
            var table = $(data);
            ws.dataTable.addRows(table);
        }
        ws.currentOffset = ws.dataTable.table.find("tr").length-1;
        console.log("Rows"+ws.dataTable.table.find("tr").length)


      });
}
