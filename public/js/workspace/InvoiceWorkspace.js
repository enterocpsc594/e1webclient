/*
    Items workspace. It is derived from a "Workspace" Object
*/

var InvoiceWorkspace = function() {
    this.name = "Invoice";
    this.tabView = null;
}
InvoiceWorkspace.prototype = new Workspace(); //InvoiceWorkspace inherits the Workspace object


/*
    InvoiceWorkspace.constructDetailView()
        Overrides Workspace.constructDetailView method. It constructs the detail view for the Items workspace
        by creating all of the tabs.
    Author: Brad Triebwasser
    Returns: Nothing
*/
InvoiceWorkspace.prototype.constructDetailView = function() { 
    this.detailViewElement  = $('<div id="ws-'+this.name+'-detailview-tabview" class="tab-view split-component-fill"><div class="tab-navigation"></div></div>');
    this.parentWorkspaceManager.detailView.append(this.detailViewElement);
    this.tabView = GetDefaultTabViewManager().addHTMLTabView(this.detailViewElement);
    var ws = this;
    //We want each tab to load it's content when it is selected ("onSelected event"). Currently it just fetches mockup (static) HTML, but will eventually be linked to back end
    this.tabView.addTab("Invoice",false).onSelected(function() { this.getTabContentObject().appendRemoteObject("/invoiceDetails/"+ws.UID+"/"+ws.dataTable.getSelectedID()); });
    this.tabView.addTab("View",false).onSelected(function() {
        this.getTabContentObject().appendRemoteObject("/invoicePDF/"+ws.UID+"/"+ws.dataTable.getSelectedID());
    });
    this.tabView.addTab("Edit Messages",false).onSelected(function() { this.getTabContentObject().appendRemoteObject("/assets/staticHTML/relatedItems.html"); });
    this.tabView.addTab("Invoice Journal Entry Details",false).onSelected(function() { this.getTabContentObject().appendRemoteObject("/assets/staticHTML/locations.html"); });
    this.tabView.addTab("Notes",false).onSelected(function() { this.getTabContentObject().appendRemoteObject("/assets/staticHTML/aliases.html"); });
    this.tabView.addTab("Payments",false).onSelected(function() { this.getTabContentObject().appendRemoteObject("/assets/staticHTML/aliases.html"); });
    this.tabView.addTab("Export History",false).onSelected(function() { this.getTabContentObject().appendRemoteObject("/assets/staticHTML/activities.html"); });

};

//Called when the currently selected item is changed in the datatable
InvoiceWorkspace.prototype.onSelectedChanged = function() {
    Workspace.prototype.onSelectedChanged.call(this);
    if (this.detailViewElement.css("display")=="none") return 0;
    //The selected item has changed, so we want to refresh the detail view tab. We can refresh it simply by selecting it again
    if (this.tabView.getSelectedTab()) //If a tab is selected, refresh it
        this.tabView.getSelectedTab().select();
    else {
        this.tabView.getTab(0).select(); //No tab was selected to refresh, so we just open the first tab
    }
};


InvoiceWorkspace.prototype.generatePDF = function() {
    if (this.dataTable.getSelectionCount()>0) {
        var invoiceID = this.dataTable.getSelectedID();
        show_modal_window("Generate PDF", "Please wait while PDF is generated...");
        set_modal_ok_callback(function() {  //Executed when the user presses "OK"

        });
        $.ajax( {
            type: "GET",
            url: "/invoicePDF/"+this.UID+"/"+invoiceID+"/download"
        })
            .done(function( data ) {
                close_modal_window();   //Close the modal Window
                window.open("/report/"+data);
            }).fail(function() {
                close_modal_window();   //Close the modal Window
                show_modal_window("Generate PDF", "PDF Generation Failed.");
                set_modal_ok_callback(function() {  //Executed when the user presses "OK"
                    close_modal_window();   //Close the modal Window
                });
            });
    }
};

/*
    InvoiceWorkspace.performSearch()
        Performs a search and displays the results. Uses any parameters currently entered into the parameters on the left side.
        Returns: Nothing
*/

InvoiceWorkspace.prototype.performSearch = function(offset) {
    var tabContent = this.searchResultsElement;
    var ws = this;
    if (offset==null) offset = 0;
    if (offset==0) { $(tabContent).html(ajaxLoadingHTML()); }
    $.post(
      "/invoiceSearchResultTable",
      {
        searchParam_payrec: $("#ws-invoice-searchparam-payrec").val(),
        searchParam_type: $("#invoiceTypeParams").val(),
        searchParam_status: $("#invoiceStatusParams").val(),
          searchParam_datetype: $("#ws-invoice-searchparam-datetype").val(),
        searchParam_datefrom: $("#ws-invoice-searchparam-datefrom").val(),
        searchParam_dateto: $("#ws-invoice-searchparam-dateto").val(),
          searchParam_invoiceref: $("#ws-invoice-searchparam-invoiceref").val(),
	    ws_uid: this.UID,
	    off: offset
      }
    )
      .done(function( data ) {
      console.log("Offset"+offset);
            if (offset==0) {
                var table = $(data);
                ws.dataTable = new Datatable();
                ws.dataTable.setPersistenceID("ws-"+this.name+"-search");
                $(tabContent).html('');
                $(tabContent).append(table);
                var menu1 = [
                    {'Generate PDF':function(menuItem,menu) {
                        ws.generatePDF();
                    } }
                ];
                ws.dataTable.setTable(table, menu1);
                ws.dataTable.setSelectionChangedCallback(
                    function() { ws.onSelectedChanged(); }
                );

                ws.setScrollCallback();
	        }else{
	            var table = $(data);
	            ws.dataTable.addRows(table);
	        }
	        ws.currentOffset = ws.dataTable.table.find("tr").length-1;
	        console.log("Rows"+ws.dataTable.table.find("tr").length);
      });
}
