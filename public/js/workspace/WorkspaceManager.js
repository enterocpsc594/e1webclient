/*
    Workspace manager is used for managing the workspaces, creating new ones, closing them, etc.
    Author: Brad Triebwasser
*/
var WorkspaceManager = function() {
    this.workspaceTabView = null;   //TabView for the workspaces
    this.detailView = null;         //jQuery object for the element containing the detail view
    this.workspaces = [];           //List of workspaces that are open
    this.activeWorkspace = null;	//The current active workspace (null if none is active)
    this.nextUID = 0;			//Unique identifiers for new workspaces (used to identify on sessions on server)
}


/*
WorkspaceManager.closeWorkspace(workspace)
	Closes the specified workspace.
Author: Brad Triebwasser
workspace: Workspace object to close.
Returns: Nothing
*/
WorkspaceManager.prototype.closeWorkspace = function(workspace) {
	this.workspaces.splice(this.workspaces.indexOf(workspace),1);
	workspace.onClose();
	if (this.workspaces.length>0) {	//Open another workspace if one is available
		this.workspaces[0].open();
		this.activeWorkspace = this.workspaces[0]; //Set the current active workspace.
	}else{
		this.activeWorkspace = null; //No active workspace
	}
}
/*
WorkspaceManager.openWorkspace(workspaceName)
	Open and show a workspace based on its name "ie Items"
Author: Brad Triebwasser
workspaceName: Name of the workspace to open (example "Items:)
Returns: the workspace object
*/
WorkspaceManager.prototype.openWorkspace = function(workspaceName) {
    for (var i=0; i<this.workspaces.length; i++) { //Check existing workspaces
        if (this.workspaces[i].name==workspaceName) { //If it matches the name we are trying to open, open it
            this.workspaces[i].open();
            this.activeWorkspace = this.workspaces[i];
            return this.workspaces[i];
        }
    }
    //No workspace was opened, so we need to create a new one based off the name
    var workspaceManager = this;
    var workspaceObject = _instantiateWorkspace(workspaceName); 
    workspaceObject.UID = this.nextUID;
    this.nextUID += 1;	//Increment the UID so the next workspace has a different one
    if (workspaceObject!=null) { //Make sure there was a workspace actually created
        workspaceObject.parentWorkspaceManager = this; //Set the parent Workspace manager to be this
        workspaceObject.tabObject = this.workspaceTabView.addTab(workspaceObject.name,true); //Create a new tab in the workspace tab view
        workspaceObject.searchResultsElement = workspaceObject.tabObject.tabContentObject; //Set search results to tabContent
        workspaceObject.constructDetailView();
        workspaceObject.tabObject.onSelected(function() {
            workspaceObject.showDetailView(); 
            workspaceManager.activeWorkspace = workspaceObject;
        }); //Switch to this workspace when the tab is clicked.
        workspaceObject.tabObject.onBeforeClosed(function(){ workspaceObject.close(); return false;}); //Close workspace when tab is closed
        workspaceObject.open();
        this.workspaces.push(workspaceObject);
        this.activeWorkspace = workspaceObject; //Set the current active workspace.
        return workspaceObject;
    }
    return null;
};
/*
WorkspaceManager.getActiveWorkspace()
	Returns the currently active workspace (the one that is currently being displayed)
Author: Brad Triebwasser
Returns: the currently active workspace (null if there is none currently active)
*/
WorkspaceManager.prototype.getActiveWorkspace = function() {
	return this.activeWorkspace;
}




var _wsManager = null;
/*
    Returns the default workspace manager
    Author: Brad Triebwasser
*/
function getWorkspaceManager() {
    return _wsManager;
}

/*
    Initialize the workspace manager
    Author: Brad Triebwasser
*/
function workspaceManagerInitialize() {
    _wsManager = new WorkspaceManager();
    _wsManager.workspaceTabView = GetDefaultTabViewManager().getTabViewByID("searchview-tabview");
    _wsManager.detailView = $(".detail-view");

}
