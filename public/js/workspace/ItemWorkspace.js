/*
    Items workspace. It is derived from a "Workspace" Object
*/

var ItemWorkspace = function() {
    this.name = "Items";
    this.tabView = null;
}
ItemWorkspace.prototype = new Workspace(); //ItemWorkspace inherits the Workspace object


/*
    ItemWorkspace.constructDetailView()
        Overrides Workspace.constructDetailView method. It constructs the detail view for the Items workspace
        by creating all of the tabs.
    Author: Brad Triebwasser
    Returns: Nothing
*/
ItemWorkspace.prototype.constructDetailView = function() { 
    this.detailViewElement  = $('<div id="ws-'+this.name+'-detailview-tabview" class="tab-view split-component-fill"><div class="tab-navigation"></div></div>');
    this.parentWorkspaceManager.detailView.append(this.detailViewElement);
    this.tabView = GetDefaultTabViewManager().addHTMLTabView(this.detailViewElement);
    var ws = this;
    //We want each tab to load it's content when it is selected ("onSelected event"). Currently it just fetches mockup (static) HTML, but will eventually be linked to back end
    this.tabView.addTab("Item",false).onSelected(function() { this.getTabContentObject().appendRemoteObject("/itemDetails/"+ws.UID+"/"+ws.dataTable.getSelectedID()); });
    this.tabView.addTab("Default Conversion Factors",false).onSelected(function() { this.getTabContentObject().appendRemoteObject("/assets/staticHTML/defaultConversionFactors.html"); });
    this.tabView.addTab("Related Items",false).onSelected(function() { this.getTabContentObject().appendRemoteObject("/assets/staticHTML/relatedItems.html"); });
    this.tabView.addTab("Locations",false).onSelected(function() { this.getTabContentObject().appendRemoteObject("/assets/staticHTML/locations.html"); });
    this.tabView.addTab("Aliases",false).onSelected(function() { this.getTabContentObject().appendRemoteObject("/assets/staticHTML/aliases.html"); });
    this.tabView.addTab("Attributes",false).onSelected(function() { this.getTabContentObject().appendRemoteObject("/itemDetails/"+ws.UID+"/"+ws.dataTable.getSelectedID()+"/attr"); });
    this.tabView.addTab("Activities",false).onSelected(function() { this.getTabContentObject().appendRemoteObject("/assets/staticHTML/activities.html"); });
    this.tabView.addTab("Groups",false).onSelected(function() { this.getTabContentObject().appendRemoteObject("/assets/staticHTML/groups.html"); });

};

//Called when the currently selected item is changed in the datatable
ItemWorkspace.prototype.onSelectedChanged = function() {
    Workspace.prototype.onSelectedChanged.call(this);
    if (this.detailViewElement.css("display")=="none") return 0;
    //The selected item has changed, so we want to refresh the detail view tab. We can refresh it simply by selecting it again
    if (this.tabView.getSelectedTab()) //If a tab is selected, refresh it
        this.tabView.getSelectedTab().select();
    else {
        this.tabView.getTab(0).select(); //No tab was selected to refresh, so we just open the first tab
    }
};


/*
    ItemWorkspace.performSearch()
        Performs a search and displays the results. Uses any parameters currently entered into the parameters on the left side.
        Returns: Nothing
*/

ItemWorkspace.prototype.performSearch = function(offset) {
    var tabContent = this.searchResultsElement;
    var ws = this;
    if (offset==null) offset = 0;
    if (offset==0) { $(tabContent).html(ajaxLoadingHTML()); }
    $.post(
      "/itemSearchResultTable",
      {
        searchParam_description: $("#ws-item-searchparam-description").val(),
        searchParam_itemGroups: $("#itemGroupParams").multiParameterListString(),
        searchParam_type: $("#ws-item-searchparam-type").val(),
	ws_uid: this.UID,
	off: offset
      }
    )
      .done(function( data ) {
      console.log("Offset"+offset);
            if (offset==0) {
                var table = $(data);
                ws.dataTable = new Datatable();
                ws.dataTable.setPersistenceID("ws-"+this.name+"-search");
                $(tabContent).html('');
                $(tabContent).append(table);
                ws.dataTable.setTable(table);
                ws.dataTable.setSelectionChangedCallback(
                    function() { ws.onSelectedChanged(); }
                );

                ws.setScrollCallback();
	        }else{
	            var table = $(data);
	            ws.dataTable.addRows(table);
	        }
	        ws.currentOffset = ws.dataTable.table.find("tr").length-1;
	        console.log("Rows"+ws.dataTable.table.find("tr").length)


      });
}
