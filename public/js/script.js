
var lastWorkspaceOnList = null;
//Sets up the workspace list(s) to toggle when they are clicked.
//Author: Brad Triebwasser
function initializeWorkspaceList(){
    $(".workspace-list .content").animate ({ //Collapse all the workspaces in the workspace list
            height: "toggle"
        }, 0, function() {
            
        });
    $(".workspace-list .title").click( function(eventObject) {   //Click event for the workspace title
        if (lastWorkspaceOnList!=null && !lastWorkspaceOnList.is($(this))) { 
                if ($(lastWorkspaceOnList).parent().hasClass("active")) { //Only toggle if it is not already collapsed
                    $(lastWorkspaceOnList).parent().find(".content").animate( { //Toggle the height
                        height: "toggle"
                    }, 200, function() {});
                    $(lastWorkspaceOnList).parent().toggleClass("active"); //Toggle the active class
                }
        }
        $(this).parent().find(".content").animate ({ //Collapse the workspace
            height: "toggle"
        }, 200, function() {      
        });
        $(this).parent().toggleClass("active");//Toggle the active  class

        lastWorkspaceOnList = $(this);
    });
}


//Initialize the toolbar by creating all of the deafault buttons
//Author: Brad Triebwasser
function initializeToolbar() {
    var mainToolbar = new Toolbar("main-toolbar");
    mainToolbar.appendWidget(new ToolBarButton("assets/img/icons/table-insert-row.png","Tooltip 1"));
    mainToolbar.appendWidget(new ToolBarButton("assets/img/icons/disk-black.png","Tooltip 2"));
    mainToolbar.appendWidget(new ToolBarButton("assets/img/icons/ui-tab--minus.png","Close the selected Tab")).onClick( function(){
        var activeWS = getWorkspaceManager().getActiveWorkspace();
        if (activeWS!=null) {
            activeWS.close();
        }
    });

    mainToolbar.appendWidget(new ToolBarButton("assets/img/icons/printer.png","Tooltip 4")).disable();
    mainToolbar.appendWidget(new ToolBarSpacer());

    mainToolbar.appendWidget(new ToolBarButton("assets/img/icons/document-search-result.png","Tooltip 5"));
    mainToolbar.appendWidget(new ToolBarSpacer());

    mainToolbar.appendWidget(new ToolBarButton("assets/img/icons/information.png","Information"));
    mainToolbar.appendWidget(new ToolBarButton("assets/img/icons/arrow-circle-225-left.png","Revert to Last Saved"));
    mainToolbar.appendWidget(new ToolBarSpacer());
    mainToolbar.appendWidget(new ToolBarButton("assets/img/icons/table-excel.png","Export Selected to Excel")).onClick( function(){
            if (getWorkspaceManager().getActiveWorkspace()) {   //Make sure there is an active workspace
                show_modal_window("Export to Excel", "Do you wish to export the selected lines to Excel?<br/> There are "+getWorkspaceManager().getActiveWorkspace().dataTable.getSelectionCount()+" records selected.");
                set_modal_ok_callback(function() {  //Executed when the user presses "OK"
                    getWorkspaceManager().getActiveWorkspace().fetchExcelExport(); //Fetch the Excel export
                    close_modal_window();   //Close the modal Window
                });
                
            }
    });


    mainToolbar.appendWidget(new ToolBarSpacer());
    mainToolbar.appendWidget(new ToolBarButton("assets/img/icons/scissors-blue.png","Cut"));
    mainToolbar.appendWidget(new ToolBarButton("assets/img/icons/documents-text.png","Copy"));
    mainToolbar.appendWidget(new ToolBarButton("assets/img/icons/clipboard-paste.png","Paste"));

}


function UUIDPost(type) {

    if (type=="item") {
        $.post(
            "/uuidlookup/item",
            {
                search: $("#uuid-searchparam").val()
            }
        )
            .done(function (data) {
                var table = $(data);
                var dataTable = new Datatable();
                $(".uuid-results").html(data);
                dataTable.setTable($(".uuid-results").children(".datatable"));
                modal_get_content().data("datatable", dataTable);


            });
    }
    if (type=="location") {
        $.post(
            "/uuidlookup/location",
            {
                search: $("#uuid-searchparam").val(),
                locationTypes: $("#locationTypeParams").multiParameterListString()
            }
        )
            .done(function (data) {
                var table = $(data);
                var dataTable = new Datatable();
                $(".uuid-results").html(data);
                dataTable.setTable($(".uuid-results").children(".datatable"));
                modal_get_content().data("datatable", dataTable);
            });
    }
}

/*
    Used to show a modal dialog where the user can browse selections for a UUID
    For example, if they want to use an item as a search parameter, they can open
    the dialogue and select an item to use as the search parameter
    type = Type of lookup (ie item, location, etc)
    textboxID = ID of the textbox that will be filled with the result of their selection
    Returns: Nothing
 */
function UUIDparameterSearch(type, textboxID) {

    var tt = "Select an item from the list below:<br/>";
    show_modal_window("UUID Lookup",tt);
    var html = '<table style="width: 100%;"><tr><td><input class="search-parameter-input" id="uuid-searchparam" type="text"></td><td><div class="search-parameter-search"></div></td></tr></table>';
    if (type=="location") {
        html += '<div id="locationTypeParams" class="multi-select-parameter-section" data-paramName="Location Type"></div>';
    }
    html += ' <div class="uuid-results uuidscrollarea"></div>';
    var ht = $.parseHTML(html);



    modal_get_content().html(ht);
    if (type=="location") {
        modal_get_content().find("#locationTypeParams").loadmultiparam();
    }

    modal_get_content().find(".search-parameter-search").click(function() {
        UUIDPost(type);
    });
    //modal_get_content().appendRemoteObject("/uuidlookup/item");
    set_modal_ok_callback(function() {  //Executed when the user presses "OK"
        if (type=="location") {
            $("#" + textboxID).data('uuid', modal_get_content().data("datatable").getSelectedProperty(0));
            $("#" + textboxID).val(modal_get_content().data("datatable").getSelectedProperty(2));
        }
        if (type=="item") {
            $("#" + textboxID).data('uuid', modal_get_content().data("datatable").getSelectedProperty(0));
            $("#" + textboxID).val(modal_get_content().data("datatable").getSelectedProperty(1));
        }
        close_modal_window();   //Close the modal Window
    });
}


//Document has initialized
//Author: Brad Triebwasser
$(document).ready(function(){
    initializeWorkspaceList(); //initilize workspace List(s)
    initializeSplitPanes(); //Initialize the split pane windows
    initializeToolbar(); //Initialize the toolbar
    tabViewInitialize(); //Initialize TabViews
    workspaceManagerInitialize(); //Initialize workspace manager


    //Refresh each multi parameter box on the page
    $(".multi-select-parameter-section").each(function() {
        $(this).loadmultiparam();
    });

    //The .search-parameter-expand button should have an attribute named "data-expandElement" with the ID of the element to toggle
    $("body").on("click",".search-parameter-expand",function() {
       var expandElement = $(this).parents(".multi-select-parameter-section").find(".multi-select-parameter-box"); //$(this).attr("data-expandElement");
        $(expandElement).animate({
                height: "toggle"
              }, 400, function() {
                // Animation complete.
              });

        if (!$(this).hasClass("expand")) {
            $(this).addClass("expand");
        }else{
            $(this).removeClass("expand");
        }
    });
    
    //Override CTRL+A and select all of the lines in the current workspace's search results
    $(document).keydown(function(e)
    {
        if (e.keyCode == 65 && e.ctrlKey) {
            lastFocused.selectAll();
           // if (getWorkspaceManager().getActiveWorkspace()) {
             //   getWorkspaceManager().getActiveWorkspace().getDataTable().selectAll();
          //  }
            e.preventDefault();
        }
    });

    $(".uuidresult").data('uuid', "");
    $(".uuidresult").on('input', function() {
        $(this).data('uuid', "");
    });
    $(".datepicker").val($.datepicker.formatDate('mm/dd/yy', new Date()));


});


$.fn.loadmultiparam = function() {
    var listID = $(this).attr("data-paramName").replace(/ /g,'');
    var section = $(this);
    $(this).html(ajaxLoadingHTML(16,16));
    $.get(
        "/multiParameterList/"+listID
    )
        .done(function( data ) {
            var dataObject = $(data);
            section.html('');
            section.append(data);
            section.refreshMultiParameterBox();
        });
}


/*
    This function is called when a multi-select search parameter is updated (something is clicked)
*/
$.fn.refreshMultiParameterBox = function () {
    var numOptionsTotal = this.find(".multi-option").length;
    var numOptionsChecked = this.find(".multi-option:checked").length;
    var thisBox = this;
    thisBox.find(".multi-option").off("click").click(function() { //Refresh this box when a chechbox is pressed
        thisBox.refreshMultiParameterBox();
    });
    if (numOptionsTotal==numOptionsChecked) { //All options are checked
        this.find(".multi-select-textbox").val("All " + this.attr("data-paramName")+"s"); //Set the text box to show that all items are checked
        this.find(".toggleAll").html("Deselect all");   //Set the button so deselect all when clicked
         this.find(".toggleAll").off("click").click(function() { //Deselect all checkboxes when clicked
            thisBox.find(".multi-option").removeAttr('checked');
            thisBox.refreshMultiParameterBox(); //Refresh the box
         });
    }else{
        if (numOptionsChecked==0) { //None are checked!
            this.find(".multi-select-textbox").val("None");
        }else{
            this.find(".multi-select-textbox").val($(this).multiParameterListString());
        }
        this.find(".toggleAll").html("Select All"); //Set the button to select all checkboxes
        this.find(".toggleAll").off("click").click(function() {
            thisBox.find(".multi-option").removeAttr('checked').click();
            thisBox.refreshMultiParameterBox();
         });
    }

};

$.fn.multiParameterListString = function () {
        var textVal = "";
        this.find(".multi-option").each(function() { //For each checkbox, we append its label and put it in the textbox
            if (this.checked) {
                textVal += $(this).next().html() + ",";
            }
        });
        textVal = textVal.substr(0,textVal.length-1);
        return textVal;
};


function ajaxLoadingHTML(width, height) {
    var w = typeof width !== 'undefined' ? width : 128;
    var h = typeof height !== 'undefined' ? height : 128;
    return '<div class="loader" style="width: '+w+'px; height: '+h+'px;"></div>'; //Display a "Loading" animation
}


$.fn.appendRemoteObject = function (url, after) {
    this.html(ajaxLoadingHTML());
    var contentReplace = this;
    $.get(
      url
    )
      .done(function( data ) {
            var dataObject = $(data);
            contentReplace.html('');
            contentReplace.append(data);
            if (after!=null) after();
      });
}


//Perform an item search
function do_search_item() {
	var workspace = getWorkspaceManager().openWorkspace("Items"); //Open the "Item" workspace
    workspace.performSearch();
}
//Perform an item search
function do_search_price() {
    var workspace = getWorkspaceManager().openWorkspace("Price"); //Open the "Item" workspace
    workspace.performSearch();
}

//Perform an item search
function do_search_invoice() {
    var workspace = getWorkspaceManager().openWorkspace("Invoice"); //Open the "Invoice" workspace
    workspace.performSearch();
}

//Shows a modal window ontop of the application with "title" and "message"
function show_modal_window(title, message) {
    $(".modal-blur").addClass("active");
    $(".modal-window").css("display","");
    $(".modal-window > .main").html(message);
    $(".modal-window > .title").html(title);
}

function modal_get_content() {
    return $(".modal-window > .main");
}

//Sets a callback function to call when the OK button is pressed
function set_modal_ok_callback(callback) {
	$(".modal-window .modal-ok-button").off("click");
    $(".modal-window .modal-ok-button").click(callback);
}

//Closes the modal window if it is active.
function close_modal_window() {
    $(".modal-blur").removeClass("active");
    $(".modal-window").css("display","none");
}