
/*
    Toolbar represents a toolbar on the page. A toolbar is a collection of widgets (usually buttons)
*/

var Toolbar = function(toolbarID) {
    this.toolbarElement = null;     //jQuery element representing the toolbar
    this.widgets = [];              //List of ToolBarWidget's in this toolbar
    //Set the toolbar element based on the ID provided
    this.toolbarElement = $("#"+toolbarID);
};
/*
    Toolbar.appendWidget(widget)
        Appends the specified widget object onto the toolbar
    Author: Brad Triebwasser
    Returns: The toolbar widget (used for chaining convinience)
*/
Toolbar.prototype.appendWidget = function(widget) {
    this.toolbarElement.append(widget.getjQueryObject());
    this.widgets.push(widget);
    return widget;
};


/*
    The base class for all widgets that can be placed onto the toolbar
*/

var ToolBarWidget = function() {
    this.jQueryObject = null;       //jQuery object for the widget
    this.isEnabled = true;            //True if the widget is enabled, false if diabled

};

/*
    ToolBarWidget.getjQueryObject()
        Gets the jQuery object for the widget
    Author: Brad Triebwasser
    Returns: jQuery object for the widget
*/
ToolBarWidget.prototype.getjQueryObject = function() {
    return this.jQueryObject;
};
/*
    ToolBarWidget.disable()
        Disables the widget
    Author: Brad Triebwasser
    Returns: this (used for chaining)
*/
ToolBarWidget.prototype.disable = function() {
    this.enabled = false;
    this.jQueryObject.addClass("disabled");
    return this;
};
/*
    ToolBarWidget.enable()
        Enables the widget
    Author: Brad Triebwasser
    Returns: this (used for chaining)
*/
ToolBarWidget.prototype.enable = function() {
    this.enabled = true;
    this.jQueryObject.removeClass("disabled");
};

/*
    ToolBarButton is derived from ToolBarWidget. USed for small clickable buttons
    icon: Path to icon to be used on the button
    tooltip: tooltip to show when hovered
*/
var ToolBarButton = function(icon, tooltip) {
    //Create the jQuery Object for this widget
    this.jQueryObject = $('<div class="toolbar-item" title="'+tooltip+'"> </div>');
    this.jQueryObject.css("background-image","url("+icon+")"); //Set the icon (bg image) on the button

};
ToolBarButton.prototype = new ToolBarWidget();  //Derived from ToolbarWidget

/*
    ToolBarButton.onClick(func)
        sets the function to be called when the button is pressed
    Author: Brad Triebwasser
    Returns: this (used for chaining)
*/
ToolBarButton.prototype.onClick = function(func) {
    if (func!=null) { //Set the function to activate when this button is clicked
        this.jQueryObject.click(func);
    }
    return this;
};

/*
    A spacer widget to seperate items on the toolbar
*/  

var ToolBarSpacer = function() {
    this.jQueryObject = $('<div class="toolbar-spacer"> </div>');
};
ToolBarSpacer.prototype = new ToolBarWidget(); //Derived from ToolBarWidget


