
/*
    TabViewTab Object. Represents a single tab in a tabView
    Author: Brad Triebwasser
*/
var TabViewTab = function() {
    this.id =  0;                       //Unique identifier for the tab
    this.title = "defaultTabTitle";     //Title of the tab
    this.tabObject =  null;             //jQuery object for the tab (clickable tab)
    this.tabContentObject =  null;      //jQuery object for the content of the tab
    this.parentTabView = null;          //Pointer to the parent tabView for this tab
    this.switchedCallback = null;       //Function to execute when this tab is switched to
    this.closedCallback = null;         //Function to execute when this tab is closed
    this.beforeClosedCallback = function () { return true;};   //Function called right before tab is about to close (we can negate the closing of it)
};

/*
    TabViewTab.getTabContentObject()
        Gets the element that is the tab content object for this tab as a jQuery object. This element
        contains the HTML for this particular tab.
    Author: Brad Triebwasser
    Returns: jQuery object representing the content for this tab
*/
TabViewTab.prototype.getTabContentObject = function() {
    return this.tabContentObject;
}
/*
    TabViewTab.close()
        Closes the tab. It can be negated by returning false in the beforeClosedCallback function.
        It will delete the tab and the tab content, and then call the closedCallback.
    Author: Brad Triebwasser
    Returns: Nothing
*/
TabViewTab.prototype.close = function() {
    if (this.beforeClosedCallback()==false) return false;
    this.tabObject.remove();
    this.tabContentObject.remove();
    this.parentTabView.tabs.splice(this.parentTabView.tabs.indexOf(this),1); //Remove ourselves from the parent's tab list
    if (this.closedCallback!=null) {
        this.closedCallback();
    }
};
/*
    TabViewTab.select()
        Select this tab (show it).
    Author: Brad Triebwasser
    Returns: Nothing
*/
TabViewTab.prototype.select = function() {
    this.parentTabView.selectTab(this);
    if (this.switchedCallback!=null) {
        this.switchedCallback();
    }
};
/*
    TabViewTab.setClickCallback()
        Sets the onClick event for the tab object, so this tab is selected (select() is called) when the tab is clicked.
    Author: Brad Triebwasser
    Returns: Nothing
*/
TabViewTab.prototype.setClickCallback = function() {
    var thisObj = this;
    this.tabObject.click( function () {
        thisObj.select();
    });
};
/*
    TabViewTab.onSelected(selectedFunction)
        Sets the function to call when this tab is selected
    Author: Brad Triebwasser
    selectedFunction: function to be called when this tab is selected
    Returns: Nothing
*/
TabViewTab.prototype.onSelected = function(selectedFunction) {
    this.switchedCallback = selectedFunction;
}
/*
    TabViewTab.onClosed(closedFunction)
        Sets the function to call when this tab is closed
    Author: Brad Triebwasser
    closedFunction: function to be called when this tab is closed
    Returns: Nothing
*/
TabViewTab.prototype.onClosed = function(closedFunction) {
    this.closedCallback = closedFunction;
};
/*
    TabViewTab.onBeforeClosed(beforeClosedFunction)
        Sets the function to call when the tab is about to close. It can stop the tab from closing by
        returning false. Return true to allow the tab to close
    Author: Brad Triebwasser
    beforeClosedFunction: function to be called when this tab is about to close. It should return true or false
    Returns: Nothing
*/
TabViewTab.prototype.onBeforeClosed = function(beforeClosedFunction) {
    if (beforeClosedFunction==null) {
        this.beforeClosedCallback = function() {return true;};
    }else{
        this.beforeClosedCallback = beforeClosedFunction;
    }
};




/*
    TabView represents a single tab view (a "cluster" of tabs)
    tabViewID: ID of the tabview (unique string)
    Author: Brad Triebwasser
*/
var TabView = function(tabViewID) {
    this.id = tabViewID;
    this.tabs = [];
    this.jQueryElement = null;
    this.tabUID = 0;
    this.currentTab = null; //The current TabViewTab object that is selected
};

/*
    Get the currently selected tab object for this tabview
    Returns: a TabViewTab object or null is no tab is selected
*/

TabView.prototype.getSelectedTab = function() {
    return this.currentTab;
};

/*
    TabView.getTab(index)
        Gets the tab at the specified index in the list of tabs
    Author: Brad Triebwasser
    index: index of the tab
    Returns: returns the tab at the specified index
*/
TabView.prototype.getTab = function(index) {
    return this.tabs[index];
};
/*
    TabView.addTab(tabName, closeable)
        Adds a new tab to the tab view based on name, and whether or not it should be closeable
    Author: Brad Triebwasser
    tabName: Name of the tab
    closeable: True if the tab should be closable with a close button
    Returns: returns the tab object for the new tab
*/
TabView.prototype.addTab = function(tabName, closeable) {
    var newTab = new TabViewTab();
    newTab.id = this.tabUID;
    newTab.parentTabView = this;
    newTab.title = tabName;
    var closeButton = "";
    if (closeable) {
        closeButton = $('<div class="tab-close-button" > </div>');
        closeButton.data("tabObject",newTab);
        closeButton.click( function() {
            $(this).data("tabObject").close();
        });
    }
    newTab.tabObject = $('<div class="tab" data-tabTarget="tab-content-'+newTab.id+'">'+newTab.title+'</div>');
    newTab.tabObject.append(closeButton);
    newTab.tabContentObject = $('<div class="tab-content" id="tab-content-'+newTab.id+'"></div>');
    newTab.setClickCallback();
    this.jQueryElement.children(".tab-navigation").append(newTab.tabObject);
    this.jQueryElement.append(newTab.tabContentObject);
    this.tabs.push(newTab); //Add the new tab to our list of tabs
    this.tabUID+=1;
    return newTab; //Return the tab object
};
/*
    TabView.selectTab(tab)
        Selects the specified tab object.
    Author: Brad Triebwasser
    tab: tab object to select
    Returns: Nothing
*/
TabView.prototype.selectTab = function(tab) {
    for (var i=0; i<this.tabs.length; i++) {
        var tabObj = this.tabs[i];
        if (tabObj==tab) {

            if (!tabObj.tabObject.hasClass("selected")) { //Only toggle if this was not previously selected
                tabObj.tabContentObject.toggle(); //Show tab if it was selected
            }
            tabObj.tabObject.addClass("selected");
            this.currentTab = tabObj; 
        }else{
            tabObj.tabObject.removeClass("selected");
            tabObj.tabContentObject.css("display","none"); //Hide tab if it was not selected
        }
    }
};



/*
    Tab view manager: Manages all of the tabviews on the page.
*/

var TabViewManager = function() {
    this.tabViews = [];   //List of tabviews
};

/*
    TabViewManager.getTabViewByID(tabViewID)
        Get tab view based of its Unique ID 
    Author: Brad Triebwasser
    tabViewID: ID (String) of the tabview to get the object for
    Returns: returns the tabview object or null if it cannot be found
*/
TabViewManager.prototype.getTabViewByID = function(tabViewID) {
    for (var i=0; i<this.tabViews.length; i++) {
        if (this.tabViews[i].id==tabViewID) {
            return this.tabViews[i];
        }
    }
    console.log("ERROR: Unknown TabView with ID: " + tabViewID);
    return null;
};

/*
    TabViewManager.addTabView(tabView)
        Add a tab view object
    Author: Brad Triebwasser
    tabView: tabview object to add to the manager
    Returns: nothing
*/
TabViewManager.prototype.addTabView = function(tabView) {
    this.tabViews.push(tabView);
};

/*
    TabViewManager.addHTMLTabView(jQueryElement)
       Adds an existing HTML tabview into the manager so it can be managed by javascript
    Author: Brad Triebwasser
    jQueryElement: The tabview element as a jQuery object (ie <div class="tabview">)
    Returns: the new tabview object
*/
TabViewManager.prototype.addHTMLTabView = function(jQueryElement) {
    var newTabView = new TabView();
    newTabView.jQueryElement = jQueryElement;
    this.addTabView(newTabView); //Add it to this tabviewManager
    return newTabView;
};

//Instantiate a TabViewManager 
var _tabviewManager = new TabViewManager();
//Get the default tabviewmanager
var GetDefaultTabViewManager = function () {
    return _tabviewManager;
}

//Initialize Tabview
//Author: Brad Triebwasser
//Returns: jQuery object for the button
//Here we basically convert the HTML into managabe tabview and tab objects.
function tabViewInitialize() {
        $(".tab-view").each(function() {
            var newTabView = new TabView($(this).attr("id"));
            newTabView.jQueryElement = $(this);
            var newTab = new TabViewTab();
            var tabCount = $(this).children(".tab-navigation").children().length;
            for (var i=0; i<tabCount; i++) {
                    newTabView.addTab($(this).children(".tab-navigation").children().eq(i).text(),false);

                    newTabView.tabObject = $(this);
                    $(this).children(".tab-navigation").children().eq(i).remove();
            }
            _tabviewManager.addTabView(newTabView);
        });

        $(".split-component-1").resize(function() {
            $(this).children(".tab-view").height($(this).height());
        });
       
}
