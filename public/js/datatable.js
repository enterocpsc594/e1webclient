/*
	Datatable represents a table of data (search results for example)
	It provides ways for easily selecting items and performing basic operations on the data contained within the
	table
*/

var lastFocused = null; //Last focused data tables so CTRL+A knows which one to do.


var Datatable = function() {
	this.table = null;				//The jQuery object representing the table on the page
	this.onSelectionChanged = null;	//A callback function that is executed when the selected element is changed
    this.uid  = null;               //UID for the datatable
};


Datatable.prototype.addRows = function(jQueryRows) {
    //this.table.find("tbody").html("");
    this.table.find("tbody").append(jQueryRows);
}

/*
	Initializes (or re-initializes) the Datatable object with a new table
	jqueryTable: A jQuery object representing the table DOM element
	Returns: Nothing
*/
Datatable.prototype.setTable = function(jqueryTable, additionalContextmenu) {
		this.table = jqueryTable;
    this.loadColumnSizes();
    if (this.table.hasClass("noloadcolumns")==false) {


        //this.table.kiketable_colsizable();
        /*
         this.table.colResizable({
         liveDrag: true,
         headerOnly: true
         });



         */

    }


    var dt = this;
    this.table.parent().on("click", function() {
        lastFocused = dt;
    });


	  this.table.parent().on("click", "tr.search-result-item", function(evt) {
    	if (!evt.ctrlKey)
        	$(this).parent().find("tr").removeClass("selected");
        $(this).addClass("selected");
        if (dt.onSelectionChanged!=null)
        	dt.onSelectionChanged();
        //$(this).children("#item-id").text()
    });

    var t=this;
    if (this.table.hasClass("exportable")) {
        console.log("erger");
        var menu1 = [
            {'Export selected to Excel (.xls)':function(menuItem,menu) { t.exportSelectedToExcel(); } }
        ];
        if (!(typeof additionalContextmenu === 'undefined')) {
            menu1 = menu1.concat(additionalContextmenu);
        }
        this.table.find("tr.search-result-item").contextMenu(menu1,{theme:'vista'});
    }

};

/*
    Set a unique ID for this datatable so it can store persistend data in the browser
    (ie resized columns)

 */
Datatable.prototype.setPersistenceID = function(uid) {
        this.uid = uid;
    var dt = this;
    //We want to save the column sizes when the window is closed
    $( window ).unload(function() {
        dt.saveColumnSizes();
    });
};

Datatable.prototype.loadColumnSizes = function() {
    return;
    if (this.uid==null) return; //Cannot load with no uid
    if(typeof(Storage) !== "undefined") {
        var count = 0;
        var cols = this.table.find("th").length;
        this.table.find("th").each(function (i, o) {
            if (count<cols) {
                var ww = localStorage.getItem("datatable-col" + this.uid + "-num-" + count);
                if (ww!=null) {
                    $(o).width(ww);
                }
            }
            count ++;
        });
    }
};

Datatable.prototype.saveColumnSizes = function() {
    if (this.uid==null) return; //Cannot save with no uid
    if(typeof(Storage) !== "undefined") {
        var count = 0;
        this.table.find("th").each(function (i, o) {
            localStorage.setItem("datatable-col"+this.uid+"-num-"+count, $(o).width());
            count ++;
        });
    }
};


/*
	Selects all rows in the table
*/
Datatable.prototype.selectAll = function() {
	 this.table.find("tr.search-result-item").addClass("selected");
	 if (this.onSelectionChanged!=null)
        	this.onSelectionChanged();
}

/*
	Returns the currently selected ID
	Returns: The ID of the currently selected item (as a string) for example: b9795dad-b755-49ba-af55-d75a473cad3a

*/
Datatable.prototype.getSelectedID = function() {
	return $(this.table).find(".selected").children("#item-id").text();
};


/*
 Returns the currently selected item text based on the specified column
 Returns: The ID of the currently selected item (as a string) for example: b9795dad-b755-49ba-af55-d75a473cad3a

 */
Datatable.prototype.getSelectedProperty = function(column) {
    return $(this.table).find(".selected").children("td:eq("+column+")").text();
};


/*
	Gets the number of items currently selected in the data table
	Returns: An integer representing the number of items currently selected in the table
*/

Datatable.prototype.getSelectionCount = function() {
	return $(this.table).find(".selected").length;
};

/*
	Get the selected IDs as an array
*/

Datatable.prototype.getSelectedIDs = function() {
	var ids = [];
	$(this.table).find(".selected").each( function(i, o) {
			ids.push($(o).children("#item-id").text());
	});

	return ids;
};

/*
	Set the function to be called when the selected item changes
*/
Datatable.prototype.setSelectionChangedCallback = function(callback) {
	this.onSelectionChanged = callback;
};


Datatable.prototype.exportSelectedToExcel = function() {
    show_modal_window("Export to Excel", "Do you wish to export the selected lines to Excel?<br/> There are "+this.getSelectionCount()+" records selected.");
    var dt = this;
    set_modal_ok_callback(function() {  //Executed when the user presses "OK"
        if (dt.getSelectionCount()>0) {
            //Set the workspace ID and selected ID's as a JSON string to pass to the server.
            var header  = [];
            var rows = [];

            dt.table.find("th").each( function() {
                header.push($(this).text());
            });
            dt.table.find("tr.selected").each( function() {
                var row = [];
                $(this).find("td").each( function() {
                    row.push($(this).text());
                });
                rows.push(row);
            });
            var str = JSON.stringify([header,rows]);
            $.ajax( {
                type: "POST",
                url: "/datatableExcelExport",
                data: str,
                contentType: "application/json; charset=utf-8",
            })
                .done(function( data ) {
                    window.location.href = "/report/"+data ;
                }).fail(function() {
                    alert("Failed to generate Excel .xls file.");
                });
        }
        close_modal_window();   //Close the modal Window
    });
};
